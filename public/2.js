(function ($) {
  Drupal.behaviors.backtotop = {
    attach: function (context, settings) {
      var exist = $('#backtotop').length;
      if (exist == 0) {
        $("body", context).once('backtotop').each(function () {
          $('body').append("<div id='backtotop'>" + Drupal.t(settings.back_to_top.back_to_top_button_text) + "</div>");
        });
      }

      backToTop();
      $(window).scroll(function () {
        backToTop();
      });

      $('#backtotop', context).once('backtotop').each(function () {
        $(this).click(function () {
          $("html, body").bind("scroll mousedown DOMMouseScroll mousewheel keyup", function () {
            $('html, body').stop();
          });
          $('html,body').animate({ scrollTop: 0 }, 1200, 'easeOutQuart', function () {
            $("html, body").unbind("scroll mousedown DOMMouseScroll mousewheel keyup");
          });
          return false;
        });
      });

      /**
       * Hide show back to top links.
       */
      function backToTop() {
        if ($(window).scrollTop() > settings.back_to_top.back_to_top_button_trigger) {
          $('#backtotop').fadeIn();
        } else {
          $('#backtotop').fadeOut();
        }
      }
    }
  };
})(jQuery);
;
(function ($, Drupal) {

  'use strict'

  $(document).ready(function () {
    // Global variables
    var popup_settings = drupalSettings.simple_popup_blocks.settings,
      _html = document.documentElement

    $.each(popup_settings, function (index, values) {

      // Declaring variable inside foreach - so it will not global.
      var modal_class = '',
        block_id = values.identifier,
        visit_counts_arr = values.visit_counts.split(','),
        allow_cookie = true,
        read_cookie = '',
        cookie_val = 1,
        match = 0,
        css_identity = '',
        spb_popup_id = '',
        modal_close_class = '',
        modal_minimize_class = '',
        modal_minimized_class = '',
        layout_class = '',
        class_exists = false,
        delays = '',
        browser_close_trigger = true
      // Always show popup, so prevent from creating cookie
      if (visit_counts_arr.length == 1 && visit_counts_arr[0] == 0) {
        allow_cookie = false
      }
      // Creating cookie
      if (allow_cookie == true) {
        read_cookie = readCookie('spb_' + block_id)
        if (read_cookie) {
          cookie_val = +read_cookie + 1
          createCookie('spb_' + block_id, cookie_val, 100)
        }
        else {
          createCookie('spb_' + block_id, cookie_val, 100)
        }
      }
      // Match cookie
      cookie_val = cookie_val.toString()
      match = $.inArray(cookie_val, visit_counts_arr)
      // Set css selector
      css_identity = '.'
      if (values.css_selector == 1) {
        css_identity = '#'
      }

      // Assign dynamic css classes
      spb_popup_id = 'spb-' + block_id
      modal_class = block_id + '-modal'
      modal_close_class = block_id + '-modal-close'
      modal_minimize_class = block_id + '-modal-minimize'
      modal_minimized_class = block_id + '-modal-minimized'
      layout_class = '.' + modal_class + ' .spb-popup-main-wrapper'
      // Wrap arround elements
      $(css_identity + block_id).
        wrap($('<div class="' + modal_class + '"></div>'))
      // Hide the popup initially
      $('.' + modal_class).hide()
      // Skip the popup based on visit counts settings
      if (match == -1 && allow_cookie == true) {
        return true
      }
      // Wrap remaining elements
      $(css_identity + block_id).
        wrap($('<div class="spb-popup-main-wrapper"></div>'))
      $('.' + modal_class).
        wrap('<div id="' + spb_popup_id +
          '" class="simple-popup-blocks-global"></div>')
      $(css_identity + block_id).
        before($('<div class="spb-controls"></div>'))

      // Skip code for non popup pages.
      class_exists = $('#' + spb_popup_id).
        hasClass('simple-popup-blocks-global')
      if (!class_exists) {
        return true
      }
      // Minimize button wrap
      if (values.minimize === "1") {
        $("#" + spb_popup_id + " .spb-controls").
          prepend($('<span class="' + modal_minimize_class +
            ' spb_minimize">-</span>'))
        $('.' + modal_class).
          before($('<span class="' + modal_minimized_class +
            ' spb_minimized"></span>'))
      }
      // Close button wrap
      if (values.close == 1) {
        $("#" + spb_popup_id + " .spb-controls").
          prepend($('<span class="' + modal_close_class +
            ' spb_close">&times;</span>'))
      }
      // Overlay
      if (values.overlay == 1) {
        $('.' + modal_class).addClass('spb_overlay')
      }
      // Inject layout class.
      switch (values.layout) {
        // Top left.
        case '0':
          $(layout_class).addClass('spb_top_left')
          $(layout_class).css({
            'width': values.width,
          })
          break
        // Top right.
        case '1':
          $(layout_class).addClass('spb_top_right')
          $(layout_class).css({
            'width': values.width,
          })
          break
        // Bottom left.
        case '2':
          $(layout_class).addClass('spb_bottom_left')
          $(layout_class).css({
            'width': values.width,
          })
          break
        // Bottom right.
        case '3':
          $(layout_class).addClass('spb_bottom_right')
          $(layout_class).css({
            'width': values.width,
          })
          break
        // Center.
        case '4':
          $(layout_class).addClass('spb_center')
          $(layout_class).css({
            'width': values.width,
          })
          break
        // Top Center.
        case '5':
          $(layout_class).addClass('spb_top_center')
          $(layout_class).css({})
          break
        // Top bar.
        case '6':
          $(layout_class).addClass('spb_top_bar')
          $(layout_class).css({})
          break
        // Right bar.
        case '7':
          $(layout_class).addClass('spb_bottom_bar')
          $(layout_class).css({})
          break
        // Bottom bar.
        case '8':
          $(layout_class).addClass('spb_left_bar')
          $(layout_class).css({
            'width': values.width,
          })
          break
        // Right bar.
        case '9':
          $(layout_class).addClass('spb_right_bar')
          $(layout_class).css({
            'width': values.width,
          })
          break
      }
      // Automatic trigger with delay
      if (values.trigger_method == 0 && values.delay > 0) {
        delays = values.delay * 1000
        $('.' + modal_class).delay(delays).fadeIn('slow')
        if (values.overlay == 1) {
          setTimeout(stopTheScroll, delays)
        }
      }
      // Automatic trigger without delay
      else if (values.trigger_method == 0) {
        $('.' + modal_class).show()
        $(css_identity + block_id).show()
        if (values.overlay == 1) {
          stopTheScroll()
        }
      }
      // Manual trigger
      else if (values.trigger_method == 1) {
        $(values.trigger_selector).click(function () {
          $('.' + modal_class).show()
          $(css_identity + block_id).show()
          if (values.overlay == 1) {
            stopTheScroll()
          }
          return false;
        })
      }
      // Browser close trigger
      else if (values.trigger_method == 2) {
        $(_html).mouseleave(function (e) {
          // Trigger only when mouse leave on top view port
          if (e.clientY > 20) { return }
          // Trigger only once per page
          if (!browser_close_trigger) { return }
          browser_close_trigger = false
          $('.' + modal_class).show()
          $(css_identity + block_id).show()
          if (values.overlay == 1) {
            stopTheScroll()
          }
        })
      }
      // Trigger for close button click
      $('.' + modal_close_class).click(function () {
        $('.' + modal_class).hide()
        startTheScroll()
      })
      // Trigger for minimize button click
      $('.' + modal_minimize_class).click(function () {
        $('.' + modal_class).hide()
        startTheScroll()
        $('.' + modal_minimized_class).show()
      })
      // Trigger for minimized button click
      $('.' + modal_minimized_class).click(function () {
        $('.' + modal_class).show()
        $(css_identity + block_id).show()
        if (values.overlay == 1) {
          stopTheScroll()
        }
        $('.' + modal_minimized_class).hide()
      })
      // Trigger for ESC button click
      if (values.escape == 1) {
        $(document).keyup(function (e) {
          if (e.keyCode == 27) { // Escape key maps to keycode `27`.
            $('.' + modal_class).hide()
            startTheScroll()
            $('.' + modal_minimized_class).show()
          }
        })
      }
    }) // Foreach end.
  }) // document.ready end.

  // Remove the scrolling while overlay active
  function stopTheScroll() {
    $('body').css({
      'overflow': 'hidden',
    })
  }

  // Enable the scrolling while overlay inactive
  function startTheScroll() {
    $('body').css({
      'overflow': '',
    })
  }

  // Creating cookie
  function createCookie(name, value, days) {
    if (days) {
      var date = new Date()
      date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000))
      var expires = '; expires=' + date.toGMTString()
    }
    else {
      var expires = ''
    }
    document.cookie = name + '=' + value + expires + '; path=/'
  }

  // Reading cookie
  function readCookie(name) {
    var nameEQ = name + '='
    var ca = document.cookie.split(';')
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i]
      while (c.charAt(0) == ' ') {
        c = c.substring(1, c.length)
      }
      if (c.indexOf(nameEQ) == 0) {
        return c.substring(nameEQ.length, c.length)
      }
    }
    return null
  }

})(jQuery, Drupal)
  ;
/* ========================================================================
 * Bootstrap: affix.js v3.3.7
 * http://getbootstrap.com/javascript/#affix
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // AFFIX CLASS DEFINITION
  // ======================

  var Affix = function (element, options) {
    this.options = $.extend({}, Affix.DEFAULTS, options)

    this.$target = $(this.options.target)
      .on('scroll.bs.affix.data-api', $.proxy(this.checkPosition, this))
      .on('click.bs.affix.data-api', $.proxy(this.checkPositionWithEventLoop, this))

    this.$element = $(element)
    this.affixed = null
    this.unpin = null
    this.pinnedOffset = null

    this.checkPosition()
  }

  Affix.VERSION = '3.3.7'

  Affix.RESET = 'affix affix-top affix-bottom'

  Affix.DEFAULTS = {
    offset: 0,
    target: window
  }

  Affix.prototype.getState = function (scrollHeight, height, offsetTop, offsetBottom) {
    var scrollTop = this.$target.scrollTop()
    var position = this.$element.offset()
    var targetHeight = this.$target.height()

    if (offsetTop != null && this.affixed == 'top') return scrollTop < offsetTop ? 'top' : false

    if (this.affixed == 'bottom') {
      if (offsetTop != null) return (scrollTop + this.unpin <= position.top) ? false : 'bottom'
      return (scrollTop + targetHeight <= scrollHeight - offsetBottom) ? false : 'bottom'
    }

    var initializing = this.affixed == null
    var colliderTop = initializing ? scrollTop : position.top
    var colliderHeight = initializing ? targetHeight : height

    if (offsetTop != null && scrollTop <= offsetTop) return 'top'
    if (offsetBottom != null && (colliderTop + colliderHeight >= scrollHeight - offsetBottom)) return 'bottom'

    return false
  }

  Affix.prototype.getPinnedOffset = function () {
    if (this.pinnedOffset) return this.pinnedOffset
    this.$element.removeClass(Affix.RESET).addClass('affix')
    var scrollTop = this.$target.scrollTop()
    var position = this.$element.offset()
    return (this.pinnedOffset = position.top - scrollTop)
  }

  Affix.prototype.checkPositionWithEventLoop = function () {
    setTimeout($.proxy(this.checkPosition, this), 1)
  }

  Affix.prototype.checkPosition = function () {
    if (!this.$element.is(':visible')) return

    var height = this.$element.height()
    var offset = this.options.offset
    var offsetTop = offset.top
    var offsetBottom = offset.bottom
    var scrollHeight = Math.max($(document).height(), $(document.body).height())

    if (typeof offset != 'object') offsetBottom = offsetTop = offset
    if (typeof offsetTop == 'function') offsetTop = offset.top(this.$element)
    if (typeof offsetBottom == 'function') offsetBottom = offset.bottom(this.$element)

    var affix = this.getState(scrollHeight, height, offsetTop, offsetBottom)

    if (this.affixed != affix) {
      if (this.unpin != null) this.$element.css('top', '')

      var affixType = 'affix' + (affix ? '-' + affix : '')
      var e = $.Event(affixType + '.bs.affix')

      this.$element.trigger(e)

      if (e.isDefaultPrevented()) return

      this.affixed = affix
      this.unpin = affix == 'bottom' ? this.getPinnedOffset() : null

      this.$element
        .removeClass(Affix.RESET)
        .addClass(affixType)
        .trigger(affixType.replace('affix', 'affixed') + '.bs.affix')
    }

    if (affix == 'bottom') {
      this.$element.offset({
        top: scrollHeight - height - offsetBottom
      })
    }
  }


  // AFFIX PLUGIN DEFINITION
  // =======================

  function Plugin(option) {
    return this.each(function () {
      var $this = $(this)
      var data = $this.data('bs.affix')
      var options = typeof option == 'object' && option

      if (!data) $this.data('bs.affix', (data = new Affix(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.affix

  $.fn.affix = Plugin
  $.fn.affix.Constructor = Affix


  // AFFIX NO CONFLICT
  // =================

  $.fn.affix.noConflict = function () {
    $.fn.affix = old
    return this
  }


  // AFFIX DATA-API
  // ==============

  $(window).on('load', function () {
    $('[data-spy="affix"]').each(function () {
      var $spy = $(this)
      var data = $spy.data()

      data.offset = data.offset || {}

      if (data.offsetBottom != null) data.offset.bottom = data.offsetBottom
      if (data.offsetTop != null) data.offset.top = data.offsetTop

      Plugin.call($spy, data)
    })
  })

}(jQuery);
;
/* ========================================================================
 * Bootstrap: alert.js v3.3.7
 * http://getbootstrap.com/javascript/#alerts
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // ALERT CLASS DEFINITION
  // ======================

  var dismiss = '[data-dismiss="alert"]'
  var Alert = function (el) {
    $(el).on('click', dismiss, this.close)
  }

  Alert.VERSION = '3.3.7'

  Alert.TRANSITION_DURATION = 150

  Alert.prototype.close = function (e) {
    var $this = $(this)
    var selector = $this.attr('data-target')

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') // strip for ie7
    }

    var $parent = $(selector === '#' ? [] : selector)

    if (e) e.preventDefault()

    if (!$parent.length) {
      $parent = $this.closest('.alert')
    }

    $parent.trigger(e = $.Event('close.bs.alert'))

    if (e.isDefaultPrevented()) return

    $parent.removeClass('in')

    function removeElement() {
      // detach from parent, fire event then clean up data
      $parent.detach().trigger('closed.bs.alert').remove()
    }

    $.support.transition && $parent.hasClass('fade') ?
      $parent
        .one('bsTransitionEnd', removeElement)
        .emulateTransitionEnd(Alert.TRANSITION_DURATION) :
      removeElement()
  }


  // ALERT PLUGIN DEFINITION
  // =======================

  function Plugin(option) {
    return this.each(function () {
      var $this = $(this)
      var data = $this.data('bs.alert')

      if (!data) $this.data('bs.alert', (data = new Alert(this)))
      if (typeof option == 'string') data[option].call($this)
    })
  }

  var old = $.fn.alert

  $.fn.alert = Plugin
  $.fn.alert.Constructor = Alert


  // ALERT NO CONFLICT
  // =================

  $.fn.alert.noConflict = function () {
    $.fn.alert = old
    return this
  }


  // ALERT DATA-API
  // ==============

  $(document).on('click.bs.alert.data-api', dismiss, Alert.prototype.close)

}(jQuery);
;
/* ========================================================================
 * Bootstrap: button.js v3.3.7
 * http://getbootstrap.com/javascript/#buttons
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // BUTTON PUBLIC CLASS DEFINITION
  // ==============================

  var Button = function (element, options) {
    this.$element = $(element)
    this.options = $.extend({}, Button.DEFAULTS, options)
    this.isLoading = false
  }

  Button.VERSION = '3.3.7'

  Button.DEFAULTS = {
    loadingText: 'loading...'
  }

  Button.prototype.setState = function (state) {
    var d = 'disabled'
    var $el = this.$element
    var val = $el.is('input') ? 'val' : 'html'
    var data = $el.data()

    state += 'Text'

    if (data.resetText == null) $el.data('resetText', $el[val]())

    // push to event loop to allow forms to submit
    setTimeout($.proxy(function () {
      $el[val](data[state] == null ? this.options[state] : data[state])

      if (state == 'loadingText') {
        this.isLoading = true
        $el.addClass(d).attr(d, d).prop(d, true)
      } else if (this.isLoading) {
        this.isLoading = false
        $el.removeClass(d).removeAttr(d).prop(d, false)
      }
    }, this), 0)
  }

  Button.prototype.toggle = function () {
    var changed = true
    var $parent = this.$element.closest('[data-toggle="buttons"]')

    if ($parent.length) {
      var $input = this.$element.find('input')
      if ($input.prop('type') == 'radio') {
        if ($input.prop('checked')) changed = false
        $parent.find('.active').removeClass('active')
        this.$element.addClass('active')
      } else if ($input.prop('type') == 'checkbox') {
        if (($input.prop('checked')) !== this.$element.hasClass('active')) changed = false
        this.$element.toggleClass('active')
      }
      $input.prop('checked', this.$element.hasClass('active'))
      if (changed) $input.trigger('change')
    } else {
      this.$element.attr('aria-pressed', !this.$element.hasClass('active'))
      this.$element.toggleClass('active')
    }
  }


  // BUTTON PLUGIN DEFINITION
  // ========================

  function Plugin(option) {
    return this.each(function () {
      var $this = $(this)
      var data = $this.data('bs.button')
      var options = typeof option == 'object' && option

      if (!data) $this.data('bs.button', (data = new Button(this, options)))

      if (option == 'toggle') data.toggle()
      else if (option) data.setState(option)
    })
  }

  var old = $.fn.button

  $.fn.button = Plugin
  $.fn.button.Constructor = Button


  // BUTTON NO CONFLICT
  // ==================

  $.fn.button.noConflict = function () {
    $.fn.button = old
    return this
  }


  // BUTTON DATA-API
  // ===============

  $(document)
    .on('click.bs.button.data-api', '[data-toggle^="button"]', function (e) {
      var $btn = $(e.target).closest('.btn')
      Plugin.call($btn, 'toggle')
      if (!($(e.target).is('input[type="radio"], input[type="checkbox"]'))) {
        // Prevent double click on radios, and the double selections (so cancellation) on checkboxes
        e.preventDefault()
        // The target component still receive the focus
        if ($btn.is('input,button')) $btn.trigger('focus')
        else $btn.find('input:visible,button:visible').first().trigger('focus')
      }
    })
    .on('focus.bs.button.data-api blur.bs.button.data-api', '[data-toggle^="button"]', function (e) {
      $(e.target).closest('.btn').toggleClass('focus', /^focus(in)?$/.test(e.type))
    })

}(jQuery);
;
/* ========================================================================
 * Bootstrap: carousel.js v3.3.7
 * http://getbootstrap.com/javascript/#carousel
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // CAROUSEL CLASS DEFINITION
  // =========================

  var Carousel = function (element, options) {
    this.$element = $(element)
    this.$indicators = this.$element.find('.carousel-indicators')
    this.options = options
    this.paused = null
    this.sliding = null
    this.interval = null
    this.$active = null
    this.$items = null

    this.options.keyboard && this.$element.on('keydown.bs.carousel', $.proxy(this.keydown, this))

    this.options.pause == 'hover' && !('ontouchstart' in document.documentElement) && this.$element
      .on('mouseenter.bs.carousel', $.proxy(this.pause, this))
      .on('mouseleave.bs.carousel', $.proxy(this.cycle, this))
  }

  Carousel.VERSION = '3.3.7'

  Carousel.TRANSITION_DURATION = 600

  Carousel.DEFAULTS = {
    interval: 5000,
    pause: 'hover',
    wrap: true,
    keyboard: true
  }

  Carousel.prototype.keydown = function (e) {
    if (/input|textarea/i.test(e.target.tagName)) return
    switch (e.which) {
      case 37: this.prev(); break
      case 39: this.next(); break
      default: return
    }

    e.preventDefault()
  }

  Carousel.prototype.cycle = function (e) {
    e || (this.paused = false)

    this.interval && clearInterval(this.interval)

    this.options.interval
      && !this.paused
      && (this.interval = setInterval($.proxy(this.next, this), this.options.interval))

    return this
  }

  Carousel.prototype.getItemIndex = function (item) {
    this.$items = item.parent().children('.item')
    return this.$items.index(item || this.$active)
  }

  Carousel.prototype.getItemForDirection = function (direction, active) {
    var activeIndex = this.getItemIndex(active)
    var willWrap = (direction == 'prev' && activeIndex === 0)
      || (direction == 'next' && activeIndex == (this.$items.length - 1))
    if (willWrap && !this.options.wrap) return active
    var delta = direction == 'prev' ? -1 : 1
    var itemIndex = (activeIndex + delta) % this.$items.length
    return this.$items.eq(itemIndex)
  }

  Carousel.prototype.to = function (pos) {
    var that = this
    var activeIndex = this.getItemIndex(this.$active = this.$element.find('.item.active'))

    if (pos > (this.$items.length - 1) || pos < 0) return

    if (this.sliding) return this.$element.one('slid.bs.carousel', function () { that.to(pos) }) // yes, "slid"
    if (activeIndex == pos) return this.pause().cycle()

    return this.slide(pos > activeIndex ? 'next' : 'prev', this.$items.eq(pos))
  }

  Carousel.prototype.pause = function (e) {
    e || (this.paused = true)

    if (this.$element.find('.next, .prev').length && $.support.transition) {
      this.$element.trigger($.support.transition.end)
      this.cycle(true)
    }

    this.interval = clearInterval(this.interval)

    return this
  }

  Carousel.prototype.next = function () {
    if (this.sliding) return
    return this.slide('next')
  }

  Carousel.prototype.prev = function () {
    if (this.sliding) return
    return this.slide('prev')
  }

  Carousel.prototype.slide = function (type, next) {
    var $active = this.$element.find('.item.active')
    var $next = next || this.getItemForDirection(type, $active)
    var isCycling = this.interval
    var direction = type == 'next' ? 'left' : 'right'
    var that = this

    if ($next.hasClass('active')) return (this.sliding = false)

    var relatedTarget = $next[0]
    var slideEvent = $.Event('slide.bs.carousel', {
      relatedTarget: relatedTarget,
      direction: direction
    })
    this.$element.trigger(slideEvent)
    if (slideEvent.isDefaultPrevented()) return

    this.sliding = true

    isCycling && this.pause()

    if (this.$indicators.length) {
      this.$indicators.find('.active').removeClass('active')
      var $nextIndicator = $(this.$indicators.children()[this.getItemIndex($next)])
      $nextIndicator && $nextIndicator.addClass('active')
    }

    var slidEvent = $.Event('slid.bs.carousel', { relatedTarget: relatedTarget, direction: direction }) // yes, "slid"
    if ($.support.transition && this.$element.hasClass('slide')) {
      $next.addClass(type)
      $next[0].offsetWidth // force reflow
      $active.addClass(direction)
      $next.addClass(direction)
      $active
        .one('bsTransitionEnd', function () {
          $next.removeClass([type, direction].join(' ')).addClass('active')
          $active.removeClass(['active', direction].join(' '))
          that.sliding = false
          setTimeout(function () {
            that.$element.trigger(slidEvent)
          }, 0)
        })
        .emulateTransitionEnd(Carousel.TRANSITION_DURATION)
    } else {
      $active.removeClass('active')
      $next.addClass('active')
      this.sliding = false
      this.$element.trigger(slidEvent)
    }

    isCycling && this.cycle()

    return this
  }


  // CAROUSEL PLUGIN DEFINITION
  // ==========================

  function Plugin(option) {
    return this.each(function () {
      var $this = $(this)
      var data = $this.data('bs.carousel')
      var options = $.extend({}, Carousel.DEFAULTS, $this.data(), typeof option == 'object' && option)
      var action = typeof option == 'string' ? option : options.slide

      if (!data) $this.data('bs.carousel', (data = new Carousel(this, options)))
      if (typeof option == 'number') data.to(option)
      else if (action) data[action]()
      else if (options.interval) data.pause().cycle()
    })
  }

  var old = $.fn.carousel

  $.fn.carousel = Plugin
  $.fn.carousel.Constructor = Carousel


  // CAROUSEL NO CONFLICT
  // ====================

  $.fn.carousel.noConflict = function () {
    $.fn.carousel = old
    return this
  }


  // CAROUSEL DATA-API
  // =================

  var clickHandler = function (e) {
    var href
    var $this = $(this)
    var $target = $($this.attr('data-target') || (href = $this.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '')) // strip for ie7
    if (!$target.hasClass('carousel')) return
    var options = $.extend({}, $target.data(), $this.data())
    var slideIndex = $this.attr('data-slide-to')
    if (slideIndex) options.interval = false

    Plugin.call($target, options)

    if (slideIndex) {
      $target.data('bs.carousel').to(slideIndex)
    }

    e.preventDefault()
  }

  $(document)
    .on('click.bs.carousel.data-api', '[data-slide]', clickHandler)
    .on('click.bs.carousel.data-api', '[data-slide-to]', clickHandler)

  $(window).on('load', function () {
    $('[data-ride="carousel"]').each(function () {
      var $carousel = $(this)
      Plugin.call($carousel, $carousel.data())
    })
  })

}(jQuery);
;
/* ========================================================================
 * Bootstrap: collapse.js v3.3.7
 * http://getbootstrap.com/javascript/#collapse
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */

/* jshint latedef: false */

+function ($) {
  'use strict';

  // COLLAPSE PUBLIC CLASS DEFINITION
  // ================================

  var Collapse = function (element, options) {
    this.$element = $(element)
    this.options = $.extend({}, Collapse.DEFAULTS, options)
    this.$trigger = $('[data-toggle="collapse"][href="#' + element.id + '"],' +
      '[data-toggle="collapse"][data-target="#' + element.id + '"]')
    this.transitioning = null

    if (this.options.parent) {
      this.$parent = this.getParent()
    } else {
      this.addAriaAndCollapsedClass(this.$element, this.$trigger)
    }

    if (this.options.toggle) this.toggle()
  }

  Collapse.VERSION = '3.3.7'

  Collapse.TRANSITION_DURATION = 350

  Collapse.DEFAULTS = {
    toggle: true
  }

  Collapse.prototype.dimension = function () {
    var hasWidth = this.$element.hasClass('width')
    return hasWidth ? 'width' : 'height'
  }

  Collapse.prototype.show = function () {
    if (this.transitioning || this.$element.hasClass('in')) return

    var activesData
    var actives = this.$parent && this.$parent.children('.panel').children('.in, .collapsing')

    if (actives && actives.length) {
      activesData = actives.data('bs.collapse')
      if (activesData && activesData.transitioning) return
    }

    var startEvent = $.Event('show.bs.collapse')
    this.$element.trigger(startEvent)
    if (startEvent.isDefaultPrevented()) return

    if (actives && actives.length) {
      Plugin.call(actives, 'hide')
      activesData || actives.data('bs.collapse', null)
    }

    var dimension = this.dimension()

    this.$element
      .removeClass('collapse')
      .addClass('collapsing')[dimension](0)
      .attr('aria-expanded', true)

    this.$trigger
      .removeClass('collapsed')
      .attr('aria-expanded', true)

    this.transitioning = 1

    var complete = function () {
      this.$element
        .removeClass('collapsing')
        .addClass('collapse in')[dimension]('')
      this.transitioning = 0
      this.$element
        .trigger('shown.bs.collapse')
    }

    if (!$.support.transition) return complete.call(this)

    var scrollSize = $.camelCase(['scroll', dimension].join('-'))

    this.$element
      .one('bsTransitionEnd', $.proxy(complete, this))
      .emulateTransitionEnd(Collapse.TRANSITION_DURATION)[dimension](this.$element[0][scrollSize])
  }

  Collapse.prototype.hide = function () {
    if (this.transitioning || !this.$element.hasClass('in')) return

    var startEvent = $.Event('hide.bs.collapse')
    this.$element.trigger(startEvent)
    if (startEvent.isDefaultPrevented()) return

    var dimension = this.dimension()

    this.$element[dimension](this.$element[dimension]())[0].offsetHeight

    this.$element
      .addClass('collapsing')
      .removeClass('collapse in')
      .attr('aria-expanded', false)

    this.$trigger
      .addClass('collapsed')
      .attr('aria-expanded', false)

    this.transitioning = 1

    var complete = function () {
      this.transitioning = 0
      this.$element
        .removeClass('collapsing')
        .addClass('collapse')
        .trigger('hidden.bs.collapse')
    }

    if (!$.support.transition) return complete.call(this)

    this.$element
    [dimension](0)
      .one('bsTransitionEnd', $.proxy(complete, this))
      .emulateTransitionEnd(Collapse.TRANSITION_DURATION)
  }

  Collapse.prototype.toggle = function () {
    this[this.$element.hasClass('in') ? 'hide' : 'show']()
  }

  Collapse.prototype.getParent = function () {
    return $(this.options.parent)
      .find('[data-toggle="collapse"][data-parent="' + this.options.parent + '"]')
      .each($.proxy(function (i, element) {
        var $element = $(element)
        this.addAriaAndCollapsedClass(getTargetFromTrigger($element), $element)
      }, this))
      .end()
  }

  Collapse.prototype.addAriaAndCollapsedClass = function ($element, $trigger) {
    var isOpen = $element.hasClass('in')

    $element.attr('aria-expanded', isOpen)
    $trigger
      .toggleClass('collapsed', !isOpen)
      .attr('aria-expanded', isOpen)
  }

  function getTargetFromTrigger($trigger) {
    var href
    var target = $trigger.attr('data-target')
      || (href = $trigger.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '') // strip for ie7

    return $(target)
  }


  // COLLAPSE PLUGIN DEFINITION
  // ==========================

  function Plugin(option) {
    return this.each(function () {
      var $this = $(this)
      var data = $this.data('bs.collapse')
      var options = $.extend({}, Collapse.DEFAULTS, $this.data(), typeof option == 'object' && option)

      if (!data && options.toggle && /show|hide/.test(option)) options.toggle = false
      if (!data) $this.data('bs.collapse', (data = new Collapse(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.collapse

  $.fn.collapse = Plugin
  $.fn.collapse.Constructor = Collapse


  // COLLAPSE NO CONFLICT
  // ====================

  $.fn.collapse.noConflict = function () {
    $.fn.collapse = old
    return this
  }


  // COLLAPSE DATA-API
  // =================

  $(document).on('click.bs.collapse.data-api', '[data-toggle="collapse"]', function (e) {
    var $this = $(this)

    if (!$this.attr('data-target')) e.preventDefault()

    var $target = getTargetFromTrigger($this)
    var data = $target.data('bs.collapse')
    var option = data ? 'toggle' : $this.data()

    Plugin.call($target, option)
  })

}(jQuery);
;
/* ========================================================================
 * Bootstrap: dropdown.js v3.3.7
 * http://getbootstrap.com/javascript/#dropdowns
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // DROPDOWN CLASS DEFINITION
  // =========================

  var backdrop = '.dropdown-backdrop'
  var toggle = '[data-toggle="dropdown"]'
  var Dropdown = function (element) {
    $(element).on('click.bs.dropdown', this.toggle)
  }

  Dropdown.VERSION = '3.3.7'

  function getParent($this) {
    var selector = $this.attr('data-target')

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && /#[A-Za-z]/.test(selector) && selector.replace(/.*(?=#[^\s]*$)/, '') // strip for ie7
    }

    var $parent = selector && $(selector)

    return $parent && $parent.length ? $parent : $this.parent()
  }

  function clearMenus(e) {
    if (e && e.which === 3) return
    $(backdrop).remove()
    $(toggle).each(function () {
      var $this = $(this)
      var $parent = getParent($this)
      var relatedTarget = { relatedTarget: this }

      if (!$parent.hasClass('open')) return

      if (e && e.type == 'click' && /input|textarea/i.test(e.target.tagName) && $.contains($parent[0], e.target)) return

      $parent.trigger(e = $.Event('hide.bs.dropdown', relatedTarget))

      if (e.isDefaultPrevented()) return

      $this.attr('aria-expanded', 'false')
      $parent.removeClass('open').trigger($.Event('hidden.bs.dropdown', relatedTarget))
    })
  }

  Dropdown.prototype.toggle = function (e) {
    var $this = $(this)

    if ($this.is('.disabled, :disabled')) return

    var $parent = getParent($this)
    var isActive = $parent.hasClass('open')

    clearMenus()

    if (!isActive) {
      if ('ontouchstart' in document.documentElement && !$parent.closest('.navbar-nav').length) {
        // if mobile we use a backdrop because click events don't delegate
        $(document.createElement('div'))
          .addClass('dropdown-backdrop')
          .insertAfter($(this))
          .on('click', clearMenus)
      }

      var relatedTarget = { relatedTarget: this }
      $parent.trigger(e = $.Event('show.bs.dropdown', relatedTarget))

      if (e.isDefaultPrevented()) return

      $this
        .trigger('focus')
        .attr('aria-expanded', 'true')

      $parent
        .toggleClass('open')
        .trigger($.Event('shown.bs.dropdown', relatedTarget))
    }

    return false
  }

  Dropdown.prototype.keydown = function (e) {
    if (!/(38|40|27|32)/.test(e.which) || /input|textarea/i.test(e.target.tagName)) return

    var $this = $(this)

    e.preventDefault()
    e.stopPropagation()

    if ($this.is('.disabled, :disabled')) return

    var $parent = getParent($this)
    var isActive = $parent.hasClass('open')

    if (!isActive && e.which != 27 || isActive && e.which == 27) {
      if (e.which == 27) $parent.find(toggle).trigger('focus')
      return $this.trigger('click')
    }

    var desc = ' li:not(.disabled):visible a'
    var $items = $parent.find('.dropdown-menu' + desc)

    if (!$items.length) return

    var index = $items.index(e.target)

    if (e.which == 38 && index > 0) index--         // up
    if (e.which == 40 && index < $items.length - 1) index++         // down
    if (!~index) index = 0

    $items.eq(index).trigger('focus')
  }


  // DROPDOWN PLUGIN DEFINITION
  // ==========================

  function Plugin(option) {
    return this.each(function () {
      var $this = $(this)
      var data = $this.data('bs.dropdown')

      if (!data) $this.data('bs.dropdown', (data = new Dropdown(this)))
      if (typeof option == 'string') data[option].call($this)
    })
  }

  var old = $.fn.dropdown

  $.fn.dropdown = Plugin
  $.fn.dropdown.Constructor = Dropdown


  // DROPDOWN NO CONFLICT
  // ====================

  $.fn.dropdown.noConflict = function () {
    $.fn.dropdown = old
    return this
  }


  // APPLY TO STANDARD DROPDOWN ELEMENTS
  // ===================================

  $(document)
    .on('click.bs.dropdown.data-api', clearMenus)
    .on('click.bs.dropdown.data-api', '.dropdown form', function (e) { e.stopPropagation() })
    .on('click.bs.dropdown.data-api', toggle, Dropdown.prototype.toggle)
    .on('keydown.bs.dropdown.data-api', toggle, Dropdown.prototype.keydown)
    .on('keydown.bs.dropdown.data-api', '.dropdown-menu', Dropdown.prototype.keydown)

}(jQuery);
;
/* ========================================================================
 * Bootstrap: modal.js v3.3.7
 * http://getbootstrap.com/javascript/#modals
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // MODAL CLASS DEFINITION
  // ======================

  var Modal = function (element, options) {
    this.options = options
    this.$body = $(document.body)
    this.$element = $(element)
    this.$dialog = this.$element.find('.modal-dialog')
    this.$backdrop = null
    this.isShown = null
    this.originalBodyPad = null
    this.scrollbarWidth = 0
    this.ignoreBackdropClick = false

    if (this.options.remote) {
      this.$element
        .find('.modal-content')
        .load(this.options.remote, $.proxy(function () {
          this.$element.trigger('loaded.bs.modal')
        }, this))
    }
  }

  Modal.VERSION = '3.3.7'

  Modal.TRANSITION_DURATION = 300
  Modal.BACKDROP_TRANSITION_DURATION = 150

  Modal.DEFAULTS = {
    backdrop: true,
    keyboard: true,
    show: true
  }

  Modal.prototype.toggle = function (_relatedTarget) {
    return this.isShown ? this.hide() : this.show(_relatedTarget)
  }

  Modal.prototype.show = function (_relatedTarget) {
    var that = this
    var e = $.Event('show.bs.modal', { relatedTarget: _relatedTarget })

    this.$element.trigger(e)

    if (this.isShown || e.isDefaultPrevented()) return

    this.isShown = true

    this.checkScrollbar()
    this.setScrollbar()
    this.$body.addClass('modal-open')

    this.escape()
    this.resize()

    this.$element.on('click.dismiss.bs.modal', '[data-dismiss="modal"]', $.proxy(this.hide, this))

    this.$dialog.on('mousedown.dismiss.bs.modal', function () {
      that.$element.one('mouseup.dismiss.bs.modal', function (e) {
        if ($(e.target).is(that.$element)) that.ignoreBackdropClick = true
      })
    })

    this.backdrop(function () {
      var transition = $.support.transition && that.$element.hasClass('fade')

      if (!that.$element.parent().length) {
        that.$element.appendTo(that.$body) // don't move modals dom position
      }

      that.$element
        .show()
        .scrollTop(0)

      that.adjustDialog()

      if (transition) {
        that.$element[0].offsetWidth // force reflow
      }

      that.$element.addClass('in')

      that.enforceFocus()

      var e = $.Event('shown.bs.modal', { relatedTarget: _relatedTarget })

      transition ?
        that.$dialog // wait for modal to slide in
          .one('bsTransitionEnd', function () {
            that.$element.trigger('focus').trigger(e)
          })
          .emulateTransitionEnd(Modal.TRANSITION_DURATION) :
        that.$element.trigger('focus').trigger(e)
    })
  }

  Modal.prototype.hide = function (e) {
    if (e) e.preventDefault()

    e = $.Event('hide.bs.modal')

    this.$element.trigger(e)

    if (!this.isShown || e.isDefaultPrevented()) return

    this.isShown = false

    this.escape()
    this.resize()

    $(document).off('focusin.bs.modal')

    this.$element
      .removeClass('in')
      .off('click.dismiss.bs.modal')
      .off('mouseup.dismiss.bs.modal')

    this.$dialog.off('mousedown.dismiss.bs.modal')

    $.support.transition && this.$element.hasClass('fade') ?
      this.$element
        .one('bsTransitionEnd', $.proxy(this.hideModal, this))
        .emulateTransitionEnd(Modal.TRANSITION_DURATION) :
      this.hideModal()
  }

  Modal.prototype.enforceFocus = function () {
    $(document)
      .off('focusin.bs.modal') // guard against infinite focus loop
      .on('focusin.bs.modal', $.proxy(function (e) {
        if (document !== e.target &&
          this.$element[0] !== e.target &&
          !this.$element.has(e.target).length) {
          this.$element.trigger('focus')
        }
      }, this))
  }

  Modal.prototype.escape = function () {
    if (this.isShown && this.options.keyboard) {
      this.$element.on('keydown.dismiss.bs.modal', $.proxy(function (e) {
        e.which == 27 && this.hide()
      }, this))
    } else if (!this.isShown) {
      this.$element.off('keydown.dismiss.bs.modal')
    }
  }

  Modal.prototype.resize = function () {
    if (this.isShown) {
      $(window).on('resize.bs.modal', $.proxy(this.handleUpdate, this))
    } else {
      $(window).off('resize.bs.modal')
    }
  }

  Modal.prototype.hideModal = function () {
    var that = this
    this.$element.hide()
    this.backdrop(function () {
      that.$body.removeClass('modal-open')
      that.resetAdjustments()
      that.resetScrollbar()
      that.$element.trigger('hidden.bs.modal')
    })
  }

  Modal.prototype.removeBackdrop = function () {
    this.$backdrop && this.$backdrop.remove()
    this.$backdrop = null
  }

  Modal.prototype.backdrop = function (callback) {
    var that = this
    var animate = this.$element.hasClass('fade') ? 'fade' : ''

    if (this.isShown && this.options.backdrop) {
      var doAnimate = $.support.transition && animate

      this.$backdrop = $(document.createElement('div'))
        .addClass('modal-backdrop ' + animate)
        .appendTo(this.$body)

      this.$element.on('click.dismiss.bs.modal', $.proxy(function (e) {
        if (this.ignoreBackdropClick) {
          this.ignoreBackdropClick = false
          return
        }
        if (e.target !== e.currentTarget) return
        this.options.backdrop == 'static'
          ? this.$element[0].focus()
          : this.hide()
      }, this))

      if (doAnimate) this.$backdrop[0].offsetWidth // force reflow

      this.$backdrop.addClass('in')

      if (!callback) return

      doAnimate ?
        this.$backdrop
          .one('bsTransitionEnd', callback)
          .emulateTransitionEnd(Modal.BACKDROP_TRANSITION_DURATION) :
        callback()

    } else if (!this.isShown && this.$backdrop) {
      this.$backdrop.removeClass('in')

      var callbackRemove = function () {
        that.removeBackdrop()
        callback && callback()
      }
      $.support.transition && this.$element.hasClass('fade') ?
        this.$backdrop
          .one('bsTransitionEnd', callbackRemove)
          .emulateTransitionEnd(Modal.BACKDROP_TRANSITION_DURATION) :
        callbackRemove()

    } else if (callback) {
      callback()
    }
  }

  // these following methods are used to handle overflowing modals

  Modal.prototype.handleUpdate = function () {
    this.adjustDialog()
  }

  Modal.prototype.adjustDialog = function () {
    var modalIsOverflowing = this.$element[0].scrollHeight > document.documentElement.clientHeight

    this.$element.css({
      paddingLeft: !this.bodyIsOverflowing && modalIsOverflowing ? this.scrollbarWidth : '',
      paddingRight: this.bodyIsOverflowing && !modalIsOverflowing ? this.scrollbarWidth : ''
    })
  }

  Modal.prototype.resetAdjustments = function () {
    this.$element.css({
      paddingLeft: '',
      paddingRight: ''
    })
  }

  Modal.prototype.checkScrollbar = function () {
    var fullWindowWidth = window.innerWidth
    if (!fullWindowWidth) { // workaround for missing window.innerWidth in IE8
      var documentElementRect = document.documentElement.getBoundingClientRect()
      fullWindowWidth = documentElementRect.right - Math.abs(documentElementRect.left)
    }
    this.bodyIsOverflowing = document.body.clientWidth < fullWindowWidth
    this.scrollbarWidth = this.measureScrollbar()
  }

  Modal.prototype.setScrollbar = function () {
    var bodyPad = parseInt((this.$body.css('padding-right') || 0), 10)
    this.originalBodyPad = document.body.style.paddingRight || ''
    if (this.bodyIsOverflowing) this.$body.css('padding-right', bodyPad + this.scrollbarWidth)
  }

  Modal.prototype.resetScrollbar = function () {
    this.$body.css('padding-right', this.originalBodyPad)
  }

  Modal.prototype.measureScrollbar = function () { // thx walsh
    var scrollDiv = document.createElement('div')
    scrollDiv.className = 'modal-scrollbar-measure'
    this.$body.append(scrollDiv)
    var scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth
    this.$body[0].removeChild(scrollDiv)
    return scrollbarWidth
  }


  // MODAL PLUGIN DEFINITION
  // =======================

  function Plugin(option, _relatedTarget) {
    return this.each(function () {
      var $this = $(this)
      var data = $this.data('bs.modal')
      var options = $.extend({}, Modal.DEFAULTS, $this.data(), typeof option == 'object' && option)

      if (!data) $this.data('bs.modal', (data = new Modal(this, options)))
      if (typeof option == 'string') data[option](_relatedTarget)
      else if (options.show) data.show(_relatedTarget)
    })
  }

  var old = $.fn.modal

  $.fn.modal = Plugin
  $.fn.modal.Constructor = Modal


  // MODAL NO CONFLICT
  // =================

  $.fn.modal.noConflict = function () {
    $.fn.modal = old
    return this
  }


  // MODAL DATA-API
  // ==============

  $(document).on('click.bs.modal.data-api', '[data-toggle="modal"]', function (e) {
    var $this = $(this)
    var href = $this.attr('href')
    var $target = $($this.attr('data-target') || (href && href.replace(/.*(?=#[^\s]+$)/, ''))) // strip for ie7
    var option = $target.data('bs.modal') ? 'toggle' : $.extend({ remote: !/#/.test(href) && href }, $target.data(), $this.data())

    if ($this.is('a')) e.preventDefault()

    $target.one('show.bs.modal', function (showEvent) {
      if (showEvent.isDefaultPrevented()) return // only register focus restorer if modal will actually get shown
      $target.one('hidden.bs.modal', function () {
        $this.is(':visible') && $this.trigger('focus')
      })
    })
    Plugin.call($target, option, this)
  })

}(jQuery);
;
/* ========================================================================
 * Bootstrap: tooltip.js v3.3.7
 * http://getbootstrap.com/javascript/#tooltip
 * Inspired by the original jQuery.tipsy by Jason Frame
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // TOOLTIP PUBLIC CLASS DEFINITION
  // ===============================

  var Tooltip = function (element, options) {
    this.type = null
    this.options = null
    this.enabled = null
    this.timeout = null
    this.hoverState = null
    this.$element = null
    this.inState = null

    this.init('tooltip', element, options)
  }

  Tooltip.VERSION = '3.3.7'

  Tooltip.TRANSITION_DURATION = 150

  Tooltip.DEFAULTS = {
    animation: true,
    placement: 'top',
    selector: false,
    template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
    trigger: 'hover focus',
    title: '',
    delay: 0,
    html: false,
    container: false,
    viewport: {
      selector: 'body',
      padding: 0
    }
  }

  Tooltip.prototype.init = function (type, element, options) {
    this.enabled = true
    this.type = type
    this.$element = $(element)
    this.options = this.getOptions(options)
    this.$viewport = this.options.viewport && $($.isFunction(this.options.viewport) ? this.options.viewport.call(this, this.$element) : (this.options.viewport.selector || this.options.viewport))
    this.inState = { click: false, hover: false, focus: false }

    if (this.$element[0] instanceof document.constructor && !this.options.selector) {
      throw new Error('`selector` option must be specified when initializing ' + this.type + ' on the window.document object!')
    }

    var triggers = this.options.trigger.split(' ')

    for (var i = triggers.length; i--;) {
      var trigger = triggers[i]

      if (trigger == 'click') {
        this.$element.on('click.' + this.type, this.options.selector, $.proxy(this.toggle, this))
      } else if (trigger != 'manual') {
        var eventIn = trigger == 'hover' ? 'mouseenter' : 'focusin'
        var eventOut = trigger == 'hover' ? 'mouseleave' : 'focusout'

        this.$element.on(eventIn + '.' + this.type, this.options.selector, $.proxy(this.enter, this))
        this.$element.on(eventOut + '.' + this.type, this.options.selector, $.proxy(this.leave, this))
      }
    }

    this.options.selector ?
      (this._options = $.extend({}, this.options, { trigger: 'manual', selector: '' })) :
      this.fixTitle()
  }

  Tooltip.prototype.getDefaults = function () {
    return Tooltip.DEFAULTS
  }

  Tooltip.prototype.getOptions = function (options) {
    options = $.extend({}, this.getDefaults(), this.$element.data(), options)

    if (options.delay && typeof options.delay == 'number') {
      options.delay = {
        show: options.delay,
        hide: options.delay
      }
    }

    return options
  }

  Tooltip.prototype.getDelegateOptions = function () {
    var options = {}
    var defaults = this.getDefaults()

    this._options && $.each(this._options, function (key, value) {
      if (defaults[key] != value) options[key] = value
    })

    return options
  }

  Tooltip.prototype.enter = function (obj) {
    var self = obj instanceof this.constructor ?
      obj : $(obj.currentTarget).data('bs.' + this.type)

    if (!self) {
      self = new this.constructor(obj.currentTarget, this.getDelegateOptions())
      $(obj.currentTarget).data('bs.' + this.type, self)
    }

    if (obj instanceof $.Event) {
      self.inState[obj.type == 'focusin' ? 'focus' : 'hover'] = true
    }

    if (self.tip().hasClass('in') || self.hoverState == 'in') {
      self.hoverState = 'in'
      return
    }

    clearTimeout(self.timeout)

    self.hoverState = 'in'

    if (!self.options.delay || !self.options.delay.show) return self.show()

    self.timeout = setTimeout(function () {
      if (self.hoverState == 'in') self.show()
    }, self.options.delay.show)
  }

  Tooltip.prototype.isInStateTrue = function () {
    for (var key in this.inState) {
      if (this.inState[key]) return true
    }

    return false
  }

  Tooltip.prototype.leave = function (obj) {
    var self = obj instanceof this.constructor ?
      obj : $(obj.currentTarget).data('bs.' + this.type)

    if (!self) {
      self = new this.constructor(obj.currentTarget, this.getDelegateOptions())
      $(obj.currentTarget).data('bs.' + this.type, self)
    }

    if (obj instanceof $.Event) {
      self.inState[obj.type == 'focusout' ? 'focus' : 'hover'] = false
    }

    if (self.isInStateTrue()) return

    clearTimeout(self.timeout)

    self.hoverState = 'out'

    if (!self.options.delay || !self.options.delay.hide) return self.hide()

    self.timeout = setTimeout(function () {
      if (self.hoverState == 'out') self.hide()
    }, self.options.delay.hide)
  }

  Tooltip.prototype.show = function () {
    var e = $.Event('show.bs.' + this.type)

    if (this.hasContent() && this.enabled) {
      this.$element.trigger(e)

      var inDom = $.contains(this.$element[0].ownerDocument.documentElement, this.$element[0])
      if (e.isDefaultPrevented() || !inDom) return
      var that = this

      var $tip = this.tip()

      var tipId = this.getUID(this.type)

      this.setContent()
      $tip.attr('id', tipId)
      this.$element.attr('aria-describedby', tipId)

      if (this.options.animation) $tip.addClass('fade')

      var placement = typeof this.options.placement == 'function' ?
        this.options.placement.call(this, $tip[0], this.$element[0]) :
        this.options.placement

      var autoToken = /\s?auto?\s?/i
      var autoPlace = autoToken.test(placement)
      if (autoPlace) placement = placement.replace(autoToken, '') || 'top'

      $tip
        .detach()
        .css({ top: 0, left: 0, display: 'block' })
        .addClass(placement)
        .data('bs.' + this.type, this)

      this.options.container ? $tip.appendTo(this.options.container) : $tip.insertAfter(this.$element)
      this.$element.trigger('inserted.bs.' + this.type)

      var pos = this.getPosition()
      var actualWidth = $tip[0].offsetWidth
      var actualHeight = $tip[0].offsetHeight

      if (autoPlace) {
        var orgPlacement = placement
        var viewportDim = this.getPosition(this.$viewport)

        placement = placement == 'bottom' && pos.bottom + actualHeight > viewportDim.bottom ? 'top' :
          placement == 'top' && pos.top - actualHeight < viewportDim.top ? 'bottom' :
            placement == 'right' && pos.right + actualWidth > viewportDim.width ? 'left' :
              placement == 'left' && pos.left - actualWidth < viewportDim.left ? 'right' :
                placement

        $tip
          .removeClass(orgPlacement)
          .addClass(placement)
      }

      var calculatedOffset = this.getCalculatedOffset(placement, pos, actualWidth, actualHeight)

      this.applyPlacement(calculatedOffset, placement)

      var complete = function () {
        var prevHoverState = that.hoverState
        that.$element.trigger('shown.bs.' + that.type)
        that.hoverState = null

        if (prevHoverState == 'out') that.leave(that)
      }

      $.support.transition && this.$tip.hasClass('fade') ?
        $tip
          .one('bsTransitionEnd', complete)
          .emulateTransitionEnd(Tooltip.TRANSITION_DURATION) :
        complete()
    }
  }

  Tooltip.prototype.applyPlacement = function (offset, placement) {
    var $tip = this.tip()
    var width = $tip[0].offsetWidth
    var height = $tip[0].offsetHeight

    // manually read margins because getBoundingClientRect includes difference
    var marginTop = parseInt($tip.css('margin-top'), 10)
    var marginLeft = parseInt($tip.css('margin-left'), 10)

    // we must check for NaN for ie 8/9
    if (isNaN(marginTop)) marginTop = 0
    if (isNaN(marginLeft)) marginLeft = 0

    offset.top += marginTop
    offset.left += marginLeft

    // $.fn.offset doesn't round pixel values
    // so we use setOffset directly with our own function B-0
    $.offset.setOffset($tip[0], $.extend({
      using: function (props) {
        $tip.css({
          top: Math.round(props.top),
          left: Math.round(props.left)
        })
      }
    }, offset), 0)

    $tip.addClass('in')

    // check to see if placing tip in new offset caused the tip to resize itself
    var actualWidth = $tip[0].offsetWidth
    var actualHeight = $tip[0].offsetHeight

    if (placement == 'top' && actualHeight != height) {
      offset.top = offset.top + height - actualHeight
    }

    var delta = this.getViewportAdjustedDelta(placement, offset, actualWidth, actualHeight)

    if (delta.left) offset.left += delta.left
    else offset.top += delta.top

    var isVertical = /top|bottom/.test(placement)
    var arrowDelta = isVertical ? delta.left * 2 - width + actualWidth : delta.top * 2 - height + actualHeight
    var arrowOffsetPosition = isVertical ? 'offsetWidth' : 'offsetHeight'

    $tip.offset(offset)
    this.replaceArrow(arrowDelta, $tip[0][arrowOffsetPosition], isVertical)
  }

  Tooltip.prototype.replaceArrow = function (delta, dimension, isVertical) {
    this.arrow()
      .css(isVertical ? 'left' : 'top', 50 * (1 - delta / dimension) + '%')
      .css(isVertical ? 'top' : 'left', '')
  }

  Tooltip.prototype.setContent = function () {
    var $tip = this.tip()
    var title = this.getTitle()

    $tip.find('.tooltip-inner')[this.options.html ? 'html' : 'text'](title)
    $tip.removeClass('fade in top bottom left right')
  }

  Tooltip.prototype.hide = function (callback) {
    var that = this
    var $tip = $(this.$tip)
    var e = $.Event('hide.bs.' + this.type)

    function complete() {
      if (that.hoverState != 'in') $tip.detach()
      if (that.$element) { // TODO: Check whether guarding this code with this `if` is really necessary.
        that.$element
          .removeAttr('aria-describedby')
          .trigger('hidden.bs.' + that.type)
      }
      callback && callback()
    }

    this.$element.trigger(e)

    if (e.isDefaultPrevented()) return

    $tip.removeClass('in')

    $.support.transition && $tip.hasClass('fade') ?
      $tip
        .one('bsTransitionEnd', complete)
        .emulateTransitionEnd(Tooltip.TRANSITION_DURATION) :
      complete()

    this.hoverState = null

    return this
  }

  Tooltip.prototype.fixTitle = function () {
    var $e = this.$element
    if ($e.attr('title') || typeof $e.attr('data-original-title') != 'string') {
      $e.attr('data-original-title', $e.attr('title') || '').attr('title', '')
    }
  }

  Tooltip.prototype.hasContent = function () {
    return this.getTitle()
  }

  Tooltip.prototype.getPosition = function ($element) {
    $element = $element || this.$element

    var el = $element[0]
    var isBody = el.tagName == 'BODY'

    var elRect = el.getBoundingClientRect()
    if (elRect.width == null) {
      // width and height are missing in IE8, so compute them manually; see https://github.com/twbs/bootstrap/issues/14093
      elRect = $.extend({}, elRect, { width: elRect.right - elRect.left, height: elRect.bottom - elRect.top })
    }
    var isSvg = window.SVGElement && el instanceof window.SVGElement
    // Avoid using $.offset() on SVGs since it gives incorrect results in jQuery 3.
    // See https://github.com/twbs/bootstrap/issues/20280
    var elOffset = isBody ? { top: 0, left: 0 } : (isSvg ? null : $element.offset())
    var scroll = { scroll: isBody ? document.documentElement.scrollTop || document.body.scrollTop : $element.scrollTop() }
    var outerDims = isBody ? { width: $(window).width(), height: $(window).height() } : null

    return $.extend({}, elRect, scroll, outerDims, elOffset)
  }

  Tooltip.prototype.getCalculatedOffset = function (placement, pos, actualWidth, actualHeight) {
    return placement == 'bottom' ? { top: pos.top + pos.height, left: pos.left + pos.width / 2 - actualWidth / 2 } :
      placement == 'top' ? { top: pos.top - actualHeight, left: pos.left + pos.width / 2 - actualWidth / 2 } :
        placement == 'left' ? { top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left - actualWidth } :
        /* placement == 'right' */ { top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left + pos.width }

  }

  Tooltip.prototype.getViewportAdjustedDelta = function (placement, pos, actualWidth, actualHeight) {
    var delta = { top: 0, left: 0 }
    if (!this.$viewport) return delta

    var viewportPadding = this.options.viewport && this.options.viewport.padding || 0
    var viewportDimensions = this.getPosition(this.$viewport)

    if (/right|left/.test(placement)) {
      var topEdgeOffset = pos.top - viewportPadding - viewportDimensions.scroll
      var bottomEdgeOffset = pos.top + viewportPadding - viewportDimensions.scroll + actualHeight
      if (topEdgeOffset < viewportDimensions.top) { // top overflow
        delta.top = viewportDimensions.top - topEdgeOffset
      } else if (bottomEdgeOffset > viewportDimensions.top + viewportDimensions.height) { // bottom overflow
        delta.top = viewportDimensions.top + viewportDimensions.height - bottomEdgeOffset
      }
    } else {
      var leftEdgeOffset = pos.left - viewportPadding
      var rightEdgeOffset = pos.left + viewportPadding + actualWidth
      if (leftEdgeOffset < viewportDimensions.left) { // left overflow
        delta.left = viewportDimensions.left - leftEdgeOffset
      } else if (rightEdgeOffset > viewportDimensions.right) { // right overflow
        delta.left = viewportDimensions.left + viewportDimensions.width - rightEdgeOffset
      }
    }

    return delta
  }

  Tooltip.prototype.getTitle = function () {
    var title
    var $e = this.$element
    var o = this.options

    title = $e.attr('data-original-title')
      || (typeof o.title == 'function' ? o.title.call($e[0]) : o.title)

    return title
  }

  Tooltip.prototype.getUID = function (prefix) {
    do prefix += ~~(Math.random() * 1000000)
    while (document.getElementById(prefix))
    return prefix
  }

  Tooltip.prototype.tip = function () {
    if (!this.$tip) {
      this.$tip = $(this.options.template)
      if (this.$tip.length != 1) {
        throw new Error(this.type + ' `template` option must consist of exactly 1 top-level element!')
      }
    }
    return this.$tip
  }

  Tooltip.prototype.arrow = function () {
    return (this.$arrow = this.$arrow || this.tip().find('.tooltip-arrow'))
  }

  Tooltip.prototype.enable = function () {
    this.enabled = true
  }

  Tooltip.prototype.disable = function () {
    this.enabled = false
  }

  Tooltip.prototype.toggleEnabled = function () {
    this.enabled = !this.enabled
  }

  Tooltip.prototype.toggle = function (e) {
    var self = this
    if (e) {
      self = $(e.currentTarget).data('bs.' + this.type)
      if (!self) {
        self = new this.constructor(e.currentTarget, this.getDelegateOptions())
        $(e.currentTarget).data('bs.' + this.type, self)
      }
    }

    if (e) {
      self.inState.click = !self.inState.click
      if (self.isInStateTrue()) self.enter(self)
      else self.leave(self)
    } else {
      self.tip().hasClass('in') ? self.leave(self) : self.enter(self)
    }
  }

  Tooltip.prototype.destroy = function () {
    var that = this
    clearTimeout(this.timeout)
    this.hide(function () {
      that.$element.off('.' + that.type).removeData('bs.' + that.type)
      if (that.$tip) {
        that.$tip.detach()
      }
      that.$tip = null
      that.$arrow = null
      that.$viewport = null
      that.$element = null
    })
  }


  // TOOLTIP PLUGIN DEFINITION
  // =========================

  function Plugin(option) {
    return this.each(function () {
      var $this = $(this)
      var data = $this.data('bs.tooltip')
      var options = typeof option == 'object' && option

      if (!data && /destroy|hide/.test(option)) return
      if (!data) $this.data('bs.tooltip', (data = new Tooltip(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.tooltip

  $.fn.tooltip = Plugin
  $.fn.tooltip.Constructor = Tooltip


  // TOOLTIP NO CONFLICT
  // ===================

  $.fn.tooltip.noConflict = function () {
    $.fn.tooltip = old
    return this
  }

}(jQuery);
;
/* ========================================================================
 * Bootstrap: popover.js v3.3.7
 * http://getbootstrap.com/javascript/#popovers
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // POPOVER PUBLIC CLASS DEFINITION
  // ===============================

  var Popover = function (element, options) {
    this.init('popover', element, options)
  }

  if (!$.fn.tooltip) throw new Error('Popover requires tooltip.js')

  Popover.VERSION = '3.3.7'

  Popover.DEFAULTS = $.extend({}, $.fn.tooltip.Constructor.DEFAULTS, {
    placement: 'right',
    trigger: 'click',
    content: '',
    template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
  })


  // NOTE: POPOVER EXTENDS tooltip.js
  // ================================

  Popover.prototype = $.extend({}, $.fn.tooltip.Constructor.prototype)

  Popover.prototype.constructor = Popover

  Popover.prototype.getDefaults = function () {
    return Popover.DEFAULTS
  }

  Popover.prototype.setContent = function () {
    var $tip = this.tip()
    var title = this.getTitle()
    var content = this.getContent()

    $tip.find('.popover-title')[this.options.html ? 'html' : 'text'](title)
    $tip.find('.popover-content').children().detach().end()[ // we use append for html objects to maintain js events
      this.options.html ? (typeof content == 'string' ? 'html' : 'append') : 'text'
    ](content)

    $tip.removeClass('fade top bottom left right in')

    // IE8 doesn't accept hiding via the `:empty` pseudo selector, we have to do
    // this manually by checking the contents.
    if (!$tip.find('.popover-title').html()) $tip.find('.popover-title').hide()
  }

  Popover.prototype.hasContent = function () {
    return this.getTitle() || this.getContent()
  }

  Popover.prototype.getContent = function () {
    var $e = this.$element
    var o = this.options

    return $e.attr('data-content')
      || (typeof o.content == 'function' ?
        o.content.call($e[0]) :
        o.content)
  }

  Popover.prototype.arrow = function () {
    return (this.$arrow = this.$arrow || this.tip().find('.arrow'))
  }


  // POPOVER PLUGIN DEFINITION
  // =========================

  function Plugin(option) {
    return this.each(function () {
      var $this = $(this)
      var data = $this.data('bs.popover')
      var options = typeof option == 'object' && option

      if (!data && /destroy|hide/.test(option)) return
      if (!data) $this.data('bs.popover', (data = new Popover(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.popover

  $.fn.popover = Plugin
  $.fn.popover.Constructor = Popover


  // POPOVER NO CONFLICT
  // ===================

  $.fn.popover.noConflict = function () {
    $.fn.popover = old
    return this
  }

}(jQuery);
;
/* ========================================================================
 * Bootstrap: scrollspy.js v3.3.7
 * http://getbootstrap.com/javascript/#scrollspy
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // SCROLLSPY CLASS DEFINITION
  // ==========================

  function ScrollSpy(element, options) {
    this.$body = $(document.body)
    this.$scrollElement = $(element).is(document.body) ? $(window) : $(element)
    this.options = $.extend({}, ScrollSpy.DEFAULTS, options)
    this.selector = (this.options.target || '') + ' .nav li > a'
    this.offsets = []
    this.targets = []
    this.activeTarget = null
    this.scrollHeight = 0

    this.$scrollElement.on('scroll.bs.scrollspy', $.proxy(this.process, this))
    this.refresh()
    this.process()
  }

  ScrollSpy.VERSION = '3.3.7'

  ScrollSpy.DEFAULTS = {
    offset: 10
  }

  ScrollSpy.prototype.getScrollHeight = function () {
    return this.$scrollElement[0].scrollHeight || Math.max(this.$body[0].scrollHeight, document.documentElement.scrollHeight)
  }

  ScrollSpy.prototype.refresh = function () {
    var that = this
    var offsetMethod = 'offset'
    var offsetBase = 0

    this.offsets = []
    this.targets = []
    this.scrollHeight = this.getScrollHeight()

    if (!$.isWindow(this.$scrollElement[0])) {
      offsetMethod = 'position'
      offsetBase = this.$scrollElement.scrollTop()
    }

    this.$body
      .find(this.selector)
      .map(function () {
        var $el = $(this)
        var href = $el.data('target') || $el.attr('href')
        var $href = /^#./.test(href) && $(href)

        return ($href
          && $href.length
          && $href.is(':visible')
          && [[$href[offsetMethod]().top + offsetBase, href]]) || null
      })
      .sort(function (a, b) { return a[0] - b[0] })
      .each(function () {
        that.offsets.push(this[0])
        that.targets.push(this[1])
      })
  }

  ScrollSpy.prototype.process = function () {
    var scrollTop = this.$scrollElement.scrollTop() + this.options.offset
    var scrollHeight = this.getScrollHeight()
    var maxScroll = this.options.offset + scrollHeight - this.$scrollElement.height()
    var offsets = this.offsets
    var targets = this.targets
    var activeTarget = this.activeTarget
    var i

    if (this.scrollHeight != scrollHeight) {
      this.refresh()
    }

    if (scrollTop >= maxScroll) {
      return activeTarget != (i = targets[targets.length - 1]) && this.activate(i)
    }

    if (activeTarget && scrollTop < offsets[0]) {
      this.activeTarget = null
      return this.clear()
    }

    for (i = offsets.length; i--;) {
      activeTarget != targets[i]
        && scrollTop >= offsets[i]
        && (offsets[i + 1] === undefined || scrollTop < offsets[i + 1])
        && this.activate(targets[i])
    }
  }

  ScrollSpy.prototype.activate = function (target) {
    this.activeTarget = target

    this.clear()

    var selector = this.selector +
      '[data-target="' + target + '"],' +
      this.selector + '[href="' + target + '"]'

    var active = $(selector)
      .parents('li')
      .addClass('active')

    if (active.parent('.dropdown-menu').length) {
      active = active
        .closest('li.dropdown')
        .addClass('active')
    }

    active.trigger('activate.bs.scrollspy')
  }

  ScrollSpy.prototype.clear = function () {
    $(this.selector)
      .parentsUntil(this.options.target, '.active')
      .removeClass('active')
  }


  // SCROLLSPY PLUGIN DEFINITION
  // ===========================

  function Plugin(option) {
    return this.each(function () {
      var $this = $(this)
      var data = $this.data('bs.scrollspy')
      var options = typeof option == 'object' && option

      if (!data) $this.data('bs.scrollspy', (data = new ScrollSpy(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.scrollspy

  $.fn.scrollspy = Plugin
  $.fn.scrollspy.Constructor = ScrollSpy


  // SCROLLSPY NO CONFLICT
  // =====================

  $.fn.scrollspy.noConflict = function () {
    $.fn.scrollspy = old
    return this
  }


  // SCROLLSPY DATA-API
  // ==================

  $(window).on('load.bs.scrollspy.data-api', function () {
    $('[data-spy="scroll"]').each(function () {
      var $spy = $(this)
      Plugin.call($spy, $spy.data())
    })
  })

}(jQuery);
;
/* ========================================================================
 * Bootstrap: tab.js v3.3.7
 * http://getbootstrap.com/javascript/#tabs
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // TAB CLASS DEFINITION
  // ====================

  var Tab = function (element) {
    // jscs:disable requireDollarBeforejQueryAssignment
    this.element = $(element)
    // jscs:enable requireDollarBeforejQueryAssignment
  }

  Tab.VERSION = '3.3.7'

  Tab.TRANSITION_DURATION = 150

  Tab.prototype.show = function () {
    var $this = this.element
    var $ul = $this.closest('ul:not(.dropdown-menu)')
    var selector = $this.data('target')

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') // strip for ie7
    }

    if ($this.parent('li').hasClass('active')) return

    var $previous = $ul.find('.active:last a')
    var hideEvent = $.Event('hide.bs.tab', {
      relatedTarget: $this[0]
    })
    var showEvent = $.Event('show.bs.tab', {
      relatedTarget: $previous[0]
    })

    $previous.trigger(hideEvent)
    $this.trigger(showEvent)

    if (showEvent.isDefaultPrevented() || hideEvent.isDefaultPrevented()) return

    var $target = $(selector)

    this.activate($this.closest('li'), $ul)
    this.activate($target, $target.parent(), function () {
      $previous.trigger({
        type: 'hidden.bs.tab',
        relatedTarget: $this[0]
      })
      $this.trigger({
        type: 'shown.bs.tab',
        relatedTarget: $previous[0]
      })
    })
  }

  Tab.prototype.activate = function (element, container, callback) {
    var $active = container.find('> .active')
    var transition = callback
      && $.support.transition
      && ($active.length && $active.hasClass('fade') || !!container.find('> .fade').length)

    function next() {
      $active
        .removeClass('active')
        .find('> .dropdown-menu > .active')
        .removeClass('active')
        .end()
        .find('[data-toggle="tab"]')
        .attr('aria-expanded', false)

      element
        .addClass('active')
        .find('[data-toggle="tab"]')
        .attr('aria-expanded', true)

      if (transition) {
        element[0].offsetWidth // reflow for transition
        element.addClass('in')
      } else {
        element.removeClass('fade')
      }

      if (element.parent('.dropdown-menu').length) {
        element
          .closest('li.dropdown')
          .addClass('active')
          .end()
          .find('[data-toggle="tab"]')
          .attr('aria-expanded', true)
      }

      callback && callback()
    }

    $active.length && transition ?
      $active
        .one('bsTransitionEnd', next)
        .emulateTransitionEnd(Tab.TRANSITION_DURATION) :
      next()

    $active.removeClass('in')
  }


  // TAB PLUGIN DEFINITION
  // =====================

  function Plugin(option) {
    return this.each(function () {
      var $this = $(this)
      var data = $this.data('bs.tab')

      if (!data) $this.data('bs.tab', (data = new Tab(this)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.tab

  $.fn.tab = Plugin
  $.fn.tab.Constructor = Tab


  // TAB NO CONFLICT
  // ===============

  $.fn.tab.noConflict = function () {
    $.fn.tab = old
    return this
  }


  // TAB DATA-API
  // ============

  var clickHandler = function (e) {
    e.preventDefault()
    Plugin.call($(this), 'show')
  }

  $(document)
    .on('click.bs.tab.data-api', '[data-toggle="tab"]', clickHandler)
    .on('click.bs.tab.data-api', '[data-toggle="pill"]', clickHandler)

}(jQuery);
;
/* ========================================================================
 * Bootstrap: transition.js v3.3.7
 * http://getbootstrap.com/javascript/#transitions
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // CSS TRANSITION SUPPORT (Shoutout: http://www.modernizr.com/)
  // ============================================================

  function transitionEnd() {
    var el = document.createElement('bootstrap')

    var transEndEventNames = {
      WebkitTransition: 'webkitTransitionEnd',
      MozTransition: 'transitionend',
      OTransition: 'oTransitionEnd otransitionend',
      transition: 'transitionend'
    }

    for (var name in transEndEventNames) {
      if (el.style[name] !== undefined) {
        return { end: transEndEventNames[name] }
      }
    }

    return false // explicit for ie8 (  ._.)
  }

  // http://blog.alexmaccaw.com/css-transitions
  $.fn.emulateTransitionEnd = function (duration) {
    var called = false
    var $el = this
    $(this).one('bsTransitionEnd', function () { called = true })
    var callback = function () { if (!called) $($el).trigger($.support.transition.end) }
    setTimeout(callback, duration)
    return this
  }

  $(function () {
    $.support.transition = transitionEnd()

    if (!$.support.transition) return

    $.event.special.bsTransitionEnd = {
      bindType: $.support.transition.end,
      delegateType: $.support.transition.end,
      handle: function (e) {
        if ($(e.target).is(this)) return e.handleObj.handler.apply(this, arguments)
      }
    }
  })

}(jQuery);
;
(function ($, Drupal) {

  Drupal.behaviors.closePopup = {
    attach: function (context, settings) {

      $('.agriservis-popup-price-trigger').click(function () {
        $('#spb-agriservis-popup-price .agriservis-popup-price-modal').once().prepend('<div class="spb_click_overlay"></div>');

        $('#spb-agriservis-popup-price > .agriservis-popup-price-modal .spb_click_overlay').once().click(function (e) {
          e.preventDefault();
          $('#block-webform-price .spb_close').triggerHandler('click');
        });
      });
    }
  }

  $(document).ready(function () {

    if ($('.field--name-dynamic-block-fieldtaxonomy-term-products .view-empty').length > 0) {
      $('.field--name-dynamic-block-fieldtaxonomy-term-products').remove();
    }

    $('#block-products-menu').click(function () {
      if ($(this).siblings('ul.menu').hasClass('open')) {
        $(this).siblings('ul.menu').removeClass('open');
        $(this).siblings('ul.menu').fadeOut();
      } else {
        $(this).siblings('ul.menu').addClass('open');
        $(this).siblings('ul.menu').fadeIn();

      }
    });

    $('#block-agriservis-search .search__title').click(function () {
      if ($(this).siblings('#search-block-form').hasClass('open')) {
        $(this).siblings('#search-block-form').removeClass('open');
        $(this).siblings('#search-block-form').fadeOut();
      } else {
        $(this).siblings('#search-block-form').addClass('open');
        $(this).siblings('#search-block-form').fadeIn();
      }
    });

    if ($('.page-node-type-branch .map-route').length > 0) {

      $('.page-node-type-branch .map-route').colorbox({
        inline: true,
        href: '#map',
        onOpen: function () {

          if (!$('#map').hasClass('init')) {
            init();
            $('#map').addClass('init');
          }
        }
      });
    }

    if ($('.page-node-type-branch .region-content .field--name-field-specialists .field--name-dynamic-token-fieldparagraph-btn-contact .btn-main').length > 0) {

      $('.page-node-type-branch .region-content .field--name-field-specialists .field--name-dynamic-token-fieldparagraph-btn-contact .btn-main').colorbox({
        inline: true,
        href: '#block-block-send-me-form',
        maxWidth: "100%",
        maxHeight: "100%",
        onLoad: function () {

          var block = $.colorbox.element().parents('.paragraph').clone();

          $('#block-block-send-me-form').prepend(block);

          $('#block-block-send-me-form .form-item-email-to input.form-email').val(block.find('.field--name-field-specialist-email').text());

          $('#block-block-send-me-form .form-item-manager input.form-text').val(block.find('.field--name-field-specialist-name').text());
        },
        onClosed: function () {
          $('#block-block-send-me-form .paragraph').remove();
        }
      });
    }

    if ($('.fotoskan-cbox').length > 0) {

      $('.fotoskan-cbox').colorbox({
        close: "Close",
        current: "{current} of {total}",
        fixed: true,
        maxHeight: "98%",
        maxWidth: "98%",
        mobiledetect: true,
        mobiledevicewidth: "480px",
        next: "Next »",
        opacity: "0.85",
        previous: "« Prev"
      });
    }

    if ($('.page-counters .counter__number').text() == '') {
      $('.page-counters .counter__number').text('0');
    }

    $.each($('.article-teaser__footer .counter__number'), function (index, val) {
      if ($(val).text() == '') {
        $(val).text('0');
      }
    });

  });

})(jQuery, Drupal);
;
/**
 * @file
 * Drupal Bootstrap object.
 */

/**
 * All Drupal Bootstrap JavaScript APIs are contained in this namespace.
 *
 * @param {underscore} _
 * @param {jQuery} $
 * @param {Drupal} Drupal
 * @param {drupalSettings} drupalSettings
 */
(function (_, $, Drupal, drupalSettings) {
  'use strict';

  /**
   * @typedef Drupal.bootstrap
   */
  var Bootstrap = {
    processedOnce: {},
    settings: drupalSettings.bootstrap || {}
  };

  /**
   * Wraps Drupal.checkPlain() to ensure value passed isn't empty.
   *
   * Encodes special characters in a plain-text string for display as HTML.
   *
   * @param {string} str
   *   The string to be encoded.
   *
   * @return {string}
   *   The encoded string.
   *
   * @ingroup sanitization
   */
  Bootstrap.checkPlain = function (str) {
    return str && Drupal.checkPlain(str) || '';
  };

  /**
   * Creates a jQuery plugin.
   *
   * @param {String} id
   *   A jQuery plugin identifier located in $.fn.
   * @param {Function} plugin
   *   A constructor function used to initialize the for the jQuery plugin.
   * @param {Boolean} [noConflict]
   *   Flag indicating whether or not to create a ".noConflict()" helper method
   *   for the plugin.
   */
  Bootstrap.createPlugin = function (id, plugin, noConflict) {
    // Immediately return if plugin doesn't exist.
    if ($.fn[id] !== void 0) {
      return this.fatal('Specified jQuery plugin identifier already exists: @id. Use Drupal.bootstrap.replacePlugin() instead.', { '@id': id });
    }

    // Immediately return if plugin isn't a function.
    if (typeof plugin !== 'function') {
      return this.fatal('You must provide a constructor function to create a jQuery plugin "@id": @plugin', { '@id': id, '@plugin': plugin });
    }

    // Add a ".noConflict()" helper method.
    this.pluginNoConflict(id, plugin, noConflict);

    $.fn[id] = plugin;
  };

  /**
   * Diff object properties.
   *
   * @param {...Object} objects
   *   Two or more objects. The first object will be used to return properties
   *   values.
   *
   * @return {Object}
   *   Returns the properties of the first passed object that are not present
   *   in all other passed objects.
   */
  Bootstrap.diffObjects = function (objects) {
    var args = Array.prototype.slice.call(arguments);
    return _.pick(args[0], _.difference.apply(_, _.map(args, function (obj) {
      return Object.keys(obj);
    })));
  };

  /**
   * Map of supported events by regular expression.
   *
   * @type {Object<Event|MouseEvent|KeyboardEvent|TouchEvent,RegExp>}
   */
  Bootstrap.eventMap = {
    Event: /^(?:load|unload|abort|error|select|change|submit|reset|focus|blur|resize|scroll)$/,
    MouseEvent: /^(?:click|dblclick|mouse(?:down|enter|leave|up|over|move|out))$/,
    KeyboardEvent: /^(?:key(?:down|press|up))$/,
    TouchEvent: /^(?:touch(?:start|end|move|cancel))$/
  };

  /**
   * Extends a jQuery Plugin.
   *
   * @param {String} id
   *   A jQuery plugin identifier located in $.fn.
   * @param {Function} callback
   *   A constructor function used to initialize the for the jQuery plugin.
   *
   * @return {Function|Boolean}
   *   The jQuery plugin constructor or FALSE if the plugin does not exist.
   */
  Bootstrap.extendPlugin = function (id, callback) {
    // Immediately return if plugin doesn't exist.
    if (typeof $.fn[id] !== 'function') {
      return this.fatal('Specified jQuery plugin identifier does not exist: @id', { '@id': id });
    }

    // Immediately return if callback isn't a function.
    if (typeof callback !== 'function') {
      return this.fatal('You must provide a callback function to extend the jQuery plugin "@id": @callback', { '@id': id, '@callback': callback });
    }

    // Determine existing plugin constructor.
    var constructor = $.fn[id] && $.fn[id].Constructor || $.fn[id];
    var plugin = callback.apply(constructor, [this.settings]);
    if (!$.isPlainObject(plugin)) {
      return this.fatal('Returned value from callback is not a plain object that can be used to extend the jQuery plugin "@id": @obj', { '@obj': plugin });
    }

    this.wrapPluginConstructor(constructor, plugin, true);

    return $.fn[id];
  };

  Bootstrap.superWrapper = function (parent, fn) {
    return function () {
      var previousSuper = this.super;
      this.super = parent;
      var ret = fn.apply(this, arguments);
      if (previousSuper) {
        this.super = previousSuper;
      }
      else {
        delete this.super;
      }
      return ret;
    };
  };

  /**
   * Provide a helper method for displaying when something is went wrong.
   *
   * @param {String} message
   *   The message to display.
   * @param {Object} [args]
   *   An arguments to use in message.
   *
   * @return {Boolean}
   *   Always returns FALSE.
   */
  Bootstrap.fatal = function (message, args) {
    if (this.settings.dev && console.warn) {
      for (var name in args) {
        if (args.hasOwnProperty(name) && typeof args[name] === 'object') {
          args[name] = JSON.stringify(args[name]);
        }
      }
      Drupal.throwError(new Error(Drupal.formatString(message, args)));
    }
    return false;
  };

  /**
   * Intersects object properties.
   *
   * @param {...Object} objects
   *   Two or more objects. The first object will be used to return properties
   *   values.
   *
   * @return {Object}
   *   Returns the properties of first passed object that intersects with all
   *   other passed objects.
   */
  Bootstrap.intersectObjects = function (objects) {
    var args = Array.prototype.slice.call(arguments);
    return _.pick(args[0], _.intersection.apply(_, _.map(args, function (obj) {
      return Object.keys(obj);
    })));
  };

  /**
   * Normalizes an object's values.
   *
   * @param {Object} obj
   *   The object to normalize.
   *
   * @return {Object}
   *   The normalized object.
   */
  Bootstrap.normalizeObject = function (obj) {
    if (!$.isPlainObject(obj)) {
      return obj;
    }

    for (var k in obj) {
      if (typeof obj[k] === 'string') {
        if (obj[k] === 'true') {
          obj[k] = true;
        }
        else if (obj[k] === 'false') {
          obj[k] = false;
        }
        else if (obj[k].match(/^[\d-.]$/)) {
          obj[k] = parseFloat(obj[k]);
        }
      }
      else if ($.isPlainObject(obj[k])) {
        obj[k] = Bootstrap.normalizeObject(obj[k]);
      }
    }

    return obj;
  };

  /**
   * An object based once plugin (similar to jquery.once, but without the DOM).
   *
   * @param {String} id
   *   A unique identifier.
   * @param {Function} callback
   *   The callback to invoke if the identifier has not yet been seen.
   *
   * @return {Bootstrap}
   */
  Bootstrap.once = function (id, callback) {
    // Immediately return if identifier has already been processed.
    if (this.processedOnce[id]) {
      return this;
    }
    callback.call(this, this.settings);
    this.processedOnce[id] = true;
    return this;
  };

  /**
   * Provide jQuery UI like ability to get/set options for Bootstrap plugins.
   *
   * @param {string|object} key
   *   A string value of the option to set, can be dot like to a nested key.
   *   An object of key/value pairs.
   * @param {*} [value]
   *   (optional) A value to set for key.
   *
   * @returns {*}
   *   - Returns nothing if key is an object or both key and value parameters
   *   were provided to set an option.
   *   - Returns the a value for a specific setting if key was provided.
   *   - Returns an object of key/value pairs of all the options if no key or
   *   value parameter was provided.
   *
   * @see https://github.com/jquery/jquery-ui/blob/master/ui/widget.js
   */
  Bootstrap.option = function (key, value) {
    var options = $.isPlainObject(key) ? $.extend({}, key) : {};

    // Get all options (clone so it doesn't reference the internal object).
    if (arguments.length === 0) {
      return $.extend({}, this.options);
    }

    // Get/set single option.
    if (typeof key === "string") {
      // Handle nested keys in dot notation.
      // e.g., "foo.bar" => { foo: { bar: true } }
      var parts = key.split('.');
      key = parts.shift();
      var obj = options;
      if (parts.length) {
        for (var i = 0; i < parts.length - 1; i++) {
          obj[parts[i]] = obj[parts[i]] || {};
          obj = obj[parts[i]];
        }
        key = parts.pop();
      }

      // Get.
      if (arguments.length === 1) {
        return obj[key] === void 0 ? null : obj[key];
      }

      // Set.
      obj[key] = value;
    }

    // Set multiple options.
    $.extend(true, this.options, options);
  };

  /**
   * Adds a ".noConflict()" helper method if needed.
   *
   * @param {String} id
   *   A jQuery plugin identifier located in $.fn.
   * @param {Function} plugin
   * @param {Function} plugin
   *   A constructor function used to initialize the for the jQuery plugin.
   * @param {Boolean} [noConflict]
   *   Flag indicating whether or not to create a ".noConflict()" helper method
   *   for the plugin.
   */
  Bootstrap.pluginNoConflict = function (id, plugin, noConflict) {
    if (plugin.noConflict === void 0 && (noConflict === void 0 || noConflict)) {
      var old = $.fn[id];
      plugin.noConflict = function () {
        $.fn[id] = old;
        return this;
      };
    }
  };

  /**
   * Creates a handler that relays to another event name.
   *
   * @param {HTMLElement|jQuery} target
   *   A target element.
   * @param {String} name
   *   The name of the event to trigger.
   * @param {Boolean} [stopPropagation=true]
   *   Flag indicating whether to stop the propagation of the event, defaults
   *   to true.
   *
   * @return {Function}
   *   An even handler callback function.
   */
  Bootstrap.relayEvent = function (target, name, stopPropagation) {
    return function (e) {
      if (stopPropagation === void 0 || stopPropagation) {
        e.stopPropagation();
      }
      var $target = $(target);
      var parts = name.split('.').filter(Boolean);
      var type = parts.shift();
      e.target = $target[0];
      e.currentTarget = $target[0];
      e.namespace = parts.join('.');
      e.type = type;
      $target.trigger(e);
    };
  };

  /**
   * Replaces a Bootstrap jQuery plugin definition.
   *
   * @param {String} id
   *   A jQuery plugin identifier located in $.fn.
   * @param {Function} callback
   *   A callback function that is immediately invoked and must return a
   *   function that will be used as the plugin constructor.
   * @param {Boolean} [noConflict]
   *   Flag indicating whether or not to create a ".noConflict()" helper method
   *   for the plugin.
   */
  Bootstrap.replacePlugin = function (id, callback, noConflict) {
    // Immediately return if plugin doesn't exist.
    if (typeof $.fn[id] !== 'function') {
      return this.fatal('Specified jQuery plugin identifier does not exist: @id', { '@id': id });
    }

    // Immediately return if callback isn't a function.
    if (typeof callback !== 'function') {
      return this.fatal('You must provide a valid callback function to replace a jQuery plugin: @callback', { '@callback': callback });
    }

    // Determine existing plugin constructor.
    var constructor = $.fn[id] && $.fn[id].Constructor || $.fn[id];
    var plugin = callback.apply(constructor, [this.settings]);

    // Immediately return if plugin isn't a function.
    if (typeof plugin !== 'function') {
      return this.fatal('Returned value from callback is not a usable function to replace a jQuery plugin "@id": @plugin', { '@id': id, '@plugin': plugin });
    }

    this.wrapPluginConstructor(constructor, plugin);

    // Add a ".noConflict()" helper method.
    this.pluginNoConflict(id, plugin, noConflict);

    $.fn[id] = plugin;
  };

  /**
   * Simulates a native event on an element in the browser.
   *
   * Note: This is a fairly complete modern implementation. If things aren't
   * working quite the way you intend (in older browsers), you may wish to use
   * the jQuery.simulate plugin. If it's available, this method will defer to
   * that plugin.
   *
   * @see https://github.com/jquery/jquery-simulate
   *
   * @param {HTMLElement|jQuery} element
   *   A DOM element to dispatch event on. Note: this may be a jQuery object,
   *   however be aware that this will trigger the same event for each element
   *   inside the jQuery collection; use with caution.
   * @param {String|String[]} type
   *   The type(s) of event to simulate.
   * @param {Object} [options]
   *   An object of options to pass to the event constructor. Typically, if
   *   an event is being proxied, you should just pass the original event
   *   object here. This allows, if the browser supports it, to be a truly
   *   simulated event.
   *
   * @return {Boolean}
   *   The return value is false if event is cancelable and at least one of the
   *   event handlers which handled this event called Event.preventDefault().
   *   Otherwise it returns true.
   */
  Bootstrap.simulate = function (element, type, options) {
    // Handle jQuery object wrappers so it triggers on each element.
    var ret = true;
    if (element instanceof $) {
      element.each(function () {
        if (!Bootstrap.simulate(this, type, options)) {
          ret = false;
        }
      });
      return ret;
    }

    if (!(element instanceof HTMLElement)) {
      this.fatal('Passed element must be an instance of HTMLElement, got "@type" instead.', {
        '@type': typeof element,
      });
    }

    // Defer to the jQuery.simulate plugin, if it's available.
    if (typeof $.simulate === 'function') {
      new $.simulate(element, type, options);
      return true;
    }

    var event;
    var ctor;
    var types = [].concat(type);
    for (var i = 0, l = types.length; i < l; i++) {
      type = types[i];
      for (var name in this.eventMap) {
        if (this.eventMap[name].test(type)) {
          ctor = name;
          break;
        }
      }
      if (!ctor) {
        throw new SyntaxError('Only rudimentary HTMLEvents, KeyboardEvents and MouseEvents are supported: ' + type);
      }
      var opts = { bubbles: true, cancelable: true };
      if (ctor === 'KeyboardEvent' || ctor === 'MouseEvent') {
        $.extend(opts, { ctrlKey: !1, altKey: !1, shiftKey: !1, metaKey: !1 });
      }
      if (ctor === 'MouseEvent') {
        $.extend(opts, { button: 0, pointerX: 0, pointerY: 0, view: window });
      }
      if (options) {
        $.extend(opts, options);
      }
      if (typeof window[ctor] === 'function') {
        event = new window[ctor](type, opts);
        if (!element.dispatchEvent(event)) {
          ret = false;
        }
      }
      else if (document.createEvent) {
        event = document.createEvent(ctor);
        event.initEvent(type, opts.bubbles, opts.cancelable);
        if (!element.dispatchEvent(event)) {
          ret = false;
        }
      }
      else if (typeof element.fireEvent === 'function') {
        event = $.extend(document.createEventObject(), opts);
        if (!element.fireEvent('on' + type, event)) {
          ret = false;
        }
      }
      else if (typeof element[type]) {
        element[type]();
      }
    }
    return ret;
  };

  /**
   * Strips HTML and returns just text.
   *
   * @param {String|Element|jQuery} html
   *   A string of HTML content, an Element DOM object or a jQuery object.
   *
   * @return {String}
   *   The text without HTML tags.
   *
   * @todo Replace with http://locutus.io/php/strings/strip_tags/
   */
  Bootstrap.stripHtml = function (html) {
    if (html instanceof $) {
      html = html.html();
    }
    else if (html instanceof Element) {
      html = html.innerHTML;
    }
    var tmp = document.createElement('DIV');
    tmp.innerHTML = html;
    return (tmp.textContent || tmp.innerText || '').replace(/^[\s\n\t]*|[\s\n\t]*$/, '');
  };

  /**
   * Provide a helper method for displaying when something is unsupported.
   *
   * @param {String} type
   *   The type of unsupported object, e.g. method or option.
   * @param {String} name
   *   The name of the unsupported object.
   * @param {*} [value]
   *   The value of the unsupported object.
   */
  Bootstrap.unsupported = function (type, name, value) {
    Bootstrap.warn('Unsupported by Drupal Bootstrap: (@type) @name -> @value', {
      '@type': type,
      '@name': name,
      '@value': typeof value === 'object' ? JSON.stringify(value) : value
    });
  };

  /**
   * Provide a helper method to display a warning.
   *
   * @param {String} message
   *   The message to display.
   * @param {Object} [args]
   *   Arguments to use as replacements in Drupal.formatString.
   */
  Bootstrap.warn = function (message, args) {
    if (this.settings.dev && console.warn) {
      console.warn(Drupal.formatString(message, args));
    }
  };

  /**
   * Wraps a plugin with common functionality.
   *
   * @param {Function} constructor
   *   A plugin constructor being wrapped.
   * @param {Object|Function} plugin
   *   The plugin being wrapped.
   * @param {Boolean} [extend = false]
   *   Whether to add super extensibility.
   */
  Bootstrap.wrapPluginConstructor = function (constructor, plugin, extend) {
    var proto = constructor.prototype;

    // Add a jQuery UI like option getter/setter method.
    var option = this.option;
    if (proto.option === void (0)) {
      proto.option = function () {
        return option.apply(this, arguments);
      };
    }

    if (extend) {
      // Handle prototype properties separately.
      if (plugin.prototype !== void 0) {
        for (var key in plugin.prototype) {
          if (!plugin.prototype.hasOwnProperty(key)) continue;
          var value = plugin.prototype[key];
          if (typeof value === 'function') {
            proto[key] = this.superWrapper(proto[key] || function () { }, value);
          }
          else {
            proto[key] = $.isPlainObject(value) ? $.extend(true, {}, proto[key], value) : value;
          }
        }
      }
      delete plugin.prototype;

      // Handle static properties.
      for (key in plugin) {
        if (!plugin.hasOwnProperty(key)) continue;
        value = plugin[key];
        if (typeof value === 'function') {
          constructor[key] = this.superWrapper(constructor[key] || function () { }, value);
        }
        else {
          constructor[key] = $.isPlainObject(value) ? $.extend(true, {}, constructor[key], value) : value;
        }
      }
    }
  };

  // Add Bootstrap to the global Drupal object.
  Drupal.bootstrap = Drupal.bootstrap || Bootstrap;

})(window._, window.jQuery, window.Drupal, window.drupalSettings);
;
(function ($, _) {

  /**
   * @class Attributes
   *
   * Modifies attributes.
   *
   * @param {Object|Attributes} attributes
   *   An object to initialize attributes with.
   */
  var Attributes = function (attributes) {
    this.data = {};
    this.data['class'] = [];
    this.merge(attributes);
  };

  /**
   * Renders the attributes object as a string to inject into an HTML element.
   *
   * @return {String}
   *   A rendered string suitable for inclusion in HTML markup.
   */
  Attributes.prototype.toString = function () {
    var output = '';
    var name, value;
    var checkPlain = function (str) {
      return str && str.toString().replace(/&/g, '&amp;').replace(/"/g, '&quot;').replace(/</g, '&lt;').replace(/>/g, '&gt;') || '';
    };
    var data = this.getData();
    for (name in data) {
      if (!data.hasOwnProperty(name)) continue;
      value = data[name];
      if (_.isFunction(value)) value = value();
      if (_.isObject(value)) value = _.values(value);
      if (_.isArray(value)) value = value.join(' ');
      output += ' ' + checkPlain(name) + '="' + checkPlain(value) + '"';
    }
    return output;
  };

  /**
   * Renders the Attributes object as a plain object.
   *
   * @return {Object}
   *   A plain object suitable for inclusion in DOM elements.
   */
  Attributes.prototype.toPlainObject = function () {
    var object = {};
    var name, value;
    var data = this.getData();
    for (name in data) {
      if (!data.hasOwnProperty(name)) continue;
      value = data[name];
      if (_.isFunction(value)) value = value();
      if (_.isObject(value)) value = _.values(value);
      if (_.isArray(value)) value = value.join(' ');
      object[name] = value;
    }
    return object;
  };

  /**
   * Add class(es) to the array.
   *
   * @param {string|Array} value
   *   An individual class or an array of classes to add.
   *
   * @return {Attributes}
   *
   * @chainable
   */
  Attributes.prototype.addClass = function (value) {
    var args = Array.prototype.slice.call(arguments);
    this.data['class'] = this.sanitizeClasses(this.data['class'].concat(args));
    return this;
  };

  /**
   * Returns whether the requested attribute exists.
   *
   * @param {string} name
   *   An attribute name to check.
   *
   * @return {boolean}
   *   TRUE or FALSE
   */
  Attributes.prototype.exists = function (name) {
    return this.data[name] !== void (0) && this.data[name] !== null;
  };

  /**
   * Retrieve a specific attribute from the array.
   *
   * @param {string} name
   *   The specific attribute to retrieve.
   * @param {*} defaultValue
   *   (optional) The default value to set if the attribute does not exist.
   *
   * @return {*}
   *   A specific attribute value, passed by reference.
   */
  Attributes.prototype.get = function (name, defaultValue) {
    if (!this.exists(name)) this.data[name] = defaultValue;
    return this.data[name];
  };

  /**
   * Retrieves a cloned copy of the internal attributes data object.
   *
   * @return {Object}
   */
  Attributes.prototype.getData = function () {
    return _.extend({}, this.data);
  };

  /**
   * Retrieves classes from the array.
   *
   * @return {Array}
   *   The classes array.
   */
  Attributes.prototype.getClasses = function () {
    return this.get('class', []);
  };

  /**
   * Indicates whether a class is present in the array.
   *
   * @param {string|Array} className
   *   The class(es) to search for.
   *
   * @return {boolean}
   *   TRUE or FALSE
   */
  Attributes.prototype.hasClass = function (className) {
    className = this.sanitizeClasses(Array.prototype.slice.call(arguments));
    var classes = this.getClasses();
    for (var i = 0, l = className.length; i < l; i++) {
      // If one of the classes fails, immediately return false.
      if (_.indexOf(classes, className[i]) === -1) {
        return false;
      }
    }
    return true;
  };

  /**
   * Merges multiple values into the array.
   *
   * @param {Attributes|Node|jQuery|Object} object
   *   An Attributes object with existing data, a Node DOM element, a jQuery
   *   instance or a plain object where the key is the attribute name and the
   *   value is the attribute value.
   * @param {boolean} [recursive]
   *   Flag determining whether or not to recursively merge key/value pairs.
   *
   * @return {Attributes}
   *
   * @chainable
   */
  Attributes.prototype.merge = function (object, recursive) {
    // Immediately return if there is nothing to merge.
    if (!object) {
      return this;
    }

    // Get attributes from a jQuery element.
    if (object instanceof $) {
      object = object[0];
    }

    // Get attributes from a DOM element.
    if (object instanceof Node) {
      object = Array.prototype.slice.call(object.attributes).reduce(function (attributes, attribute) {
        attributes[attribute.name] = attribute.value;
        return attributes;
      }, {});
    }
    // Get attributes from an Attributes instance.
    else if (object instanceof Attributes) {
      object = object.getData();
    }
    // Otherwise, clone the object.
    else {
      object = _.extend({}, object);
    }

    // By this point, there should be a valid plain object.
    if (!$.isPlainObject(object)) {
      setTimeout(function () {
        throw new Error('Passed object is not supported: ' + object);
      });
      return this;
    }

    // Handle classes separately.
    if (object && object['class'] !== void 0) {
      this.addClass(object['class']);
      delete object['class'];
    }

    if (recursive === void 0 || recursive) {
      this.data = $.extend(true, {}, this.data, object);
    }
    else {
      this.data = $.extend({}, this.data, object);
    }

    return this;
  };

  /**
   * Removes an attribute from the array.
   *
   * @param {string} name
   *   The name of the attribute to remove.
   *
   * @return {Attributes}
   *
   * @chainable
   */
  Attributes.prototype.remove = function (name) {
    if (this.exists(name)) delete this.data[name];
    return this;
  };

  /**
   * Removes a class from the attributes array.
   *
   * @param {...string|Array} className
   *   An individual class or an array of classes to remove.
   *
   * @return {Attributes}
   *
   * @chainable
   */
  Attributes.prototype.removeClass = function (className) {
    var remove = this.sanitizeClasses(Array.prototype.slice.apply(arguments));
    this.data['class'] = _.without(this.getClasses(), remove);
    return this;
  };

  /**
   * Replaces a class in the attributes array.
   *
   * @param {string} oldValue
   *   The old class to remove.
   * @param {string} newValue
   *   The new class. It will not be added if the old class does not exist.
   *
   * @return {Attributes}
   *
   * @chainable
   */
  Attributes.prototype.replaceClass = function (oldValue, newValue) {
    var classes = this.getClasses();
    var i = _.indexOf(this.sanitizeClasses(oldValue), classes);
    if (i >= 0) {
      classes[i] = newValue;
      this.set('class', classes);
    }
    return this;
  };

  /**
   * Ensures classes are flattened into a single is an array and sanitized.
   *
   * @param {...String|Array} classes
   *   The class or classes to sanitize.
   *
   * @return {Array}
   *   A sanitized array of classes.
   */
  Attributes.prototype.sanitizeClasses = function (classes) {
    return _.chain(Array.prototype.slice.call(arguments))
      // Flatten in case there's a mix of strings and arrays.
      .flatten()

      // Split classes that may have been added with a space as a separator.
      .map(function (string) {
        return string.split(' ');
      })

      // Flatten again since it was just split into arrays.
      .flatten()

      // Filter out empty items.
      .filter()

      // Clean the class to ensure it's a valid class name.
      .map(function (value) {
        return Attributes.cleanClass(value);
      })

      // Ensure classes are unique.
      .uniq()

      // Retrieve the final value.
      .value();
  };

  /**
   * Sets an attribute on the array.
   *
   * @param {string} name
   *   The name of the attribute to set.
   * @param {*} value
   *   The value of the attribute to set.
   *
   * @return {Attributes}
   *
   * @chainable
   */
  Attributes.prototype.set = function (name, value) {
    var obj = $.isPlainObject(name) ? name : {};
    if (typeof name === 'string') {
      obj[name] = value;
    }
    return this.merge(obj);
  };

  /**
   * Prepares a string for use as a CSS identifier (element, class, or ID name).
   *
   * Note: this is essentially a direct copy from
   * \Drupal\Component\Utility\Html::cleanCssIdentifier
   *
   * @param {string} identifier
   *   The identifier to clean.
   * @param {Object} [filter]
   *   An object of string replacements to use on the identifier.
   *
   * @return {string}
   *   The cleaned identifier.
   */
  Attributes.cleanClass = function (identifier, filter) {
    filter = filter || {
      ' ': '-',
      '_': '-',
      '/': '-',
      '[': '-',
      ']': ''
    };

    identifier = identifier.toLowerCase();

    if (filter['__'] === void 0) {
      identifier = identifier.replace('__', '#DOUBLE_UNDERSCORE#');
    }

    identifier = identifier.replace(Object.keys(filter), Object.keys(filter).map(function (key) { return filter[key]; }));

    if (filter['__'] === void 0) {
      identifier = identifier.replace('#DOUBLE_UNDERSCORE#', '__');
    }

    identifier = identifier.replace(/[^\u002D\u0030-\u0039\u0041-\u005A\u005F\u0061-\u007A\u00A1-\uFFFF]/g, '');
    identifier = identifier.replace(['/^[0-9]/', '/^(-[0-9])|^(--)/'], ['_', '__']);

    return identifier;
  };

  /**
   * Creates an Attributes instance.
   *
   * @param {object|Attributes} [attributes]
   *   An object to initialize attributes with.
   *
   * @return {Attributes}
   *   An Attributes instance.
   *
   * @constructor
   */
  Attributes.create = function (attributes) {
    return new Attributes(attributes);
  };

  window.Attributes = Attributes;

})(window.jQuery, window._);
;
/**
 * @file
 * Theme hooks for the Drupal Bootstrap base theme.
 */
(function ($, Drupal, Bootstrap, Attributes) {

  /**
   * Fallback for theming an icon if the Icon API module is not installed.
   */
  if (!Drupal.icon) Drupal.icon = { bundles: {} };
  if (!Drupal.theme.icon || Drupal.theme.prototype.icon) {
    $.extend(Drupal.theme, /** @lends Drupal.theme */ {
      /**
       * Renders an icon.
       *
       * @param {string} bundle
       *   The bundle which the icon belongs to.
       * @param {string} icon
       *   The name of the icon to render.
       * @param {object|Attributes} [attributes]
       *   An object of attributes to also apply to the icon.
       *
       * @returns {string}
       */
      icon: function (bundle, icon, attributes) {
        if (!Drupal.icon.bundles[bundle]) return '';
        attributes = Attributes.create(attributes).addClass('icon').set('aria-hidden', 'true');
        icon = Drupal.icon.bundles[bundle](icon, attributes);
        return '<span' + attributes + '></span>';
      }
    });
  }

  /**
   * Callback for modifying an icon in the "bootstrap" icon bundle.
   *
   * @param {string} icon
   *   The icon being rendered.
   * @param {Attributes} attributes
   *   Attributes object for the icon.
   */
  Drupal.icon.bundles.bootstrap = function (icon, attributes) {
    attributes.addClass(['glyphicon', 'glyphicon-' + icon]);
  };

  /**
   * Add necessary theming hooks.
   */
  $.extend(Drupal.theme, /** @lends Drupal.theme */ {

    /**
     * Renders a Bootstrap AJAX glyphicon throbber.
     *
     * @returns {string}
     */
    ajaxThrobber: function () {
      return Drupal.theme('bootstrapIcon', 'refresh', { 'class': ['ajax-throbber', 'glyphicon-spin'] });
    },

    /**
     * Renders a button element.
     *
     * @param {object|Attributes} attributes
     *   An object of attributes to apply to the button. If it contains one of:
     *   - value: The label of the button.
     *   - context: The context type of Bootstrap button, can be one of:
     *     - default
     *     - primary
     *     - success
     *     - info
     *     - warning
     *     - danger
     *     - link
     *
     * @returns {string}
     */
    button: function (attributes) {
      attributes = Attributes.create(attributes).addClass('btn');
      var context = attributes.get('context', 'default');
      var label = attributes.get('value', '');
      attributes.remove('context').remove('value');
      if (!attributes.hasClass(['btn-default', 'btn-primary', 'btn-success', 'btn-info', 'btn-warning', 'btn-danger', 'btn-link'])) {
        attributes.addClass('btn-' + Bootstrap.checkPlain(context));
      }

      // Attempt to, intelligently, provide a default button "type".
      if (!attributes.exists('type')) {
        attributes.set('type', attributes.hasClass('form-submit') ? 'submit' : 'button');
      }

      return '<button' + attributes + '>' + label + '</button>';
    },

    /**
     * Alias for "button" theme hook.
     *
     * @param {object|Attributes} attributes
     *   An object of attributes to apply to the button.
     *
     * @see Drupal.theme.button()
     *
     * @returns {string}
     */
    btn: function (attributes) {
      return Drupal.theme('button', attributes);
    },

    /**
     * Renders a button block element.
     *
     * @param {object|Attributes} attributes
     *   An object of attributes to apply to the button.
     *
     * @see Drupal.theme.button()
     *
     * @returns {string}
     */
    'btn-block': function (attributes) {
      return Drupal.theme('button', Attributes.create(attributes).addClass('btn-block'));
    },

    /**
     * Renders a large button element.
     *
     * @param {object|Attributes} attributes
     *   An object of attributes to apply to the button.
     *
     * @see Drupal.theme.button()
     *
     * @returns {string}
     */
    'btn-lg': function (attributes) {
      return Drupal.theme('button', Attributes.create(attributes).addClass('btn-lg'));
    },

    /**
     * Renders a small button element.
     *
     * @param {object|Attributes} attributes
     *   An object of attributes to apply to the button.
     *
     * @see Drupal.theme.button()
     *
     * @returns {string}
     */
    'btn-sm': function (attributes) {
      return Drupal.theme('button', Attributes.create(attributes).addClass('btn-sm'));
    },

    /**
     * Renders an extra small button element.
     *
     * @param {object|Attributes} attributes
     *   An object of attributes to apply to the button.
     *
     * @see Drupal.theme.button()
     *
     * @returns {string}
     */
    'btn-xs': function (attributes) {
      return Drupal.theme('button', Attributes.create(attributes).addClass('btn-xs'));
    },

    /**
     * Renders a glyphicon.
     *
     * @param {string} name
     *   The name of the glyphicon.
     * @param {object|Attributes} [attributes]
     *   An object of attributes to apply to the icon.
     *
     * @returns {string}
     */
    bootstrapIcon: function (name, attributes) {
      return Drupal.theme('icon', 'bootstrap', name, attributes);
    }

  });

})(window.jQuery, window.Drupal, window.Drupal.bootstrap, window.Attributes);
;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function ($, Drupal) {
  var states = {
    postponed: []
  };

  Drupal.states = states;

  function invert(a, invertState) {
    return invertState && typeof a !== 'undefined' ? !a : a;
  }

  function _compare2(a, b) {
    if (a === b) {
      return typeof a === 'undefined' ? a : true;
    }

    return typeof a === 'undefined' || typeof b === 'undefined';
  }

  function ternary(a, b) {
    if (typeof a === 'undefined') {
      return b;
    }
    if (typeof b === 'undefined') {
      return a;
    }

    return a && b;
  }

  Drupal.behaviors.states = {
    attach: function attach(context, settings) {
      var $states = $(context).find('[data-drupal-states]');
      var il = $states.length;

      var _loop = function _loop(i) {
        var config = JSON.parse($states[i].getAttribute('data-drupal-states'));
        Object.keys(config || {}).forEach(function (state) {
          new states.Dependent({
            element: $($states[i]),
            state: states.State.sanitize(state),
            constraints: config[state]
          });
        });
      };

      for (var i = 0; i < il; i++) {
        _loop(i);
      }

      while (states.postponed.length) {
        states.postponed.shift()();
      }
    }
  };

  states.Dependent = function (args) {
    var _this = this;

    $.extend(this, { values: {}, oldValue: null }, args);

    this.dependees = this.getDependees();
    Object.keys(this.dependees || {}).forEach(function (selector) {
      _this.initializeDependee(selector, _this.dependees[selector]);
    });
  };

  states.Dependent.comparisons = {
    RegExp: function RegExp(reference, value) {
      return reference.test(value);
    },
    Function: function Function(reference, value) {
      return reference(value);
    },
    Number: function Number(reference, value) {
      return typeof value === 'string' ? _compare2(reference.toString(), value) : _compare2(reference, value);
    }
  };

  states.Dependent.prototype = {
    initializeDependee: function initializeDependee(selector, dependeeStates) {
      var _this2 = this;

      this.values[selector] = {};

      Object.keys(dependeeStates).forEach(function (i) {
        var state = dependeeStates[i];

        if ($.inArray(state, dependeeStates) === -1) {
          return;
        }

        state = states.State.sanitize(state);

        _this2.values[selector][state.name] = null;

        $(selector).on('state:' + state, { selector: selector, state: state }, function (e) {
          _this2.update(e.data.selector, e.data.state, e.value);
        });

        new states.Trigger({ selector: selector, state: state });
      });
    },
    compare: function compare(reference, selector, state) {
      var value = this.values[selector][state.name];
      if (reference.constructor.name in states.Dependent.comparisons) {
        return states.Dependent.comparisons[reference.constructor.name](reference, value);
      }

      return _compare2(reference, value);
    },
    update: function update(selector, state, value) {
      if (value !== this.values[selector][state.name]) {
        this.values[selector][state.name] = value;
        this.reevaluate();
      }
    },
    reevaluate: function reevaluate() {
      var value = this.verifyConstraints(this.constraints);

      if (value !== this.oldValue) {
        this.oldValue = value;

        value = invert(value, this.state.invert);

        this.element.trigger({
          type: 'state:' + this.state,
          value: value,
          trigger: true
        });
      }
    },
    verifyConstraints: function verifyConstraints(constraints, selector) {
      var result = void 0;
      if ($.isArray(constraints)) {
        var hasXor = $.inArray('xor', constraints) === -1;
        var len = constraints.length;
        for (var i = 0; i < len; i++) {
          if (constraints[i] !== 'xor') {
            var constraint = this.checkConstraints(constraints[i], selector, i);

            if (constraint && (hasXor || result)) {
              return hasXor;
            }
            result = result || constraint;
          }
        }
      } else if ($.isPlainObject(constraints)) {
        for (var n in constraints) {
          if (constraints.hasOwnProperty(n)) {
            result = ternary(result, this.checkConstraints(constraints[n], selector, n));

            if (result === false) {
              return false;
            }
          }
        }
      }
      return result;
    },
    checkConstraints: function checkConstraints(value, selector, state) {
      if (typeof state !== 'string' || /[0-9]/.test(state[0])) {
        state = null;
      } else if (typeof selector === 'undefined') {
        selector = state;
        state = null;
      }

      if (state !== null) {
        state = states.State.sanitize(state);
        return invert(this.compare(value, selector, state), state.invert);
      }

      return this.verifyConstraints(value, selector);
    },
    getDependees: function getDependees() {
      var cache = {};

      var _compare = this.compare;
      this.compare = function (reference, selector, state) {
        (cache[selector] || (cache[selector] = [])).push(state.name);
      };

      this.verifyConstraints(this.constraints);

      this.compare = _compare;

      return cache;
    }
  };

  states.Trigger = function (args) {
    $.extend(this, args);

    if (this.state in states.Trigger.states) {
      this.element = $(this.selector);

      if (!this.element.data('trigger:' + this.state)) {
        this.initialize();
      }
    }
  };

  states.Trigger.prototype = {
    initialize: function initialize() {
      var _this3 = this;

      var trigger = states.Trigger.states[this.state];

      if (typeof trigger === 'function') {
        trigger.call(window, this.element);
      } else {
        Object.keys(trigger || {}).forEach(function (event) {
          _this3.defaultTrigger(event, trigger[event]);
        });
      }

      this.element.data('trigger:' + this.state, true);
    },
    defaultTrigger: function defaultTrigger(event, valueFn) {
      var oldValue = valueFn.call(this.element);

      this.element.on(event, $.proxy(function (e) {
        var value = valueFn.call(this.element, e);

        if (oldValue !== value) {
          this.element.trigger({
            type: 'state:' + this.state,
            value: value,
            oldValue: oldValue
          });
          oldValue = value;
        }
      }, this));

      states.postponed.push($.proxy(function () {
        this.element.trigger({
          type: 'state:' + this.state,
          value: oldValue,
          oldValue: null
        });
      }, this));
    }
  };

  states.Trigger.states = {
    empty: {
      keyup: function keyup() {
        return this.val() === '';
      }
    },

    checked: {
      change: function change() {
        var checked = false;
        this.each(function () {
          checked = $(this).prop('checked');

          return !checked;
        });
        return checked;
      }
    },

    value: {
      keyup: function keyup() {
        if (this.length > 1) {
          return this.filter(':checked').val() || false;
        }
        return this.val();
      },
      change: function change() {
        if (this.length > 1) {
          return this.filter(':checked').val() || false;
        }
        return this.val();
      }
    },

    collapsed: {
      collapsed: function collapsed(e) {
        return typeof e !== 'undefined' && 'value' in e ? e.value : !this.is('[open]');
      }
    }
  };

  states.State = function (state) {
    this.pristine = state;
    this.name = state;

    var process = true;
    do {
      while (this.name.charAt(0) === '!') {
        this.name = this.name.substring(1);
        this.invert = !this.invert;
      }

      if (this.name in states.State.aliases) {
        this.name = states.State.aliases[this.name];
      } else {
        process = false;
      }
    } while (process);
  };

  states.State.sanitize = function (state) {
    if (state instanceof states.State) {
      return state;
    }

    return new states.State(state);
  };

  states.State.aliases = {
    enabled: '!disabled',
    invisible: '!visible',
    invalid: '!valid',
    untouched: '!touched',
    optional: '!required',
    filled: '!empty',
    unchecked: '!checked',
    irrelevant: '!relevant',
    expanded: '!collapsed',
    open: '!collapsed',
    closed: 'collapsed',
    readwrite: '!readonly'
  };

  states.State.prototype = {
    invert: false,

    toString: function toString() {
      return this.name;
    }
  };

  var $document = $(document);
  $document.on('state:disabled', function (e) {
    if (e.trigger) {
      $(e.target).prop('disabled', e.value).closest('.js-form-item, .js-form-submit, .js-form-wrapper').toggleClass('form-disabled', e.value).find('select, input, textarea').prop('disabled', e.value);
    }
  });

  $document.on('state:required', function (e) {
    if (e.trigger) {
      if (e.value) {
        var label = 'label' + (e.target.id ? '[for=' + e.target.id + ']' : '');
        var $label = $(e.target).attr({ required: 'required', 'aria-required': 'true' }).closest('.js-form-item, .js-form-wrapper').find(label);

        if (!$label.hasClass('js-form-required').length) {
          $label.addClass('js-form-required form-required');
        }
      } else {
        $(e.target).removeAttr('required aria-required').closest('.js-form-item, .js-form-wrapper').find('label.js-form-required').removeClass('js-form-required form-required');
      }
    }
  });

  $document.on('state:visible', function (e) {
    if (e.trigger) {
      $(e.target).closest('.js-form-item, .js-form-submit, .js-form-wrapper').toggle(e.value);
    }
  });

  $document.on('state:checked', function (e) {
    if (e.trigger) {
      $(e.target).prop('checked', e.value);
    }
  });

  $document.on('state:collapsed', function (e) {
    if (e.trigger) {
      if ($(e.target).is('[open]') === e.value) {
        $(e.target).find('> summary').trigger('click');
      }
    }
  });
})(jQuery, Drupal);;
/**
 * @file
 * Extends core/misc/states.js.
 */
(function ($) {

  // Unbind core state.js from document first so we can then override below.
  $(document).unbind('state:disabled');

  /**
   * Global state change handlers. These are bound to "document" to cover all
   * elements whose state changes. Events sent to elements within the page
   * bubble up to these handlers. We use this system so that themes and modules
   * can override these state change handlers for particular parts of a page.
   */
  $(document).bind('state:disabled', function (e) {
    // Only act when this change was triggered by a dependency and not by the
    // element monitoring itself.
    if (e.trigger) {
      $(e.target)
        .attr('disabled', e.value)
        .closest('.form-item, .form-submit, .form-wrapper').toggleClass('form-disabled', e.value)
        .find(':input').attr('disabled', e.value);

      // Note: WebKit nightlies don't reflect that change correctly.
      // See https://bugs.webkit.org/show_bug.cgi?id=23789
    }
  });

})(jQuery);
;
/**
 * @file
 * JavaScript behaviors for custom webform #states.
 */

(function ($, Drupal) {

  'use strict';

  Drupal.webform = Drupal.webform || {};
  Drupal.webform.states = Drupal.webform.states || {};
  Drupal.webform.states.slideDown = Drupal.webform.states.slideDown || {};
  Drupal.webform.states.slideDown.duration = 'slow';
  Drupal.webform.states.slideUp = Drupal.webform.states.slideUp || {};
  Drupal.webform.states.slideUp.duration = 'fast';

  /**
   * Check if an element has a specified data attribute.
   *
   * @param {string} data
   *   The data attribute name.
   *
   * @returns {boolean}
   *   TRUE if an element has a specified data attribute.
   */
  $.fn.hasData = function (data) {
    return (typeof this.data(data) !== 'undefined');
  };

  /**
   * Check if element is within the webform or not.
   *
   * @returns {boolean}
   *   TRUE if element is within the webform.
   */
  $.fn.isWebform = function () {
    return $(this).closest('form[id^="webform"]').length ? true : false;
  };

  // The change event is triggered by cut-n-paste and select menus.
  // Issue #2445271: #states element empty check not triggered on mouse
  // based paste.
  // @see https://www.drupal.org/node/2445271
  Drupal.states.Trigger.states.empty.change = function change() {
    return this.val() === '';
  };

  // Apply solution included in #1962800 patch.
  // Issue #1962800: Form #states not working with literal integers as
  // values in IE11.
  // @see https://www.drupal.org/project/drupal/issues/1962800
  // @see https://www.drupal.org/files/issues/core-states-not-working-with-integers-ie11_1962800_46.patch
  //
  // This issue causes pattern, less than, and greater than support to break.
  // @see https://www.drupal.org/project/webform/issues/2981724
  var states = Drupal.states;
  Drupal.states.Dependent.prototype.compare = function compare(reference, selector, state) {
    var value = this.values[selector][state.name];

    var name = reference.constructor.name;
    if (!name) {
      name = $.type(reference);

      name = name.charAt(0).toUpperCase() + name.slice(1);
    }
    if (name in states.Dependent.comparisons) {
      return states.Dependent.comparisons[name](reference, value);
    }

    if (reference.constructor.name in states.Dependent.comparisons) {
      return states.Dependent.comparisons[reference.constructor.name](reference, value);
    }

    return _compare2(reference, value);
  };
  function _compare2(a, b) {
    if (a === b) {
      return typeof a === 'undefined' ? a : true;
    }

    return typeof a === 'undefined' || typeof b === 'undefined';
  }

  // Adds pattern, less than, and greater than support to #state API.
  // @see http://drupalsun.com/julia-evans/2012/03/09/extending-form-api-states-regular-expressions
  Drupal.states.Dependent.comparisons.Object = function (reference, value) {
    if ('pattern' in reference) {
      return (new RegExp(reference['pattern'])).test(value);
    }
    else if ('!pattern' in reference) {
      return !((new RegExp(reference['!pattern'])).test(value));
    }
    else if ('less' in reference) {
      return (value !== '' && parseFloat(reference['less']) > parseFloat(value));
    }
    else if ('greater' in reference) {
      return (value !== '' && parseFloat(reference['greater']) < parseFloat(value));
    }
    else {
      return reference.indexOf(value) !== false;
    }
  };

  var $document = $(document);

  $document.on('state:required', function (e) {
    if (e.trigger && $(e.target).isWebform()) {
      var $target = $(e.target);
      // Fix #required file upload.
      // @see Issue #2860529: Conditional required File upload field don't work.
      if (e.value) {
        $target.find('input[type="file"]').attr({ 'required': 'required', 'aria-required': 'true' });
      }
      else {
        $target.find('input[type="file"]').removeAttr('required aria-required');
      }

      // Fix required label for checkboxes and radios.
      // @see Issue #2938414: Checkboxes don't support #states required
      // @see Issue #2731991: Setting required on radios marks all options required.
      // @see Issue #2856315: Conditional Logic - Requiring Radios in a Fieldset.
      // Fix #required for fieldsets.
      // @see Issue #2977569: Hidden fieldsets that become visible with conditional logic cannot be made required.
      if ($target.is('.js-webform-type-radios, .js-webform-type-checkboxes, fieldset')) {
        if (e.value) {
          $target.find('legend span:not(.visually-hidden)').addClass('js-form-required form-required');
        }
        else {
          $target.find('legend span:not(.visually-hidden)').removeClass('js-form-required form-required');
        }
      }

      // Fix #required for radios.
      // @see Issue #2856795: If radio buttons are required but not filled form is nevertheless submitted.
      if ($target.is('.js-webform-type-radios, .js-form-type-webform-radios-other')) {
        if (e.value) {
          $target.find('input[type="radio"]').attr({ 'required': 'required', 'aria-required': 'true' });
        }
        else {
          $target.find('input[type="radio"]').removeAttr('required aria-required');
        }
      }

      // Fix #required for checkboxes.
      // @see Issue #2938414: Checkboxes don't support #states required.
      // @see checkboxRequiredhandler
      if ($target.is('.js-webform-type-checkboxes, .js-form-type-webform-checkboxes-other')) {
        var $checkboxes = $target.find('input[type="checkbox"]');
        if (e.value) {
          // Bind the event handler and add custom HTML5 required validation
          // to all checkboxes.
          $checkboxes.bind('click', checkboxRequiredhandler);
          if (!$checkboxes.is(':checked')) {
            $checkboxes.attr({ 'required': 'required', 'aria-required': 'true' });
          }
        }
        else {
          // Remove custom HTML5 required validation from all checkboxes
          // and unbind the event handler.
          $checkboxes
            .removeAttr('required aria-required')
            .unbind('click', checkboxRequiredhandler);
        }
      }

      // Issue #2986017: Fieldsets shouldn't have required attribute.
      if ($target.is('fieldset')) {
        $target.removeAttr('required aria-required');
      }
    }

  });

  $document.on('state:readonly', function (e) {
    if (e.trigger && $(e.target).isWebform()) {
      $(e.target).prop('readonly', e.value).closest('.js-form-item, .js-form-wrapper').toggleClass('webform-readonly', e.value).find('input, textarea').prop('readonly', e.value);
    }
  });

  $document.on('state:visible state:visible-slide', function (e) {
    if (e.trigger && $(e.target).isWebform()) {
      if (e.value) {
        $(':input', e.target).addBack().each(function () {
          restoreValueAndRequired(this);
          triggerEventHandlers(this);
        });
      }
      else {
        // @see https://www.sitepoint.com/jquery-function-clear-form-data/
        $(':input', e.target).addBack().each(function () {
          backupValueAndRequired(this);
          clearValueAndRequired(this);
          triggerEventHandlers(this);
        });
      }
    }
  });

  $document.bind('state:visible-slide', function (e) {
    if (e.trigger && $(e.target).isWebform()) {
      var effect = e.value ? 'slideDown' : 'slideUp';
      var duration = Drupal.webform.states[effect].duration;
      $(e.target).closest('.js-form-item, .js-form-submit, .js-form-wrapper')[effect](duration);
    }
  });
  Drupal.states.State.aliases['invisible-slide'] = '!visible-slide';

  $document.on('state:disabled', function (e) {
    if (e.trigger && $(e.target).isWebform()) {
      // Make sure disabled property is set before triggering webform:disabled.
      // Copied from: core/misc/states.js
      $(e.target)
        .prop('disabled', e.value)
        .closest('.js-form-item, .js-form-submit, .js-form-wrapper').toggleClass('form-disabled', e.value)
        .find('select, input, textarea').prop('disabled', e.value);

      // Trigger webform:disabled.
      $(e.target).trigger('webform:disabled')
        .find('select, input, textarea').trigger('webform:disabled');
    }
  });

  /**
   * Trigger custom HTML5 multiple checkboxes validation.
   *
   * @see https://stackoverflow.com/a/37825072/145846
   */
  function checkboxRequiredhandler() {
    var $checkboxes = $(this).closest('.js-webform-type-checkboxes, .js-form-type-webform-checkboxes-other').find('input[type="checkbox"]');
    if ($checkboxes.is(':checked')) {
      $checkboxes.removeAttr('required aria-required');
    }
    else {
      $checkboxes.attr({ 'required': 'required', 'aria-required': 'true' });
    }
  }

  /**
   * Trigger an input's event handlers.
   *
   * @param input
   *   An input.
   */
  function triggerEventHandlers(input) {
    var $input = $(input);
    var type = input.type;
    var tag = input.tagName.toLowerCase();
    // Add 'webform.states' as extra parameter to event handlers.
    // @see Drupal.behaviors.webformUnsaved
    var extraParameters = ['webform.states'];
    if (type === 'checkbox' || type === 'radio') {
      $input
        .trigger('change', extraParameters)
        .trigger('blur', extraParameters);
    }
    else if (tag === 'select') {
      $input
        .trigger('change', extraParameters)
        .trigger('blur', extraParameters);
    }
    else if (type !== 'submit' && type !== 'button' && type !== 'file') {
      $input
        .trigger('input', extraParameters)
        .trigger('change', extraParameters)
        .trigger('keydown', extraParameters)
        .trigger('keyup', extraParameters)
        .trigger('blur', extraParameters);
    }
  }

  /**
   * Backup an input's current value and required attribute
   *
   * @param input
   *   An input.
   */
  function backupValueAndRequired(input) {
    var $input = $(input);
    var type = input.type;
    var tag = input.tagName.toLowerCase(); // Normalize case.

    // Backup required.
    if ($input.prop('required') && !$input.hasData('webform-required')) {
      $input.data('webform-required', true);
    }

    // Backup value.
    if (!$input.hasData('webform-value')) {
      if (type === 'checkbox' || type === 'radio') {
        $input.data('webform-value', $input.prop('checked'));
      }
      else if (tag === 'select') {
        var values = [];
        $input.find('option:selected').each(function (i, option) {
          values[i] = option.value;
        });
        $input.data('webform-value', values);
      }
      else if (type !== 'submit' && type !== 'button') {
        $input.data('webform-value', input.value);
      }
    }
  }

  /**
   * Restore an input's value and required attribute.
   *
   * @param input
   *   An input.
   */
  function restoreValueAndRequired(input) {
    var $input = $(input);

    // Restore value.
    var value = $input.data('webform-value');
    if (typeof value !== 'undefined') {
      var type = input.type;
      var tag = input.tagName.toLowerCase(); // Normalize case.

      if (type === 'checkbox' || type === 'radio') {
        $input.prop('checked', value);
      }
      else if (tag === 'select') {
        $.each(value, function (i, option_value) {
          $input.find("option[value='" + option_value + "']").prop('selected', true);
        });
      }
      else if (type !== 'submit' && type !== 'button') {
        input.value = value;
      }
      $input.removeData('webform-value');
    }

    // Restore required.
    var required = $input.data('webform-required');
    if (typeof required !== 'undefined') {
      if (required) {
        $input.prop('required', true);
      }
      $input.removeData('webform-required');
    }
  }

  /**
   * Clear an input's value and required attributes.
   *
   * @param input
   *   An input.
   */
  function clearValueAndRequired(input) {
    var $input = $(input);

    // Check for #states no clear attribute.
    // @see https://css-tricks.com/snippets/jquery/make-an-jquery-hasattr/
    if ($input.closest('[data-webform-states-no-clear]').length) {
      return;
    }

    // Clear value.
    var type = input.type;
    var tag = input.tagName.toLowerCase(); // Normalize case.
    if (type === 'checkbox' || type === 'radio') {
      $input.prop('checked', false);
    }
    else if (tag === 'select') {
      if ($input.find('option[value=""]').length) {
        $input.val('');
      }
      else {
        input.selectedIndex = -1;
      }
    }
    else if (type !== 'submit' && type !== 'button') {
      input.value = (type === 'color') ? '#000000' : '';
    }

    // Clear required.
    $input.prop('required', false);
  }

})(jQuery, Drupal);
;
/**
 * @file
 * JavaScript behaviors for custom webform #states.
 */

(function ($, Drupal) {

  'use strict';

  $(document).on('state:required', function (e) {
    if (e.trigger && $(e.target).isWebform()) {
      var $target = $(e.target);

      // @see Issue #2856315: Conditional Logic - Requiring Radios in a Fieldset.
      // Fix #required for fieldsets.
      if ($target.is('.js-form-wrapper.panel')) {
        if (e.value) {
          $target.find('.panel-heading .panel-title').addClass('js-form-required form-required');
        }
        else {
          $target.find('.panel-heading .panel-title').removeClass('js-form-required form-required');
        }
      }

    }
  });

})(jQuery, Drupal);
;
/**
 * @file
 * Bootstrap Popovers.
 */

var Drupal = Drupal || {};

(function ($, Drupal, Bootstrap) {
  "use strict";

  var $document = $(document);

  /**
   * Extend the Bootstrap Popover plugin constructor class.
   */
  Bootstrap.extendPlugin('popover', function (settings) {
    return {
      DEFAULTS: {
        animation: !!settings.popover_animation,
        autoClose: !!settings.popover_auto_close,
        enabled: settings.popover_enabled,
        html: !!settings.popover_html,
        placement: settings.popover_placement,
        selector: settings.popover_selector,
        trigger: settings.popover_trigger,
        title: settings.popover_title,
        content: settings.popover_content,
        delay: parseInt(settings.popover_delay, 10),
        container: settings.popover_container
      }
    };
  });

  /**
   * Bootstrap Popovers.
   *
   * @todo This should really be properly delegated if selector option is set.
   */
  Drupal.behaviors.bootstrapPopovers = {
    $activePopover: null,
    attach: function (context) {
      // Immediately return if popovers are not available.
      if (!$.fn.popover || !$.fn.popover.Constructor.DEFAULTS.enabled) {
        return;
      }

      var _this = this;

      $document
        .on('show.bs.popover', '[data-toggle=popover]', function () {
          var $trigger = $(this);
          var popover = $trigger.data('bs.popover');

          // Only keep track of clicked triggers that we're manually handling.
          if (popover.options.originalTrigger === 'click') {
            if (_this.$activePopover && _this.getOption('autoClose') && !_this.$activePopover.is($trigger)) {
              _this.$activePopover.popover('hide');
            }
            _this.$activePopover = $trigger;
          }
        })
        // Unfortunately, :focusable is only made available when using jQuery
        // UI. While this would be the most semantic pseudo selector to use
        // here, jQuery UI may not always be loaded. Instead, just use :visible
        // here as this just needs some sort of selector here. This activates
        // delegate binding to elements in jQuery so it can work it's bubbling
        // focus magic since elements don't really propagate their focus events.
        // @see https://www.drupal.org/project/bootstrap/issues/3013236
        .on('focus.bs.popover', ':visible', function (e) {
          var $target = $(e.target);
          if (_this.$activePopover && _this.getOption('autoClose') && !_this.$activePopover.is($target) && !$target.closest('.popover.in')[0]) {
            _this.$activePopover.popover('hide');
            _this.$activePopover = null;
          }
        })
        .on('click.bs.popover', function (e) {
          var $target = $(e.target);
          if (_this.$activePopover && _this.getOption('autoClose') && !$target.is('[data-toggle=popover]') && !$target.closest('.popover.in')[0]) {
            _this.$activePopover.popover('hide');
            _this.$activePopover = null;
          }
        })
        .on('keyup.bs.popover', function (e) {
          if (_this.$activePopover && _this.getOption('autoClose') && e.which === 27) {
            _this.$activePopover.popover('hide');
            _this.$activePopover = null;
          }
        })
        ;

      var elements = $(context).find('[data-toggle=popover]').toArray();
      for (var i = 0; i < elements.length; i++) {
        var $element = $(elements[i]);
        var options = $.extend({}, $.fn.popover.Constructor.DEFAULTS, $element.data());

        // Store the original trigger.
        options.originalTrigger = options.trigger;

        // If the trigger is "click", then we'll handle it manually here.
        if (options.trigger === 'click') {
          options.trigger = 'manual';
        }

        // Retrieve content from a target element.
        var target = options.target || $element.is('a[href^="#"]') && $element.attr('href');
        var $target = $document.find(target).clone();
        if (!options.content && $target[0]) {
          $target.removeClass('visually-hidden hidden').removeAttr('aria-hidden');
          options.content = $target.wrap('<div/>').parent()[options.html ? 'html' : 'text']() || '';
        }

        // Initialize the popover.
        $element.popover(options);

        // Handle clicks manually.
        if (options.originalTrigger === 'click') {
          // To ensure the element is bound multiple times, remove any
          // previously set event handler before adding another one.
          $element
            .off('click.drupal.bootstrap.popover')
            .on('click.drupal.bootstrap.popover', function (e) {
              $(this).popover('toggle');
              e.preventDefault();
              e.stopPropagation();
            })
            ;
        }
      }
    },
    detach: function (context) {
      // Immediately return if popovers are not available.
      if (!$.fn.popover || !$.fn.popover.Constructor.DEFAULTS.enabled) {
        return;
      }

      // Destroy all popovers.
      $(context).find('[data-toggle="popover"]')
        .off('click.drupal.bootstrap.popover')
        .popover('destroy')
        ;
    },
    getOption: function (name, defaultValue, element) {
      var $element = element ? $(element) : this.$activePopover;
      var options = $.extend(true, {}, $.fn.popover.Constructor.DEFAULTS, ($element && $element.data('bs.popover') || {}).options);
      if (options[name] !== void 0) {
        return options[name];
      }
      return defaultValue !== void 0 ? defaultValue : void 0;
    }
  };

})(window.jQuery, window.Drupal, window.Drupal.bootstrap);
;
/**
 * @file
 * Bootstrap Tooltips.
 */

var Drupal = Drupal || {};

(function ($, Drupal, Bootstrap) {
  "use strict";

  /**
   * Extend the Bootstrap Tooltip plugin constructor class.
   */
  Bootstrap.extendPlugin('tooltip', function (settings) {
    return {
      DEFAULTS: {
        animation: !!settings.tooltip_animation,
        enabled: settings.tooltip_enabled,
        html: !!settings.tooltip_html,
        placement: settings.tooltip_placement,
        selector: settings.tooltip_selector,
        trigger: settings.tooltip_trigger,
        delay: parseInt(settings.tooltip_delay, 10),
        container: settings.tooltip_container
      }
    };
  });

  /**
   * Bootstrap Tooltips.
   *
   * @todo This should really be properly delegated if selector option is set.
   */
  Drupal.behaviors.bootstrapTooltips = {
    attach: function (context) {
      // Immediately return if tooltips are not available.
      if (!$.fn.tooltip || !$.fn.tooltip.Constructor.DEFAULTS.enabled) {
        return;
      }

      var elements = $(context).find('[data-toggle="tooltip"]').toArray();
      for (var i = 0; i < elements.length; i++) {
        var $element = $(elements[i]);
        var options = $.extend({}, $.fn.tooltip.Constructor.DEFAULTS, $element.data());
        $element.tooltip(options);
      }
    },
    detach: function (context) {
      // Immediately return if tooltips are not available.
      if (!$.fn.tooltip || !$.fn.tooltip.Constructor.DEFAULTS.enabled) {
        return;
      }

      // Destroy all tooltips.
      $(context).find('[data-toggle="tooltip"]').tooltip('destroy');
    }
  };

})(window.jQuery, window.Drupal, window.Drupal.bootstrap);
;
/*!
	Colorbox 1.6.4
	license: MIT
	http://www.jacklmoore.com/colorbox
*/
(function (t, e, i) { function n(i, n, o) { var r = e.createElement(i); return n && (r.id = Z + n), o && (r.style.cssText = o), t(r) } function o() { return i.innerHeight ? i.innerHeight : t(i).height() } function r(e, i) { i !== Object(i) && (i = {}), this.cache = {}, this.el = e, this.value = function (e) { var n; return void 0 === this.cache[e] && (n = t(this.el).attr("data-cbox-" + e), void 0 !== n ? this.cache[e] = n : void 0 !== i[e] ? this.cache[e] = i[e] : void 0 !== X[e] && (this.cache[e] = X[e])), this.cache[e] }, this.get = function (e) { var i = this.value(e); return t.isFunction(i) ? i.call(this.el, this) : i } } function h(t) { var e = W.length, i = (A + t) % e; return 0 > i ? e + i : i } function a(t, e) { return Math.round((/%/.test(t) ? ("x" === e ? E.width() : o()) / 100 : 1) * parseInt(t, 10)) } function s(t, e) { return t.get("photo") || t.get("photoRegex").test(e) } function l(t, e) { return t.get("retinaUrl") && i.devicePixelRatio > 1 ? e.replace(t.get("photoRegex"), t.get("retinaSuffix")) : e } function d(t) { "contains" in x[0] && !x[0].contains(t.target) && t.target !== v[0] && (t.stopPropagation(), x.focus()) } function c(t) { c.str !== t && (x.add(v).removeClass(c.str).addClass(t), c.str = t) } function g(e) { A = 0, e && e !== !1 && "nofollow" !== e ? (W = t("." + te).filter(function () { var i = t.data(this, Y), n = new r(this, i); return n.get("rel") === e }), A = W.index(_.el), -1 === A && (W = W.add(_.el), A = W.length - 1)) : W = t(_.el) } function u(i) { t(e).trigger(i), ae.triggerHandler(i) } function f(i) { var o; if (!G) { if (o = t(i).data(Y), _ = new r(i, o), g(_.get("rel")), !U) { U = $ = !0, c(_.get("className")), x.css({ visibility: "hidden", display: "block", opacity: "" }), I = n(se, "LoadedContent", "width:0; height:0; overflow:hidden; visibility:hidden"), b.css({ width: "", height: "" }).append(I), j = T.height() + k.height() + b.outerHeight(!0) - b.height(), D = C.width() + H.width() + b.outerWidth(!0) - b.width(), N = I.outerHeight(!0), z = I.outerWidth(!0); var h = a(_.get("initialWidth"), "x"), s = a(_.get("initialHeight"), "y"), l = _.get("maxWidth"), f = _.get("maxHeight"); _.w = Math.max((l !== !1 ? Math.min(h, a(l, "x")) : h) - z - D, 0), _.h = Math.max((f !== !1 ? Math.min(s, a(f, "y")) : s) - N - j, 0), I.css({ width: "", height: _.h }), J.position(), u(ee), _.get("onOpen"), O.add(F).hide(), x.focus(), _.get("trapFocus") && e.addEventListener && (e.addEventListener("focus", d, !0), ae.one(re, function () { e.removeEventListener("focus", d, !0) })), _.get("returnFocus") && ae.one(re, function () { t(_.el).focus() }) } var p = parseFloat(_.get("opacity")); v.css({ opacity: p === p ? p : "", cursor: _.get("overlayClose") ? "pointer" : "", visibility: "visible" }).show(), _.get("closeButton") ? B.html(_.get("close")).appendTo(b) : B.appendTo("<div/>"), w() } } function p() { x || (V = !1, E = t(i), x = n(se).attr({ id: Y, "class": t.support.opacity === !1 ? Z + "IE" : "", role: "dialog", tabindex: "-1" }).hide(), v = n(se, "Overlay").hide(), L = t([n(se, "LoadingOverlay")[0], n(se, "LoadingGraphic")[0]]), y = n(se, "Wrapper"), b = n(se, "Content").append(F = n(se, "Title"), R = n(se, "Current"), P = t('<button type="button"/>').attr({ id: Z + "Previous" }), K = t('<button type="button"/>').attr({ id: Z + "Next" }), S = t('<button type="button"/>').attr({ id: Z + "Slideshow" }), L), B = t('<button type="button"/>').attr({ id: Z + "Close" }), y.append(n(se).append(n(se, "TopLeft"), T = n(se, "TopCenter"), n(se, "TopRight")), n(se, !1, "clear:left").append(C = n(se, "MiddleLeft"), b, H = n(se, "MiddleRight")), n(se, !1, "clear:left").append(n(se, "BottomLeft"), k = n(se, "BottomCenter"), n(se, "BottomRight"))).find("div div").css({ "float": "left" }), M = n(se, !1, "position:absolute; width:9999px; visibility:hidden; display:none; max-width:none;"), O = K.add(P).add(R).add(S)), e.body && !x.parent().length && t(e.body).append(v, x.append(y, M)) } function m() { function i(t) { t.which > 1 || t.shiftKey || t.altKey || t.metaKey || t.ctrlKey || (t.preventDefault(), f(this)) } return x ? (V || (V = !0, K.click(function () { J.next() }), P.click(function () { J.prev() }), B.click(function () { J.close() }), v.click(function () { _.get("overlayClose") && J.close() }), t(e).bind("keydown." + Z, function (t) { var e = t.keyCode; U && _.get("escKey") && 27 === e && (t.preventDefault(), J.close()), U && _.get("arrowKey") && W[1] && !t.altKey && (37 === e ? (t.preventDefault(), P.click()) : 39 === e && (t.preventDefault(), K.click())) }), t.isFunction(t.fn.on) ? t(e).on("click." + Z, "." + te, i) : t("." + te).live("click." + Z, i)), !0) : !1 } function w() { var e, o, r, h = J.prep, d = ++le; if ($ = !0, q = !1, u(he), u(ie), _.get("onLoad"), _.h = _.get("height") ? a(_.get("height"), "y") - N - j : _.get("innerHeight") && a(_.get("innerHeight"), "y"), _.w = _.get("width") ? a(_.get("width"), "x") - z - D : _.get("innerWidth") && a(_.get("innerWidth"), "x"), _.mw = _.w, _.mh = _.h, _.get("maxWidth") && (_.mw = a(_.get("maxWidth"), "x") - z - D, _.mw = _.w && _.w < _.mw ? _.w : _.mw), _.get("maxHeight") && (_.mh = a(_.get("maxHeight"), "y") - N - j, _.mh = _.h && _.h < _.mh ? _.h : _.mh), e = _.get("href"), Q = setTimeout(function () { L.show() }, 100), _.get("inline")) { var c = t(e).eq(0); r = t("<div>").hide().insertBefore(c), ae.one(he, function () { r.replaceWith(c) }), h(c) } else _.get("iframe") ? h(" ") : _.get("html") ? h(_.get("html")) : s(_, e) ? (e = l(_, e), q = _.get("createImg"), t(q).addClass(Z + "Photo").bind("error." + Z, function () { h(n(se, "Error").html(_.get("imgError"))) }).one("load", function () { d === le && setTimeout(function () { var e; _.get("retinaImage") && i.devicePixelRatio > 1 && (q.height = q.height / i.devicePixelRatio, q.width = q.width / i.devicePixelRatio), _.get("scalePhotos") && (o = function () { q.height -= q.height * e, q.width -= q.width * e }, _.mw && q.width > _.mw && (e = (q.width - _.mw) / q.width, o()), _.mh && q.height > _.mh && (e = (q.height - _.mh) / q.height, o())), _.h && (q.style.marginTop = Math.max(_.mh - q.height, 0) / 2 + "px"), W[1] && (_.get("loop") || W[A + 1]) && (q.style.cursor = "pointer", t(q).bind("click." + Z, function () { J.next() })), q.style.width = q.width + "px", q.style.height = q.height + "px", h(q) }, 1) }), q.src = e) : e && M.load(e, _.get("data"), function (e, i) { d === le && h("error" === i ? n(se, "Error").html(_.get("xhrError")) : t(this).contents()) }) } var v, x, y, b, T, C, H, k, W, E, I, M, L, F, R, S, K, P, B, O, _, j, D, N, z, A, q, U, $, G, Q, J, V, X = { html: !1, photo: !1, iframe: !1, inline: !1, transition: "elastic", speed: 300, fadeOut: 300, width: !1, initialWidth: "600", innerWidth: !1, maxWidth: !1, height: !1, initialHeight: "450", innerHeight: !1, maxHeight: !1, scalePhotos: !0, scrolling: !0, opacity: .9, preloading: !0, className: !1, overlayClose: !0, escKey: !0, arrowKey: !0, top: !1, bottom: !1, left: !1, right: !1, fixed: !1, data: void 0, closeButton: !0, fastIframe: !0, open: !1, reposition: !0, loop: !0, slideshow: !1, slideshowAuto: !0, slideshowSpeed: 2500, slideshowStart: "start slideshow", slideshowStop: "stop slideshow", photoRegex: /\.(gif|png|jp(e|g|eg)|bmp|ico|webp|jxr|svg)((#|\?).*)?$/i, retinaImage: !1, retinaUrl: !1, retinaSuffix: "@2x.$1", current: "image {current} of {total}", previous: "previous", next: "next", close: "close", xhrError: "This content failed to load.", imgError: "This image failed to load.", returnFocus: !0, trapFocus: !0, onOpen: !1, onLoad: !1, onComplete: !1, onCleanup: !1, onClosed: !1, rel: function () { return this.rel }, href: function () { return t(this).attr("href") }, title: function () { return this.title }, createImg: function () { var e = new Image, i = t(this).data("cbox-img-attrs"); return "object" == typeof i && t.each(i, function (t, i) { e[t] = i }), e }, createIframe: function () { var i = e.createElement("iframe"), n = t(this).data("cbox-iframe-attrs"); return "object" == typeof n && t.each(n, function (t, e) { i[t] = e }), "frameBorder" in i && (i.frameBorder = 0), "allowTransparency" in i && (i.allowTransparency = "true"), i.name = (new Date).getTime(), i.allowFullscreen = !0, i } }, Y = "colorbox", Z = "cbox", te = Z + "Element", ee = Z + "_open", ie = Z + "_load", ne = Z + "_complete", oe = Z + "_cleanup", re = Z + "_closed", he = Z + "_purge", ae = t("<a/>"), se = "div", le = 0, de = {}, ce = function () { function t() { clearTimeout(h) } function e() { (_.get("loop") || W[A + 1]) && (t(), h = setTimeout(J.next, _.get("slideshowSpeed"))) } function i() { S.html(_.get("slideshowStop")).unbind(s).one(s, n), ae.bind(ne, e).bind(ie, t), x.removeClass(a + "off").addClass(a + "on") } function n() { t(), ae.unbind(ne, e).unbind(ie, t), S.html(_.get("slideshowStart")).unbind(s).one(s, function () { J.next(), i() }), x.removeClass(a + "on").addClass(a + "off") } function o() { r = !1, S.hide(), t(), ae.unbind(ne, e).unbind(ie, t), x.removeClass(a + "off " + a + "on") } var r, h, a = Z + "Slideshow_", s = "click." + Z; return function () { r ? _.get("slideshow") || (ae.unbind(oe, o), o()) : _.get("slideshow") && W[1] && (r = !0, ae.one(oe, o), _.get("slideshowAuto") ? i() : n(), S.show()) } }(); t[Y] || (t(p), J = t.fn[Y] = t[Y] = function (e, i) { var n, o = this; return e = e || {}, t.isFunction(o) && (o = t("<a/>"), e.open = !0), o[0] ? (p(), m() && (i && (e.onComplete = i), o.each(function () { var i = t.data(this, Y) || {}; t.data(this, Y, t.extend(i, e)) }).addClass(te), n = new r(o[0], e), n.get("open") && f(o[0])), o) : o }, J.position = function (e, i) { function n() { T[0].style.width = k[0].style.width = b[0].style.width = parseInt(x[0].style.width, 10) - D + "px", b[0].style.height = C[0].style.height = H[0].style.height = parseInt(x[0].style.height, 10) - j + "px" } var r, h, s, l = 0, d = 0, c = x.offset(); if (E.unbind("resize." + Z), x.css({ top: -9e4, left: -9e4 }), h = E.scrollTop(), s = E.scrollLeft(), _.get("fixed") ? (c.top -= h, c.left -= s, x.css({ position: "fixed" })) : (l = h, d = s, x.css({ position: "absolute" })), d += _.get("right") !== !1 ? Math.max(E.width() - _.w - z - D - a(_.get("right"), "x"), 0) : _.get("left") !== !1 ? a(_.get("left"), "x") : Math.round(Math.max(E.width() - _.w - z - D, 0) / 2), l += _.get("bottom") !== !1 ? Math.max(o() - _.h - N - j - a(_.get("bottom"), "y"), 0) : _.get("top") !== !1 ? a(_.get("top"), "y") : Math.round(Math.max(o() - _.h - N - j, 0) / 2), x.css({ top: c.top, left: c.left, visibility: "visible" }), y[0].style.width = y[0].style.height = "9999px", r = { width: _.w + z + D, height: _.h + N + j, top: l, left: d }, e) { var g = 0; t.each(r, function (t) { return r[t] !== de[t] ? (g = e, void 0) : void 0 }), e = g } de = r, e || x.css(r), x.dequeue().animate(r, { duration: e || 0, complete: function () { n(), $ = !1, y[0].style.width = _.w + z + D + "px", y[0].style.height = _.h + N + j + "px", _.get("reposition") && setTimeout(function () { E.bind("resize." + Z, J.position) }, 1), t.isFunction(i) && i() }, step: n }) }, J.resize = function (t) { var e; U && (t = t || {}, t.width && (_.w = a(t.width, "x") - z - D), t.innerWidth && (_.w = a(t.innerWidth, "x")), I.css({ width: _.w }), t.height && (_.h = a(t.height, "y") - N - j), t.innerHeight && (_.h = a(t.innerHeight, "y")), t.innerHeight || t.height || (e = I.scrollTop(), I.css({ height: "auto" }), _.h = I.height()), I.css({ height: _.h }), e && I.scrollTop(e), J.position("none" === _.get("transition") ? 0 : _.get("speed"))) }, J.prep = function (i) { function o() { return _.w = _.w || I.width(), _.w = _.mw && _.mw < _.w ? _.mw : _.w, _.w } function a() { return _.h = _.h || I.height(), _.h = _.mh && _.mh < _.h ? _.mh : _.h, _.h } if (U) { var d, g = "none" === _.get("transition") ? 0 : _.get("speed"); I.remove(), I = n(se, "LoadedContent").append(i), I.hide().appendTo(M.show()).css({ width: o(), overflow: _.get("scrolling") ? "auto" : "hidden" }).css({ height: a() }).prependTo(b), M.hide(), t(q).css({ "float": "none" }), c(_.get("className")), d = function () { function i() { t.support.opacity === !1 && x[0].style.removeAttribute("filter") } var n, o, a = W.length; U && (o = function () { clearTimeout(Q), L.hide(), u(ne), _.get("onComplete") }, F.html(_.get("title")).show(), I.show(), a > 1 ? ("string" == typeof _.get("current") && R.html(_.get("current").replace("{current}", A + 1).replace("{total}", a)).show(), K[_.get("loop") || a - 1 > A ? "show" : "hide"]().html(_.get("next")), P[_.get("loop") || A ? "show" : "hide"]().html(_.get("previous")), ce(), _.get("preloading") && t.each([h(-1), h(1)], function () { var i, n = W[this], o = new r(n, t.data(n, Y)), h = o.get("href"); h && s(o, h) && (h = l(o, h), i = e.createElement("img"), i.src = h) })) : O.hide(), _.get("iframe") ? (n = _.get("createIframe"), _.get("scrolling") || (n.scrolling = "no"), t(n).attr({ src: _.get("href"), "class": Z + "Iframe" }).one("load", o).appendTo(I), ae.one(he, function () { n.src = "//about:blank" }), _.get("fastIframe") && t(n).trigger("load")) : o(), "fade" === _.get("transition") ? x.fadeTo(g, 1, i) : i()) }, "fade" === _.get("transition") ? x.fadeTo(g, 0, function () { J.position(0, d) }) : J.position(g, d) } }, J.next = function () { !$ && W[1] && (_.get("loop") || W[A + 1]) && (A = h(1), f(W[A])) }, J.prev = function () { !$ && W[1] && (_.get("loop") || A) && (A = h(-1), f(W[A])) }, J.close = function () { U && !G && (G = !0, U = !1, u(oe), _.get("onCleanup"), E.unbind("." + Z), v.fadeTo(_.get("fadeOut") || 0, 0), x.stop().fadeTo(_.get("fadeOut") || 0, 0, function () { x.hide(), v.hide(), u(he), I.remove(), setTimeout(function () { G = !1, u(re), _.get("onClosed") }, 1) })) }, J.remove = function () { x && (x.stop(), t[Y].close(), x.stop(!1, !0).remove(), v.remove(), G = !1, x = null, t("." + te).removeData(Y).removeClass(te), t(e).unbind("click." + Z).unbind("keydown." + Z)) }, J.element = function () { return t(_.el) }, J.settings = X) })(jQuery, document, window);;
(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.initColorbox = {
    attach: function (context, settings) {
      if (!$.isFunction($.colorbox) || typeof settings.colorbox === 'undefined') {
        return;
      }

      if (settings.colorbox.mobiledetect && window.matchMedia) {
        // Disable Colorbox for small screens.
        var mq = window.matchMedia('(max-device-width: ' + settings.colorbox.mobiledevicewidth + ')');
        if (mq.matches) {
          return;
        }
      }

      settings.colorbox.rel = function () {
        return $(this).data('colorbox-gallery')
      };

      $('.colorbox', context)
        .once('init-colorbox')
        .colorbox(settings.colorbox);
    }
  };

})(jQuery, Drupal);
;
(function ($) {

  Drupal.behaviors.initColorboxDefaultStyle = {
    attach: function (context, settings) {
      $(context).bind('cbox_complete', function () {
        // Only run if there is a title.
        if ($('#cboxTitle:empty', context).length == false) {
          $('#cboxLoadedContent img', context).bind('mouseover', function () {
            $('#cboxTitle', context).slideDown();
          });
          $('#cboxOverlay', context).bind('mouseover', function () {
            $('#cboxTitle', context).slideUp();
          });
        }
        else {
          $('#cboxTitle', context).hide();
        }
      });
    }
  };

})(jQuery);
;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function ($, Drupal) {
  Drupal.theme.progressBar = function (id) {
    return '<div id="' + id + '" class="progress" aria-live="polite">' + '<div class="progress__label">&nbsp;</div>' + '<div class="progress__track"><div class="progress__bar"></div></div>' + '<div class="progress__percentage"></div>' + '<div class="progress__description">&nbsp;</div>' + '</div>';
  };

  Drupal.ProgressBar = function (id, updateCallback, method, errorCallback) {
    this.id = id;
    this.method = method || 'GET';
    this.updateCallback = updateCallback;
    this.errorCallback = errorCallback;

    this.element = $(Drupal.theme('progressBar', id));
  };

  $.extend(Drupal.ProgressBar.prototype, {
    setProgress: function setProgress(percentage, message, label) {
      if (percentage >= 0 && percentage <= 100) {
        $(this.element).find('div.progress__bar').css('width', percentage + '%');
        $(this.element).find('div.progress__percentage').html(percentage + '%');
      }
      $('div.progress__description', this.element).html(message);
      $('div.progress__label', this.element).html(label);
      if (this.updateCallback) {
        this.updateCallback(percentage, message, this);
      }
    },
    startMonitoring: function startMonitoring(uri, delay) {
      this.delay = delay;
      this.uri = uri;
      this.sendPing();
    },
    stopMonitoring: function stopMonitoring() {
      clearTimeout(this.timer);

      this.uri = null;
    },
    sendPing: function sendPing() {
      if (this.timer) {
        clearTimeout(this.timer);
      }
      if (this.uri) {
        var pb = this;

        var uri = this.uri;
        if (uri.indexOf('?') === -1) {
          uri += '?';
        } else {
          uri += '&';
        }
        uri += '_format=json';
        $.ajax({
          type: this.method,
          url: uri,
          data: '',
          dataType: 'json',
          success: function success(progress) {
            if (progress.status === 0) {
              pb.displayError(progress.data);
              return;
            }

            pb.setProgress(progress.percentage, progress.message, progress.label);

            pb.timer = setTimeout(function () {
              pb.sendPing();
            }, pb.delay);
          },
          error: function error(xmlhttp) {
            var e = new Drupal.AjaxError(xmlhttp, pb.uri);
            pb.displayError('<pre>' + e.message + '</pre>');
          }
        });
      }
    },
    displayError: function displayError(string) {
      var error = $('<div class="messages messages--error"></div>').html(string);
      $(this.element).before(error).hide();

      if (this.errorCallback) {
        this.errorCallback(this);
      }
    }
  });
})(jQuery, Drupal);;
/**
 * @file
 * Extends methods from core/misc/progress.js.
 */

(function ($, Drupal) {

  'use strict';

  /**
   * Theme function for the progress bar.
   *
   * @param {string} id
   *
   * @return {string}
   *   The HTML for the progress bar.
   */
  Drupal.theme.progressBar = function (id) {
    return '<div class="progress-wrapper" aria-live="polite">' +
      '<div class="message"></div>' +
      '<div id ="' + id + '" class="progress progress-striped active">' +
      '<div class="progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">' +
      '<span class="percentage"></span>' +
      '</div>' +
      '</div>' +
      '<div class="progress-label"></div>' +
      '</div>';
  };

  $.extend(Drupal.ProgressBar.prototype, /** @lends Drupal.ProgressBar */{

    /**
     * Set the percentage and status message for the progressbar.
     *
     * @param {number} percentage
     * @param {string} message
     * @param {string} label
     */
    setProgress: function (percentage, message, label) {
      if (percentage >= 0 && percentage <= 100) {
        $(this.element).find('.progress-bar').css('width', percentage + '%').attr('aria-valuenow', percentage);
        $(this.element).find('.percentage').html(percentage + '%');
      }
      if (message) {
        // Remove the unnecessary whitespace at the end of the message.
        message = message.replace(/<br\/>&nbsp;|\s*$/, '');

        $('.message', this.element).html(message);
      }
      if (label) {
        $('.progress-label', this.element).html(label);
      }
      if (this.updateCallback) {
        this.updateCallback(percentage, message, this);
      }
    },

    /**
     * Display errors on the page.
     *
     * @param {string} string
     */
    displayError: function (string) {
      var error = $('<div class="alert alert-block alert-error"><button class="close" data-dismiss="alert">&times;</button><h4>' + Drupal.t('Error message') + '</h4></div>').append(string);
      $(this.element).before(error).hide();

      if (this.errorCallback) {
        this.errorCallback(this);
      }
    }
  });

})(jQuery, Drupal);
;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/
function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

(function ($, window, Drupal, drupalSettings) {
  Drupal.behaviors.AJAX = {
    attach: function attach(context, settings) {
      function loadAjaxBehavior(base) {
        var elementSettings = settings.ajax[base];
        if (typeof elementSettings.selector === 'undefined') {
          elementSettings.selector = '#' + base;
        }
        $(elementSettings.selector).once('drupal-ajax').each(function () {
          elementSettings.element = this;
          elementSettings.base = base;
          Drupal.ajax(elementSettings);
        });
      }

      Object.keys(settings.ajax || {}).forEach(function (base) {
        return loadAjaxBehavior(base);
      });

      Drupal.ajax.bindAjaxLinks(document.body);

      $('.use-ajax-submit').once('ajax').each(function () {
        var elementSettings = {};

        elementSettings.url = $(this.form).attr('action');

        elementSettings.setClick = true;

        elementSettings.event = 'click';

        elementSettings.progress = { type: 'throbber' };
        elementSettings.base = $(this).attr('id');
        elementSettings.element = this;

        Drupal.ajax(elementSettings);
      });
    },
    detach: function detach(context, settings, trigger) {
      if (trigger === 'unload') {
        Drupal.ajax.expired().forEach(function (instance) {
          Drupal.ajax.instances[instance.instanceIndex] = null;
        });
      }
    }
  };

  Drupal.AjaxError = function (xmlhttp, uri, customMessage) {
    var statusCode = void 0;
    var statusText = void 0;
    var responseText = void 0;
    if (xmlhttp.status) {
      statusCode = '\n' + Drupal.t('An AJAX HTTP error occurred.') + '\n' + Drupal.t('HTTP Result Code: !status', { '!status': xmlhttp.status });
    } else {
      statusCode = '\n' + Drupal.t('An AJAX HTTP request terminated abnormally.');
    }
    statusCode += '\n' + Drupal.t('Debugging information follows.');
    var pathText = '\n' + Drupal.t('Path: !uri', { '!uri': uri });
    statusText = '';

    try {
      statusText = '\n' + Drupal.t('StatusText: !statusText', {
        '!statusText': $.trim(xmlhttp.statusText)
      });
    } catch (e) { }

    responseText = '';

    try {
      responseText = '\n' + Drupal.t('ResponseText: !responseText', {
        '!responseText': $.trim(xmlhttp.responseText)
      });
    } catch (e) { }

    responseText = responseText.replace(/<("[^"]*"|'[^']*'|[^'">])*>/gi, '');
    responseText = responseText.replace(/[\n]+\s+/g, '\n');

    var readyStateText = xmlhttp.status === 0 ? '\n' + Drupal.t('ReadyState: !readyState', {
      '!readyState': xmlhttp.readyState
    }) : '';

    customMessage = customMessage ? '\n' + Drupal.t('CustomMessage: !customMessage', {
      '!customMessage': customMessage
    }) : '';

    this.message = statusCode + pathText + statusText + customMessage + responseText + readyStateText;

    this.name = 'AjaxError';
  };

  Drupal.AjaxError.prototype = new Error();
  Drupal.AjaxError.prototype.constructor = Drupal.AjaxError;

  Drupal.ajax = function (settings) {
    if (arguments.length !== 1) {
      throw new Error('Drupal.ajax() function must be called with one configuration object only');
    }

    var base = settings.base || false;
    var element = settings.element || false;
    delete settings.base;
    delete settings.element;

    if (!settings.progress && !element) {
      settings.progress = false;
    }

    var ajax = new Drupal.Ajax(base, element, settings);
    ajax.instanceIndex = Drupal.ajax.instances.length;
    Drupal.ajax.instances.push(ajax);

    return ajax;
  };

  Drupal.ajax.instances = [];

  Drupal.ajax.expired = function () {
    return Drupal.ajax.instances.filter(function (instance) {
      return instance && instance.element !== false && !document.body.contains(instance.element);
    });
  };

  Drupal.ajax.bindAjaxLinks = function (element) {
    $(element).find('.use-ajax').once('ajax').each(function (i, ajaxLink) {
      var $linkElement = $(ajaxLink);

      var elementSettings = {
        progress: { type: 'throbber' },
        dialogType: $linkElement.data('dialog-type'),
        dialog: $linkElement.data('dialog-options'),
        dialogRenderer: $linkElement.data('dialog-renderer'),
        base: $linkElement.attr('id'),
        element: ajaxLink
      };
      var href = $linkElement.attr('href');

      if (href) {
        elementSettings.url = href;
        elementSettings.event = 'click';
      }
      Drupal.ajax(elementSettings);
    });
  };

  Drupal.Ajax = function (base, element, elementSettings) {
    var defaults = {
      event: element ? 'mousedown' : null,
      keypress: true,
      selector: base ? '#' + base : null,
      effect: 'none',
      speed: 'none',
      method: 'replaceWith',
      progress: {
        type: 'throbber',
        message: Drupal.t('Please wait...')
      },
      submit: {
        js: true
      }
    };

    $.extend(this, defaults, elementSettings);

    this.commands = new Drupal.AjaxCommands();

    this.instanceIndex = false;

    if (this.wrapper) {
      this.wrapper = '#' + this.wrapper;
    }

    this.element = element;

    this.element_settings = elementSettings;

    this.elementSettings = elementSettings;

    if (this.element && this.element.form) {
      this.$form = $(this.element.form);
    }

    if (!this.url) {
      var $element = $(this.element);
      if ($element.is('a')) {
        this.url = $element.attr('href');
      } else if (this.element && element.form) {
        this.url = this.$form.attr('action');
      }
    }

    var originalUrl = this.url;

    this.url = this.url.replace(/\/nojs(\/|$|\?|#)/, '/ajax$1');

    if (drupalSettings.ajaxTrustedUrl[originalUrl]) {
      drupalSettings.ajaxTrustedUrl[this.url] = true;
    }

    var ajax = this;

    ajax.options = {
      url: ajax.url,
      data: ajax.submit,
      beforeSerialize: function beforeSerialize(elementSettings, options) {
        return ajax.beforeSerialize(elementSettings, options);
      },
      beforeSubmit: function beforeSubmit(formValues, elementSettings, options) {
        ajax.ajaxing = true;
        return ajax.beforeSubmit(formValues, elementSettings, options);
      },
      beforeSend: function beforeSend(xmlhttprequest, options) {
        ajax.ajaxing = true;
        return ajax.beforeSend(xmlhttprequest, options);
      },
      success: function success(response, status, xmlhttprequest) {
        if (typeof response === 'string') {
          response = $.parseJSON(response);
        }

        if (response !== null && !drupalSettings.ajaxTrustedUrl[ajax.url]) {
          if (xmlhttprequest.getResponseHeader('X-Drupal-Ajax-Token') !== '1') {
            var customMessage = Drupal.t('The response failed verification so will not be processed.');
            return ajax.error(xmlhttprequest, ajax.url, customMessage);
          }
        }

        return ajax.success(response, status);
      },
      complete: function complete(xmlhttprequest, status) {
        ajax.ajaxing = false;
        if (status === 'error' || status === 'parsererror') {
          return ajax.error(xmlhttprequest, ajax.url);
        }
      },

      dataType: 'json',
      type: 'POST'
    };

    if (elementSettings.dialog) {
      ajax.options.data.dialogOptions = elementSettings.dialog;
    }

    if (ajax.options.url.indexOf('?') === -1) {
      ajax.options.url += '?';
    } else {
      ajax.options.url += '&';
    }

    var wrapper = 'drupal_' + (elementSettings.dialogType || 'ajax');
    if (elementSettings.dialogRenderer) {
      wrapper += '.' + elementSettings.dialogRenderer;
    }
    ajax.options.url += Drupal.ajax.WRAPPER_FORMAT + '=' + wrapper;

    $(ajax.element).on(elementSettings.event, function (event) {
      if (!drupalSettings.ajaxTrustedUrl[ajax.url] && !Drupal.url.isLocal(ajax.url)) {
        throw new Error(Drupal.t('The callback URL is not local and not trusted: !url', {
          '!url': ajax.url
        }));
      }
      return ajax.eventResponse(this, event);
    });

    if (elementSettings.keypress) {
      $(ajax.element).on('keypress', function (event) {
        return ajax.keypressResponse(this, event);
      });
    }

    if (elementSettings.prevent) {
      $(ajax.element).on(elementSettings.prevent, false);
    }
  };

  Drupal.ajax.WRAPPER_FORMAT = '_wrapper_format';

  Drupal.Ajax.AJAX_REQUEST_PARAMETER = '_drupal_ajax';

  Drupal.Ajax.prototype.execute = function () {
    if (this.ajaxing) {
      return;
    }

    try {
      this.beforeSerialize(this.element, this.options);

      return $.ajax(this.options);
    } catch (e) {
      this.ajaxing = false;
      window.alert('An error occurred while attempting to process ' + this.options.url + ': ' + e.message);

      return $.Deferred().reject();
    }
  };

  Drupal.Ajax.prototype.keypressResponse = function (element, event) {
    var ajax = this;

    if (event.which === 13 || event.which === 32 && element.type !== 'text' && element.type !== 'textarea' && element.type !== 'tel' && element.type !== 'number') {
      event.preventDefault();
      event.stopPropagation();
      $(element).trigger(ajax.elementSettings.event);
    }
  };

  Drupal.Ajax.prototype.eventResponse = function (element, event) {
    event.preventDefault();
    event.stopPropagation();

    var ajax = this;

    if (ajax.ajaxing) {
      return;
    }

    try {
      if (ajax.$form) {
        if (ajax.setClick) {
          element.form.clk = element;
        }

        ajax.$form.ajaxSubmit(ajax.options);
      } else {
        ajax.beforeSerialize(ajax.element, ajax.options);
        $.ajax(ajax.options);
      }
    } catch (e) {
      ajax.ajaxing = false;
      window.alert('An error occurred while attempting to process ' + ajax.options.url + ': ' + e.message);
    }
  };

  Drupal.Ajax.prototype.beforeSerialize = function (element, options) {
    if (this.$form && document.body.contains(this.$form.get(0))) {
      var settings = this.settings || drupalSettings;
      Drupal.detachBehaviors(this.$form.get(0), settings, 'serialize');
    }

    options.data[Drupal.Ajax.AJAX_REQUEST_PARAMETER] = 1;

    var pageState = drupalSettings.ajaxPageState;
    options.data['ajax_page_state[theme]'] = pageState.theme;
    options.data['ajax_page_state[theme_token]'] = pageState.theme_token;
    options.data['ajax_page_state[libraries]'] = pageState.libraries;
  };

  Drupal.Ajax.prototype.beforeSubmit = function (formValues, element, options) { };

  Drupal.Ajax.prototype.beforeSend = function (xmlhttprequest, options) {
    if (this.$form) {
      options.extraData = options.extraData || {};

      options.extraData.ajax_iframe_upload = '1';

      var v = $.fieldValue(this.element);
      if (v !== null) {
        options.extraData[this.element.name] = v;
      }
    }

    $(this.element).prop('disabled', true);

    if (!this.progress || !this.progress.type) {
      return;
    }

    var progressIndicatorMethod = 'setProgressIndicator' + this.progress.type.slice(0, 1).toUpperCase() + this.progress.type.slice(1).toLowerCase();
    if (progressIndicatorMethod in this && typeof this[progressIndicatorMethod] === 'function') {
      this[progressIndicatorMethod].call(this);
    }
  };

  Drupal.theme.ajaxProgressThrobber = function (message) {
    var messageMarkup = typeof message === 'string' ? Drupal.theme('ajaxProgressMessage', message) : '';
    var throbber = '<div class="throbber">&nbsp;</div>';

    return '<div class="ajax-progress ajax-progress-throbber">' + throbber + messageMarkup + '</div>';
  };

  Drupal.theme.ajaxProgressIndicatorFullscreen = function () {
    return '<div class="ajax-progress ajax-progress-fullscreen">&nbsp;</div>';
  };

  Drupal.theme.ajaxProgressMessage = function (message) {
    return '<div class="message">' + message + '</div>';
  };

  Drupal.Ajax.prototype.setProgressIndicatorBar = function () {
    var progressBar = new Drupal.ProgressBar('ajax-progress-' + this.element.id, $.noop, this.progress.method, $.noop);
    if (this.progress.message) {
      progressBar.setProgress(-1, this.progress.message);
    }
    if (this.progress.url) {
      progressBar.startMonitoring(this.progress.url, this.progress.interval || 1500);
    }
    this.progress.element = $(progressBar.element).addClass('ajax-progress ajax-progress-bar');
    this.progress.object = progressBar;
    $(this.element).after(this.progress.element);
  };

  Drupal.Ajax.prototype.setProgressIndicatorThrobber = function () {
    this.progress.element = $(Drupal.theme('ajaxProgressThrobber', this.progress.message));
    $(this.element).after(this.progress.element);
  };

  Drupal.Ajax.prototype.setProgressIndicatorFullscreen = function () {
    this.progress.element = $(Drupal.theme('ajaxProgressIndicatorFullscreen'));
    $('body').after(this.progress.element);
  };

  Drupal.Ajax.prototype.success = function (response, status) {
    var _this = this;

    if (this.progress.element) {
      $(this.progress.element).remove();
    }
    if (this.progress.object) {
      this.progress.object.stopMonitoring();
    }
    $(this.element).prop('disabled', false);

    var elementParents = $(this.element).parents('[data-drupal-selector]').addBack().toArray();

    var focusChanged = false;
    Object.keys(response || {}).forEach(function (i) {
      if (response[i].command && _this.commands[response[i].command]) {
        _this.commands[response[i].command](_this, response[i], status);
        if (response[i].command === 'invoke' && response[i].method === 'focus') {
          focusChanged = true;
        }
      }
    });

    if (!focusChanged && this.element && !$(this.element).data('disable-refocus')) {
      var target = false;

      for (var n = elementParents.length - 1; !target && n >= 0; n--) {
        target = document.querySelector('[data-drupal-selector="' + elementParents[n].getAttribute('data-drupal-selector') + '"]');
      }

      if (target) {
        $(target).trigger('focus');
      }
    }

    if (this.$form && document.body.contains(this.$form.get(0))) {
      var settings = this.settings || drupalSettings;
      Drupal.attachBehaviors(this.$form.get(0), settings);
    }

    this.settings = null;
  };

  Drupal.Ajax.prototype.getEffect = function (response) {
    var type = response.effect || this.effect;
    var speed = response.speed || this.speed;

    var effect = {};
    if (type === 'none') {
      effect.showEffect = 'show';
      effect.hideEffect = 'hide';
      effect.showSpeed = '';
    } else if (type === 'fade') {
      effect.showEffect = 'fadeIn';
      effect.hideEffect = 'fadeOut';
      effect.showSpeed = speed;
    } else {
      effect.showEffect = type + 'Toggle';
      effect.hideEffect = type + 'Toggle';
      effect.showSpeed = speed;
    }

    return effect;
  };

  Drupal.Ajax.prototype.error = function (xmlhttprequest, uri, customMessage) {
    if (this.progress.element) {
      $(this.progress.element).remove();
    }
    if (this.progress.object) {
      this.progress.object.stopMonitoring();
    }

    $(this.wrapper).show();

    $(this.element).prop('disabled', false);

    if (this.$form && document.body.contains(this.$form.get(0))) {
      var settings = this.settings || drupalSettings;
      Drupal.attachBehaviors(this.$form.get(0), settings);
    }
    throw new Drupal.AjaxError(xmlhttprequest, uri, customMessage);
  };

  Drupal.theme.ajaxWrapperNewContent = function ($newContent, ajax, response) {
    return (response.effect || ajax.effect) !== 'none' && $newContent.filter(function (i) {
      return !($newContent[i].nodeName === '#comment' || $newContent[i].nodeName === '#text' && /^(\s|\n|\r)*$/.test($newContent[i].textContent));
    }).length > 1 ? Drupal.theme('ajaxWrapperMultipleRootElements', $newContent) : $newContent;
  };

  Drupal.theme.ajaxWrapperMultipleRootElements = function ($elements) {
    return $('<div></div>').append($elements);
  };

  Drupal.AjaxCommands = function () { };
  Drupal.AjaxCommands.prototype = {
    insert: function insert(ajax, response) {
      var $wrapper = response.selector ? $(response.selector) : $(ajax.wrapper);
      var method = response.method || ajax.method;
      var effect = ajax.getEffect(response);

      var settings = response.settings || ajax.settings || drupalSettings;

      var $newContent = $($.parseHTML(response.data, document, true));

      $newContent = Drupal.theme('ajaxWrapperNewContent', $newContent, ajax, response);

      switch (method) {
        case 'html':
        case 'replaceWith':
        case 'replaceAll':
        case 'empty':
        case 'remove':
          Drupal.detachBehaviors($wrapper.get(0), settings);
          break;
        default:
          break;
      }

      $wrapper[method]($newContent);

      if (effect.showEffect !== 'show') {
        $newContent.hide();
      }

      var $ajaxNewContent = $newContent.find('.ajax-new-content');
      if ($ajaxNewContent.length) {
        $ajaxNewContent.hide();
        $newContent.show();
        $ajaxNewContent[effect.showEffect](effect.showSpeed);
      } else if (effect.showEffect !== 'show') {
        $newContent[effect.showEffect](effect.showSpeed);
      }

      if ($newContent.parents('html').length) {
        $newContent.each(function (index, element) {
          if (element.nodeType === Node.ELEMENT_NODE) {
            Drupal.attachBehaviors(element, settings);
          }
        });
      }
    },
    remove: function remove(ajax, response, status) {
      var settings = response.settings || ajax.settings || drupalSettings;
      $(response.selector).each(function () {
        Drupal.detachBehaviors(this, settings);
      }).remove();
    },
    changed: function changed(ajax, response, status) {
      var $element = $(response.selector);
      if (!$element.hasClass('ajax-changed')) {
        $element.addClass('ajax-changed');
        if (response.asterisk) {
          $element.find(response.asterisk).append(' <abbr class="ajax-changed" title="' + Drupal.t('Changed') + '">*</abbr> ');
        }
      }
    },
    alert: function alert(ajax, response, status) {
      window.alert(response.text, response.title);
    },
    announce: function announce(ajax, response) {
      if (response.priority) {
        Drupal.announce(response.text, response.priority);
      } else {
        Drupal.announce(response.text);
      }
    },
    redirect: function redirect(ajax, response, status) {
      window.location = response.url;
    },
    css: function css(ajax, response, status) {
      $(response.selector).css(response.argument);
    },
    settings: function settings(ajax, response, status) {
      var ajaxSettings = drupalSettings.ajax;

      if (ajaxSettings) {
        Drupal.ajax.expired().forEach(function (instance) {

          if (instance.selector) {
            var selector = instance.selector.replace('#', '');
            if (selector in ajaxSettings) {
              delete ajaxSettings[selector];
            }
          }
        });
      }

      if (response.merge) {
        $.extend(true, drupalSettings, response.settings);
      } else {
        ajax.settings = response.settings;
      }
    },
    data: function data(ajax, response, status) {
      $(response.selector).data(response.name, response.value);
    },
    invoke: function invoke(ajax, response, status) {
      var $element = $(response.selector);
      $element[response.method].apply($element, _toConsumableArray(response.args));
    },
    restripe: function restripe(ajax, response, status) {
      $(response.selector).find('> tbody > tr:visible, > tr:visible').removeClass('odd even').filter(':even').addClass('odd').end().filter(':odd').addClass('even');
    },
    update_build_id: function update_build_id(ajax, response, status) {
      $('input[name="form_build_id"][value="' + response.old + '"]').val(response.new);
    },
    add_css: function add_css(ajax, response, status) {
      $('head').prepend(response.data);

      var match = void 0;
      var importMatch = /^@import url\("(.*)"\);$/gim;
      if (document.styleSheets[0].addImport && importMatch.test(response.data)) {
        importMatch.lastIndex = 0;
        do {
          match = importMatch.exec(response.data);
          document.styleSheets[0].addImport(match[1]);
        } while (match);
      }
    }
  };
})(jQuery, window, Drupal, drupalSettings);;
/**
 * @file
 * Extends methods from core/misc/ajax.js.
 */

(function ($, window, Drupal, drupalSettings) {

  /**
   * Attempts to find the closest glyphicon progress indicator.
   *
   * @param {jQuery|Element} element
   *   A DOM element.
   *
   * @returns {jQuery}
   *   A jQuery object.
   */
  Drupal.Ajax.prototype.findGlyphicon = function (element) {
    return $(element).closest('.form-item').find('.ajax-progress.glyphicon')
  };

  /**
   * Starts the spinning of the glyphicon progress indicator.
   *
   * @param {jQuery|Element} element
   *   A DOM element.
   * @param {string} [message]
   *   An optional message to display (tooltip) for the progress.
   *
   * @returns {jQuery}
   *   A jQuery object.
   */
  Drupal.Ajax.prototype.glyphiconStart = function (element, message) {
    var $glyphicon = this.findGlyphicon(element);
    if ($glyphicon[0]) {
      $glyphicon.addClass('glyphicon-spin');

      // Add any message as a tooltip to the glyphicon.
      if ($.fn.tooltip && drupalSettings.bootstrap.tooltip_enabled) {
        $glyphicon
          .removeAttr('data-toggle')
          .removeAttr('data-original-title')
          .removeAttr('title')
          .tooltip('destroy')
          ;

        if (message) {
          $glyphicon.attr('data-toggle', 'tooltip').attr('title', message).tooltip();
        }
      }

      // Append a message for screen readers.
      if (message) {
        $glyphicon.parent().append('<div class="sr-only message">' + message + '</div>');
      }
    }
    return $glyphicon;
  };

  /**
   * Stop the spinning of a glyphicon progress indicator.
   *
   * @param {jQuery|Element} element
   *   A DOM element.
   */
  Drupal.Ajax.prototype.glyphiconStop = function (element) {
    var $glyphicon = this.findGlyphicon(element);
    if ($glyphicon[0]) {
      $glyphicon.removeClass('glyphicon-spin');
      if ($.fn.tooltip && drupalSettings.bootstrap.tooltip_enabled) {
        $glyphicon
          .removeAttr('data-toggle')
          .removeAttr('data-original-title')
          .removeAttr('title')
          .tooltip('destroy')
          ;
      }
    }
  };

  /**
   * Sets the throbber progress indicator.
   */
  Drupal.Ajax.prototype.setProgressIndicatorThrobber = function () {
    var $element = $(this.element);

    // Find an existing glyphicon progress indicator.
    var $glyphicon = this.glyphiconStart($element, this.progress.message);
    if ($glyphicon[0]) {
      this.progress.element = $glyphicon.parent();
      this.progress.glyphicon = true;
      return;
    }

    // Otherwise, add a glyphicon throbber after the element.
    if (!this.progress.element) {
      this.progress.element = $(Drupal.theme('ajaxThrobber'));
    }
    if (this.progress.message) {
      this.progress.element.after('<div class="message">' + this.progress.message + '</div>');
    }

    // If element is an input DOM element type (not :input), append after.
    if ($element.is('input')) {
      $element.after(this.progress.element);
    }
    // Otherwise append the throbber inside the element.
    else {
      $element.append(this.progress.element);
    }
  };


  /**
   * Handler for the form redirection completion.
   *
   * @param {Array.<Drupal.AjaxCommands~commandDefinition>} response
   * @param {number} status
   */
  Drupal.Ajax.prototype.success = function (response, status) {
    if (this.progress.element) {

      // Stop a glyphicon throbber.
      if (this.progress.glyphicon) {
        this.glyphiconStop(this.progress.element);
      }
      // Remove the progress element.
      else {
        this.progress.element.remove();
      }

      // Remove any message set.
      this.progress.element.parent().find('.message').remove();
    }

    // --------------------------------------------------------
    // Everything below is from core/misc/ajax.js.
    // --------------------------------------------------------

    if (this.progress.object) {
      this.progress.object.stopMonitoring();
    }
    $(this.element).prop('disabled', false);

    // Save element's ancestors tree so if the element is removed from the dom
    // we can try to refocus one of its parents. Using addBack reverse the
    // result array, meaning that index 0 is the highest parent in the hierarchy
    // in this situation it is usually a <form> element.
    var elementParents = $(this.element).parents('[data-drupal-selector]').addBack().toArray();

    // Track if any command is altering the focus so we can avoid changing the
    // focus set by the Ajax command.
    var focusChanged = false;
    for (var i in response) {
      if (response.hasOwnProperty(i) && response[i].command && this.commands[response[i].command]) {
        this.commands[response[i].command](this, response[i], status);
        if (response[i].command === 'invoke' && response[i].method === 'focus') {
          focusChanged = true;
        }
      }
    }

    // If the focus hasn't be changed by the ajax commands, try to refocus the
    // triggering element or one of its parents if that element does not exist
    // anymore.
    if (!focusChanged && this.element && !$(this.element).data('disable-refocus')) {
      var target = false;

      for (var n = elementParents.length - 1; !target && n > 0; n--) {
        target = document.querySelector('[data-drupal-selector="' + elementParents[n].getAttribute('data-drupal-selector') + '"]');
      }

      if (target) {
        $(target).trigger('focus');
      }
    }

    // Reattach behaviors, if they were detached in beforeSerialize(). The
    // attachBehaviors() called on the new content from processing the response
    // commands is not sufficient, because behaviors from the entire form need
    // to be reattached.
    if (this.$form) {
      var settings = this.settings || drupalSettings;
      Drupal.attachBehaviors(this.$form.get(0), settings);
    }

    // Remove any response-specific settings so they don't get used on the next
    // call by mistake.
    this.settings = null;
  };

})(jQuery, this, Drupal, drupalSettings);
;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

Drupal.debounce = function (func, wait, immediate) {
  var timeout = void 0;
  var result = void 0;
  return function () {
    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var context = this;
    var later = function later() {
      timeout = null;
      if (!immediate) {
        result = func.apply(context, args);
      }
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) {
      result = func.apply(context, args);
    }
    return result;
  };
};;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function ($, Drupal, debounce) {
  var offsets = {
    top: 0,
    right: 0,
    bottom: 0,
    left: 0
  };

  function getRawOffset(el, edge) {
    var $el = $(el);
    var documentElement = document.documentElement;
    var displacement = 0;
    var horizontal = edge === 'left' || edge === 'right';

    var placement = $el.offset()[horizontal ? 'left' : 'top'];

    placement -= window['scroll' + (horizontal ? 'X' : 'Y')] || document.documentElement['scroll' + (horizontal ? 'Left' : 'Top')] || 0;

    switch (edge) {
      case 'top':
        displacement = placement + $el.outerHeight();
        break;

      case 'left':
        displacement = placement + $el.outerWidth();
        break;

      case 'bottom':
        displacement = documentElement.clientHeight - placement;
        break;

      case 'right':
        displacement = documentElement.clientWidth - placement;
        break;

      default:
        displacement = 0;
    }
    return displacement;
  }

  function calculateOffset(edge) {
    var edgeOffset = 0;
    var displacingElements = document.querySelectorAll('[data-offset-' + edge + ']');
    var n = displacingElements.length;
    for (var i = 0; i < n; i++) {
      var el = displacingElements[i];

      if (el.style.display === 'none') {
        continue;
      }

      var displacement = parseInt(el.getAttribute('data-offset-' + edge), 10);

      if (isNaN(displacement)) {
        displacement = getRawOffset(el, edge);
      }

      edgeOffset = Math.max(edgeOffset, displacement);
    }

    return edgeOffset;
  }

  function calculateOffsets() {
    return {
      top: calculateOffset('top'),
      right: calculateOffset('right'),
      bottom: calculateOffset('bottom'),
      left: calculateOffset('left')
    };
  }

  function displace(broadcast) {
    offsets = calculateOffsets();
    Drupal.displace.offsets = offsets;
    if (typeof broadcast === 'undefined' || broadcast) {
      $(document).trigger('drupalViewportOffsetChange', offsets);
    }
    return offsets;
  }

  Drupal.behaviors.drupalDisplace = {
    attach: function attach() {
      if (this.displaceProcessed) {
        return;
      }
      this.displaceProcessed = true;

      $(window).on('resize.drupalDisplace', debounce(displace, 200));
    }
  };

  Drupal.displace = displace;
  $.extend(Drupal.displace, {
    offsets: offsets,

    calculateOffset: calculateOffset
  });
})(jQuery, Drupal, Drupal.debounce);;
/**
 * @file
 * Bootstrap Modals.
 *
 * @param {jQuery} $
 * @param {Drupal} Drupal
 * @param {Drupal.bootstrap} Bootstrap
 * @param {Attributes} Attributes
 */
(function ($, Drupal, Bootstrap, Attributes) {
  'use strict';

  /**
   * Document jQuery object.
   *
   * @type {jQuery}
   */
  var $document = $(document);

  /**
   * Finds the first available and visible focusable input element.
   *
   * This is abstracted from the main code below so sub-themes can override
   * this method to return their own element if desired.
   *
   * @param {Modal} modal
   *   The Bootstrap modal instance.
   *
   * @return {jQuery}
   *   A jQuery object containing the element that should be focused. Note: if
   *   this object contains multiple elements, only the first visible one will
   *   be used.
   */
  Bootstrap.modalFindFocusableElement = function (modal) {
    return modal.$dialogBody.find(':input,:button,.btn').not('.visually-hidden,.sr-only');
  };

  $document.on('shown.bs.modal', function (e) {
    var $modal = $(e.target);
    var modal = $modal.data('bs.modal');

    // Focus the first input element found.
    if (modal && modal.options.focusInput) {
      var $focusable = Bootstrap.modalFindFocusableElement(modal);
      if ($focusable && $focusable[0]) {
        var $input = $focusable.filter(':visible:first').focus();

        // Select text if input is text.
        if (modal.options.selectText && $input.is(':text')) {
          $input[0].setSelectionRange(0, $input[0].value.length)
        }
      }
      else if (modal.$close.is(':visible')) {
        modal.$close.focus();
      }
    }
  });

  /**
   * Only process this once.
   */
  Bootstrap.once('modal', function (settings) {

    /**
     * Replace the Bootstrap Modal jQuery plugin definition.
     *
     * This adds a little bit of functionality so it works better with Drupal.
     */
    Bootstrap.replacePlugin('modal', function () {
      var BootstrapModal = this;

      // Override the Modal constructor.
      Bootstrap.Modal = function (element, options) {
        this.$body = $(document.body);
        this.$element = $(element);
        this.$dialog = this.$element.find('.modal-dialog');
        this.$header = this.$dialog.find('.modal-header');
        this.$title = this.$dialog.find('.modal-title');
        this.$close = this.$header.find('.close');
        this.$footer = this.$dialog.find('.modal-footer');
        this.$content = this.$dialog.find('.modal-content');
        this.$dialogBody = this.$dialog.find('.modal-body');
        this.$backdrop = null;
        this.isShown = null;
        this.originalBodyPad = null;
        this.scrollbarWidth = 0;
        this.ignoreBackdropClick = false;
        this.options = this.mapDialogOptions(options);
      };

      // Extend defaults to take into account for theme settings.
      Bootstrap.Modal.DEFAULTS = $.extend({}, BootstrapModal.DEFAULTS, {
        animation: !!settings.modal_animation,
        backdrop: settings.modal_backdrop === 'static' ? 'static' : !!settings.modal_backdrop,
        focusInput: !!settings.modal_focus_input,
        selectText: !!settings.modal_select_text,
        keyboard: !!settings.modal_keyboard,
        remote: null,
        show: !!settings.modal_show,
        size: settings.modal_size
      });

      // Copy over the original prototype methods.
      Bootstrap.Modal.prototype = BootstrapModal.prototype;

      /**
       * Handler for $.fn.modal('destroy').
       */
      Bootstrap.Modal.prototype.destroy = function () {
        this.hide();
        Drupal.detachBehaviors(this.$element[0]);
        this.$element.removeData('bs.modal').remove();
      };

      /**
       * Initialize the modal.
       */
      Bootstrap.Modal.prototype.init = function () {
        if (this.options.remote) {
          this.$content.load(this.options.remote, $.proxy(function () {
            this.$element.trigger('loaded.bs.modal');
          }, this));
        }
      };

      /**
       * Map dialog options.
       *
       * Note: this is primarily for use in modal.jquery.ui.bridge.js.
       *
       * @param {Object} options
       *   The passed options.
       */
      Bootstrap.Modal.prototype.mapDialogOptions = function (options) {
        return options || {};
      }

      // Modal jQuery Plugin Definition.
      var Plugin = function () {
        // Extract the arguments.
        var args = Array.prototype.slice.call(arguments);
        var method = args[0];
        var options = args[1] || {};
        var relatedTarget = args[2] || null;
        // Move arguments down if no method was passed.
        if ($.isPlainObject(method)) {
          relatedTarget = options || null;
          options = method;
          method = null;
        }
        var ret = void 0;
        this.each(function () {
          var $this = $(this);
          var data = $this.data('bs.modal');
          var initialize = false;

          // Immediately return if there's no instance to invoke a valid method.
          var showMethods = ['open', 'show', 'toggle'];
          if (!data && method && showMethods.indexOf(method) === -1) {
            return;
          }

          options = Bootstrap.normalizeObject($.extend({}, Bootstrap.Modal.DEFAULTS, data && data.options, $this.data(), options));
          delete options['bs.modal'];

          if (!data) {
            $this.data('bs.modal', (data = new Bootstrap.Modal(this, options)));
            initialize = true;
          }

          // Initialize the modal.
          if (initialize || (!method && !args.length)) {
            data.init();
          }

          // Explicit method passed.
          if (method) {
            if (typeof data[method] === 'function') {
              try {
                ret = data[method].apply(data, args.slice(1));
              }
              catch (e) {
                Drupal.throwError(e);
              }
            }
            else {
              Bootstrap.unsupported('method', method);
            }
          }
          // No method, set options and open if necessary.
          else {
            data.option(options);
            if (options.show && !data.isShown) {
              data.show(relatedTarget);
            }
          }
        });

        // If just one element and there was a result returned for the option passed,
        // then return the result. Otherwise, just return the jQuery object.
        return this.length === 1 && ret !== void 0 ? ret : this;
      };

      // Replace the plugin constructor with the new Modal constructor.
      Plugin.Constructor = Bootstrap.Modal;

      // Replace the data API so that it calls $.fn.modal rather than Plugin.
      // This allows sub-themes to replace the jQuery Plugin if they like with
      // out having to redo all this boilerplate.
      $document
        .off('click.bs.modal.data-api')
        .on('click.bs.modal.data-api', '[data-toggle="modal"]', function (e) {
          var $this = $(this);
          var href = $this.attr('href');
          var target = $this.attr('data-target') || (href && href.replace(/.*(?=#[^\s]+$)/, '')); // strip for ie7
          var $target = $document.find(target);
          var options = $target.data('bs.modal') ? 'toggle' : $.extend({ remote: !/#/.test(href) && href }, $target.data(), $this.data());

          if ($this.is('a')) e.preventDefault();

          $target.one('show.bs.modal', function (showEvent) {
            // Only register focus restorer if modal will actually get shown.
            if (showEvent.isDefaultPrevented()) return;
            $target.one('hidden.bs.modal', function () {
              $this.is(':visible') && $this.trigger('focus');
            });
          });
          $target.modal(options, this);
        });

      return Plugin;
    });

    /**
     * Extend Drupal theming functions.
     */
    $.extend(Drupal.theme, /** @lend Drupal.theme */ {
      /**
       * Theme function for a Bootstrap Modal.
       *
       * @param {Object} [variables]
       *   An object containing key/value pairs of variables.
       *
       * @return {string}
       *   The HTML for the modal.
       */
      bootstrapModal: function (variables) {
        var output = '';
        var settings = drupalSettings.bootstrap || {};
        var defaults = {
          attributes: {
            class: ['modal'],
            tabindex: -1,
            role: 'dialog'
          },
          body: '',
          closeButton: true,
          description: {
            attributes: {
              class: ['help-block']
            },
            content: null,
            position: 'before'
          },
          footer: '',
          id: 'drupal-modal',
          size: settings.modal_size ? settings.modal_size : '',
          title: {
            attributes: {
              class: ['modal-title']
            },
            content: Drupal.t('Loading...'),
            html: false,
            tag: 'h4'
          }
        };
        variables = $.extend(true, {}, defaults, variables);

        var attributes = Attributes.create(defaults.attributes).merge(variables.attributes);
        attributes.set('id', attributes.get('id', variables.id));

        if (settings.modal_animation) {
          attributes.addClass('fade');
        }

        // Build the modal wrapper.
        output += '<div' + attributes + '>';

        // Build the modal-dialog wrapper.
        output += Drupal.theme('bootstrapModalDialog', _.omit(variables, 'attributes'));

        // Close the modal wrapper.
        output += '</div>';

        // Return the constructed modal.
        return output;
      },

      /**
       * Theme function for a Bootstrap Modal dialog markup.
       *
       * @param {Object} [variables]
       *   An object containing key/value pairs of variables.
       *
       * @return {string}
       *   The HTML for the modal close button.
       */
      bootstrapModalDialog: function (variables) {
        var output = '';

        var defaults = {
          attributes: {
            class: ['modal-dialog'],
            role: 'document'
          },
          id: 'drupal-modal'
        };
        variables = $.extend(true, {}, defaults, variables);

        var attributes = Attributes.create(defaults.attributes).merge(variables.attributes);
        attributes.set('id', attributes.get('id', variables.id + '--dialog'));

        if (variables.size) {
          attributes.addClass(variables.size);
        }
        output += '<div' + attributes + '>';

        // Build the modal-content wrapper.
        output += Drupal.theme('bootstrapModalContent', _.omit(variables, 'attributes'));

        // Close the modal-dialog wrapper.
        output += '</div>';
        return output;
      },

      /**
       * Theme function for a Bootstrap Modal content markup.
       *
       * @param {Object} [variables]
       *   An object containing key/value pairs of variables.
       *
       * @return {string}
       *   The HTML for the modal close button.
       */
      bootstrapModalContent: function (variables) {
        var output = '';

        var defaults = {
          attributes: {
            class: ['modal-content']
          },
          id: 'drupal-modal'
        };
        variables = $.extend(true, {}, defaults, variables);

        var attributes = Attributes.create(defaults.attributes).merge(variables.attributes);
        attributes.set('id', attributes.get('id', variables.id + '--content'));

        // Build the modal-content wrapper.
        output += '<div' + attributes + '>';
        variables = _.omit(variables, 'attributes');

        // Build the header wrapper and title.
        output += Drupal.theme('bootstrapModalHeader', variables);

        // Build the body.
        output += Drupal.theme('bootstrapModalBody', variables);

        // Build the footer.
        output += Drupal.theme('bootstrapModalFooter', variables);

        // Close the modal-content wrapper.
        output += '</div>';

        return output;
      },

      /**
       * Theme function for a Bootstrap Modal body markup.
       *
       * @param {Object} [variables]
       *   An object containing key/value pairs of variables.
       *
       * @return {string}
       *   The HTML for the modal close button.
       */
      bootstrapModalBody: function (variables) {
        var output = '';

        var defaults = {
          attributes: {
            class: ['modal-body']
          },
          body: '',
          description: {
            attributes: {
              class: ['help-block']
            },
            content: null,
            position: 'before'
          },
          id: 'drupal-modal'
        };
        variables = $.extend(true, {}, defaults, variables);

        var attributes = Attributes.create(defaults.attributes).merge(variables.attributes);
        attributes.set('id', attributes.get('id', variables.id + '--body'));

        output += '<div' + attributes + '>';

        if (typeof variables.description === 'string') {
          variables.description = $.extend({}, defaults.description, { content: variables.description });
        }

        var description = variables.description;
        description.attributes = Attributes.create(defaults.description.attributes).merge(description.attributes);

        if (description.content && description.position === 'invisible') {
          description.attributes.addClass('sr-only');
        }

        if (description.content && description.position === 'before') {
          output += '<p' + description.attributes + '>' + description.content + '</p>';
        }

        output += variables.body;

        if (description.content && (description.position === 'after' || description.position === 'invisible')) {
          output += '<p' + description.attributes + '>' + description.content + '</p>';
        }

        output += '</div>';

        return output;
      },

      /**
       * Theme function for a Bootstrap Modal close button.
       *
       * @param {Object} [variables]
       *   An object containing key/value pairs of variables.
       *
       * @return {string}
       *   The HTML for the modal close button.
       */
      bootstrapModalClose: function (variables) {
        var defaults = {
          attributes: {
            'aria-label': Drupal.t('Close'),
            class: ['close'],
            'data-dismiss': 'modal',
            type: 'button'
          }
        };
        variables = $.extend(true, {}, defaults, variables);
        var attributes = Attributes.create(defaults.attributes).merge(variables.attributes);
        return '<button' + attributes + '><span aria-hidden="true">&times;</span></button>';
      },

      /**
       * Theme function for a Bootstrap Modal footer.
       *
       * @param {Object} [variables]
       *   An object containing key/value pairs of variables.
       * @param {boolean} [force]
       *   Flag to force rendering the footer, regardless if there's content.
       *
       * @return {string}
       *   The HTML for the modal footer.
       */
      bootstrapModalFooter: function (variables, force) {
        var output = '';
        var defaults = {
          attributes: {
            class: ['modal-footer']
          },
          footer: '',
          id: 'drupal-modal'
        };

        variables = $.extend(true, {}, defaults, variables);

        if (force || variables.footer) {
          var attributes = Attributes.create(defaults.attributes).merge(variables.attributes);
          attributes.set('id', attributes.get('id', variables.id + '--footer'));
          output += '<div' + attributes + '>';
          output += variables.footer;
          output += '</div>';
        }

        return output;
      },

      /**
       * Theme function for a Bootstrap Modal header.
       *
       * @param {Object} [variables]
       *   An object containing key/value pairs of variables.
       *
       * @return {string}
       *   The HTML for the modal header.
       */
      bootstrapModalHeader: function (variables) {
        var output = '';

        var defaults = {
          attributes: {
            class: ['modal-header']
          },
          closeButton: true,
          id: 'drupal-modal',
          title: {
            attributes: {
              class: ['modal-title']
            },
            content: Drupal.t('Loading...'),
            html: false,
            tag: 'h4'
          }
        };
        variables = $.extend(true, {}, defaults, variables);

        if (typeof variables.title === 'string') {
          variables.title = $.extend({}, defaults, { content: variables.title });
        }

        variables.title.attributes = Attributes.create(defaults.title.attributes).merge(variables.title.attributes);

        var title = Drupal.theme('bootstrapModalTitle', variables.title);
        if (title) {
          var attributes = Attributes.create(defaults.attributes).merge(variables.attributes);
          attributes.set('id', attributes.get('id', variables.id + '--header'));

          output += '<div' + attributes + '>';

          if (variables.closeButton) {
            output += Drupal.theme('bootstrapModalClose', _.omit(variables, 'attributes'));
          }

          output += title;

          output += '</div>';
        }

        return output;
      },

      /**
       * Theme function for a Bootstrap Modal title.
       *
       * @param {Object} [variables]
       *   An object containing key/value pairs of variables.
       *
       * @return {string}
       *   The HTML for the modal title.
       */
      bootstrapModalTitle: function (variables) {
        var output = '';

        var defaults = {
          attributes: {
            class: ['modal-title']
          },
          closeButton: true,
          id: 'drupal-modal',
          content: Drupal.t('Loading...'),
          html: false,
          tag: 'h4'
        };

        if (typeof variables === 'string') {
          variables = $.extend({}, defaults, { content: title });
        }

        variables = $.extend(true, {}, defaults, variables);

        var attributes = Attributes.create(defaults.attributes).merge(variables.attributes);
        attributes.set('id', attributes.get('id', variables.id + '--title'));

        output += '<' + Drupal.checkPlain(variables.tag) + Attributes.create(defaults.attributes).merge(variables.attributes) + '>';

        if (variables.closeButton) {
          output += Drupal.theme('bootstrapModalClose', _.omit(variables, 'attributes'));
        }

        output += (variables.html ? variables.content : Drupal.checkPlain(variables.content));

        output += '</' + Drupal.checkPlain(variables.tag) + '>';

        return output;
      }

    })

  });

})(window.jQuery, window.Drupal, window.Drupal.bootstrap, window.Attributes);
;
/**
 * @file
 * dialog.js
 */
(function ($, Drupal, Bootstrap, Attributes) {

  Bootstrap.Dialog = Bootstrap.Dialog || {};

  /**
   * A collection of Drupal dialog handlers.
   *
   * @type {Object<String, Drupal.bootstrap.Dialog.Handler>}
   */
  Bootstrap.Dialog.handlers = {};

  /**
   * @class Drupal.bootstrap.Dialog.Handler
   *
   * @param type
   * @param data
   */
  Bootstrap.Dialog.Handler = function (type, data) {
    this.ctor = $.fn.modal;
    this.extend = null;
    this.plugin = 'modal';
    this.prefix = 'modal';
    this.themeHooks = {
      modal: 'bootstrapModal',
      dialog: 'bootstrapModalDialog',
      header: 'bootstrapModalHeader',
      title: 'bootstrapModalTitle',
      close: 'bootstrapModalClose',
      content: 'bootstrapModalContent',
      body: 'bootstrapModalBody',
      footer: 'bootstrapModalFooter',
    };
    this.type = type;
    this.selectors = {
      dialog: '.modal-dialog',
      header: '.modal-header',
      title: '.modal-title',
      close: '.close',
      content: '.modal-content',
      body: '.modal-body',
      footer: '.modal-footer',
      buttons: '.modal-buttons'
    };

    // Extend the object with subclassed data.
    $.extend(this, data);

    // Extend the jQuery plugin.
    if (this.extend) {
      Bootstrap.extend(this.plugin, this.extend);
    }
  };

  /**
   * Retrieves a Drupal dialog type handler.
   *
   * @param {String|HTMLElement|jQuery} type
   *   The dialog type to retrieve.
   *
   * @return {Drupal.bootstrap.Dialog.Handler}
   *   A Bootstrap.Dialog.Handler instance.
   */
  Bootstrap.Dialog.Handler.get = function (type) {
    if (type instanceof $) {
      type = type[0];
    }
    if (type instanceof HTMLElement) {
      type = type.dialogType;
    }
    if (!type) {
      type = 'modal';
    }
    if (!Bootstrap.Dialog.handlers[type]) {
      Bootstrap.Dialog.handlers[type] = new Bootstrap.Dialog.Handler();
    }
    return Bootstrap.Dialog.handlers[type];
  };

  /**
   * Registers a Drupal dialog type handler.
   *
   * @param {String} type
   *   The dialog type to
   * @param {Object} [data]
   *   Optional. Additional data to use to create the dialog handler. By
   *   default, this assumes values relative to the Bootstrap Modal plugin.
   */
  Bootstrap.Dialog.Handler.register = function (type, data) {
    Bootstrap.Dialog.handlers[type] = new Bootstrap.Dialog.Handler(type, data);
  };

  Bootstrap.Dialog.Handler.prototype.invoke = function (context) {
    var args = Array.prototype.slice.call(arguments);
    return this.ctor.apply(context, args.slice(1));
  };

  Bootstrap.Dialog.Handler.prototype.theme = function (hook) {
    var args = Array.prototype.slice.call(arguments);
    return $(Drupal.theme.apply(Drupal.theme, [this.themeHooks[hook]].concat(args.slice(1))));
  };

  /**
   * Ensures a DOM element has the appropriate structure for a modal.
   *
   * Note: this can get a little tricky. Core potentially already
   * semi-processes a "dialog" if was created using an Ajax command
   * (i.e. prepareDialogButtons in drupal.ajax.js). Because of this, the
   * contents (HTML) of the existing element cannot simply be dumped into a
   * newly created modal. This would destroy any existing event bindings.
   * Instead, the contents must be "moved" (appended) to the new modal and
   * then "moved" again back to the to the existing container as needed.
   *
   * @param {HTMLElement|jQuery} element
   *   The element to ensure is a modal structure.
   * @param {Object} options
   *   THe dialog options to use to construct the modal.
   */
  Bootstrap.Dialog.Handler.prototype.ensureModalStructure = function (element, options) {
    var $element = $(element);

    // Immediately return if the modal was already converted into a proper modal.
    if ($element.is('[data-drupal-theme="' + this.themeHooks.modal + '"]')) {
      return;
    }

    options = $.extend(true, {}, options, {
      attributes: Attributes.create(element).remove('style').set('data-drupal-theme', this.themeHooks.modal),
    });

    // Create a new modal.
    var $modal = this.theme('modal', options);

    // Store a reference to the content inside the existing element container.
    // This references the actual DOM node elements which will allow
    // jQuery to "move" then when appending below. Using $.fn.children()
    // does not return any text nodes present and $.fn.html() only returns
    // a string representation of the content, which effectively destroys
    // any prior event bindings or processing.
    var $body = $element.find(this.selectors.body);
    var $existing = $body[0] ? $body.contents() : $element.contents();

    // Set the attributes of the dialog to that of the newly created modal.
    $element.attr(Attributes.create($modal).toPlainObject());

    // Append the newly created modal markup.
    $element.append($modal.html());

    // Move the existing HTML into the modal markup that was just appended.
    $element.find(this.selectors.body).append($existing);
  };

})(jQuery, Drupal, Drupal.bootstrap, Attributes);
;
/*! jQuery UI - v1.12.1 - 2017-03-31
* http://jqueryui.com
* Copyright jQuery Foundation and other contributors; Licensed  */
!function (a) { "function" == typeof define && define.amd ? define(["jquery", "../ie", "../version", "../widget"], a) : a(jQuery) }(function (a) { var b = !1; return a(document).on("mouseup", function () { b = !1 }), a.widget("ui.mouse", { version: "1.12.1", options: { cancel: "input, textarea, button, select, option", distance: 1, delay: 0 }, _mouseInit: function () { var b = this; this.element.on("mousedown." + this.widgetName, function (a) { return b._mouseDown(a) }).on("click." + this.widgetName, function (c) { if (!0 === a.data(c.target, b.widgetName + ".preventClickEvent")) return a.removeData(c.target, b.widgetName + ".preventClickEvent"), c.stopImmediatePropagation(), !1 }), this.started = !1 }, _mouseDestroy: function () { this.element.off("." + this.widgetName), this._mouseMoveDelegate && this.document.off("mousemove." + this.widgetName, this._mouseMoveDelegate).off("mouseup." + this.widgetName, this._mouseUpDelegate) }, _mouseDown: function (c) { if (!b) { this._mouseMoved = !1, this._mouseStarted && this._mouseUp(c), this._mouseDownEvent = c; var d = this, e = 1 === c.which, f = !("string" != typeof this.options.cancel || !c.target.nodeName) && a(c.target).closest(this.options.cancel).length; return !(e && !f && this._mouseCapture(c)) || (this.mouseDelayMet = !this.options.delay, this.mouseDelayMet || (this._mouseDelayTimer = setTimeout(function () { d.mouseDelayMet = !0 }, this.options.delay)), this._mouseDistanceMet(c) && this._mouseDelayMet(c) && (this._mouseStarted = this._mouseStart(c) !== !1, !this._mouseStarted) ? (c.preventDefault(), !0) : (!0 === a.data(c.target, this.widgetName + ".preventClickEvent") && a.removeData(c.target, this.widgetName + ".preventClickEvent"), this._mouseMoveDelegate = function (a) { return d._mouseMove(a) }, this._mouseUpDelegate = function (a) { return d._mouseUp(a) }, this.document.on("mousemove." + this.widgetName, this._mouseMoveDelegate).on("mouseup." + this.widgetName, this._mouseUpDelegate), c.preventDefault(), b = !0, !0)) } }, _mouseMove: function (b) { if (this._mouseMoved) { if (a.ui.ie && (!document.documentMode || document.documentMode < 9) && !b.button) return this._mouseUp(b); if (!b.which) if (b.originalEvent.altKey || b.originalEvent.ctrlKey || b.originalEvent.metaKey || b.originalEvent.shiftKey) this.ignoreMissingWhich = !0; else if (!this.ignoreMissingWhich) return this._mouseUp(b) } return (b.which || b.button) && (this._mouseMoved = !0), this._mouseStarted ? (this._mouseDrag(b), b.preventDefault()) : (this._mouseDistanceMet(b) && this._mouseDelayMet(b) && (this._mouseStarted = this._mouseStart(this._mouseDownEvent, b) !== !1, this._mouseStarted ? this._mouseDrag(b) : this._mouseUp(b)), !this._mouseStarted) }, _mouseUp: function (c) { this.document.off("mousemove." + this.widgetName, this._mouseMoveDelegate).off("mouseup." + this.widgetName, this._mouseUpDelegate), this._mouseStarted && (this._mouseStarted = !1, c.target === this._mouseDownEvent.target && a.data(c.target, this.widgetName + ".preventClickEvent", !0), this._mouseStop(c)), this._mouseDelayTimer && (clearTimeout(this._mouseDelayTimer), delete this._mouseDelayTimer), this.ignoreMissingWhich = !1, b = !1, c.preventDefault() }, _mouseDistanceMet: function (a) { return Math.max(Math.abs(this._mouseDownEvent.pageX - a.pageX), Math.abs(this._mouseDownEvent.pageY - a.pageY)) >= this.options.distance }, _mouseDelayMet: function () { return this.mouseDelayMet }, _mouseStart: function () { }, _mouseDrag: function () { }, _mouseStop: function () { }, _mouseCapture: function () { return !0 } }) });;
/*! jQuery UI - v1.12.1 - 2017-03-31
* http://jqueryui.com
* Copyright jQuery Foundation and other contributors; Licensed  */
!function (a) { "function" == typeof define && define.amd ? define(["jquery", "./mouse", "../data", "../plugin", "../safe-active-element", "../safe-blur", "../scroll-parent", "../version", "../widget"], a) : a(jQuery) }(function (a) { return a.widget("ui.draggable", a.ui.mouse, { version: "1.12.1", widgetEventPrefix: "drag", options: { addClasses: !0, appendTo: "parent", axis: !1, connectToSortable: !1, containment: !1, cursor: "auto", cursorAt: !1, grid: !1, handle: !1, helper: "original", iframeFix: !1, opacity: !1, refreshPositions: !1, revert: !1, revertDuration: 500, scope: "default", scroll: !0, scrollSensitivity: 20, scrollSpeed: 20, snap: !1, snapMode: "both", snapTolerance: 20, stack: !1, zIndex: !1, drag: null, start: null, stop: null }, _create: function () { "original" === this.options.helper && this._setPositionRelative(), this.options.addClasses && this._addClass("ui-draggable"), this._setHandleClassName(), this._mouseInit() }, _setOption: function (a, b) { this._super(a, b), "handle" === a && (this._removeHandleClassName(), this._setHandleClassName()) }, _destroy: function () { return (this.helper || this.element).is(".ui-draggable-dragging") ? void (this.destroyOnClear = !0) : (this._removeHandleClassName(), void this._mouseDestroy()) }, _mouseCapture: function (b) { var c = this.options; return !(this.helper || c.disabled || a(b.target).closest(".ui-resizable-handle").length > 0) && (this.handle = this._getHandle(b), !!this.handle && (this._blurActiveElement(b), this._blockFrames(c.iframeFix === !0 ? "iframe" : c.iframeFix), !0)) }, _blockFrames: function (b) { this.iframeBlocks = this.document.find(b).map(function () { var b = a(this); return a("<div>").css("position", "absolute").appendTo(b.parent()).outerWidth(b.outerWidth()).outerHeight(b.outerHeight()).offset(b.offset())[0] }) }, _unblockFrames: function () { this.iframeBlocks && (this.iframeBlocks.remove(), delete this.iframeBlocks) }, _blurActiveElement: function (b) { var c = a.ui.safeActiveElement(this.document[0]), d = a(b.target); d.closest(c).length || a.ui.safeBlur(c) }, _mouseStart: function (b) { var c = this.options; return this.helper = this._createHelper(b), this._addClass(this.helper, "ui-draggable-dragging"), this._cacheHelperProportions(), a.ui.ddmanager && (a.ui.ddmanager.current = this), this._cacheMargins(), this.cssPosition = this.helper.css("position"), this.scrollParent = this.helper.scrollParent(!0), this.offsetParent = this.helper.offsetParent(), this.hasFixedAncestor = this.helper.parents().filter(function () { return "fixed" === a(this).css("position") }).length > 0, this.positionAbs = this.element.offset(), this._refreshOffsets(b), this.originalPosition = this.position = this._generatePosition(b, !1), this.originalPageX = b.pageX, this.originalPageY = b.pageY, c.cursorAt && this._adjustOffsetFromHelper(c.cursorAt), this._setContainment(), this._trigger("start", b) === !1 ? (this._clear(), !1) : (this._cacheHelperProportions(), a.ui.ddmanager && !c.dropBehaviour && a.ui.ddmanager.prepareOffsets(this, b), this._mouseDrag(b, !0), a.ui.ddmanager && a.ui.ddmanager.dragStart(this, b), !0) }, _refreshOffsets: function (a) { this.offset = { top: this.positionAbs.top - this.margins.top, left: this.positionAbs.left - this.margins.left, scroll: !1, parent: this._getParentOffset(), relative: this._getRelativeOffset() }, this.offset.click = { left: a.pageX - this.offset.left, top: a.pageY - this.offset.top } }, _mouseDrag: function (b, c) { if (this.hasFixedAncestor && (this.offset.parent = this._getParentOffset()), this.position = this._generatePosition(b, !0), this.positionAbs = this._convertPositionTo("absolute"), !c) { var d = this._uiHash(); if (this._trigger("drag", b, d) === !1) return this._mouseUp(new a.Event("mouseup", b)), !1; this.position = d.position } return this.helper[0].style.left = this.position.left + "px", this.helper[0].style.top = this.position.top + "px", a.ui.ddmanager && a.ui.ddmanager.drag(this, b), !1 }, _mouseStop: function (b) { var c = this, d = !1; return a.ui.ddmanager && !this.options.dropBehaviour && (d = a.ui.ddmanager.drop(this, b)), this.dropped && (d = this.dropped, this.dropped = !1), "invalid" === this.options.revert && !d || "valid" === this.options.revert && d || this.options.revert === !0 || a.isFunction(this.options.revert) && this.options.revert.call(this.element, d) ? a(this.helper).animate(this.originalPosition, parseInt(this.options.revertDuration, 10), function () { c._trigger("stop", b) !== !1 && c._clear() }) : this._trigger("stop", b) !== !1 && this._clear(), !1 }, _mouseUp: function (b) { return this._unblockFrames(), a.ui.ddmanager && a.ui.ddmanager.dragStop(this, b), this.handleElement.is(b.target) && this.element.trigger("focus"), a.ui.mouse.prototype._mouseUp.call(this, b) }, cancel: function () { return this.helper.is(".ui-draggable-dragging") ? this._mouseUp(new a.Event("mouseup", { target: this.element[0] })) : this._clear(), this }, _getHandle: function (b) { return !this.options.handle || !!a(b.target).closest(this.element.find(this.options.handle)).length }, _setHandleClassName: function () { this.handleElement = this.options.handle ? this.element.find(this.options.handle) : this.element, this._addClass(this.handleElement, "ui-draggable-handle") }, _removeHandleClassName: function () { this._removeClass(this.handleElement, "ui-draggable-handle") }, _createHelper: function (b) { var c = this.options, d = a.isFunction(c.helper), e = d ? a(c.helper.apply(this.element[0], [b])) : "clone" === c.helper ? this.element.clone().removeAttr("id") : this.element; return e.parents("body").length || e.appendTo("parent" === c.appendTo ? this.element[0].parentNode : c.appendTo), d && e[0] === this.element[0] && this._setPositionRelative(), e[0] === this.element[0] || /(fixed|absolute)/.test(e.css("position")) || e.css("position", "absolute"), e }, _setPositionRelative: function () { /^(?:r|a|f)/.test(this.element.css("position")) || (this.element[0].style.position = "relative") }, _adjustOffsetFromHelper: function (b) { "string" == typeof b && (b = b.split(" ")), a.isArray(b) && (b = { left: +b[0], top: +b[1] || 0 }), "left" in b && (this.offset.click.left = b.left + this.margins.left), "right" in b && (this.offset.click.left = this.helperProportions.width - b.right + this.margins.left), "top" in b && (this.offset.click.top = b.top + this.margins.top), "bottom" in b && (this.offset.click.top = this.helperProportions.height - b.bottom + this.margins.top) }, _isRootNode: function (a) { return /(html|body)/i.test(a.tagName) || a === this.document[0] }, _getParentOffset: function () { var b = this.offsetParent.offset(), c = this.document[0]; return "absolute" === this.cssPosition && this.scrollParent[0] !== c && a.contains(this.scrollParent[0], this.offsetParent[0]) && (b.left += this.scrollParent.scrollLeft(), b.top += this.scrollParent.scrollTop()), this._isRootNode(this.offsetParent[0]) && (b = { top: 0, left: 0 }), { top: b.top + (parseInt(this.offsetParent.css("borderTopWidth"), 10) || 0), left: b.left + (parseInt(this.offsetParent.css("borderLeftWidth"), 10) || 0) } }, _getRelativeOffset: function () { if ("relative" !== this.cssPosition) return { top: 0, left: 0 }; var a = this.element.position(), b = this._isRootNode(this.scrollParent[0]); return { top: a.top - (parseInt(this.helper.css("top"), 10) || 0) + (b ? 0 : this.scrollParent.scrollTop()), left: a.left - (parseInt(this.helper.css("left"), 10) || 0) + (b ? 0 : this.scrollParent.scrollLeft()) } }, _cacheMargins: function () { this.margins = { left: parseInt(this.element.css("marginLeft"), 10) || 0, top: parseInt(this.element.css("marginTop"), 10) || 0, right: parseInt(this.element.css("marginRight"), 10) || 0, bottom: parseInt(this.element.css("marginBottom"), 10) || 0 } }, _cacheHelperProportions: function () { this.helperProportions = { width: this.helper.outerWidth(), height: this.helper.outerHeight() } }, _setContainment: function () { var b, c, d, e = this.options, f = this.document[0]; return this.relativeContainer = null, e.containment ? "window" === e.containment ? void (this.containment = [a(window).scrollLeft() - this.offset.relative.left - this.offset.parent.left, a(window).scrollTop() - this.offset.relative.top - this.offset.parent.top, a(window).scrollLeft() + a(window).width() - this.helperProportions.width - this.margins.left, a(window).scrollTop() + (a(window).height() || f.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top]) : "document" === e.containment ? void (this.containment = [0, 0, a(f).width() - this.helperProportions.width - this.margins.left, (a(f).height() || f.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top]) : e.containment.constructor === Array ? void (this.containment = e.containment) : ("parent" === e.containment && (e.containment = this.helper[0].parentNode), c = a(e.containment), d = c[0], void (d && (b = /(scroll|auto)/.test(c.css("overflow")), this.containment = [(parseInt(c.css("borderLeftWidth"), 10) || 0) + (parseInt(c.css("paddingLeft"), 10) || 0), (parseInt(c.css("borderTopWidth"), 10) || 0) + (parseInt(c.css("paddingTop"), 10) || 0), (b ? Math.max(d.scrollWidth, d.offsetWidth) : d.offsetWidth) - (parseInt(c.css("borderRightWidth"), 10) || 0) - (parseInt(c.css("paddingRight"), 10) || 0) - this.helperProportions.width - this.margins.left - this.margins.right, (b ? Math.max(d.scrollHeight, d.offsetHeight) : d.offsetHeight) - (parseInt(c.css("borderBottomWidth"), 10) || 0) - (parseInt(c.css("paddingBottom"), 10) || 0) - this.helperProportions.height - this.margins.top - this.margins.bottom], this.relativeContainer = c))) : void (this.containment = null) }, _convertPositionTo: function (a, b) { b || (b = this.position); var c = "absolute" === a ? 1 : -1, d = this._isRootNode(this.scrollParent[0]); return { top: b.top + this.offset.relative.top * c + this.offset.parent.top * c - ("fixed" === this.cssPosition ? -this.offset.scroll.top : d ? 0 : this.offset.scroll.top) * c, left: b.left + this.offset.relative.left * c + this.offset.parent.left * c - ("fixed" === this.cssPosition ? -this.offset.scroll.left : d ? 0 : this.offset.scroll.left) * c } }, _generatePosition: function (a, b) { var c, d, e, f, g = this.options, h = this._isRootNode(this.scrollParent[0]), i = a.pageX, j = a.pageY; return h && this.offset.scroll || (this.offset.scroll = { top: this.scrollParent.scrollTop(), left: this.scrollParent.scrollLeft() }), b && (this.containment && (this.relativeContainer ? (d = this.relativeContainer.offset(), c = [this.containment[0] + d.left, this.containment[1] + d.top, this.containment[2] + d.left, this.containment[3] + d.top]) : c = this.containment, a.pageX - this.offset.click.left < c[0] && (i = c[0] + this.offset.click.left), a.pageY - this.offset.click.top < c[1] && (j = c[1] + this.offset.click.top), a.pageX - this.offset.click.left > c[2] && (i = c[2] + this.offset.click.left), a.pageY - this.offset.click.top > c[3] && (j = c[3] + this.offset.click.top)), g.grid && (e = g.grid[1] ? this.originalPageY + Math.round((j - this.originalPageY) / g.grid[1]) * g.grid[1] : this.originalPageY, j = c ? e - this.offset.click.top >= c[1] || e - this.offset.click.top > c[3] ? e : e - this.offset.click.top >= c[1] ? e - g.grid[1] : e + g.grid[1] : e, f = g.grid[0] ? this.originalPageX + Math.round((i - this.originalPageX) / g.grid[0]) * g.grid[0] : this.originalPageX, i = c ? f - this.offset.click.left >= c[0] || f - this.offset.click.left > c[2] ? f : f - this.offset.click.left >= c[0] ? f - g.grid[0] : f + g.grid[0] : f), "y" === g.axis && (i = this.originalPageX), "x" === g.axis && (j = this.originalPageY)), { top: j - this.offset.click.top - this.offset.relative.top - this.offset.parent.top + ("fixed" === this.cssPosition ? -this.offset.scroll.top : h ? 0 : this.offset.scroll.top), left: i - this.offset.click.left - this.offset.relative.left - this.offset.parent.left + ("fixed" === this.cssPosition ? -this.offset.scroll.left : h ? 0 : this.offset.scroll.left) } }, _clear: function () { this._removeClass(this.helper, "ui-draggable-dragging"), this.helper[0] === this.element[0] || this.cancelHelperRemoval || this.helper.remove(), this.helper = null, this.cancelHelperRemoval = !1, this.destroyOnClear && this.destroy() }, _trigger: function (b, c, d) { return d = d || this._uiHash(), a.ui.plugin.call(this, b, [c, d, this], !0), /^(drag|start|stop)/.test(b) && (this.positionAbs = this._convertPositionTo("absolute"), d.offset = this.positionAbs), a.Widget.prototype._trigger.call(this, b, c, d) }, plugins: {}, _uiHash: function () { return { helper: this.helper, position: this.position, originalPosition: this.originalPosition, offset: this.positionAbs } } }), a.ui.plugin.add("draggable", "connectToSortable", { start: function (b, c, d) { var e = a.extend({}, c, { item: d.element }); d.sortables = [], a(d.options.connectToSortable).each(function () { var c = a(this).sortable("instance"); c && !c.options.disabled && (d.sortables.push(c), c.refreshPositions(), c._trigger("activate", b, e)) }) }, stop: function (b, c, d) { var e = a.extend({}, c, { item: d.element }); d.cancelHelperRemoval = !1, a.each(d.sortables, function () { var a = this; a.isOver ? (a.isOver = 0, d.cancelHelperRemoval = !0, a.cancelHelperRemoval = !1, a._storedCSS = { position: a.placeholder.css("position"), top: a.placeholder.css("top"), left: a.placeholder.css("left") }, a._mouseStop(b), a.options.helper = a.options._helper) : (a.cancelHelperRemoval = !0, a._trigger("deactivate", b, e)) }) }, drag: function (b, c, d) { a.each(d.sortables, function () { var e = !1, f = this; f.positionAbs = d.positionAbs, f.helperProportions = d.helperProportions, f.offset.click = d.offset.click, f._intersectsWith(f.containerCache) && (e = !0, a.each(d.sortables, function () { return this.positionAbs = d.positionAbs, this.helperProportions = d.helperProportions, this.offset.click = d.offset.click, this !== f && this._intersectsWith(this.containerCache) && a.contains(f.element[0], this.element[0]) && (e = !1), e })), e ? (f.isOver || (f.isOver = 1, d._parent = c.helper.parent(), f.currentItem = c.helper.appendTo(f.element).data("ui-sortable-item", !0), f.options._helper = f.options.helper, f.options.helper = function () { return c.helper[0] }, b.target = f.currentItem[0], f._mouseCapture(b, !0), f._mouseStart(b, !0, !0), f.offset.click.top = d.offset.click.top, f.offset.click.left = d.offset.click.left, f.offset.parent.left -= d.offset.parent.left - f.offset.parent.left, f.offset.parent.top -= d.offset.parent.top - f.offset.parent.top, d._trigger("toSortable", b), d.dropped = f.element, a.each(d.sortables, function () { this.refreshPositions() }), d.currentItem = d.element, f.fromOutside = d), f.currentItem && (f._mouseDrag(b), c.position = f.position)) : f.isOver && (f.isOver = 0, f.cancelHelperRemoval = !0, f.options._revert = f.options.revert, f.options.revert = !1, f._trigger("out", b, f._uiHash(f)), f._mouseStop(b, !0), f.options.revert = f.options._revert, f.options.helper = f.options._helper, f.placeholder && f.placeholder.remove(), c.helper.appendTo(d._parent), d._refreshOffsets(b), c.position = d._generatePosition(b, !0), d._trigger("fromSortable", b), d.dropped = !1, a.each(d.sortables, function () { this.refreshPositions() })) }) } }), a.ui.plugin.add("draggable", "cursor", { start: function (b, c, d) { var e = a("body"), f = d.options; e.css("cursor") && (f._cursor = e.css("cursor")), e.css("cursor", f.cursor) }, stop: function (b, c, d) { var e = d.options; e._cursor && a("body").css("cursor", e._cursor) } }), a.ui.plugin.add("draggable", "opacity", { start: function (b, c, d) { var e = a(c.helper), f = d.options; e.css("opacity") && (f._opacity = e.css("opacity")), e.css("opacity", f.opacity) }, stop: function (b, c, d) { var e = d.options; e._opacity && a(c.helper).css("opacity", e._opacity) } }), a.ui.plugin.add("draggable", "scroll", { start: function (a, b, c) { c.scrollParentNotHidden || (c.scrollParentNotHidden = c.helper.scrollParent(!1)), c.scrollParentNotHidden[0] !== c.document[0] && "HTML" !== c.scrollParentNotHidden[0].tagName && (c.overflowOffset = c.scrollParentNotHidden.offset()) }, drag: function (b, c, d) { var e = d.options, f = !1, g = d.scrollParentNotHidden[0], h = d.document[0]; g !== h && "HTML" !== g.tagName ? (e.axis && "x" === e.axis || (d.overflowOffset.top + g.offsetHeight - b.pageY < e.scrollSensitivity ? g.scrollTop = f = g.scrollTop + e.scrollSpeed : b.pageY - d.overflowOffset.top < e.scrollSensitivity && (g.scrollTop = f = g.scrollTop - e.scrollSpeed)), e.axis && "y" === e.axis || (d.overflowOffset.left + g.offsetWidth - b.pageX < e.scrollSensitivity ? g.scrollLeft = f = g.scrollLeft + e.scrollSpeed : b.pageX - d.overflowOffset.left < e.scrollSensitivity && (g.scrollLeft = f = g.scrollLeft - e.scrollSpeed))) : (e.axis && "x" === e.axis || (b.pageY - a(h).scrollTop() < e.scrollSensitivity ? f = a(h).scrollTop(a(h).scrollTop() - e.scrollSpeed) : a(window).height() - (b.pageY - a(h).scrollTop()) < e.scrollSensitivity && (f = a(h).scrollTop(a(h).scrollTop() + e.scrollSpeed))), e.axis && "y" === e.axis || (b.pageX - a(h).scrollLeft() < e.scrollSensitivity ? f = a(h).scrollLeft(a(h).scrollLeft() - e.scrollSpeed) : a(window).width() - (b.pageX - a(h).scrollLeft()) < e.scrollSensitivity && (f = a(h).scrollLeft(a(h).scrollLeft() + e.scrollSpeed)))), f !== !1 && a.ui.ddmanager && !e.dropBehaviour && a.ui.ddmanager.prepareOffsets(d, b) } }), a.ui.plugin.add("draggable", "snap", { start: function (b, c, d) { var e = d.options; d.snapElements = [], a(e.snap.constructor !== String ? e.snap.items || ":data(ui-draggable)" : e.snap).each(function () { var b = a(this), c = b.offset(); this !== d.element[0] && d.snapElements.push({ item: this, width: b.outerWidth(), height: b.outerHeight(), top: c.top, left: c.left }) }) }, drag: function (b, c, d) { var e, f, g, h, i, j, k, l, m, n, o = d.options, p = o.snapTolerance, q = c.offset.left, r = q + d.helperProportions.width, s = c.offset.top, t = s + d.helperProportions.height; for (m = d.snapElements.length - 1; m >= 0; m--)i = d.snapElements[m].left - d.margins.left, j = i + d.snapElements[m].width, k = d.snapElements[m].top - d.margins.top, l = k + d.snapElements[m].height, r < i - p || q > j + p || t < k - p || s > l + p || !a.contains(d.snapElements[m].item.ownerDocument, d.snapElements[m].item) ? (d.snapElements[m].snapping && d.options.snap.release && d.options.snap.release.call(d.element, b, a.extend(d._uiHash(), { snapItem: d.snapElements[m].item })), d.snapElements[m].snapping = !1) : ("inner" !== o.snapMode && (e = Math.abs(k - t) <= p, f = Math.abs(l - s) <= p, g = Math.abs(i - r) <= p, h = Math.abs(j - q) <= p, e && (c.position.top = d._convertPositionTo("relative", { top: k - d.helperProportions.height, left: 0 }).top), f && (c.position.top = d._convertPositionTo("relative", { top: l, left: 0 }).top), g && (c.position.left = d._convertPositionTo("relative", { top: 0, left: i - d.helperProportions.width }).left), h && (c.position.left = d._convertPositionTo("relative", { top: 0, left: j }).left)), n = e || f || g || h, "outer" !== o.snapMode && (e = Math.abs(k - s) <= p, f = Math.abs(l - t) <= p, g = Math.abs(i - q) <= p, h = Math.abs(j - r) <= p, e && (c.position.top = d._convertPositionTo("relative", { top: k, left: 0 }).top), f && (c.position.top = d._convertPositionTo("relative", { top: l - d.helperProportions.height, left: 0 }).top), g && (c.position.left = d._convertPositionTo("relative", { top: 0, left: i }).left), h && (c.position.left = d._convertPositionTo("relative", { top: 0, left: j - d.helperProportions.width }).left)), !d.snapElements[m].snapping && (e || f || g || h || n) && d.options.snap.snap && d.options.snap.snap.call(d.element, b, a.extend(d._uiHash(), { snapItem: d.snapElements[m].item })), d.snapElements[m].snapping = e || f || g || h || n) } }), a.ui.plugin.add("draggable", "stack", { start: function (b, c, d) { var e, f = d.options, g = a.makeArray(a(f.stack)).sort(function (b, c) { return (parseInt(a(b).css("zIndex"), 10) || 0) - (parseInt(a(c).css("zIndex"), 10) || 0) }); g.length && (e = parseInt(a(g[0]).css("zIndex"), 10) || 0, a(g).each(function (b) { a(this).css("zIndex", e + b) }), this.css("zIndex", e + g.length)) } }), a.ui.plugin.add("draggable", "zIndex", { start: function (b, c, d) { var e = a(c.helper), f = d.options; e.css("zIndex") && (f._zIndex = e.css("zIndex")), e.css("zIndex", f.zIndex) }, stop: function (b, c, d) { var e = d.options; e._zIndex && a(c.helper).css("zIndex", e._zIndex) } }), a.ui.draggable });;
/*! jQuery UI - v1.12.1 - 2017-03-31
* http://jqueryui.com
* Copyright jQuery Foundation and other contributors; Licensed  */
!function (a) { "function" == typeof define && define.amd ? define(["jquery", "./version"], a) : a(jQuery) }(function (a) { return function () { function b(a, b, c) { return [parseFloat(a[0]) * (l.test(a[0]) ? b / 100 : 1), parseFloat(a[1]) * (l.test(a[1]) ? c / 100 : 1)] } function c(b, c) { return parseInt(a.css(b, c), 10) || 0 } function d(b) { var c = b[0]; return 9 === c.nodeType ? { width: b.width(), height: b.height(), offset: { top: 0, left: 0 } } : a.isWindow(c) ? { width: b.width(), height: b.height(), offset: { top: b.scrollTop(), left: b.scrollLeft() } } : c.preventDefault ? { width: 0, height: 0, offset: { top: c.pageY, left: c.pageX } } : { width: b.outerWidth(), height: b.outerHeight(), offset: b.offset() } } var e, f = Math.max, g = Math.abs, h = /left|center|right/, i = /top|center|bottom/, j = /[\+\-]\d+(\.[\d]+)?%?/, k = /^\w+/, l = /%$/, m = a.fn.position; a.position = { scrollbarWidth: function () { if (void 0 !== e) return e; var b, c, d = a("<div style='display:block;position:absolute;width:50px;height:50px;overflow:hidden;'><div style='height:100px;width:auto;'></div></div>"), f = d.children()[0]; return a("body").append(d), b = f.offsetWidth, d.css("overflow", "scroll"), c = f.offsetWidth, b === c && (c = d[0].clientWidth), d.remove(), e = b - c }, getScrollInfo: function (b) { var c = b.isWindow || b.isDocument ? "" : b.element.css("overflow-x"), d = b.isWindow || b.isDocument ? "" : b.element.css("overflow-y"), e = "scroll" === c || "auto" === c && b.width < b.element[0].scrollWidth, f = "scroll" === d || "auto" === d && b.height < b.element[0].scrollHeight; return { width: f ? a.position.scrollbarWidth() : 0, height: e ? a.position.scrollbarWidth() : 0 } }, getWithinInfo: function (b) { var c = a(b || window), d = a.isWindow(c[0]), e = !!c[0] && 9 === c[0].nodeType, f = !d && !e; return { element: c, isWindow: d, isDocument: e, offset: f ? a(b).offset() : { left: 0, top: 0 }, scrollLeft: c.scrollLeft(), scrollTop: c.scrollTop(), width: c.outerWidth(), height: c.outerHeight() } } }, a.fn.position = function (e) { if (!e || !e.of) return m.apply(this, arguments); e = a.extend({}, e); var l, n, o, p, q, r, s = a(e.of), t = a.position.getWithinInfo(e.within), u = a.position.getScrollInfo(t), v = (e.collision || "flip").split(" "), w = {}; return r = d(s), s[0].preventDefault && (e.at = "left top"), n = r.width, o = r.height, p = r.offset, q = a.extend({}, p), a.each(["my", "at"], function () { var a, b, c = (e[this] || "").split(" "); 1 === c.length && (c = h.test(c[0]) ? c.concat(["center"]) : i.test(c[0]) ? ["center"].concat(c) : ["center", "center"]), c[0] = h.test(c[0]) ? c[0] : "center", c[1] = i.test(c[1]) ? c[1] : "center", a = j.exec(c[0]), b = j.exec(c[1]), w[this] = [a ? a[0] : 0, b ? b[0] : 0], e[this] = [k.exec(c[0])[0], k.exec(c[1])[0]] }), 1 === v.length && (v[1] = v[0]), "right" === e.at[0] ? q.left += n : "center" === e.at[0] && (q.left += n / 2), "bottom" === e.at[1] ? q.top += o : "center" === e.at[1] && (q.top += o / 2), l = b(w.at, n, o), q.left += l[0], q.top += l[1], this.each(function () { var d, h, i = a(this), j = i.outerWidth(), k = i.outerHeight(), m = c(this, "marginLeft"), r = c(this, "marginTop"), x = j + m + c(this, "marginRight") + u.width, y = k + r + c(this, "marginBottom") + u.height, z = a.extend({}, q), A = b(w.my, i.outerWidth(), i.outerHeight()); "right" === e.my[0] ? z.left -= j : "center" === e.my[0] && (z.left -= j / 2), "bottom" === e.my[1] ? z.top -= k : "center" === e.my[1] && (z.top -= k / 2), z.left += A[0], z.top += A[1], d = { marginLeft: m, marginTop: r }, a.each(["left", "top"], function (b, c) { a.ui.position[v[b]] && a.ui.position[v[b]][c](z, { targetWidth: n, targetHeight: o, elemWidth: j, elemHeight: k, collisionPosition: d, collisionWidth: x, collisionHeight: y, offset: [l[0] + A[0], l[1] + A[1]], my: e.my, at: e.at, within: t, elem: i }) }), e.using && (h = function (a) { var b = p.left - z.left, c = b + n - j, d = p.top - z.top, h = d + o - k, l = { target: { element: s, left: p.left, top: p.top, width: n, height: o }, element: { element: i, left: z.left, top: z.top, width: j, height: k }, horizontal: c < 0 ? "left" : b > 0 ? "right" : "center", vertical: h < 0 ? "top" : d > 0 ? "bottom" : "middle" }; n < j && g(b + c) < n && (l.horizontal = "center"), o < k && g(d + h) < o && (l.vertical = "middle"), f(g(b), g(c)) > f(g(d), g(h)) ? l.important = "horizontal" : l.important = "vertical", e.using.call(this, a, l) }), i.offset(a.extend(z, { using: h })) }) }, a.ui.position = { fit: { left: function (a, b) { var c, d = b.within, e = d.isWindow ? d.scrollLeft : d.offset.left, g = d.width, h = a.left - b.collisionPosition.marginLeft, i = e - h, j = h + b.collisionWidth - g - e; b.collisionWidth > g ? i > 0 && j <= 0 ? (c = a.left + i + b.collisionWidth - g - e, a.left += i - c) : j > 0 && i <= 0 ? a.left = e : i > j ? a.left = e + g - b.collisionWidth : a.left = e : i > 0 ? a.left += i : j > 0 ? a.left -= j : a.left = f(a.left - h, a.left) }, top: function (a, b) { var c, d = b.within, e = d.isWindow ? d.scrollTop : d.offset.top, g = b.within.height, h = a.top - b.collisionPosition.marginTop, i = e - h, j = h + b.collisionHeight - g - e; b.collisionHeight > g ? i > 0 && j <= 0 ? (c = a.top + i + b.collisionHeight - g - e, a.top += i - c) : j > 0 && i <= 0 ? a.top = e : i > j ? a.top = e + g - b.collisionHeight : a.top = e : i > 0 ? a.top += i : j > 0 ? a.top -= j : a.top = f(a.top - h, a.top) } }, flip: { left: function (a, b) { var c, d, e = b.within, f = e.offset.left + e.scrollLeft, h = e.width, i = e.isWindow ? e.scrollLeft : e.offset.left, j = a.left - b.collisionPosition.marginLeft, k = j - i, l = j + b.collisionWidth - h - i, m = "left" === b.my[0] ? -b.elemWidth : "right" === b.my[0] ? b.elemWidth : 0, n = "left" === b.at[0] ? b.targetWidth : "right" === b.at[0] ? -b.targetWidth : 0, o = -2 * b.offset[0]; k < 0 ? (c = a.left + m + n + o + b.collisionWidth - h - f, (c < 0 || c < g(k)) && (a.left += m + n + o)) : l > 0 && (d = a.left - b.collisionPosition.marginLeft + m + n + o - i, (d > 0 || g(d) < l) && (a.left += m + n + o)) }, top: function (a, b) { var c, d, e = b.within, f = e.offset.top + e.scrollTop, h = e.height, i = e.isWindow ? e.scrollTop : e.offset.top, j = a.top - b.collisionPosition.marginTop, k = j - i, l = j + b.collisionHeight - h - i, m = "top" === b.my[1], n = m ? -b.elemHeight : "bottom" === b.my[1] ? b.elemHeight : 0, o = "top" === b.at[1] ? b.targetHeight : "bottom" === b.at[1] ? -b.targetHeight : 0, p = -2 * b.offset[1]; k < 0 ? (d = a.top + n + o + p + b.collisionHeight - h - f, (d < 0 || d < g(k)) && (a.top += n + o + p)) : l > 0 && (c = a.top - b.collisionPosition.marginTop + n + o + p - i, (c > 0 || g(c) < l) && (a.top += n + o + p)) } }, flipfit: { left: function () { a.ui.position.flip.left.apply(this, arguments), a.ui.position.fit.left.apply(this, arguments) }, top: function () { a.ui.position.flip.top.apply(this, arguments), a.ui.position.fit.top.apply(this, arguments) } } } }(), a.ui.position });;
/*! jQuery UI - v1.12.1 - 2017-03-31
* http://jqueryui.com
* Copyright jQuery Foundation and other contributors; Licensed  */
!function (a) { "function" == typeof define && define.amd ? define(["jquery", "./mouse", "../disable-selection", "../plugin", "../version", "../widget"], a) : a(jQuery) }(function (a) { return a.widget("ui.resizable", a.ui.mouse, { version: "1.12.1", widgetEventPrefix: "resize", options: { alsoResize: !1, animate: !1, animateDuration: "slow", animateEasing: "swing", aspectRatio: !1, autoHide: !1, classes: { "ui-resizable-se": "ui-icon ui-icon-gripsmall-diagonal-se" }, containment: !1, ghost: !1, grid: !1, handles: "e,s,se", helper: !1, maxHeight: null, maxWidth: null, minHeight: 10, minWidth: 10, zIndex: 90, resize: null, start: null, stop: null }, _num: function (a) { return parseFloat(a) || 0 }, _isNumber: function (a) { return !isNaN(parseFloat(a)) }, _hasScroll: function (b, c) { if ("hidden" === a(b).css("overflow")) return !1; var d = c && "left" === c ? "scrollLeft" : "scrollTop", e = !1; return b[d] > 0 || (b[d] = 1, e = b[d] > 0, b[d] = 0, e) }, _create: function () { var b, c = this.options, d = this; this._addClass("ui-resizable"), a.extend(this, { _aspectRatio: !!c.aspectRatio, aspectRatio: c.aspectRatio, originalElement: this.element, _proportionallyResizeElements: [], _helper: c.helper || c.ghost || c.animate ? c.helper || "ui-resizable-helper" : null }), this.element[0].nodeName.match(/^(canvas|textarea|input|select|button|img)$/i) && (this.element.wrap(a("<div class='ui-wrapper' style='overflow: hidden;'></div>").css({ position: this.element.css("position"), width: this.element.outerWidth(), height: this.element.outerHeight(), top: this.element.css("top"), left: this.element.css("left") })), this.element = this.element.parent().data("ui-resizable", this.element.resizable("instance")), this.elementIsWrapper = !0, b = { marginTop: this.originalElement.css("marginTop"), marginRight: this.originalElement.css("marginRight"), marginBottom: this.originalElement.css("marginBottom"), marginLeft: this.originalElement.css("marginLeft") }, this.element.css(b), this.originalElement.css("margin", 0), this.originalResizeStyle = this.originalElement.css("resize"), this.originalElement.css("resize", "none"), this._proportionallyResizeElements.push(this.originalElement.css({ position: "static", zoom: 1, display: "block" })), this.originalElement.css(b), this._proportionallyResize()), this._setupHandles(), c.autoHide && a(this.element).on("mouseenter", function () { c.disabled || (d._removeClass("ui-resizable-autohide"), d._handles.show()) }).on("mouseleave", function () { c.disabled || d.resizing || (d._addClass("ui-resizable-autohide"), d._handles.hide()) }), this._mouseInit() }, _destroy: function () { this._mouseDestroy(); var b, c = function (b) { a(b).removeData("resizable").removeData("ui-resizable").off(".resizable").find(".ui-resizable-handle").remove() }; return this.elementIsWrapper && (c(this.element), b = this.element, this.originalElement.css({ position: b.css("position"), width: b.outerWidth(), height: b.outerHeight(), top: b.css("top"), left: b.css("left") }).insertAfter(b), b.remove()), this.originalElement.css("resize", this.originalResizeStyle), c(this.originalElement), this }, _setOption: function (a, b) { switch (this._super(a, b), a) { case "handles": this._removeHandles(), this._setupHandles() } }, _setupHandles: function () { var b, c, d, e, f, g = this.options, h = this; if (this.handles = g.handles || (a(".ui-resizable-handle", this.element).length ? { n: ".ui-resizable-n", e: ".ui-resizable-e", s: ".ui-resizable-s", w: ".ui-resizable-w", se: ".ui-resizable-se", sw: ".ui-resizable-sw", ne: ".ui-resizable-ne", nw: ".ui-resizable-nw" } : "e,s,se"), this._handles = a(), this.handles.constructor === String) for ("all" === this.handles && (this.handles = "n,e,s,w,se,sw,ne,nw"), d = this.handles.split(","), this.handles = {}, c = 0; c < d.length; c++)b = a.trim(d[c]), e = "ui-resizable-" + b, f = a("<div>"), this._addClass(f, "ui-resizable-handle " + e), f.css({ zIndex: g.zIndex }), this.handles[b] = ".ui-resizable-" + b, this.element.append(f); this._renderAxis = function (b) { var c, d, e, f; b = b || this.element; for (c in this.handles) this.handles[c].constructor === String ? this.handles[c] = this.element.children(this.handles[c]).first().show() : (this.handles[c].jquery || this.handles[c].nodeType) && (this.handles[c] = a(this.handles[c]), this._on(this.handles[c], { mousedown: h._mouseDown })), this.elementIsWrapper && this.originalElement[0].nodeName.match(/^(textarea|input|select|button)$/i) && (d = a(this.handles[c], this.element), f = /sw|ne|nw|se|n|s/.test(c) ? d.outerHeight() : d.outerWidth(), e = ["padding", /ne|nw|n/.test(c) ? "Top" : /se|sw|s/.test(c) ? "Bottom" : /^e$/.test(c) ? "Right" : "Left"].join(""), b.css(e, f), this._proportionallyResize()), this._handles = this._handles.add(this.handles[c]) }, this._renderAxis(this.element), this._handles = this._handles.add(this.element.find(".ui-resizable-handle")), this._handles.disableSelection(), this._handles.on("mouseover", function () { h.resizing || (this.className && (f = this.className.match(/ui-resizable-(se|sw|ne|nw|n|e|s|w)/i)), h.axis = f && f[1] ? f[1] : "se") }), g.autoHide && (this._handles.hide(), this._addClass("ui-resizable-autohide")) }, _removeHandles: function () { this._handles.remove() }, _mouseCapture: function (b) { var c, d, e = !1; for (c in this.handles) d = a(this.handles[c])[0], (d === b.target || a.contains(d, b.target)) && (e = !0); return !this.options.disabled && e }, _mouseStart: function (b) { var c, d, e, f = this.options, g = this.element; return this.resizing = !0, this._renderProxy(), c = this._num(this.helper.css("left")), d = this._num(this.helper.css("top")), f.containment && (c += a(f.containment).scrollLeft() || 0, d += a(f.containment).scrollTop() || 0), this.offset = this.helper.offset(), this.position = { left: c, top: d }, this.size = this._helper ? { width: this.helper.width(), height: this.helper.height() } : { width: g.width(), height: g.height() }, this.originalSize = this._helper ? { width: g.outerWidth(), height: g.outerHeight() } : { width: g.width(), height: g.height() }, this.sizeDiff = { width: g.outerWidth() - g.width(), height: g.outerHeight() - g.height() }, this.originalPosition = { left: c, top: d }, this.originalMousePosition = { left: b.pageX, top: b.pageY }, this.aspectRatio = "number" == typeof f.aspectRatio ? f.aspectRatio : this.originalSize.width / this.originalSize.height || 1, e = a(".ui-resizable-" + this.axis).css("cursor"), a("body").css("cursor", "auto" === e ? this.axis + "-resize" : e), this._addClass("ui-resizable-resizing"), this._propagate("start", b), !0 }, _mouseDrag: function (b) { var c, d, e = this.originalMousePosition, f = this.axis, g = b.pageX - e.left || 0, h = b.pageY - e.top || 0, i = this._change[f]; return this._updatePrevProperties(), !!i && (c = i.apply(this, [b, g, h]), this._updateVirtualBoundaries(b.shiftKey), (this._aspectRatio || b.shiftKey) && (c = this._updateRatio(c, b)), c = this._respectSize(c, b), this._updateCache(c), this._propagate("resize", b), d = this._applyChanges(), !this._helper && this._proportionallyResizeElements.length && this._proportionallyResize(), a.isEmptyObject(d) || (this._updatePrevProperties(), this._trigger("resize", b, this.ui()), this._applyChanges()), !1) }, _mouseStop: function (b) { this.resizing = !1; var c, d, e, f, g, h, i, j = this.options, k = this; return this._helper && (c = this._proportionallyResizeElements, d = c.length && /textarea/i.test(c[0].nodeName), e = d && this._hasScroll(c[0], "left") ? 0 : k.sizeDiff.height, f = d ? 0 : k.sizeDiff.width, g = { width: k.helper.width() - f, height: k.helper.height() - e }, h = parseFloat(k.element.css("left")) + (k.position.left - k.originalPosition.left) || null, i = parseFloat(k.element.css("top")) + (k.position.top - k.originalPosition.top) || null, j.animate || this.element.css(a.extend(g, { top: i, left: h })), k.helper.height(k.size.height), k.helper.width(k.size.width), this._helper && !j.animate && this._proportionallyResize()), a("body").css("cursor", "auto"), this._removeClass("ui-resizable-resizing"), this._propagate("stop", b), this._helper && this.helper.remove(), !1 }, _updatePrevProperties: function () { this.prevPosition = { top: this.position.top, left: this.position.left }, this.prevSize = { width: this.size.width, height: this.size.height } }, _applyChanges: function () { var a = {}; return this.position.top !== this.prevPosition.top && (a.top = this.position.top + "px"), this.position.left !== this.prevPosition.left && (a.left = this.position.left + "px"), this.size.width !== this.prevSize.width && (a.width = this.size.width + "px"), this.size.height !== this.prevSize.height && (a.height = this.size.height + "px"), this.helper.css(a), a }, _updateVirtualBoundaries: function (a) { var b, c, d, e, f, g = this.options; f = { minWidth: this._isNumber(g.minWidth) ? g.minWidth : 0, maxWidth: this._isNumber(g.maxWidth) ? g.maxWidth : 1 / 0, minHeight: this._isNumber(g.minHeight) ? g.minHeight : 0, maxHeight: this._isNumber(g.maxHeight) ? g.maxHeight : 1 / 0 }, (this._aspectRatio || a) && (b = f.minHeight * this.aspectRatio, d = f.minWidth / this.aspectRatio, c = f.maxHeight * this.aspectRatio, e = f.maxWidth / this.aspectRatio, b > f.minWidth && (f.minWidth = b), d > f.minHeight && (f.minHeight = d), c < f.maxWidth && (f.maxWidth = c), e < f.maxHeight && (f.maxHeight = e)), this._vBoundaries = f }, _updateCache: function (a) { this.offset = this.helper.offset(), this._isNumber(a.left) && (this.position.left = a.left), this._isNumber(a.top) && (this.position.top = a.top), this._isNumber(a.height) && (this.size.height = a.height), this._isNumber(a.width) && (this.size.width = a.width) }, _updateRatio: function (a) { var b = this.position, c = this.size, d = this.axis; return this._isNumber(a.height) ? a.width = a.height * this.aspectRatio : this._isNumber(a.width) && (a.height = a.width / this.aspectRatio), "sw" === d && (a.left = b.left + (c.width - a.width), a.top = null), "nw" === d && (a.top = b.top + (c.height - a.height), a.left = b.left + (c.width - a.width)), a }, _respectSize: function (a) { var b = this._vBoundaries, c = this.axis, d = this._isNumber(a.width) && b.maxWidth && b.maxWidth < a.width, e = this._isNumber(a.height) && b.maxHeight && b.maxHeight < a.height, f = this._isNumber(a.width) && b.minWidth && b.minWidth > a.width, g = this._isNumber(a.height) && b.minHeight && b.minHeight > a.height, h = this.originalPosition.left + this.originalSize.width, i = this.originalPosition.top + this.originalSize.height, j = /sw|nw|w/.test(c), k = /nw|ne|n/.test(c); return f && (a.width = b.minWidth), g && (a.height = b.minHeight), d && (a.width = b.maxWidth), e && (a.height = b.maxHeight), f && j && (a.left = h - b.minWidth), d && j && (a.left = h - b.maxWidth), g && k && (a.top = i - b.minHeight), e && k && (a.top = i - b.maxHeight), a.width || a.height || a.left || !a.top ? a.width || a.height || a.top || !a.left || (a.left = null) : a.top = null, a }, _getPaddingPlusBorderDimensions: function (a) { for (var b = 0, c = [], d = [a.css("borderTopWidth"), a.css("borderRightWidth"), a.css("borderBottomWidth"), a.css("borderLeftWidth")], e = [a.css("paddingTop"), a.css("paddingRight"), a.css("paddingBottom"), a.css("paddingLeft")]; b < 4; b++)c[b] = parseFloat(d[b]) || 0, c[b] += parseFloat(e[b]) || 0; return { height: c[0] + c[2], width: c[1] + c[3] } }, _proportionallyResize: function () { if (this._proportionallyResizeElements.length) for (var a, b = 0, c = this.helper || this.element; b < this._proportionallyResizeElements.length; b++)a = this._proportionallyResizeElements[b], this.outerDimensions || (this.outerDimensions = this._getPaddingPlusBorderDimensions(a)), a.css({ height: c.height() - this.outerDimensions.height || 0, width: c.width() - this.outerDimensions.width || 0 }) }, _renderProxy: function () { var b = this.element, c = this.options; this.elementOffset = b.offset(), this._helper ? (this.helper = this.helper || a("<div style='overflow:hidden;'></div>"), this._addClass(this.helper, this._helper), this.helper.css({ width: this.element.outerWidth(), height: this.element.outerHeight(), position: "absolute", left: this.elementOffset.left + "px", top: this.elementOffset.top + "px", zIndex: ++c.zIndex }), this.helper.appendTo("body").disableSelection()) : this.helper = this.element }, _change: { e: function (a, b) { return { width: this.originalSize.width + b } }, w: function (a, b) { var c = this.originalSize, d = this.originalPosition; return { left: d.left + b, width: c.width - b } }, n: function (a, b, c) { var d = this.originalSize, e = this.originalPosition; return { top: e.top + c, height: d.height - c } }, s: function (a, b, c) { return { height: this.originalSize.height + c } }, se: function (b, c, d) { return a.extend(this._change.s.apply(this, arguments), this._change.e.apply(this, [b, c, d])) }, sw: function (b, c, d) { return a.extend(this._change.s.apply(this, arguments), this._change.w.apply(this, [b, c, d])) }, ne: function (b, c, d) { return a.extend(this._change.n.apply(this, arguments), this._change.e.apply(this, [b, c, d])) }, nw: function (b, c, d) { return a.extend(this._change.n.apply(this, arguments), this._change.w.apply(this, [b, c, d])) } }, _propagate: function (b, c) { a.ui.plugin.call(this, b, [c, this.ui()]), "resize" !== b && this._trigger(b, c, this.ui()) }, plugins: {}, ui: function () { return { originalElement: this.originalElement, element: this.element, helper: this.helper, position: this.position, size: this.size, originalSize: this.originalSize, originalPosition: this.originalPosition } } }), a.ui.plugin.add("resizable", "animate", { stop: function (b) { var c = a(this).resizable("instance"), d = c.options, e = c._proportionallyResizeElements, f = e.length && /textarea/i.test(e[0].nodeName), g = f && c._hasScroll(e[0], "left") ? 0 : c.sizeDiff.height, h = f ? 0 : c.sizeDiff.width, i = { width: c.size.width - h, height: c.size.height - g }, j = parseFloat(c.element.css("left")) + (c.position.left - c.originalPosition.left) || null, k = parseFloat(c.element.css("top")) + (c.position.top - c.originalPosition.top) || null; c.element.animate(a.extend(i, k && j ? { top: k, left: j } : {}), { duration: d.animateDuration, easing: d.animateEasing, step: function () { var d = { width: parseFloat(c.element.css("width")), height: parseFloat(c.element.css("height")), top: parseFloat(c.element.css("top")), left: parseFloat(c.element.css("left")) }; e && e.length && a(e[0]).css({ width: d.width, height: d.height }), c._updateCache(d), c._propagate("resize", b) } }) } }), a.ui.plugin.add("resizable", "containment", { start: function () { var b, c, d, e, f, g, h, i = a(this).resizable("instance"), j = i.options, k = i.element, l = j.containment, m = l instanceof a ? l.get(0) : /parent/.test(l) ? k.parent().get(0) : l; m && (i.containerElement = a(m), /document/.test(l) || l === document ? (i.containerOffset = { left: 0, top: 0 }, i.containerPosition = { left: 0, top: 0 }, i.parentData = { element: a(document), left: 0, top: 0, width: a(document).width(), height: a(document).height() || document.body.parentNode.scrollHeight }) : (b = a(m), c = [], a(["Top", "Right", "Left", "Bottom"]).each(function (a, d) { c[a] = i._num(b.css("padding" + d)) }), i.containerOffset = b.offset(), i.containerPosition = b.position(), i.containerSize = { height: b.innerHeight() - c[3], width: b.innerWidth() - c[1] }, d = i.containerOffset, e = i.containerSize.height, f = i.containerSize.width, g = i._hasScroll(m, "left") ? m.scrollWidth : f, h = i._hasScroll(m) ? m.scrollHeight : e, i.parentData = { element: m, left: d.left, top: d.top, width: g, height: h })) }, resize: function (b) { var c, d, e, f, g = a(this).resizable("instance"), h = g.options, i = g.containerOffset, j = g.position, k = g._aspectRatio || b.shiftKey, l = { top: 0, left: 0 }, m = g.containerElement, n = !0; m[0] !== document && /static/.test(m.css("position")) && (l = i), j.left < (g._helper ? i.left : 0) && (g.size.width = g.size.width + (g._helper ? g.position.left - i.left : g.position.left - l.left), k && (g.size.height = g.size.width / g.aspectRatio, n = !1), g.position.left = h.helper ? i.left : 0), j.top < (g._helper ? i.top : 0) && (g.size.height = g.size.height + (g._helper ? g.position.top - i.top : g.position.top), k && (g.size.width = g.size.height * g.aspectRatio, n = !1), g.position.top = g._helper ? i.top : 0), e = g.containerElement.get(0) === g.element.parent().get(0), f = /relative|absolute/.test(g.containerElement.css("position")), e && f ? (g.offset.left = g.parentData.left + g.position.left, g.offset.top = g.parentData.top + g.position.top) : (g.offset.left = g.element.offset().left, g.offset.top = g.element.offset().top), c = Math.abs(g.sizeDiff.width + (g._helper ? g.offset.left - l.left : g.offset.left - i.left)), d = Math.abs(g.sizeDiff.height + (g._helper ? g.offset.top - l.top : g.offset.top - i.top)), c + g.size.width >= g.parentData.width && (g.size.width = g.parentData.width - c, k && (g.size.height = g.size.width / g.aspectRatio, n = !1)), d + g.size.height >= g.parentData.height && (g.size.height = g.parentData.height - d, k && (g.size.width = g.size.height * g.aspectRatio, n = !1)), n || (g.position.left = g.prevPosition.left, g.position.top = g.prevPosition.top, g.size.width = g.prevSize.width, g.size.height = g.prevSize.height) }, stop: function () { var b = a(this).resizable("instance"), c = b.options, d = b.containerOffset, e = b.containerPosition, f = b.containerElement, g = a(b.helper), h = g.offset(), i = g.outerWidth() - b.sizeDiff.width, j = g.outerHeight() - b.sizeDiff.height; b._helper && !c.animate && /relative/.test(f.css("position")) && a(this).css({ left: h.left - e.left - d.left, width: i, height: j }), b._helper && !c.animate && /static/.test(f.css("position")) && a(this).css({ left: h.left - e.left - d.left, width: i, height: j }) } }), a.ui.plugin.add("resizable", "alsoResize", { start: function () { var b = a(this).resizable("instance"), c = b.options; a(c.alsoResize).each(function () { var b = a(this); b.data("ui-resizable-alsoresize", { width: parseFloat(b.width()), height: parseFloat(b.height()), left: parseFloat(b.css("left")), top: parseFloat(b.css("top")) }) }) }, resize: function (b, c) { var d = a(this).resizable("instance"), e = d.options, f = d.originalSize, g = d.originalPosition, h = { height: d.size.height - f.height || 0, width: d.size.width - f.width || 0, top: d.position.top - g.top || 0, left: d.position.left - g.left || 0 }; a(e.alsoResize).each(function () { var b = a(this), d = a(this).data("ui-resizable-alsoresize"), e = {}, f = b.parents(c.originalElement[0]).length ? ["width", "height"] : ["width", "height", "top", "left"]; a.each(f, function (a, b) { var c = (d[b] || 0) + (h[b] || 0); c && c >= 0 && (e[b] = c || null) }), b.css(e) }) }, stop: function () { a(this).removeData("ui-resizable-alsoresize") } }), a.ui.plugin.add("resizable", "ghost", { start: function () { var b = a(this).resizable("instance"), c = b.size; b.ghost = b.originalElement.clone(), b.ghost.css({ opacity: .25, display: "block", position: "relative", height: c.height, width: c.width, margin: 0, left: 0, top: 0 }), b._addClass(b.ghost, "ui-resizable-ghost"), a.uiBackCompat !== !1 && "string" == typeof b.options.ghost && b.ghost.addClass(this.options.ghost), b.ghost.appendTo(b.helper) }, resize: function () { var b = a(this).resizable("instance"); b.ghost && b.ghost.css({ position: "relative", height: b.size.height, width: b.size.width }) }, stop: function () { var b = a(this).resizable("instance"); b.ghost && b.helper && b.helper.get(0).removeChild(b.ghost.get(0)) } }), a.ui.plugin.add("resizable", "grid", { resize: function () { var b, c = a(this).resizable("instance"), d = c.options, e = c.size, f = c.originalSize, g = c.originalPosition, h = c.axis, i = "number" == typeof d.grid ? [d.grid, d.grid] : d.grid, j = i[0] || 1, k = i[1] || 1, l = Math.round((e.width - f.width) / j) * j, m = Math.round((e.height - f.height) / k) * k, n = f.width + l, o = f.height + m, p = d.maxWidth && d.maxWidth < n, q = d.maxHeight && d.maxHeight < o, r = d.minWidth && d.minWidth > n, s = d.minHeight && d.minHeight > o; d.grid = i, r && (n += j), s && (o += k), p && (n -= j), q && (o -= k), /^(se|s|e)$/.test(h) ? (c.size.width = n, c.size.height = o) : /^(ne)$/.test(h) ? (c.size.width = n, c.size.height = o, c.position.top = g.top - m) : /^(sw)$/.test(h) ? (c.size.width = n, c.size.height = o, c.position.left = g.left - l) : ((o - k <= 0 || n - j <= 0) && (b = c._getPaddingPlusBorderDimensions(this)), o - k > 0 ? (c.size.height = o, c.position.top = g.top - m) : (o = k - b.height, c.size.height = o, c.position.top = g.top + f.height - o), n - j > 0 ? (c.size.width = n, c.position.left = g.left - l) : (n = j - b.width, c.size.width = n, c.position.left = g.left + f.width - n)) } }), a.ui.resizable });;
/**
 * @file
 * Bootstrap Modals.
 *
 * @param {jQuery} $
 * @param {Drupal} Drupal
 * @param {Drupal.bootstrap} Bootstrap
 * @param {Attributes} Attributes
 * @param {drupalSettings} drupalSettings
 */
(function ($, Drupal, Bootstrap, Attributes, drupalSettings) {
  'use strict';

  /**
   * Only process this once.
   */
  Bootstrap.once('modal.jquery.ui.bridge', function (settings) {
    // RTL support.
    var rtl = document.documentElement.getAttribute('dir').toLowerCase() === 'rtl';

    // Override drupal.dialog button classes. This must be done on DOM ready
    // since core/drupal.dialog technically depends on this file and has not
    // yet set their default settings.
    $(function () {
      drupalSettings.dialog.buttonClass = 'btn';
      drupalSettings.dialog.buttonPrimaryClass = 'btn-primary';
    });

    // Create the "dialog" plugin bridge.
    Bootstrap.Dialog.Bridge = function (options) {
      var args = Array.prototype.slice.call(arguments);
      var $element = $(this);
      var type = options && options.dialogType || $element[0].dialogType || 'modal';

      $element[0].dialogType = type;

      var handler = Bootstrap.Dialog.Handler.get(type);

      // When only options are passed, jQuery UI dialog treats this like a
      // initialization method. Destroy any existing Bootstrap modal and
      // recreate it using the contents of the dialog HTML.
      if (args.length === 1 && typeof options === 'object') {
        this.each(function () {
          handler.ensureModalStructure(this, options);
        });

        // Proxy to the Bootstrap Modal plugin, indicating that this is a
        // jQuery UI dialog bridge.
        return handler.invoke(this, {
          dialogOptions: options,
          jQueryUiBridge: true
        });
      }

      // Otherwise, proxy all arguments to the Bootstrap Modal plugin.
      var ret;
      try {
        ret = handler.invoke.apply(handler, [this].concat(args));
      }
      catch (e) {
        Bootstrap.warn(e);
      }

      // If just one element and there was a result returned for the option passed,
      // then return the result. Otherwise, just return the jQuery object.
      return this.length === 1 && ret !== void 0 ? ret : this;
    };

    // Assign the jQuery "dialog" plugin to use to the bridge.
    Bootstrap.createPlugin('dialog', Bootstrap.Dialog.Bridge);

    // Create the "modal" plugin bridge.
    Bootstrap.Modal.Bridge = function () {
      var Modal = this;

      return {
        DEFAULTS: {
          // By default, this option is disabled. It's only flagged when a modal
          // was created using $.fn.dialog above.
          jQueryUiBridge: false
        },
        prototype: {

          /**
           * Handler for $.fn.dialog('close').
           */
          close: function () {
            var _this = this;

            this.hide.apply(this, arguments);

            // For some reason (likely due to the transition event not being
            // registered properly), the backdrop doesn't always get removed
            // after the above "hide" method is invoked . Instead, ensure the
            // backdrop is removed after the transition duration by manually
            // invoking the internal "hideModal" method shortly thereafter.
            setTimeout(function () {
              if (!_this.isShown && _this.$backdrop) {
                _this.hideModal();
              }
            }, (Modal.TRANSITION_DURATION !== void 0 ? Modal.TRANSITION_DURATION : 300) + 10);
          },

          /**
           * Creates any necessary buttons from dialog options.
           */
          createButtons: function () {
            var handler = Bootstrap.Dialog.Handler.get(this.$element);
            this.$footer.find(handler.selectors.buttons).remove();

            // jQuery UI supports both objects and arrays. Unfortunately
            // developers have misunderstood and abused this by simply placing
            // the objects that should be in an array inside an object with
            // arbitrary keys (likely to target specific buttons as a hack).
            var buttons = this.options.dialogOptions && this.options.dialogOptions.buttons || [];
            if (!Array.isArray(buttons)) {
              var array = [];
              for (var k in buttons) {
                // Support the proper object values: label => click callback.
                if (typeof buttons[k] === 'function') {
                  array.push({
                    label: k,
                    click: buttons[k],
                  });
                }
                // Support nested objects, but log a warning.
                else if (buttons[k].text || buttons[k].label) {
                  Bootstrap.warn('Malformed jQuery UI dialog button: @key. The button object should be inside an array.', {
                    '@key': k
                  });
                  array.push(buttons[k]);
                }
                else {
                  Bootstrap.unsupported('button', k, buttons[k]);
                }
              }
              buttons = array;
            }

            if (buttons.length) {
              var $buttons = $('<div class="modal-buttons"/>').appendTo(this.$footer);
              for (var i = 0, l = buttons.length; i < l; i++) {
                var button = buttons[i];
                var $button = $(Drupal.theme('bootstrapModalDialogButton', button));

                // Invoke the "create" method for jQuery UI buttons.
                if (typeof button.create === 'function') {
                  button.create.call($button[0]);
                }

                // Bind the "click" method for jQuery UI buttons to the modal.
                if (typeof button.click === 'function') {
                  $button.on('click', button.click.bind(this.$element));
                }

                $buttons.append($button);
              }
            }

            // Toggle footer visibility based on whether it has child elements.
            this.$footer[this.$footer.children()[0] ? 'show' : 'hide']();
          },

          /**
           * Initializes the Bootstrap Modal.
           */
          init: function () {
            var handler = Bootstrap.Dialog.Handler.get(this.$element);
            if (!this.$dialog) {
              this.$dialog = this.$element.find(handler.selectors.dialog);
            }
            this.$dialog.addClass('js-drupal-dialog');

            if (!this.$header) {
              this.$header = this.$dialog.find(handler.selectors.header);
            }
            if (!this.$title) {
              this.$title = this.$dialog.find(handler.selectors.title);
            }
            if (!this.$close) {
              this.$close = this.$header.find(handler.selectors.close);
            }
            if (!this.$footer) {
              this.$footer = this.$dialog.find(handler.selectors.footer);
            }
            if (!this.$content) {
              this.$content = this.$dialog.find(handler.selectors.content);
            }
            if (!this.$dialogBody) {
              this.$dialogBody = this.$dialog.find(handler.selectors.body);
            }

            // Relay necessary events.
            if (this.options.jQueryUiBridge) {
              this.$element.on('hide.bs.modal', Bootstrap.relayEvent(this.$element, 'dialogbeforeclose', false));
              this.$element.on('hidden.bs.modal', Bootstrap.relayEvent(this.$element, 'dialogclose', false));
              this.$element.on('show.bs.modal', Bootstrap.relayEvent(this.$element, 'dialogcreate', false));
              this.$element.on('shown.bs.modal', Bootstrap.relayEvent(this.$element, 'dialogopen', false));
            }

            // Create a footer if one doesn't exist.
            // This is necessary in case dialog.ajax.js decides to add buttons.
            if (!this.$footer[0]) {
              this.$footer = handler.theme('footer', {}, true).insertAfter(this.$dialogBody);
            }

            // Map the initial options.
            $.extend(true, this.options, this.mapDialogOptions(this.options));

            // Update buttons.
            this.createButtons();

            // Now call the parent init method.
            this.super();

            // Handle autoResize option (this is a drupal.dialog option).
            if (this.options.dialogOptions && this.options.dialogOptions.autoResize && this.options.dialogOptions.position) {
              this.position(this.options.dialogOptions.position);
            }

            // If show is enabled and currently not shown, show it.
            if (this.options.jQueryUiBridge && this.options.show && !this.isShown) {
              this.show();
            }
          },

          /**
           * Handler for $.fn.dialog('instance').
           */
          instance: function () {
            Bootstrap.unsupported('method', 'instance', arguments);
          },

          /**
           * Handler for $.fn.dialog('isOpen').
           */
          isOpen: function () {
            return !!this.isShown;
          },

          /**
           * Maps dialog options to the modal.
           *
           * @param {Object} options
           *   The options to map.
           */
          mapDialogOptions: function (options) {
            // Retrieve the dialog handler for this type.
            var handler = Bootstrap.Dialog.Handler.get(this.$element);

            var mappedOptions = {};
            var dialogOptions = options.dialogOptions || {};

            // Remove any existing dialog options.
            delete options.dialogOptions;

            // Separate Bootstrap modal options from jQuery UI dialog options.
            for (var k in options) {
              if (Modal.DEFAULTS.hasOwnProperty(k)) {
                mappedOptions[k] = options[k];
              }
              else {
                dialogOptions[k] = options[k];
              }
            }


            // Handle CSS properties.
            var cssUnitRegExp = /^([+-]?(?:\d+|\d*\.\d+))([a-z]*|%)?$/;
            var parseCssUnit = function (value, defaultUnit) {
              var parts = ('' + value).match(cssUnitRegExp);
              return parts && parts[1] !== void 0 ? parts[1] + (parts[2] || defaultUnit || 'px') : null;
            };
            var styles = {};
            var cssProperties = ['height', 'maxHeight', 'maxWidth', 'minHeight', 'minWidth', 'width'];
            for (var i = 0, l = cssProperties.length; i < l; i++) {
              var prop = cssProperties[i];
              if (dialogOptions[prop] !== void 0) {
                var value = parseCssUnit(dialogOptions[prop]);
                if (value) {
                  styles[prop] = value;

                  // If there's a defined height of some kind, enforce the modal
                  // to use flex (on modern browsers). This will ensure that
                  // the core autoResize calculations don't cause the content
                  // to overflow.
                  if (dialogOptions.autoResize && (prop === 'height' || prop === 'maxHeight')) {
                    styles.display = 'flex';
                    styles.flexDirection = 'column';
                    this.$dialogBody.css('overflow', 'scroll');
                  }
                }
              }
            }

            // Apply mapped CSS styles to the modal-content container.
            this.$content.css(styles);

            // Handle deprecated "dialogClass" option by merging it with "classes".
            var classesMap = {
              'ui-dialog': 'modal-content',
              'ui-dialog-titlebar': 'modal-header',
              'ui-dialog-title': 'modal-title',
              'ui-dialog-titlebar-close': 'close',
              'ui-dialog-content': 'modal-body',
              'ui-dialog-buttonpane': 'modal-footer'
            };
            if (dialogOptions.dialogClass) {
              if (dialogOptions.classes === void 0) {
                dialogOptions.classes = {};
              }
              if (dialogOptions.classes['ui-dialog'] === void 0) {
                dialogOptions.classes['ui-dialog'] = '';
              }
              var dialogClass = dialogOptions.classes['ui-dialog'].split(' ');
              dialogClass.push(dialogOptions.dialogClass);
              dialogOptions.classes['ui-dialog'] = dialogClass.join(' ');
              delete dialogOptions.dialogClass;
            }

            // Add jQuery UI classes to elements in case developers target them
            // in callbacks.
            for (k in classesMap) {
              this.$element.find('.' + classesMap[k]).addClass(k);
            }

            // Bind events.
            var events = [
              'beforeClose', 'close',
              'create',
              'drag', 'dragStart', 'dragStop',
              'focus',
              'open',
              'resize', 'resizeStart', 'resizeStop'
            ];
            for (i = 0, l = events.length; i < l; i++) {
              var event = events[i].toLowerCase();
              if (dialogOptions[event] === void 0 || typeof dialogOptions[event] !== 'function') continue;
              this.$element.on('dialog' + event, dialogOptions[event]);
            }

            // Support title attribute on the modal.
            var title;
            if ((dialogOptions.title === null || dialogOptions.title === void 0) && (title = this.$element.attr('title'))) {
              dialogOptions.title = title;
            }

            // Handle the reset of the options.
            for (var name in dialogOptions) {
              if (!dialogOptions.hasOwnProperty(name) || dialogOptions[name] === void 0) continue;

              switch (name) {
                case 'appendTo':
                  Bootstrap.unsupported('option', name, dialogOptions.appendTo);
                  break;

                case 'autoOpen':
                  mappedOptions.show = dialogOptions.show = !!dialogOptions.autoOpen;
                  break;

                case 'classes':
                  if (dialogOptions.classes) {
                    for (var key in dialogOptions.classes) {
                      if (dialogOptions.classes.hasOwnProperty(key) && classesMap[key] !== void 0) {
                        // Run through Attributes to sanitize classes.
                        var attributes = Attributes.create().addClass(dialogOptions.classes[key]).toPlainObject();
                        var selector = '.' + classesMap[key];
                        this.$element.find(selector).addClass(attributes['class']);
                      }
                    }
                  }
                  break;

                case 'closeOnEscape':
                  mappedOptions.keyboard = !!dialogOptions.closeOnEscape;
                  if (!dialogOptions.closeOnEscape && dialogOptions.modal) {
                    mappedOptions.backdrop = 'static';
                  }
                  break;

                case 'closeText':
                  Bootstrap.unsupported('option', name, dialogOptions.closeText);
                  break;

                case 'draggable':
                  this.$content
                    .draggable({
                      handle: handler.selectors.header,
                      drag: Bootstrap.relayEvent(this.$element, 'dialogdrag'),
                      start: Bootstrap.relayEvent(this.$element, 'dialogdragstart'),
                      end: Bootstrap.relayEvent(this.$element, 'dialogdragend')
                    })
                    .draggable(dialogOptions.draggable ? 'enable' : 'disable');
                  break;

                case 'hide':
                  if (dialogOptions.hide === false || dialogOptions.hide === true) {
                    this.$element[dialogOptions.hide ? 'addClass' : 'removeClass']('fade');
                    mappedOptions.animation = dialogOptions.hide;
                  }
                  else {
                    Bootstrap.unsupported('option', name + ' (complex animation)', dialogOptions.hide);
                  }
                  break;

                case 'modal':
                  if (!dialogOptions.closeOnEscape && dialogOptions.modal) {
                    mappedOptions.backdrop = 'static';
                  }
                  else {
                    mappedOptions.backdrop = dialogOptions.modal;
                  }

                  // If not a modal and no initial position, center it.
                  if (!dialogOptions.modal && !dialogOptions.position) {
                    this.position({ my: 'center', of: window });
                  }
                  break;

                case 'position':
                  this.position(dialogOptions.position);
                  break;

                // Resizable support (must initialize first).
                case 'resizable':
                  this.$content
                    .resizable({
                      resize: Bootstrap.relayEvent(this.$element, 'dialogresize'),
                      start: Bootstrap.relayEvent(this.$element, 'dialogresizestart'),
                      end: Bootstrap.relayEvent(this.$element, 'dialogresizeend')
                    })
                    .resizable(dialogOptions.resizable ? 'enable' : 'disable');
                  break;

                case 'show':
                  if (dialogOptions.show === false || dialogOptions.show === true) {
                    this.$element[dialogOptions.show ? 'addClass' : 'removeClass']('fade');
                    mappedOptions.animation = dialogOptions.show;
                  }
                  else {
                    Bootstrap.unsupported('option', name + ' (complex animation)', dialogOptions.show);
                  }
                  break;

                case 'title':
                  this.$title.text(dialogOptions.title);
                  break;

              }
            }

            // Add the supported dialog options to the mapped options.
            mappedOptions.dialogOptions = dialogOptions;

            return mappedOptions;
          },

          /**
           * Handler for $.fn.dialog('moveToTop').
           */
          moveToTop: function () {
            Bootstrap.unsupported('method', 'moveToTop', arguments);
          },

          /**
           * Handler for $.fn.dialog('option').
           */
          option: function () {
            var clone = { options: {} };

            // Apply the parent option method to the clone of current options.
            this.super.apply(clone, arguments);

            // Merge in the cloned mapped options.
            $.extend(true, this.options, this.mapDialogOptions(clone.options));

            // Update buttons.
            this.createButtons();
          },

          position: function (position) {
            // Reset modal styling.
            this.$element.css({
              bottom: 'initial',
              overflow: 'visible',
              right: 'initial'
            });

            // Position the modal.
            this.$element.position(position);
          },

          /**
           * Handler for $.fn.dialog('open').
           */
          open: function () {
            this.show.apply(this, arguments);
          },

          /**
           * Handler for $.fn.dialog('widget').
           */
          widget: function () {
            return this.$element;
          }
        }
      };
    };

    // Extend the Bootstrap Modal plugin constructor class.
    Bootstrap.extendPlugin('modal', Bootstrap.Modal.Bridge);

    // Register default core dialog type handlers.
    Bootstrap.Dialog.Handler.register('dialog');
    Bootstrap.Dialog.Handler.register('modal');

    /**
     * Extend Drupal theming functions.
     */
    $.extend(Drupal.theme, /** @lend Drupal.theme */ {

      /**
       * Renders a jQuery UI Dialog compatible button element.
       *
       * @param {Object} button
       *   The button object passed in the dialog options.
       *
       * @return {String}
       *   The modal dialog button markup.
       *
       * @see http://api.jqueryui.com/dialog/#option-buttons
       * @see http://api.jqueryui.com/button/
       */
      bootstrapModalDialogButton: function (button) {
        var attributes = Attributes.create();

        var icon = '';
        var iconPosition = button.iconPosition || 'beginning';
        iconPosition = (iconPosition === 'end' && !rtl) || (iconPosition === 'beginning' && rtl) ? 'after' : 'before';

        // Handle Bootstrap icons differently.
        if (button.bootstrapIcon) {
          icon = Drupal.theme('icon', 'bootstrap', button.icon);
        }
        // Otherwise, assume it's a jQuery UI icon.
        // @todo Map jQuery UI icons to Bootstrap icons?
        else if (button.icon) {
          var iconAttributes = Attributes.create()
            .addClass(['ui-icon', button.icon])
            .set('aria-hidden', 'true');
          icon = '<span' + iconAttributes + '></span>';
        }

        // Label. Note: jQuery UI dialog has an inconsistency where it uses
        // "text" instead of "label", so both need to be supported.
        var value = button.label || button.text;

        // Show/hide label.
        if (icon && ((button.showLabel !== void 0 && !button.showLabel) || (button.text !== void 0 && !button.text))) {
          value = '<span' + Attributes.create().addClass('sr-only') + '>' + value + '</span>';
        }
        attributes.set('value', iconPosition === 'before' ? icon + value : value + icon);

        // Handle disabled.
        attributes[button.disabled ? 'set' : 'remove']('disabled', 'disabled');

        if (button.classes) {
          attributes.addClass(Object.keys(button.classes).map(function (key) { return button.classes[key]; }));
        }
        if (button['class']) {
          attributes.addClass(button['class']);
        }
        if (button.primary) {
          attributes.addClass('btn-primary');
        }

        return Drupal.theme('button', attributes);
      }

    });

  });


})(window.jQuery, window.Drupal, window.Drupal.bootstrap, window.Attributes, window.drupalSettings);
;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function ($, Drupal, drupalSettings) {
  drupalSettings.dialog = {
    autoOpen: true,
    dialogClass: '',

    buttonClass: 'button',
    buttonPrimaryClass: 'button--primary',
    close: function close(event) {
      Drupal.dialog(event.target).close();
      Drupal.detachBehaviors(event.target, null, 'unload');
    }
  };

  Drupal.dialog = function (element, options) {
    var undef = void 0;
    var $element = $(element);
    var dialog = {
      open: false,
      returnValue: undef
    };

    function openDialog(settings) {
      settings = $.extend({}, drupalSettings.dialog, options, settings);

      $(window).trigger('dialog:beforecreate', [dialog, $element, settings]);
      $element.dialog(settings);
      dialog.open = true;
      $(window).trigger('dialog:aftercreate', [dialog, $element, settings]);
    }

    function closeDialog(value) {
      $(window).trigger('dialog:beforeclose', [dialog, $element]);
      $element.dialog('close');
      dialog.returnValue = value;
      dialog.open = false;
      $(window).trigger('dialog:afterclose', [dialog, $element]);
    }

    dialog.show = function () {
      openDialog({ modal: false });
    };
    dialog.showModal = function () {
      openDialog({ modal: true });
    };
    dialog.close = closeDialog;

    return dialog;
  };
})(jQuery, Drupal, drupalSettings);;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function ($, Drupal, drupalSettings, debounce, displace) {
  drupalSettings.dialog = $.extend({ autoResize: true, maxHeight: '95%' }, drupalSettings.dialog);

  function resetPosition(options) {
    var offsets = displace.offsets;
    var left = offsets.left - offsets.right;
    var top = offsets.top - offsets.bottom;

    var leftString = (left > 0 ? '+' : '-') + Math.abs(Math.round(left / 2)) + 'px';
    var topString = (top > 0 ? '+' : '-') + Math.abs(Math.round(top / 2)) + 'px';
    options.position = {
      my: 'center' + (left !== 0 ? leftString : '') + ' center' + (top !== 0 ? topString : ''),
      of: window
    };
    return options;
  }

  function resetSize(event) {
    var positionOptions = ['width', 'height', 'minWidth', 'minHeight', 'maxHeight', 'maxWidth', 'position'];
    var adjustedOptions = {};
    var windowHeight = $(window).height();
    var option = void 0;
    var optionValue = void 0;
    var adjustedValue = void 0;
    for (var n = 0; n < positionOptions.length; n++) {
      option = positionOptions[n];
      optionValue = event.data.settings[option];
      if (optionValue) {
        if (typeof optionValue === 'string' && /%$/.test(optionValue) && /height/i.test(option)) {
          windowHeight -= displace.offsets.top + displace.offsets.bottom;
          adjustedValue = parseInt(0.01 * parseInt(optionValue, 10) * windowHeight, 10);

          if (option === 'height' && event.data.$element.parent().outerHeight() < adjustedValue) {
            adjustedValue = 'auto';
          }
          adjustedOptions[option] = adjustedValue;
        }
      }
    }

    if (!event.data.settings.modal) {
      adjustedOptions = resetPosition(adjustedOptions);
    }
    event.data.$element.dialog('option', adjustedOptions).trigger('dialogContentResize');
  }

  $(window).on({
    'dialog:aftercreate': function dialogAftercreate(event, dialog, $element, settings) {
      var autoResize = debounce(resetSize, 20);
      var eventData = { settings: settings, $element: $element };
      if (settings.autoResize === true || settings.autoResize === 'true') {
        $element.dialog('option', { resizable: false, draggable: false }).dialog('widget').css('position', 'fixed');
        $(window).on('resize.dialogResize scroll.dialogResize', eventData, autoResize).trigger('resize.dialogResize');
        $(document).on('drupalViewportOffsetChange.dialogResize', eventData, autoResize);
      }
    },
    'dialog:beforeclose': function dialogBeforeclose(event, dialog, $element) {
      $(window).off('.dialogResize');
      $(document).off('.dialogResize');
    }
  });
})(jQuery, Drupal, drupalSettings, Drupal.debounce, Drupal.displace);;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function ($, Drupal) {
  Drupal.behaviors.dialog = {
    attach: function attach(context, settings) {
      var $context = $(context);

      if (!$('#drupal-modal').length) {
        $('<div id="drupal-modal" class="ui-front"/>').hide().appendTo('body');
      }

      var $dialog = $context.closest('.ui-dialog-content');
      if ($dialog.length) {
        if ($dialog.dialog('option', 'drupalAutoButtons')) {
          $dialog.trigger('dialogButtonsChange');
        }

        $dialog.dialog('widget').trigger('focus');
      }

      var originalClose = settings.dialog.close;

      settings.dialog.close = function (event) {
        for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
          args[_key - 1] = arguments[_key];
        }

        originalClose.apply(settings.dialog, [event].concat(args));
        $(event.target).remove();
      };
    },
    prepareDialogButtons: function prepareDialogButtons($dialog) {
      var buttons = [];
      var $buttons = $dialog.find('.form-actions input[type=submit], .form-actions a.button');
      $buttons.each(function () {
        var $originalButton = $(this).css({
          display: 'block',
          width: 0,
          height: 0,
          padding: 0,
          border: 0,
          overflow: 'hidden'
        });
        buttons.push({
          text: $originalButton.html() || $originalButton.attr('value'),
          class: $originalButton.attr('class'),
          click: function click(e) {
            if ($originalButton.is('a')) {
              $originalButton[0].click();
            } else {
              $originalButton.trigger('mousedown').trigger('mouseup').trigger('click');
              e.preventDefault();
            }
          }
        });
      });
      return buttons;
    }
  };

  Drupal.AjaxCommands.prototype.openDialog = function (ajax, response, status) {
    if (!response.selector) {
      return false;
    }
    var $dialog = $(response.selector);
    if (!$dialog.length) {
      $dialog = $('<div id="' + response.selector.replace(/^#/, '') + '" class="ui-front"/>').appendTo('body');
    }

    if (!ajax.wrapper) {
      ajax.wrapper = $dialog.attr('id');
    }

    response.command = 'insert';
    response.method = 'html';
    ajax.commands.insert(ajax, response, status);

    if (!response.dialogOptions.buttons) {
      response.dialogOptions.drupalAutoButtons = true;
      response.dialogOptions.buttons = Drupal.behaviors.dialog.prepareDialogButtons($dialog);
    }

    $dialog.on('dialogButtonsChange', function () {
      var buttons = Drupal.behaviors.dialog.prepareDialogButtons($dialog);
      $dialog.dialog('option', 'buttons', buttons);
    });

    response.dialogOptions = response.dialogOptions || {};
    var dialog = Drupal.dialog($dialog.get(0), response.dialogOptions);
    if (response.dialogOptions.modal) {
      dialog.showModal();
    } else {
      dialog.show();
    }

    $dialog.parent().find('.ui-dialog-buttonset').addClass('form-actions');
  };

  Drupal.AjaxCommands.prototype.closeDialog = function (ajax, response, status) {
    var $dialog = $(response.selector);
    if ($dialog.length) {
      Drupal.dialog($dialog.get(0)).close();
      if (!response.persist) {
        $dialog.remove();
      }
    }

    $dialog.off('dialogButtonsChange');
  };

  Drupal.AjaxCommands.prototype.setDialogOption = function (ajax, response, status) {
    var $dialog = $(response.selector);
    if ($dialog.length) {
      $dialog.dialog('option', response.optionName, response.optionValue);
    }
  };

  $(window).on('dialog:aftercreate', function (e, dialog, $element, settings) {
    $element.on('click.dialog', '.dialog-cancel', function (e) {
      dialog.close('cancel');
      e.preventDefault();
      e.stopPropagation();
    });
  });

  $(window).on('dialog:beforeclose', function (e, dialog, $element) {
    $element.off('.dialog');
  });
})(jQuery, Drupal);;
/**
 * @file
 * dialog.ajax.js
 */
(function ($, Drupal, Bootstrap) {

  Drupal.behaviors.dialog.ajaxCurrentButton = null;
  Drupal.behaviors.dialog.ajaxOriginalButton = null;

  // Intercept the success event to add the dialog type to commands.
  var success = Drupal.Ajax.prototype.success;
  Drupal.Ajax.prototype.success = function (response, status) {
    if (this.dialogType) {
      for (var i = 0, l = response.length; i < l; i++) {
        if (response[i].dialogOptions) {
          response[i].dialogType = response[i].dialogOptions.dialogType = this.dialogType;
          response[i].$trigger = response[i].dialogOptions.$trigger = $(this.element);
        }
      }
    }
    return success.apply(this, [response, status]);
  };

  var beforeSerialize = Drupal.Ajax.prototype.beforeSerialize;
  Drupal.Ajax.prototype.beforeSerialize = function (element, options) {
    // Add the dialog type currently in use.
    if (this.dialogType) {
      options.data['ajax_page_state[dialogType]'] = this.dialogType;

      // Add the dialog element ID if it can be found (useful for closing it).
      var id = $(this.element).parents('.js-drupal-dialog:first').attr('id');
      if (id) {
        options.data['ajax_page_state[dialogId]'] = id;
      }
    }
    return beforeSerialize.apply(this, arguments);
  };

  /**
   * Synchronizes a faux button with its original counterpart.
   *
   * @param {Boolean} [reset = false]
   *   Whether to reset the current and original buttons after synchronizing.
   */
  Drupal.behaviors.dialog.ajaxUpdateButtons = function (reset) {
    if (this.ajaxCurrentButton && this.ajaxOriginalButton) {
      this.ajaxCurrentButton.html(this.ajaxOriginalButton.html());
      this.ajaxCurrentButton.prop('disabled', this.ajaxOriginalButton.prop('disabled'));
    }
    if (reset) {
      this.ajaxCurrentButton = null;
      this.ajaxOriginalButton = null;
    }
  };

  $(document)
    .ajaxSend(function () {
      Drupal.behaviors.dialog.ajaxUpdateButtons();
    })
    .ajaxComplete(function () {
      Drupal.behaviors.dialog.ajaxUpdateButtons(true);
    })
    ;

  /**
   * {@inheritdoc}
   */
  Drupal.behaviors.dialog.prepareDialogButtons = function prepareDialogButtons($dialog) {
    var _this = this;
    var buttons = [];
    var $buttons = $dialog.find('.form-actions').find('button, input[type=submit], a.button, .btn');
    $buttons.each(function () {
      var $originalButton = $(this)
        // Prevent original button from being tabbed to.
        .attr('tabindex', -1)
        // Visually make the original button invisible, but don't actually hide
        // or remove it from the DOM because the click needs to be proxied from
        // the faux button created in the footer to its original counterpart.
        .css({
          display: 'block',
          width: 0,
          height: 0,
          padding: 0,
          border: 0,
          overflow: 'hidden'
        });

      buttons.push({
        // Strip all HTML from the actual text value. This value is escaped.
        // It actual HTML value will be synced with the original button's HTML
        // below in the "create" method.
        text: Bootstrap.stripHtml($originalButton),
        class: $originalButton.attr('class').replace('use-ajax-submit', ''),
        click: function click(e) {
          e.preventDefault();
          e.stopPropagation();
          _this.ajaxCurrentButton = $(e.target);
          _this.ajaxOriginalButton = $originalButton;
          // Some core JS binds dialog buttons to the mousedown or mouseup
          // events instead of click; all three events must be simulated here.
          // @see https://www.drupal.org/project/bootstrap/issues/3016254
          Bootstrap.simulate($originalButton, ['mousedown', 'mouseup', 'click']);
        },
        create: function () {
          _this.ajaxCurrentButton = $(this);
          _this.ajaxOriginalButton = $originalButton;
          _this.ajaxUpdateButtons(true);
        }
      });
    });

    return buttons;
  };

})(window.jQuery, window.Drupal, window.Drupal.bootstrap);
;
(function ($) {
  Drupal.behaviors.custom = {
    attach: function (context, settings) {
      if ($('.taxonomy-term-4').length) {
        var $termDescription = $('.taxonomy-term--type-product-type .field--name-description');
        var $termDescriptionHeight = $termDescription.height();
        if ($termDescriptionHeight > 200) {
          var $button = $('<div class="btn btn__custom"></div>');
          var fixedDescriptionHeight = 195;
          var state = true;
          var buttonText = state ? 'Показать полное описание' : 'Скрыть полное описание';
          $termDescription.css({
            height: '195px',
            overflow: 'hidden',
          });
          $button.text(buttonText);
          $button.on('click', function () {
            console.log(state);
            if (!state) {
              $termDescription.animate({
                height: fixedDescriptionHeight + 'px'
              });
            } else {
              $termDescription.animate({
                height: $termDescriptionHeight + 'px'
              });
            }
            state = !state;
            $button.text(state ? 'Показать полное описание' : 'Скрыть полное описание');
          });
          $termDescription.after($button);
        }

        var $termBody = $('#block-views-block-information-block-1 .view-information > .view-content');
        var $termBodyHeight = $termBody.height();
        if ($termBodyHeight > 530) {
          var $button1 = $('<div class="btn btn__custom"></div>');
          var fixedHeight = 530;
          var state1 = true;
          var buttonText1 = state1 ? 'Показать полное описание' : 'Скрыть полное описание';
          $termBody.css({
            height: '530px',
            overflow: 'hidden',
          });
          $button1.text(buttonText1);
          $button1.on('click', function () {
            if (!state1) {
              $termBody.animate({
                height: fixedHeight + 'px'
              });
            } else {
              $termBody.animate({
                height: $termBodyHeight + 'px'
              });
            }
            state1 = !state1;
            $button1.text(state1 ? 'Показать полное описание' : 'Скрыть полное описание');
          });
          $termBody.after($button1);
        }
      }

      if ($('.rekvizity-popup-trigger').length) {
        var $rekvizitsBlock = $('.rekvizity-popup');
        console.log($rekvizitsBlock);
        $('.rekvizity-popup-trigger').colorbox({
          html: true,
          width: 680,
          height: 420,
          inline: true,
          onComplete: function () {
            $rekvizitsBlock.removeClass('hidden-block').prependTo("#cboxLoadedContent");
          }
        });
      }

      /**
       * We set «like» on «five stars» vote and on the opposite - set
       * «five stars» on «like» vote. It's possible to do with
       * hook_vote_insert() but a bit more complicated as causes infinite loop.
        */
      $('.vote-like').once().on('click', 'a:not(.voted)', function () {
        $(this).parents('.node').find('.fivestar-widget .star-last:not(.on) a').triggerHandler('click');
      });
      $('.fivestar-widget .star-last:not(.on)').once().on('click', 'a', function () {
        $(this).parents('.node').find('.vote-like a:not(.voted)').triggerHandler('click');
      });

      $('.vote-dislike').once().on('click', 'a:not(.voted)', function () {
        $(this).parents('.node').find('.fivestar-widget .star-first a').triggerHandler('click');
      });
      $('.fivestar-widget .star-first:not(.on)').once().on('click', 'a', function () {
        $(this).parents('.node').find('.vote-dislike a:not(.voted)').triggerHandler('click');
      });
    }
  }

})(jQuery, Drupal, drupalSettings);;
(function ($, Drupal) {

  Drupal.behaviors.goal = {
    attach: function (context, settings) {

      $('#block-header-contacts .contacts-block__phone').once().click(function () {
        ym(31243273, 'reachGoal', 'klik_na_telephone_verx');
      });
      $('#block-footer-contacts .contacts-block__phone').once().click(function () {
        ym(31243273, 'reachGoal', 'klik_na_telephone_niz');
      });
      $('#block-footer-contacts .contacts-block__description').once().click(function () {
        ym(31243273, 'reachGoal', 'klik_na_pochty_niz');
      });

      $('.taxonomy-term-4 .group-left .field--name-field-buklet .file-link a').once().click(function () {
        ym(31243273, 'reachGoal', 'byklet_kap_oroshenie_glavnaya');
      });
      $('.taxonomy-term-4 .group-left .field--name-field-certificates .file-link a').once().click(function () {
        ym(31243273, 'reachGoal', 'sertifikat_kap_oroshenie_glavnaya');
      });
      $('.taxonomy-term-5 .group-left .field--name-field-buklet .file-link a').once().click(function () {
        ym(31243273, 'reachGoal', 'buklet_setka_glavnaya');
      });
      $('.taxonomy-term-6 .group-left .field--name-field-buklet .file-link a').once().click(function () {
        ym(31243273, 'reachGoal', 'buklet_upakovka_dlya_ovoschey_glavnaya');
      });


      $('#spb-agriservis-popup-price .webform-submission-price-form .webform-button--submit').once().click(function () {

        if ($('#spb-agriservis-popup-price .webform-submission-price-form .form-item-phone input').val() != '') {
          ym(31243273, 'reachGoal', 'klik_na_znat_cena');
        }
      });
      $('#block-request-call .webform-submission-request-call-form .webform-button--submit').once().click(function () {

        if ($('#block-request-call .webform-submission-request-call-form .form-item-phone input').val() != '') {
          ym(31243273, 'reachGoal', 'klik_na_formy_verx');
        }
      });
      $('#block-vebforma .webform-submission-ostavit-zayavku-form .webform-button--submit').once().click(function () {

        if ($('#block-vebforma .webform-submission-ostavit-zayavku-form .form-item-phone input').val() != '') {
          ym(31243273, 'reachGoal', 'klik_na_formy_niz');
        }
      });

      $('#block-block-send-me-form .form-actions .btn').once().click(function () {

        if ($('#block-block-send-me-form .form-item-fz152-agreement input').prop('checked') === true) {
          var paragraph = 'p' + $('#block-block-send-me-form .paragraph').data('pid');
          if (formManager[paragraph] !== undefined) {
            ym(31243273, 'reachGoal', formManager[paragraph]);
          }
        }
      });
    }
  }


  var formManager = {
    p34: 'obr_svyaz_lopatin_bataysk',
    p28: 'obr_svyaz_rykalo_volgograd',
    p29: 'obr_svyaz_scherenko_volgograd',
    p30: 'obr_svyaz_chernishov_volgograd',
    p31: 'obr_svyaz_svitocheva_volgograd',
    p132: 'obr_svyaz_yahudina_volgograd',
    p35: 'obr_svyaz_antipov_voronezh',
    p41: 'obr_svyaz_maiboroda_voronezh',
    p32: 'obr_svyaz_naumov_krasnodar',
    p33: 'obr_svyaz_eremenko_krasnodar',
    p37: 'obr_svyaz_belyaev_krasnoyarsk',
    p38: 'obr_svyaz_azarova_krasnoyarsk',
    p23: 'obr_svyaz_puchkova_moskva',
    p24: 'obr_svyaz_antipov_moskva',
    p25: 'obr_svyaz_dorozhkin_moskva',
    p26: 'obr_svyaz_tikhomirov_moskva',
    p39: 'obr_svyaz_puchkova_bryanskaya',
    p40: 'obr_svyaz_buraya_bryanskaya',
    p36: 'obr_svyaz_tatarinceva_samara',
    p92: 'obr_svyaz_ershov_samara',
    p27: 'obr_svyaz_nikolaev_cheboksary'
  };

})(jQuery, Drupal);;
/**
 * @file
 * Like and dislike icons behavior.
 */
(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.likeAndDislike = {
    attach: function (context, settings) {
      $('.vote-widget--like-and-dislike', context).once('like-and-dislike').each(function () {
        var $widget = $(this);
        $widget.find('.vote-like a').click(function () {
          var entity_id, entity_type;
          if (!$(this).hasClass('disable-status')) {
            entity_id = $(this).data('entity-id');
            entity_type = $(this).data('entity-type');
            likeAndDislikeService.vote(entity_id, entity_type, 'like');
          }
        });
        $widget.find('.vote-dislike a').click(function () {
          var entity_id, entity_type;
          if (!$(this).hasClass('disable-status')) {
            entity_id = $(this).data('entity-id');
            entity_type = $(this).data('entity-type');
            likeAndDislikeService.vote(entity_id, entity_type, 'dislike');
          }
        });
      });
    }
  };

})(jQuery, Drupal);
;
/**
 * @file
 * Like and dislike icons behavior.
 */
(function ($, Drupal) {

  'use strict';

  window.likeAndDislikeService = window.likeAndDislikeService || (function () {
    function likeAndDislikeService() { }
    likeAndDislikeService.vote = function (entity_id, entity_type, tag) {
      $.ajax({
        type: "POST",
        url: drupalSettings.path.baseUrl + 'like_and_dislike/' + entity_type + '/' + tag + '/' + entity_id,
        success: function (response) {
          // Expected response is a json object where likes is the new number
          // of likes, dislikes is the new number of dislikes, message_type is
          // the type of message to display ("status" or "warning") and message
          // is the message to display.
          ['like', 'dislike'].forEach(function (iconType) {
            var selector = '#' + iconType + '-container-' + entity_type + '-' + entity_id;
            var $aTag = $(selector + ' a');
            if ($aTag.length == 0) {
              return;
            }
            response.operation[iconType] ? $aTag.addClass('voted') : $aTag.removeClass('voted');
            $(selector + ' .count').text(response[iconType + 's']);
          });

          // Display a message whether the vote was registered or an error
          // happened.
          // @todo - this will work only for case when theme has messages in
          // highlighted region.
          $('.region.region-highlighted').html("<div class='messages__wrapper layout-container'><div class='messages messages--" + response.message_type + " role='contentinfo'>" + response.message + "</div></div>");
        }
      });
    };
    return likeAndDislikeService;
  })();

})(jQuery, Drupal);
;
!function (a, b, c) { "use strict"; function d(d, e) { function k() { h.randomize && !f.hasClass("slick-initiliazed") && n(), f.on("setPosition.sl", function (a, b) { o(b) }), a(".media--loading", f).closest(".slide__content").addClass("is-loading"), "blazy" === h.lazyLoad && b.blazy && f.on("beforeChange.sl", function () { var c = a(".b-lazy:not(.b-loaded)", f); c.length && b.blazy.init.load(c) }) } function l() { var c = (f.slick("getSlick"), f.find(".media--player").length); f.parent().on("click.sl", ".slick-down", function (b) { b.preventDefault(); var c = a(this); a("html, body").stop().animate({ scrollTop: a(c.data("target")).offset().top - (c.data("offset") || 0) }, 800, h.easing || "swing") }), h.mouseWheel && f.on("mousewheel.sl", function (a, b) { return a.preventDefault(), f.slick(b < 0 ? "slickNext" : "slickPrev") }), f.on("lazyLoaded lazyLoadError", function (a, b, c) { m(c) }), c && (f.on("afterChange.sl", p), f.on("click.sl", ".media__icon--close", p), f.on("click.sl", ".media__icon--play", q)) } function m(b) { var c = a(b), d = c.closest(".media--background"), e = c.closest(".slide") || c.closest(".unslick"); c.parentsUntil(e).removeClass(function (a, b) { return (b.match(/(\S+)loading/g) || []).join(" ") }), d.length && (d.css("background-image", "url(" + c.attr("src") + ")"), d.find("> img").remove(), d.removeAttr("data-lazy")) } function n() { f.children().sort(function () { return .5 - Math.random() }).each(function () { f.append(this) }) } function o(a) { var b = a.slideCount <= h.slidesToShow, c = b || h.arrows === !1; if (f.attr("id") === a.$slider.attr("id")) return h.centerPadding && "0" !== h.centerPadding || a.$list.css("padding", ""), b && a.$slideTrack.width() <= a.$slider.width() && a.$slideTrack.css({ left: "", transform: "" }), g[c ? "addClass" : "removeClass"]("visually-hidden") } function p() { f.removeClass("is-paused"), f.find(".is-playing").length && f.find(".is-playing").removeClass("is-playing").find(".media__icon--close").click() } function q() { f.addClass("is-paused").slick("slickPause") } function r(c) { return { slide: c.slide, lazyLoad: c.lazyLoad, dotsClass: c.dotsClass, rtl: c.rtl, appendDots: ".slick__arrow" === c.appendDots ? g : c.appendDots || a(f), prevArrow: a(".slick-prev", g), nextArrow: a(".slick-next", g), appendArrows: g, customPaging: function (a, d) { var e = a.$slides.eq(d).find("[data-thumb]") || null, f = '<img alt="' + b.t(e.attr("alt")) + '" src="' + e.data("thumb") + '">', g = e.length && c.dotsClass.indexOf("thumbnail") > 0 ? '<div class="slick-dots__thumbnail">' + f + "</div>" : ""; return a.defaults.customPaging(a, d).add(g) } } } var j, f = a("> .slick__slider", e).length ? a("> .slick__slider", e) : a(e), g = a("> .slick__arrow", e), h = f.data("slick") ? a.extend({}, c.slick, f.data("slick")) : c.slick, i = !("array" !== a.type(h.responsive) || !h.responsive.length) && h.responsive; if (i) for (j in i) i.hasOwnProperty(j) && "unslick" !== i[j].settings && (i[j].settings = a.extend({}, c.slick, r(h), i[j].settings)); f.data("slick", h), h = f.data("slick"), k(), f.slick(r(h)), l(), f.hasClass("unslick") && f.slick("unslick"), a(e).addClass("slick--initialized") } b.behaviors.slick = { attach: function (b) { a(".slick", b).once("slick").each(d) } } }(jQuery, Drupal, drupalSettings);
;
/**
 * @file
 * JavaScript behaviors for webforms.
 */

(function ($, Drupal) {

  'use strict';

  /**
   * Autofocus first input.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the behavior for the webform autofocusing.
   */
  Drupal.behaviors.webformAutofocus = {
    attach: function (context) {
      $(context).find('.webform-submission-form.js-webform-autofocus :input:visible:enabled:first').focus();
    }
  };

  /**
   * Prevent webform autosubmit on wizard pages.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the behavior for disabling webform autosubmit.
   *   Wizard pages need to be progressed with the Previous or Next buttons, not by pressing Enter.
   */
  Drupal.behaviors.webformDisableAutoSubmit = {
    attach: function (context) {
      // @see http://stackoverflow.com/questions/11235622/jquery-disable-form-submit-on-enter
      $(context).find('.webform-submission-form.js-webform-disable-autosubmit input')
        .not(':button, :submit, :reset, :image, :file')
        .once('webform-disable-autosubmit')
        .on('keyup keypress', function (e) {
          var keyCode = e.keyCode || e.which;
          if (keyCode === 13) {
            e.preventDefault();
            return false;
          }
        });
    }
  };

  /**
   * Skip client-side validation when submit button is pressed.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the behavior for the skipping client-side validation.
   */
  Drupal.behaviors.webformSubmitNoValidate = {
    attach: function (context) {
      $(context).find(':submit.js-webform-novalidate').once('webform-novalidate').on('click', function () {
        $(this.form).attr('novalidate', 'novalidate');
      });
    }
  };

  /**
   * Attach behaviors to trigger submit button from input onchange.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches form trigger submit events.
   */
  Drupal.behaviors.webformSubmitTrigger = {
    attach: function (context) {
      $('[data-webform-trigger-submit]').once('webform-trigger-submit').on('change', function () {
        var submit = $(this).attr('data-webform-trigger-submit');
        $(submit).mousedown();
      });
    }
  };

  /**
   * Custom required error message.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the behavior for the webform custom required error message.
   *
   * @see http://stackoverflow.com/questions/5272433/html5-form-required-attribute-set-custom-validation-message
   */
  Drupal.behaviors.webformRequiredError = {
    attach: function (context) {
      $(context).find(':input[data-webform-required-error]').once('webform-required-error')
        .on('invalid', function () {
          this.setCustomValidity('');
          if (!this.valid) {
            this.setCustomValidity($(this).attr('data-webform-required-error'));
          }
        })
        .on('input, change', function () {
          // Find all related elements by name and reset custom validity.
          // This specifically applies to required radios and checkboxes.
          var name = $(this).attr('name');
          $(this.form).find(':input[name="' + name + '"]').each(function () {
            this.setCustomValidity('');
          });
        });
    }
  };

  // When #state:required is triggered we need to reset the target elements
  // custom validity.
  $(document).on('state:required', function (e) {
    $(e.target).filter('[data-webform-required-error]')
      .each(function () { this.setCustomValidity(''); });
  });

  if (window.imceInput) {
    window.imceInput.processUrlInput = function (i, el) {
      var button = imceInput.createUrlButton(el.id, el.getAttribute('data-imce-type'));
      el.parentNode.insertAfter(button, el);
    };
  }

})(jQuery, Drupal);
;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function (Drupal, debounce) {
  var liveElement = void 0;
  var announcements = [];

  Drupal.behaviors.drupalAnnounce = {
    attach: function attach(context) {
      if (!liveElement) {
        liveElement = document.createElement('div');
        liveElement.id = 'drupal-live-announce';
        liveElement.className = 'visually-hidden';
        liveElement.setAttribute('aria-live', 'polite');
        liveElement.setAttribute('aria-busy', 'false');
        document.body.appendChild(liveElement);
      }
    }
  };

  function announce() {
    var text = [];
    var priority = 'polite';
    var announcement = void 0;

    var il = announcements.length;
    for (var i = 0; i < il; i++) {
      announcement = announcements.pop();
      text.unshift(announcement.text);

      if (announcement.priority === 'assertive') {
        priority = 'assertive';
      }
    }

    if (text.length) {
      liveElement.innerHTML = '';

      liveElement.setAttribute('aria-busy', 'true');

      liveElement.setAttribute('aria-live', priority);

      liveElement.innerHTML = text.join('\n');

      liveElement.setAttribute('aria-busy', 'false');
    }
  }

  Drupal.announce = function (text, priority) {
    announcements.push({
      text: text,
      priority: priority
    });

    return debounce(announce, 200)();
  };
})(Drupal, Drupal.debounce);;
/**
 * @file
 * JavaScript behaviors for details element.
 */

(function ($, Drupal) {

  'use strict';

  Drupal.webform = Drupal.webform || {};
  Drupal.webform.detailsToggle = Drupal.webform.detailsToggle || {};
  Drupal.webform.detailsToggle.options = Drupal.webform.detailsToggle.options || {};

  /**
   * Attach handler to toggle details open/close state.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.webformDetailsToggle = {
    attach: function (context) {
      $('.js-webform-details-toggle', context).once('webform-details-toggle').each(function () {
        var $form = $(this);
        var $tabs = $form.find('.webform-tabs');

        // Get only the main details elements and ignore all nested details.
        var selector = ($tabs.length) ? '.webform-tab' : '.js-webform-details-toggle';
        var $details = $form.find('details').filter(function () {
          // @todo Figure out how to optimize the below code.
          var $parents = $(this).parentsUntil(selector);
          return ($parents.find('details').length === 0);
        });

        // Toggle is only useful when there are two or more details elements.
        if ($details.length < 2) {
          return;
        }

        var options = $.extend({
          'button': '<button type="button" class="webform-details-toggle-state"></button>'
        }, Drupal.webform.detailsToggle.options);

        // Create toggle buttons.
        var $toggle = $(options.button)
          .attr('title', Drupal.t('Toggle details widget state.'))
          .on('click', function (e) {
            var open;
            if (isFormDetailsOpen($form)) {
              $form.find('details').removeAttr('open');
              open = 0;
            }
            else {
              $form.find('details').attr('open', 'open');
              open = 1;
            }
            setDetailsToggleLabel($form);

            // Set the saved states for all the details elements.
            // @see webform.element.details.save.js
            if (Drupal.webformDetailsSaveGetName) {
              $form.find('details').each(function () {
                var name = Drupal.webformDetailsSaveGetName($(this));
                if (name) {
                  localStorage.setItem(name, open);
                }
              });
            }
          })
          .wrap('<div class="webform-details-toggle-state-wrapper"></div>')
          .parent();

        if ($tabs.length) {
          // Add toggle state before the tabs.
          $tabs.find('.item-list').before($toggle);
        }
        else {
          // Add toggle state link to first details element.
          $details.eq(0).before($toggle);
        }

        setDetailsToggleLabel($form);
      });
    }
  };

  /**
   * Determine if a webform's details are all opened.
   *
   * @param {jQuery} $form
   *   A webform.
   *
   * @return {boolean}
   *   TRUE if a webform's details are all opened.
   */
  function isFormDetailsOpen($form) {
    return ($form.find('details[open]').length === $form.find('details').length);
  }

  /**
   * Set a webform's details toggle state widget label.
   *
   * @param {jQuery} $form
   *   A webform.
   */
  function setDetailsToggleLabel($form) {
    var isOpen = isFormDetailsOpen($form);

    var label = (isOpen) ? Drupal.t('Collapse all') : Drupal.t('Expand all');
    $form.find('.webform-details-toggle-state').html(label);

    var text = (isOpen) ? Drupal.t('All details have been expanded.') : Drupal.t('All details have been collapsed.');
    Drupal.announce(text);
  }

})(jQuery, Drupal);
;
/**
 * @file
 * JavaScript behaviors for details element.
 */

(function ($, Drupal) {

  'use strict';

  /**
   * Attach handler to save details open/close state.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.webformDetailsSave = {
    attach: function (context) {
      if (!window.localStorage) {
        return;
      }

      // Summary click event handler.
      $('details > summary', context).once('webform-details-summary-save').click(function () {
        var $details = $(this).parent();


        // @see https://css-tricks.com/snippets/jquery/make-an-jquery-hasattr/
        if ($details[0].hasAttribute('data-webform-details-nosave')) {
          return;
        }

        var name = Drupal.webformDetailsSaveGetName($details);
        if (!name) {
          return;
        }

        var open = ($details.attr('open') !== 'open') ? '1' : '0';
        localStorage.setItem(name, open);
      });

      // Initialize details open state via local storage.
      $('details', context).once('webform-details-save').each(function () {
        var $details = $(this);

        var name = Drupal.webformDetailsSaveGetName($details);
        if (!name) {
          return;
        }

        var open = localStorage.getItem(name);
        if (open === null) {
          return;
        }

        if (open === '1') {
          $details.attr('open', 'open');
        }
        else {
          $details.removeAttr('open');
        }
      });
    }

  };

  /**
   * Get the name used to store the state of details element.
   *
   * @param {jQuery} $details
   *   A details element.
   *
   * @return string
   *   The name used to store the state of details element.
   */
  Drupal.webformDetailsSaveGetName = function ($details) {
    if (!window.localStorage) {
      return '';
    }

    // Any details element not included a webform must have define its own id.
    var webformId = $details.attr('data-webform-element-id');
    if (webformId) {
      return 'Drupal.webform.' + webformId.replace('--', '.');
    }

    var detailsId = $details.attr('id');
    if (!detailsId) {
      return '';
    }

    var $form = $details.parents('form');
    if (!$form.length || !$form.attr('id')) {
      return '';
    }

    var formId = $form.attr('id');
    if (!formId) {
      return '';
    }

    // ISSUE: When Drupal renders a webform in a modal dialog it appends a unique
    // identifier to webform ids and details ids. (i.e. my-form--FeSFISegTUI)
    // WORKAROUND: Remove the unique id that delimited using double dashes.
    formId = formId.replace(/--.+?$/, '').replace(/-/g, '_');
    detailsId = detailsId.replace(/--.+?$/, '').replace(/-/g, '_');
    return 'Drupal.webform.' + formId + '.' + detailsId;
  }

})(jQuery, Drupal);
;
/*!
 * jQuery Form Plugin
 * version: 4.2.2
 * Requires jQuery v1.7.2 or later
 * Project repository: https://github.com/jquery-form/form

 * Copyright 2017 Kevin Morris
 * Copyright 2006 M. Alsup

 * Dual licensed under the LGPL-2.1+ or MIT licenses
 * https://github.com/jquery-form/form#license

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
!function (e) { "function" == typeof define && define.amd ? define(["jquery"], e) : "object" == typeof module && module.exports ? module.exports = function (t, r) { return void 0 === r && (r = "undefined" != typeof window ? require("jquery") : require("jquery")(t)), e(r), r } : e(jQuery) }(function (e) { "use strict"; function t(t) { var r = t.data; t.isDefaultPrevented() || (t.preventDefault(), e(t.target).closest("form").ajaxSubmit(r)) } function r(t) { var r = t.target, a = e(r); if (!a.is("[type=submit],[type=image]")) { var n = a.closest("[type=submit]"); if (0 === n.length) return; r = n[0] } var i = r.form; if (i.clk = r, "image" === r.type) if (void 0 !== t.offsetX) i.clk_x = t.offsetX, i.clk_y = t.offsetY; else if ("function" == typeof e.fn.offset) { var o = a.offset(); i.clk_x = t.pageX - o.left, i.clk_y = t.pageY - o.top } else i.clk_x = t.pageX - r.offsetLeft, i.clk_y = t.pageY - r.offsetTop; setTimeout(function () { i.clk = i.clk_x = i.clk_y = null }, 100) } function a() { if (e.fn.ajaxSubmit.debug) { var t = "[jquery.form] " + Array.prototype.join.call(arguments, ""); window.console && window.console.log ? window.console.log(t) : window.opera && window.opera.postError && window.opera.postError(t) } } var n = /\r?\n/g, i = {}; i.fileapi = void 0 !== e('<input type="file">').get(0).files, i.formdata = void 0 !== window.FormData; var o = !!e.fn.prop; e.fn.attr2 = function () { if (!o) return this.attr.apply(this, arguments); var e = this.prop.apply(this, arguments); return e && e.jquery || "string" == typeof e ? e : this.attr.apply(this, arguments) }, e.fn.ajaxSubmit = function (t, r, n, s) { function u(r) { var a, n, i = e.param(r, t.traditional).split("&"), o = i.length, s = []; for (a = 0; a < o; a++)i[a] = i[a].replace(/\+/g, " "), n = i[a].split("="), s.push([decodeURIComponent(n[0]), decodeURIComponent(n[1])]); return s } function c(r) { function n(e) { var t = null; try { e.contentWindow && (t = e.contentWindow.document) } catch (e) { a("cannot get iframe.contentWindow document: " + e) } if (t) return t; try { t = e.contentDocument ? e.contentDocument : e.document } catch (r) { a("cannot get iframe.contentDocument: " + r), t = e.document } return t } function i() { function t() { try { var e = n(v).readyState; a("state = " + e), e && "uninitialized" === e.toLowerCase() && setTimeout(t, 50) } catch (e) { a("Server abort: ", e, " (", e.name, ")"), s(L), j && clearTimeout(j), j = void 0 } } var r = p.attr2("target"), i = p.attr2("action"), o = p.attr("enctype") || p.attr("encoding") || "multipart/form-data"; w.setAttribute("target", m), l && !/post/i.test(l) || w.setAttribute("method", "POST"), i !== f.url && w.setAttribute("action", f.url), f.skipEncodingOverride || l && !/post/i.test(l) || p.attr({ encoding: "multipart/form-data", enctype: "multipart/form-data" }), f.timeout && (j = setTimeout(function () { T = !0, s(A) }, f.timeout)); var u = []; try { if (f.extraData) for (var c in f.extraData) f.extraData.hasOwnProperty(c) && (e.isPlainObject(f.extraData[c]) && f.extraData[c].hasOwnProperty("name") && f.extraData[c].hasOwnProperty("value") ? u.push(e('<input type="hidden" name="' + f.extraData[c].name + '">', k).val(f.extraData[c].value).appendTo(w)[0]) : u.push(e('<input type="hidden" name="' + c + '">', k).val(f.extraData[c]).appendTo(w)[0])); f.iframeTarget || h.appendTo(D), v.attachEvent ? v.attachEvent("onload", s) : v.addEventListener("load", s, !1), setTimeout(t, 15); try { w.submit() } catch (e) { document.createElement("form").submit.apply(w) } } finally { w.setAttribute("action", i), w.setAttribute("enctype", o), r ? w.setAttribute("target", r) : p.removeAttr("target"), e(u).remove() } } function s(t) { if (!x.aborted && !X) { if ((O = n(v)) || (a("cannot access response document"), t = L), t === A && x) return x.abort("timeout"), void S.reject(x, "timeout"); if (t === L && x) return x.abort("server abort"), void S.reject(x, "error", "server abort"); if (O && O.location.href !== f.iframeSrc || T) { v.detachEvent ? v.detachEvent("onload", s) : v.removeEventListener("load", s, !1); var r, i = "success"; try { if (T) throw "timeout"; var o = "xml" === f.dataType || O.XMLDocument || e.isXMLDoc(O); if (a("isXml=" + o), !o && window.opera && (null === O.body || !O.body.innerHTML) && --C) return a("requeing onLoad callback, DOM not available"), void setTimeout(s, 250); var u = O.body ? O.body : O.documentElement; x.responseText = u ? u.innerHTML : null, x.responseXML = O.XMLDocument ? O.XMLDocument : O, o && (f.dataType = "xml"), x.getResponseHeader = function (e) { return { "content-type": f.dataType }[e.toLowerCase()] }, u && (x.status = Number(u.getAttribute("status")) || x.status, x.statusText = u.getAttribute("statusText") || x.statusText); var c = (f.dataType || "").toLowerCase(), l = /(json|script|text)/.test(c); if (l || f.textarea) { var p = O.getElementsByTagName("textarea")[0]; if (p) x.responseText = p.value, x.status = Number(p.getAttribute("status")) || x.status, x.statusText = p.getAttribute("statusText") || x.statusText; else if (l) { var m = O.getElementsByTagName("pre")[0], g = O.getElementsByTagName("body")[0]; m ? x.responseText = m.textContent ? m.textContent : m.innerText : g && (x.responseText = g.textContent ? g.textContent : g.innerText) } } else "xml" === c && !x.responseXML && x.responseText && (x.responseXML = q(x.responseText)); try { M = N(x, c, f) } catch (e) { i = "parsererror", x.error = r = e || i } } catch (e) { a("error caught: ", e), i = "error", x.error = r = e || i } x.aborted && (a("upload aborted"), i = null), x.status && (i = x.status >= 200 && x.status < 300 || 304 === x.status ? "success" : "error"), "success" === i ? (f.success && f.success.call(f.context, M, "success", x), S.resolve(x.responseText, "success", x), d && e.event.trigger("ajaxSuccess", [x, f])) : i && (void 0 === r && (r = x.statusText), f.error && f.error.call(f.context, x, i, r), S.reject(x, "error", r), d && e.event.trigger("ajaxError", [x, f, r])), d && e.event.trigger("ajaxComplete", [x, f]), d && !--e.active && e.event.trigger("ajaxStop"), f.complete && f.complete.call(f.context, x, i), X = !0, f.timeout && clearTimeout(j), setTimeout(function () { f.iframeTarget ? h.attr("src", f.iframeSrc) : h.remove(), x.responseXML = null }, 100) } } } var u, c, f, d, m, h, v, x, y, b, T, j, w = p[0], S = e.Deferred(); if (S.abort = function (e) { x.abort(e) }, r) for (c = 0; c < g.length; c++)u = e(g[c]), o ? u.prop("disabled", !1) : u.removeAttr("disabled"); (f = e.extend(!0, {}, e.ajaxSettings, t)).context = f.context || f, m = "jqFormIO" + (new Date).getTime(); var k = w.ownerDocument, D = p.closest("body"); if (f.iframeTarget ? (b = (h = e(f.iframeTarget, k)).attr2("name")) ? m = b : h.attr2("name", m) : (h = e('<iframe name="' + m + '" src="' + f.iframeSrc + '" />', k)).css({ position: "absolute", top: "-1000px", left: "-1000px" }), v = h[0], x = { aborted: 0, responseText: null, responseXML: null, status: 0, statusText: "n/a", getAllResponseHeaders: function () { }, getResponseHeader: function () { }, setRequestHeader: function () { }, abort: function (t) { var r = "timeout" === t ? "timeout" : "aborted"; a("aborting upload... " + r), this.aborted = 1; try { v.contentWindow.document.execCommand && v.contentWindow.document.execCommand("Stop") } catch (e) { } h.attr("src", f.iframeSrc), x.error = r, f.error && f.error.call(f.context, x, r, t), d && e.event.trigger("ajaxError", [x, f, r]), f.complete && f.complete.call(f.context, x, r) } }, (d = f.global) && 0 == e.active++ && e.event.trigger("ajaxStart"), d && e.event.trigger("ajaxSend", [x, f]), f.beforeSend && !1 === f.beforeSend.call(f.context, x, f)) return f.global && e.active-- , S.reject(), S; if (x.aborted) return S.reject(), S; (y = w.clk) && (b = y.name) && !y.disabled && (f.extraData = f.extraData || {}, f.extraData[b] = y.value, "image" === y.type && (f.extraData[b + ".x"] = w.clk_x, f.extraData[b + ".y"] = w.clk_y)); var A = 1, L = 2, F = e("meta[name=csrf-token]").attr("content"), E = e("meta[name=csrf-param]").attr("content"); E && F && (f.extraData = f.extraData || {}, f.extraData[E] = F), f.forceSync ? i() : setTimeout(i, 10); var M, O, X, C = 50, q = e.parseXML || function (e, t) { return window.ActiveXObject ? ((t = new ActiveXObject("Microsoft.XMLDOM")).async = "false", t.loadXML(e)) : t = (new DOMParser).parseFromString(e, "text/xml"), t && t.documentElement && "parsererror" !== t.documentElement.nodeName ? t : null }, _ = e.parseJSON || function (e) { return window.eval("(" + e + ")") }, N = function (t, r, a) { var n = t.getResponseHeader("content-type") || "", i = ("xml" === r || !r) && n.indexOf("xml") >= 0, o = i ? t.responseXML : t.responseText; return i && "parsererror" === o.documentElement.nodeName && e.error && e.error("parsererror"), a && a.dataFilter && (o = a.dataFilter(o, r)), "string" == typeof o && (("json" === r || !r) && n.indexOf("json") >= 0 ? o = _(o) : ("script" === r || !r) && n.indexOf("javascript") >= 0 && e.globalEval(o)), o }; return S } if (!this.length) return a("ajaxSubmit: skipping submit process - no element selected"), this; var l, f, d, p = this; "function" == typeof t ? t = { success: t } : "string" == typeof t || !1 === t && arguments.length > 0 ? (t = { url: t, data: r, dataType: n }, "function" == typeof s && (t.success = s)) : void 0 === t && (t = {}), l = t.method || t.type || this.attr2("method"), (d = (d = "string" == typeof (f = t.url || this.attr2("action")) ? e.trim(f) : "") || window.location.href || "") && (d = (d.match(/^([^#]+)/) || [])[1]), t = e.extend(!0, { url: d, success: e.ajaxSettings.success, type: l || e.ajaxSettings.type, iframeSrc: /^https/i.test(window.location.href || "") ? "javascript:false" : "about:blank" }, t); var m = {}; if (this.trigger("form-pre-serialize", [this, t, m]), m.veto) return a("ajaxSubmit: submit vetoed via form-pre-serialize trigger"), this; if (t.beforeSerialize && !1 === t.beforeSerialize(this, t)) return a("ajaxSubmit: submit aborted via beforeSerialize callback"), this; var h = t.traditional; void 0 === h && (h = e.ajaxSettings.traditional); var v, g = [], x = this.formToArray(t.semantic, g, t.filtering); if (t.data) { var y = e.isFunction(t.data) ? t.data(x) : t.data; t.extraData = y, v = e.param(y, h) } if (t.beforeSubmit && !1 === t.beforeSubmit(x, this, t)) return a("ajaxSubmit: submit aborted via beforeSubmit callback"), this; if (this.trigger("form-submit-validate", [x, this, t, m]), m.veto) return a("ajaxSubmit: submit vetoed via form-submit-validate trigger"), this; var b = e.param(x, h); v && (b = b ? b + "&" + v : v), "GET" === t.type.toUpperCase() ? (t.url += (t.url.indexOf("?") >= 0 ? "&" : "?") + b, t.data = null) : t.data = b; var T = []; if (t.resetForm && T.push(function () { p.resetForm() }), t.clearForm && T.push(function () { p.clearForm(t.includeHidden) }), !t.dataType && t.target) { var j = t.success || function () { }; T.push(function (r, a, n) { var i = arguments, o = t.replaceTarget ? "replaceWith" : "html"; e(t.target)[o](r).each(function () { j.apply(this, i) }) }) } else t.success && (e.isArray(t.success) ? e.merge(T, t.success) : T.push(t.success)); if (t.success = function (e, r, a) { for (var n = t.context || this, i = 0, o = T.length; i < o; i++)T[i].apply(n, [e, r, a || p, p]) }, t.error) { var w = t.error; t.error = function (e, r, a) { var n = t.context || this; w.apply(n, [e, r, a, p]) } } if (t.complete) { var S = t.complete; t.complete = function (e, r) { var a = t.context || this; S.apply(a, [e, r, p]) } } var k = e("input[type=file]:enabled", this).filter(function () { return "" !== e(this).val() }).length > 0, D = "multipart/form-data", A = p.attr("enctype") === D || p.attr("encoding") === D, L = i.fileapi && i.formdata; a("fileAPI :" + L); var F, E = (k || A) && !L; !1 !== t.iframe && (t.iframe || E) ? t.closeKeepAlive ? e.get(t.closeKeepAlive, function () { F = c(x) }) : F = c(x) : F = (k || A) && L ? function (r) { for (var a = new FormData, n = 0; n < r.length; n++)a.append(r[n].name, r[n].value); if (t.extraData) { var i = u(t.extraData); for (n = 0; n < i.length; n++)i[n] && a.append(i[n][0], i[n][1]) } t.data = null; var o = e.extend(!0, {}, e.ajaxSettings, t, { contentType: !1, processData: !1, cache: !1, type: l || "POST" }); t.uploadProgress && (o.xhr = function () { var r = e.ajaxSettings.xhr(); return r.upload && r.upload.addEventListener("progress", function (e) { var r = 0, a = e.loaded || e.position, n = e.total; e.lengthComputable && (r = Math.ceil(a / n * 100)), t.uploadProgress(e, a, n, r) }, !1), r }), o.data = null; var s = o.beforeSend; return o.beforeSend = function (e, r) { t.formData ? r.data = t.formData : r.data = a, s && s.call(this, e, r) }, e.ajax(o) }(x) : e.ajax(t), p.removeData("jqxhr").data("jqxhr", F); for (var M = 0; M < g.length; M++)g[M] = null; return this.trigger("form-submit-notify", [this, t]), this }, e.fn.ajaxForm = function (n, i, o, s) { if (("string" == typeof n || !1 === n && arguments.length > 0) && (n = { url: n, data: i, dataType: o }, "function" == typeof s && (n.success = s)), n = n || {}, n.delegation = n.delegation && e.isFunction(e.fn.on), !n.delegation && 0 === this.length) { var u = { s: this.selector, c: this.context }; return !e.isReady && u.s ? (a("DOM not ready, queuing ajaxForm"), e(function () { e(u.s, u.c).ajaxForm(n) }), this) : (a("terminating; zero elements found by selector" + (e.isReady ? "" : " (DOM not ready)")), this) } return n.delegation ? (e(document).off("submit.form-plugin", this.selector, t).off("click.form-plugin", this.selector, r).on("submit.form-plugin", this.selector, n, t).on("click.form-plugin", this.selector, n, r), this) : this.ajaxFormUnbind().on("submit.form-plugin", n, t).on("click.form-plugin", n, r) }, e.fn.ajaxFormUnbind = function () { return this.off("submit.form-plugin click.form-plugin") }, e.fn.formToArray = function (t, r, a) { var n = []; if (0 === this.length) return n; var o, s = this[0], u = this.attr("id"), c = t || void 0 === s.elements ? s.getElementsByTagName("*") : s.elements; if (c && (c = e.makeArray(c)), u && (t || /(Edge|Trident)\//.test(navigator.userAgent)) && (o = e(':input[form="' + u + '"]').get()).length && (c = (c || []).concat(o)), !c || !c.length) return n; e.isFunction(a) && (c = e.map(c, a)); var l, f, d, p, m, h, v; for (l = 0, h = c.length; l < h; l++)if (m = c[l], (d = m.name) && !m.disabled) if (t && s.clk && "image" === m.type) s.clk === m && (n.push({ name: d, value: e(m).val(), type: m.type }), n.push({ name: d + ".x", value: s.clk_x }, { name: d + ".y", value: s.clk_y })); else if ((p = e.fieldValue(m, !0)) && p.constructor === Array) for (r && r.push(m), f = 0, v = p.length; f < v; f++)n.push({ name: d, value: p[f] }); else if (i.fileapi && "file" === m.type) { r && r.push(m); var g = m.files; if (g.length) for (f = 0; f < g.length; f++)n.push({ name: d, value: g[f], type: m.type }); else n.push({ name: d, value: "", type: m.type }) } else null !== p && void 0 !== p && (r && r.push(m), n.push({ name: d, value: p, type: m.type, required: m.required })); if (!t && s.clk) { var x = e(s.clk), y = x[0]; (d = y.name) && !y.disabled && "image" === y.type && (n.push({ name: d, value: x.val() }), n.push({ name: d + ".x", value: s.clk_x }, { name: d + ".y", value: s.clk_y })) } return n }, e.fn.formSerialize = function (t) { return e.param(this.formToArray(t)) }, e.fn.fieldSerialize = function (t) { var r = []; return this.each(function () { var a = this.name; if (a) { var n = e.fieldValue(this, t); if (n && n.constructor === Array) for (var i = 0, o = n.length; i < o; i++)r.push({ name: a, value: n[i] }); else null !== n && void 0 !== n && r.push({ name: this.name, value: n }) } }), e.param(r) }, e.fn.fieldValue = function (t) { for (var r = [], a = 0, n = this.length; a < n; a++) { var i = this[a], o = e.fieldValue(i, t); null === o || void 0 === o || o.constructor === Array && !o.length || (o.constructor === Array ? e.merge(r, o) : r.push(o)) } return r }, e.fieldValue = function (t, r) { var a = t.name, i = t.type, o = t.tagName.toLowerCase(); if (void 0 === r && (r = !0), r && (!a || t.disabled || "reset" === i || "button" === i || ("checkbox" === i || "radio" === i) && !t.checked || ("submit" === i || "image" === i) && t.form && t.form.clk !== t || "select" === o && -1 === t.selectedIndex)) return null; if ("select" === o) { var s = t.selectedIndex; if (s < 0) return null; for (var u = [], c = t.options, l = "select-one" === i, f = l ? s + 1 : c.length, d = l ? s : 0; d < f; d++) { var p = c[d]; if (p.selected && !p.disabled) { var m = p.value; if (m || (m = p.attributes && p.attributes.value && !p.attributes.value.specified ? p.text : p.value), l) return m; u.push(m) } } return u } return e(t).val().replace(n, "\r\n") }, e.fn.clearForm = function (t) { return this.each(function () { e("input,select,textarea", this).clearFields(t) }) }, e.fn.clearFields = e.fn.clearInputs = function (t) { var r = /^(?:color|date|datetime|email|month|number|password|range|search|tel|text|time|url|week)$/i; return this.each(function () { var a = this.type, n = this.tagName.toLowerCase(); r.test(a) || "textarea" === n ? this.value = "" : "checkbox" === a || "radio" === a ? this.checked = !1 : "select" === n ? this.selectedIndex = -1 : "file" === a ? /MSIE/.test(navigator.userAgent) ? e(this).replaceWith(e(this).clone(!0)) : e(this).val("") : t && (!0 === t && /hidden/.test(a) || "string" == typeof t && e(this).is(t)) && (this.value = "") }) }, e.fn.resetForm = function () { return this.each(function () { var t = e(this), r = this.tagName.toLowerCase(); switch (r) { case "input": this.checked = this.defaultChecked; case "textarea": return this.value = this.defaultValue, !0; case "option": case "optgroup": var a = t.parents("select"); return a.length && a[0].multiple ? "option" === r ? this.selected = this.defaultSelected : t.find("option").resetForm() : a.resetForm(), !0; case "select": return t.find("option").each(function (e) { if (this.selected = this.defaultSelected, this.defaultSelected && !t[0].multiple) return t[0].selectedIndex = e, !1 }), !0; case "label": var n = e(t.attr("for")), i = t.find("input,select,textarea"); return n[0] && i.unshift(n[0]), i.resetForm(), !0; case "form": return ("function" == typeof this.reset || "object" == typeof this.reset && !this.reset.nodeType) && this.reset(), !0; default: return t.find("form,input,label,select,textarea").resetForm(), !0 } }) }, e.fn.enable = function (e) { return void 0 === e && (e = !0), this.each(function () { this.disabled = !e }) }, e.fn.selected = function (t) { return void 0 === t && (t = !0), this.each(function () { var r = this.type; if ("checkbox" === r || "radio" === r) this.checked = t; else if ("option" === this.tagName.toLowerCase()) { var a = e(this).parent("select"); t && a[0] && "select-one" === a[0].type && a.find("option").selected(!1), this.selected = t } }) }, e.fn.ajaxSubmit.debug = !1 });

;
/**
 * @file
 * JavaScript behaviors for Ajax.
 */

(function ($, Drupal) {

  'use strict';

  Drupal.webform = Drupal.webform || {};
  Drupal.webform.ajax = Drupal.webform.ajax || {};
  // Allow scrollTopOffset to be custom defined or based on whether there is a
  // floating toolbar.
  Drupal.webform.ajax.scrollTopOffset = Drupal.webform.ajax.scrollTopOffset || ($('#toolbar-administration').length ? 140 : 10);

  /**
   * Provide Webform Ajax link behavior.
   *
   * Display fullscreen progress indicator instead of throbber.
   * Copied from: Drupal.behaviors.AJAX
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the behavior to a.webform-ajax-link.
   */
  Drupal.behaviors.webformAjaxLink = {
    attach: function (context) {
      $('.webform-ajax-link').once('webform-ajax-link').each(function () {
        var element_settings = {};
        element_settings.progress = { type: 'fullscreen' };

        // For anchor tags, these will go to the target of the anchor rather
        // than the usual location.
        var href = $(this).attr('href');
        if (href) {
          element_settings.url = href;
          element_settings.event = 'click';
        }
        element_settings.dialogType = $(this).data('dialog-type');
        element_settings.dialogRenderer = $(this).data('dialog-renderer');
        element_settings.dialog = $(this).data('dialog-options');
        element_settings.base = $(this).attr('id');
        element_settings.element = this;
        Drupal.ajax(element_settings);

        // For anchor tags with 'data-hash' attribute, add the hash to current
        // pages location.
        // @see \Drupal\webform_ui\WebformUiEntityElementsForm::getElementRow
        // @see Drupal.behaviors.webformFormTabs
        var hash = $(this).data('hash');
        if (hash) {
          $(this).on('click', function () {
            location.hash = $(this).data('hash');
          });
        }

        // Close all open modal dialogs when opening off-canvas dialog.
        if (element_settings.dialogRenderer === 'off_canvas') {
          $(this).on('click', function () {
            $(".ui-dialog.webform-ui-dialog:visible").find('.ui-dialog-content').dialog('close');
          });
        }
      });
    }
  };

  /**
   * Provide Ajax callback for confirmation back to link.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the behavior to confirmation back to link.
   */
  Drupal.behaviors.webformConfirmationBackAjax = {
    attach: function (context) {
      $('.js-webform-confirmation-back-link-ajax', context)
        .once('webform-confirmation-back-ajax')
        .click(function (event) {
          var $form = $(this).parents('form');

          // Trigger the Ajax call back for the hidden submit button.
          // @see \Drupal\webform\WebformSubmissionForm::getCustomForm
          $form.find('.js-webform-confirmation-back-submit-ajax').click();

          // Move the progress indicator from the submit button to after this link.
          // @todo Figure out a better way to set a progress indicator.
          var $progress_indicator = $form.find('.ajax-progress');
          if ($progress_indicator) {
            $(this).after($progress_indicator);
          }

          // Cancel the click event.
          event.preventDefault();
          event.stopPropagation();
        });
    }
  };

  /****************************************************************************/
  // Ajax commands.
  /****************************************************************************/

  /**
   * Track the updated table row key.
   */
  var updateKey;

  /**
   * Command to insert new content into the DOM.
   *
   * @param {Drupal.Ajax} ajax
   *   {@link Drupal.Ajax} object created by {@link Drupal.ajax}.
   * @param {object} response
   *   The response from the Ajax request.
   * @param {string} response.data
   *   The data to use with the jQuery method.
   * @param {string} [response.method]
   *   The jQuery DOM manipulation method to be used.
   * @param {string} [response.selector]
   *   A optional jQuery selector string.
   * @param {object} [response.settings]
   *   An optional array of settings that will be used.
   * @param {number} [status]
   *   The XMLHttpRequest status.
   */
  Drupal.AjaxCommands.prototype.webformInsert = function (ajax, response, status) {
    // Insert the HTML.
    this.insert(ajax, response, status);

    // Scroll to and highlight the updated table row.
    if (updateKey) {
      var $element = $('tr[data-webform-key="' + updateKey + '"]');

      // Highlight the updated element's row.
      $element.addClass('color-success');
      setTimeout(function () { $element.removeClass('color-success') }, 3000);

      // Focus first tabbable item for the updated elements and handlers.
      $element.find(':tabbable:not(.tabledrag-handle)').eq(0).focus();

      // Scroll to elements that are not visible.
      if (!isScrolledIntoView($element)) {
        $('html, body').animate({ scrollTop: $element.offset().top - Drupal.webform.ajax.scrollTopOffset }, 500);
      }
    }
    else {
      // Focus main content.
      $('#main-content').focus();
    }

    updateKey = null; // Reset element update.

    // Display main page's status message in a floating container.
    var $wrapper = $(response.selector);
    if ($wrapper.parents('.ui-dialog').length === 0) {
      var $messages = $wrapper.find('.messages');
      if ($messages.length) {
        var $floatingMessage = $('#webform-ajax-messages');
        if ($floatingMessage.length === 0) {
          $floatingMessage = $('<div id="webform-ajax-messages" class="webform-ajax-messages"></div>');
          $('body').append($floatingMessage);
        }
        if ($floatingMessage.is(":animated")) {
          $floatingMessage.stop(true, true);
        }
        $floatingMessage.html($messages).show().delay(3000).fadeOut(1000);
      }
    }
  };

  /**
   * Scroll to top ajax command.
   *
   * @param {Drupal.Ajax} [ajax]
   *   A {@link Drupal.ajax} object.
   * @param {object} response
   *   Ajax response.
   * @param {string} response.selector
   *   Selector to use.
   *
   * @see Drupal.AjaxCommands.prototype.viewScrollTop
   */
  Drupal.AjaxCommands.prototype.webformScrollTop = function (ajax, response) {
    // Scroll to the top of the view. This will allow users
    // to browse newly loaded content after e.g. clicking a pager
    // link.
    var offset = $(response.selector).offset();
    // We can't guarantee that the scrollable object should be
    // the body, as the view could be embedded in something
    // more complex such as a modal popup. Recurse up the DOM
    // and scroll the first element that has a non-zero top.
    var scrollTarget = response.selector;
    while ($(scrollTarget).scrollTop() === 0 && $(scrollTarget).parent()) {
      scrollTarget = $(scrollTarget).parent();
    }

    if (response.target == 'page' && $(scrollTarget).length && $(scrollTarget)[0].tagName === 'HTML') {
      // Scroll to top when scroll target is the entire page.
      // @see https://stackoverflow.com/questions/123999/how-to-tell-if-a-dom-element-is-visible-in-the-current-viewport
      var rect = $(scrollTarget)[0].getBoundingClientRect();
      if (!(rect.top >= 0 && rect.left >= 0 && rect.bottom <= $(window).height() && rect.right <= $(window).width())) {
        $(scrollTarget).animate({ scrollTop: 0 }, 500);
      }
    }
    else {
      // Only scroll upward.
      if (offset.top - Drupal.webform.ajax.scrollTopOffset < $(scrollTarget).scrollTop()) {
        $(scrollTarget).animate({ scrollTop: (offset.top - Drupal.webform.ajax.scrollTopOffset) }, 500);
      }
    }

    // Focus on the form wrapper content bookmark if
    // .js-webform-autofocus is not enabled.
    // @see \Drupal\webform\Form\WebformAjaxFormTrait::buildAjaxForm
    var $form = $(response.selector + '-content').find('form');
    if (!$form.hasClass('js-webform-autofocus')) {
      $(response.selector + '-content').focus();
    }
  };

  /**
   * Command to refresh the current webform page.
   *
   * @param {Drupal.Ajax} [ajax]
   *   {@link Drupal.Ajax} object created by {@link Drupal.ajax}.
   * @param {object} response
   *   The response from the Ajax request.
   * @param {string} response.url
   *   The URL to redirect to.
   * @param {number} [status]
   *   The XMLHttpRequest status.
   */
  Drupal.AjaxCommands.prototype.webformRefresh = function (ajax, response, status) {
    // Get URL path name.
    // @see https://stackoverflow.com/questions/6944744/javascript-get-portion-of-url-path
    var a = document.createElement('a');
    a.href = response.url;
    if (a.pathname == window.location.pathname && $('.webform-ajax-refresh').length) {
      updateKey = (response.url.match(/[\?|&]update=(.*)($|&)/)) ? RegExp.$1 : null;
      $('.webform-ajax-refresh').click();
    }
    else {
      this.redirect(ajax, response, status);
    }
  };

  /**
   * Command to close a dialog.
   *
   * If no selector is given, it defaults to trying to close the modal.
   *
   * @param {Drupal.Ajax} [ajax]
   * @param {object} response
   * @param {string} response.selector
   * @param {bool} response.persist
   * @param {number} [status]
   */
  Drupal.AjaxCommands.prototype.webformCloseDialog = function (ajax, response, status) {
    if ($('#drupal-off-canvas').length) {
      // Close off-canvas system tray which is not triggered by close dialog
      // command.
      // @see Drupal.behaviors.offCanvasEvents
      $('#drupal-off-canvas').remove();
      $('body').removeClass('js-tray-open');
      // Remove all *.off-canvas events
      $(document).off('.off-canvas');
      $(window).off('.off-canvas');
      var edge = document.documentElement.dir === 'rtl' ? 'left' : 'right';
      var $mainCanvasWrapper = $('[data-off-canvas-main-canvas]');
      $mainCanvasWrapper.css('padding-' + edge, 0);
    }
    else {
      // https://stackoverflow.com/questions/15763909/jquery-ui-dialog-check-if-exists-by-instance-method
      if ($(response.selector).hasClass('ui-dialog-content')) {
        this.closeDialog(ajax, response, status);
      }
    }
  };

  /**
   * Triggers audio UAs to read the supplied text.
   *
   * @param {Drupal.Ajax} [ajax]
   *   A {@link Drupal.ajax} object.
   * @param {object} response
   *   Ajax response.
   * @param {string} response.text
   *   A string to be read by the UA.
   * @param {string} [response.priority='polite']
   *   A string to indicate the priority of the message. Can be either
   *   'polite' or 'assertive'.
   *
   * @see Drupal.announce
   */
  Drupal.AjaxCommands.prototype.webformAnnounce = function (ajax, response) {
    // Delay the announcement.
    setTimeout(function () { Drupal.announce(response.text, response.priority); }, 200);
  };

  /****************************************************************************/
  // Helper functions.
  /****************************************************************************/

  /**
   * Determine if element is visible in the viewport.
   *
   * @param element
   *   An element.
   *
   * @returns {boolean}
   *   TRUE if element is visible in the viewport.
   *
   * @see https://stackoverflow.com/questions/487073/check-if-element-is-visible-after-scrolling
   */
  function isScrolledIntoView(element) {
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(element).offset().top;
    var elemBottom = elemTop + $(element).height();

    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
  }

})(jQuery, Drupal);
;
/*!
* jquery.inputmask.bundle.js
* https://github.com/RobinHerbots/Inputmask
* Copyright (c) 2010 - 2018 Robin Herbots
* Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php)
* Version: 4.0.0
*/

!function (e) { var t = {}; function n(i) { if (t[i]) return t[i].exports; var a = t[i] = { i: i, l: !1, exports: {} }; return e[i].call(a.exports, a, a.exports, n), a.l = !0, a.exports } n.m = e, n.c = t, n.d = function (e, t, i) { n.o(e, t) || Object.defineProperty(e, t, { configurable: !1, enumerable: !0, get: i }) }, n.n = function (e) { var t = e && e.__esModule ? function () { return e.default } : function () { return e }; return n.d(t, "a", t), t }, n.o = function (e, t) { return Object.prototype.hasOwnProperty.call(e, t) }, n.p = "", n(n.s = 3) }([function (e, t, n) { "use strict"; var i, a, r, o; "function" == typeof Symbol && Symbol.iterator; o = function (e) { return e }, a = [n(2)], void 0 === (r = "function" == typeof (i = o) ? i.apply(t, a) : i) || (e.exports = r) }, function (e, t, n) { "use strict"; var i, a, r, o, s = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) { return typeof e } : function (e) { return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e }; o = function (e, t, n, i) { var a = navigator.userAgent, r = f("touchstart"), o = /iemobile/i.test(a), l = /iphone/i.test(a) && !o; function u(t, n, a) { if (!(this instanceof u)) return new u(t, n, a); this.el = i, this.events = {}, this.maskset = i, this.refreshValue = !1, !0 !== a && (e.isPlainObject(t) ? n = t : (n = n || {}, t && (n.alias = t)), this.opts = e.extend(!0, {}, this.defaults, n), this.noMasksCache = n && n.definitions !== i, this.userOptions = n || {}, this.isRTL = this.opts.numericInput, c(this.opts.alias, n, this.opts)) } function c(t, n, a) { var r = u.prototype.aliases[t]; return r ? (r.alias && c(r.alias, i, a), e.extend(!0, a, r), e.extend(!0, a, n), !0) : (null === a.mask && (a.mask = t), !1) } function p(t, n) { function a(t, a, r) { var o = !1; if (null !== t && "" !== t || ((o = null !== r.regex) ? t = (t = r.regex).replace(/^(\^)(.*)(\$)$/, "$2") : (o = !0, t = ".*")), 1 === t.length && !1 === r.greedy && 0 !== r.repeat && (r.placeholder = ""), r.repeat > 0 || "*" === r.repeat || "+" === r.repeat) { var s = "*" === r.repeat ? 0 : "+" === r.repeat ? 1 : r.repeat; t = r.groupmarker[0] + t + r.groupmarker[1] + r.quantifiermarker[0] + s + "," + r.repeat + r.quantifiermarker[1] } var l, c = o ? "regex_" + r.regex : r.numericInput ? t.split("").reverse().join("") : t; return u.prototype.masksCache[c] === i || !0 === n ? (l = { mask: t, maskToken: u.prototype.analyseMask(t, o, r), validPositions: {}, _buffer: i, buffer: i, tests: {}, excludes: {}, metadata: a, maskLength: i }, !0 !== n && (u.prototype.masksCache[c] = l, l = e.extend(!0, {}, u.prototype.masksCache[c]))) : l = e.extend(!0, {}, u.prototype.masksCache[c]), l } if (e.isFunction(t.mask) && (t.mask = t.mask(t)), e.isArray(t.mask)) { if (t.mask.length > 1) { if (null === t.keepStatic) { t.keepStatic = "auto"; for (var r = 0; r < t.mask.length; r++)if (t.mask[r].charAt(0) !== t.mask[0].charAt(0)) { t.keepStatic = !0; break } } var o = t.groupmarker[0]; return e.each(t.isRTL ? t.mask.reverse() : t.mask, function (n, a) { o.length > 1 && (o += t.groupmarker[1] + t.alternatormarker + t.groupmarker[0]), a.mask === i || e.isFunction(a.mask) ? o += a : o += a.mask }), a(o += t.groupmarker[1], t.mask, t) } t.mask = t.mask.pop() } return t.mask && t.mask.mask !== i && !e.isFunction(t.mask.mask) ? a(t.mask.mask, t.mask, t) : a(t.mask, t.mask, t) } function f(e) { var t = n.createElement("input"), i = "on" + e, a = i in t; return a || (t.setAttribute(i, "return;"), a = "function" == typeof t[i]), t = null, a } function m(a, c, p) { c = c || this.maskset, p = p || this.opts; var d, h, g, v, k = this, y = this.el, b = this.isRTL, x = !1, P = !1, S = !1, A = !1; function C(e, t, n, a, r) { !0 !== a && (i, 0); var o = p.greedy; r && (p.greedy = !1), t = t || 0; var s, l, u, c = [], f = 0, m = M(); do { if (!0 === e && E().validPositions[f]) l = (u = r && !0 === E().validPositions[f].match.optionality && E().validPositions[f + 1] === i && (!0 === E().validPositions[f].generatedInput || E().validPositions[f].input == p.skipOptionalPartCharacter && f > 0) ? _(f, R(f, s, f - 1)) : E().validPositions[f]).match, s = u.locator.slice(), c.push(!0 === n ? u.input : !1 === n ? l.nativeDef : W(f, l)); else { l = (u = j(f, s, f - 1)).match, s = u.locator.slice(); var d = !0 !== a && (!1 !== p.jitMasking ? p.jitMasking : l.jit); !1 === d || d === i || f < m || "number" == typeof d && isFinite(d) && d > f ? c.push(!1 === n ? l.nativeDef : W(f, l)) : l.jit && l.optionalQuantifier !== i && (f, 0) } "auto" === p.keepStatic && l.newBlockMarker && null !== l.fn && (p.keepStatic = f - 1), f++ } while ((g === i || f < g) && (null !== l.fn || "" !== l.def) || t > f); return "" === c[c.length - 1] && c.pop(), !1 === n && E().maskLength !== i || (E().maskLength = f - 1), p.greedy = o, c } function E() { return c } function w(e) { var t = E(); t.buffer = i, !0 !== e && (t.validPositions = {}, t.p = 0) } function M(e, t, n) { var a = -1, r = -1, o = n || E().validPositions; for (var s in e === i && (e = -1), o) { var l = parseInt(s); o[l] && (t || !0 !== o[l].generatedInput) && (l <= e && (a = l), l >= e && (r = l)) } return -1 === a || a == e ? r : -1 == r ? a : e - a < r - e ? a : r } function D(e) { var t = e.locator[e.alternation]; return "string" == typeof t && t.length > 0 && (t = t.split(",")[0]), t !== i ? t.toString() : "" } function O(e, t) { var n = (e.alternation != i ? e.mloc[D(e)] : e.locator).join(""); if ("" !== n) for (; n.length < t;)n += "0"; return n } function _(e, t) { for (var n, a, r, o = O(F(e = e > 0 ? e - 1 : 0)), s = 0; s < t.length; s++) { var l = t[s]; n = O(l, o.length); var u = Math.abs(n - o); (a === i || "" !== n && u < a || r && r.match.optionality && "master" === r.match.newBlockMarker && (!l.match.optionality || !l.match.newBlockMarker) || r && r.match.optionalQuantifier && !l.match.optionalQuantifier) && (a = u, r = l) } return r } function j(e, t, n) { return E().validPositions[e] || _(e, R(e, t ? t.slice() : t, n)) } function F(e, t) { return E().validPositions[e] ? E().validPositions[e] : (t || R(e))[0] } function T(e, t) { for (var n = !1, i = R(e), a = 0; a < i.length; a++)if (i[a].match && i[a].match.def === t) { n = !0; break } return n } function R(t, n, a) { var r, o, s, l, u = E().maskToken, c = n ? a : 0, f = n ? n.slice() : [0], m = [], d = !1, h = n ? n.join("") : ""; function g(n, a, o, s) { function l(o, s, u) { function f(t, n) { var i = 0 === e.inArray(t, n.matches); return i || e.each(n.matches, function (e, a) { if (!0 === a.isQuantifier ? i = f(t, n.matches[e - 1]) : !0 === a.isOptional ? i = f(t, a) : !0 === a.isAlternate && (i = f(t, a)), i) return !1 }), i } function v(t, n, a) { var r, o; if ((E().tests[t] || E().validPositions[t]) && e.each(E().tests[t] || [E().validPositions[t]], function (e, t) { if (t.mloc[n]) return r = t, !1; var s = a !== i ? a : t.alternation, l = t.locator[s] !== i ? t.locator[s].toString().indexOf(n) : -1; (o === i || l < o) && -1 !== l && (r = t, o = l) }), r) { var s = r.locator[r.alternation]; return (r.mloc[n] || r.mloc[s] || r.locator).slice((a !== i ? a : r.alternation) + 1) } return a !== i ? v(t, n) : i } function k(e, t) { function n(e) { for (var t, n, i = [], a = 0, r = e.length; a < r; a++)if ("-" === e.charAt(a)) for (n = e.charCodeAt(a + 1); ++t < n;)i.push(String.fromCharCode(t)); else t = e.charCodeAt(a), i.push(e.charAt(a)); return i.join("") } return p.regex && null !== e.match.fn && null !== t.match.fn ? -1 !== n(t.match.def.replace(/[\[\]]/g, "")).indexOf(n(e.match.def.replace(/[\[\]]/g, ""))) : e.match.def === t.match.nativeDef } function y(e, t) { if (t === i || e.alternation === t.alternation && -1 === e.locator[e.alternation].toString().indexOf(t.locator[t.alternation])) { e.mloc = e.mloc || {}; var n = e.locator[e.alternation]; if (n !== i) { if ("string" == typeof n && (n = n.split(",")[0]), e.mloc[n] === i && (e.mloc[n] = e.locator.slice()), t !== i) { for (var a in t.mloc) "string" == typeof a && (a = a.split(",")[0]), e.mloc[a] === i && (e.mloc[a] = t.mloc[a]); e.locator[e.alternation] = Object.keys(e.mloc).join(",") } return !0 } e.alternation = i } return !1 } if (c > 5e3) throw "Inputmask: There is probably an error in your mask definition or in the code. Create an issue on github with an example of the mask you are using. " + E().mask; if (c === t && o.matches === i) return m.push({ match: o, locator: s.reverse(), cd: h, mloc: {} }), !0; if (o.matches !== i) { if (o.isGroup && u !== o) { if (o = l(n.matches[e.inArray(o, n.matches) + 1], s, u)) return !0 } else if (o.isOptional) { var b = o; if (o = g(o, a, s, u)) { if (e.each(m, function (e, t) { t.match.optionality = !0 }), r = m[m.length - 1].match, u !== i || !f(r, b)) return !0; d = !0, c = t } } else if (o.isAlternator) { var x, P = o, S = [], A = m.slice(), C = s.length, w = a.length > 0 ? a.shift() : -1; if (-1 === w || "string" == typeof w) { var M, D = c, O = a.slice(), _ = []; if ("string" == typeof w) _ = w.split(","); else for (M = 0; M < P.matches.length; M++)_.push(M.toString()); if (E().excludes[t]) { for (var j = _.slice(), F = 0, T = E().excludes[t].length; F < T; F++)_.splice(_.indexOf(E().excludes[t][F].toString()), 1); 0 === _.length && (E().excludes[t] = i, _ = j) } (!0 === p.keepStatic || isFinite(parseInt(p.keepStatic)) && D >= p.keepStatic) && (_ = _.slice(0, 1)); for (var R = !1, N = 0; N < _.length; N++) { M = parseInt(_[N]), m = [], a = "string" == typeof w && v(c, M, C) || O.slice(), P.matches[M] && l(P.matches[M], [M].concat(s), u) ? o = !0 : 0 === N && (R = !0), x = m.slice(), c = D, m = []; for (var G = 0; G < x.length; G++) { var I = x[G], B = !1; I.match.jit = I.match.jit || R, I.alternation = I.alternation || C, y(I); for (var L = 0; L < S.length; L++) { var H = S[L]; if ("string" != typeof w || I.alternation !== i && -1 !== e.inArray(I.locator[I.alternation].toString(), _)) { if (I.match.nativeDef === H.match.nativeDef) { B = !0, y(H, I); break } if (k(I, H)) { y(I, H) && (B = !0, S.splice(S.indexOf(H), 0, I)); break } if (k(H, I)) { y(H, I); break } if (q = H, null === (z = I).match.fn && null !== q.match.fn && q.match.fn.test(z.match.def, E(), t, !1, p, !1)) { y(I, H) && (B = !0, S.splice(S.indexOf(H), 0, I)); break } } } B || S.push(I) } } m = A.concat(S), c = t, d = m.length > 0, o = S.length > 0, a = O.slice() } else o = l(P.matches[w] || n.matches[w], [w].concat(s), u); if (o) return !0 } else if (o.isQuantifier && u !== n.matches[e.inArray(o, n.matches) - 1]) for (var V = o, K = a.length > 0 ? a.shift() : 0; K < (isNaN(V.quantifier.max) ? K + 1 : V.quantifier.max) && c <= t; K++) { var U = n.matches[e.inArray(V, n.matches) - 1]; if (o = l(U, [K].concat(s), U)) { if ((r = m[m.length - 1].match).optionalQuantifier = K > V.quantifier.min - 1, r.jit = K + U.matches.indexOf(r) >= V.quantifier.jit, f(r, U) && K > V.quantifier.min - 1) { d = !0, c = t; break } if (V.quantifier.jit !== i && isNaN(V.quantifier.max) && r.optionalQuantifier && E().validPositions[t - 1] === i) { m.pop(), d = !0, c = t, h = i; break } return !0 } } else if (o = g(o, a, s, u)) return !0 } else c++; var z, q } for (var u = a.length > 0 ? a.shift() : 0; u < n.matches.length; u++)if (!0 !== n.matches[u].isQuantifier) { var f = l(n.matches[u], [u].concat(o), s); if (f && c === t) return f; if (c > t) break } } if (t > -1) { if (n === i) { for (var v, k = t - 1; (v = E().validPositions[k] || E().tests[k]) === i && k > -1;)k--; v !== i && k > -1 && (o = k, s = v, l = [], e.isArray(s) || (s = [s]), s.length > 0 && (s[0].alternation === i ? 0 === (l = _(o, s.slice()).locator.slice()).length && (l = s[0].locator.slice()) : e.each(s, function (e, t) { if ("" !== t.def) if (0 === l.length) l = t.locator.slice(); else for (var n = 0; n < l.length; n++)t.locator[n] && -1 === l[n].toString().indexOf(t.locator[n]) && (l[n] += "," + t.locator[n]) })), h = (f = l).join(""), c = k) } if (E().tests[t] && E().tests[t][0].cd === h) return E().tests[t]; for (var y = f.shift(); y < u.length; y++) { if (g(u[y], f, [y]) && c === t || c > t) break } } return (0 === m.length || d) && m.push({ match: { fn: null, optionality: !1, casing: null, def: "", placeholder: "" }, locator: [], mloc: {}, cd: h }), n !== i && E().tests[t] ? e.extend(!0, [], m) : (E().tests[t] = e.extend(!0, [], m), E().tests[t]) } function N() { return E()._buffer === i && (E()._buffer = C(!1, 1), E().buffer === i && (E().buffer = E()._buffer.slice())), E()._buffer } function G(e) { return E().buffer !== i && !0 !== e || (E().buffer = C(!0, M(), !0)), E().buffer } function I(e, t, n) { var a, r; if (!0 === e) w(), e = 0, t = n.length; else for (a = e; a < t; a++)delete E().validPositions[a]; for (r = e, a = e; a < t; a++)if (w(!0), n[a] !== p.skipOptionalPartCharacter) { var o = H(r, n[a], !0, !0); !1 !== o && (w(!0), r = o.caret !== i ? o.caret : o.pos + 1) } } function B(t, n, a) { for (var r, o = p.greedy ? n : n.slice(0, 1), s = !1, l = a !== i ? a.split(",") : [], u = 0; u < l.length; u++)-1 !== (r = t.indexOf(l[u])) && t.splice(r, 1); for (var c = 0; c < t.length; c++)if (-1 !== e.inArray(t[c], o)) { s = !0; break } return s } function L(t, n, a, r, o) { var s, l, u, c, p, f, m, d = e.extend(!0, {}, E().validPositions), h = !1, g = o !== i ? o : M(); if (-1 === g && o === i) l = (c = F(s = 0)).alternation; else for (; g >= 0; g--)if ((u = E().validPositions[g]) && u.alternation !== i) { if (c && c.locator[u.alternation] !== u.locator[u.alternation]) break; s = g, l = E().validPositions[s].alternation, c = u } if (l !== i) { m = parseInt(s), E().excludes[m] = E().excludes[m] || [], !0 !== t && E().excludes[m].push(D(c)); var v = [], k = 0; for (p = m; p < M(i, !0) + 1; p++)(f = E().validPositions[p]) && !0 !== f.generatedInput ? v.push(f.input) : p < t && k++ , delete E().validPositions[p]; for (; E().excludes[m] && E().excludes[m].length < 10;) { var y = -1 * k, b = v.slice(); for (E().tests[m] = i, w(!0), h = !0; b.length > 0;) { var x = b.shift(); if (!(h = H(M(i, !0) + 1, x, !1, r, !0))) break } if (h && n !== i) { var P = M(t) + 1; for (p = m; p < M() + 1; p++)((f = E().validPositions[p]) === i || null == f.match.fn) && p < t + y && y++; h = H((t += y) > P ? P : t, n, a, r, !0) } if (h) break; if (w(), c = F(m), E().validPositions = e.extend(!0, {}, d), !E().excludes[m]) { h = L(t, n, a, r, m - 1); break } var S = D(c); if (-1 !== E().excludes[m].indexOf(S)) { h = L(t, n, a, r, m - 1); break } for (E().excludes[m].push(S), p = m; p < M(i, !0) + 1; p++)delete E().validPositions[p] } } return E().excludes[m] = i, h } function H(t, n, a, r, o, s) { function l(e) { return b ? e.begin - e.end > 1 || e.begin - e.end == 1 : e.end - e.begin > 1 || e.end - e.begin == 1 } a = !0 === a; var c = t; function f(n, a, o) { var s = !1; return e.each(R(n), function (c, f) { var m = f.match; if (G(!0), !1 !== (s = null != m.fn ? m.fn.test(a, E(), n, o, p, l(t)) : (a === m.def || a === p.skipOptionalPartCharacter) && "" !== m.def && { c: W(n, m, !0) || m.def, pos: n })) { var d = s.c !== i ? s.c : a, h = n; return d = d === p.skipOptionalPartCharacter && null === m.fn ? W(n, m, !0) || m.def : d, s.remove !== i && (e.isArray(s.remove) || (s.remove = [s.remove]), e.each(s.remove.sort(function (e, t) { return t - e }), function (e, t) { K({ begin: t, end: t + 1 }) })), s.insert !== i && (e.isArray(s.insert) || (s.insert = [s.insert]), e.each(s.insert.sort(function (e, t) { return e - t }), function (e, t) { H(t.pos, t.c, !0, r) })), !0 !== s && s.pos !== i && s.pos !== n && (h = s.pos), !0 !== s && s.pos === i && s.c === i ? !1 : (K(t, e.extend({}, f, { input: function (t, n, i) { switch (p.casing || n.casing) { case "upper": t = t.toUpperCase(); break; case "lower": t = t.toLowerCase(); break; case "title": var a = E().validPositions[i - 1]; t = 0 === i || a && a.input === String.fromCharCode(u.keyCode.SPACE) ? t.toUpperCase() : t.toLowerCase(); break; default: if (e.isFunction(p.casing)) { var r = Array.prototype.slice.call(arguments); r.push(E().validPositions), t = p.casing.apply(this, r) } }return t }(d, m, h) }), r, h) || (s = !1), !1) } }), s } t.begin !== i && (c = b ? t.end : t.begin); var m = !0, d = e.extend(!0, {}, E().validPositions); if (e.isFunction(p.preValidation) && !a && !0 !== r && !0 !== s && (m = p.preValidation(G(), c, n, l(t), p, E())), !0 === m) { if (V(i, c, !0), (g === i || c < g) && (m = f(c, n, a), (!a || !0 === r) && !1 === m && !0 !== s)) { var h = E().validPositions[c]; if (!h || null !== h.match.fn || h.match.def !== n && n !== p.skipOptionalPartCharacter) { if ((p.insertMode || E().validPositions[z(c)] === i) && !U(c, !0)) for (var v = c + 1, k = z(c); v <= k; v++)if (!1 !== (m = f(v, n, a))) { m = V(c, m.pos !== i ? m.pos : v) || m, c = v; break } } else m = { caret: z(c) } } !1 !== m || !1 === p.keepStatic || null != p.regex && !ae(G()) || a || !0 === o || (m = L(c, n, a, r)), !0 === m && (m = { pos: c }) } if (e.isFunction(p.postValidation) && !1 !== m && !a && !0 !== r && !0 !== s) { var y = p.postValidation(G(!0), m, p); if (y !== i) { if (y.refreshFromBuffer && y.buffer) { var x = y.refreshFromBuffer; I(!0 === x ? x : x.start, x.end, y.buffer) } m = !0 === y ? m : y } } return m && m.pos === i && (m.pos = c), !1 !== m && !0 !== s || (w(!0), E().validPositions = e.extend(!0, {}, d)), m } function V(t, n, a) { var r; if (t === i) for (t = n - 1; t > 0 && !E().validPositions[t]; t--); for (var o = t; o < n; o++) { if (E().validPositions[o] === i && !U(o, !0)) if (0 == o ? F(o) : E().validPositions[o - 1]) { var s = R(o).slice(); "" === s[s.length - 1].match.def && s.pop(); var l = _(o, s); if ((l = e.extend({}, l, { input: W(o, l.match, !0) || l.match.def })).generatedInput = !0, K(o, l, !0), !0 !== a) { var u = E().validPositions[n].input; E().validPositions[n] = i, r = H(n, u, !0, !0) } } } return r } function K(t, n, a, r) { function o(e, t, n) { var a = t[e]; if (a !== i && (null === a.match.fn && !0 !== a.match.optionality || a.input === p.radixPoint)) { var r = n.begin <= e - 1 ? t[e - 1] && null === t[e - 1].match.fn && t[e - 1] : t[e - 1], o = n.end > e + 1 ? t[e + 1] && null === t[e + 1].match.fn && t[e + 1] : t[e + 1]; return r && o } return !1 } var s = t.begin !== i ? t.begin : t, l = t.end !== i ? t.end : t; if (t.begin > t.end && (s = t.end, l = t.begin), r = r !== i ? r : s, s !== l || p.insertMode && E().validPositions[r] !== i && a === i) { var u = e.extend(!0, {}, E().validPositions), c = M(i, !0); for (E().p = s, g = c; g >= s; g--)E().validPositions[g] && "+" === E().validPositions[g].match.nativeDef && (p.isNegative = !1), delete E().validPositions[g]; var f = !0, m = r, d = (E().validPositions, !1), h = m, g = m; for (n && (E().validPositions[r] = e.extend(!0, {}, n), h++ , m++ , s < l && g++); g <= c; g++) { var v = u[g]; if (v !== i && (g >= l || g >= s && !0 !== v.generatedInput && o(g, u, { begin: s, end: l }))) { for (; "" !== F(h).match.def;) { if (!1 === d && u[h] && u[h].match.nativeDef === v.match.nativeDef) E().validPositions[h] = e.extend(!0, {}, u[h]), E().validPositions[h].input = v.input, V(i, h, !0), m = h + 1, f = !0; else if (T(h, v.match.def)) { var k = H(h, v.input, !0, !0); f = !1 !== k, m = k.caret || k.insert ? M() : h + 1, d = !0 } else if (!(f = !0 === v.generatedInput || v.input === p.radixPoint && !0 === p.numericInput) && "" === F(h).match.def) break; if (f) break; h++ } "" == F(h).match.def && (f = !1), h = m } if (!f) break } if (!f) return E().validPositions = e.extend(!0, {}, u), w(!0), !1 } else n && (E().validPositions[r] = e.extend(!0, {}, n)); return w(!0), !0 } function U(e, t) { var n = j(e).match; if ("" === n.def && (n = F(e).match), null != n.fn) return n.fn; if (!0 !== t && e > -1) { var i = R(e); return i.length > 1 + ("" === i[i.length - 1].match.def ? 1 : 0) } return !1 } function z(e, t) { for (var n = e + 1; "" !== F(n).match.def && (!0 === t && (!0 !== F(n).match.newBlockMarker || !U(n)) || !0 !== t && !U(n));)n++; return n } function q(e, t) { var n, i = e; if (i <= 0) return 0; for (; --i > 0 && (!0 === t && !0 !== F(i).match.newBlockMarker || !0 !== t && !U(i) && ((n = R(i)).length < 2 || 2 === n.length && "" === n[1].match.def));); return i } function Q(t, n, a, r, o) { if (r && e.isFunction(p.onBeforeWrite)) { var s = p.onBeforeWrite.call(k, r, n, a, p); if (s) { if (s.refreshFromBuffer) { var l = s.refreshFromBuffer; I(!0 === l ? l : l.start, l.end, s.buffer || n), n = G(!0) } a !== i && (a = s.caret !== i ? s.caret : a) } } if (t !== i && (t.inputmask._valueSet(n.join("")), a === i || r !== i && "blur" === r.type ? se(t, a, 0 === n.length) : te(t, a), !0 === o)) { var u = e(t), c = t.inputmask._valueGet(); P = !0, u.trigger("input"), setTimeout(function () { c === N().join("") ? u.trigger("cleared") : !0 === ae(n) && u.trigger("complete") }, 0) } } function W(t, n, a) { if ((n = n || F(t).match).placeholder !== i || !0 === a) return e.isFunction(n.placeholder) ? n.placeholder(p) : n.placeholder; if (null === n.fn) { if (t > -1 && E().validPositions[t] === i) { var r, o = R(t), s = []; if (o.length > 1 + ("" === o[o.length - 1].match.def ? 1 : 0)) for (var l = 0; l < o.length; l++)if (!0 !== o[l].match.optionality && !0 !== o[l].match.optionalQuantifier && (null === o[l].match.fn || r === i || !1 !== o[l].match.fn.test(r.match.def, E(), t, !0, p)) && (s.push(o[l]), null === o[l].match.fn && (r = o[l]), s.length > 1 && /[0-9a-bA-Z]/.test(s[0].match.def))) return p.placeholder.charAt(t % p.placeholder.length) } return n.def } return p.placeholder.charAt(t % p.placeholder.length) } var $, Z = { on: function (t, n, a) { var s = function (t) { var n = this; if (n.inputmask === i && "FORM" !== this.nodeName) { var s = e.data(n, "_inputmask_opts"); s ? new u(s).mask(n) : Z.off(n) } else { if ("setvalue" === t.type || "FORM" === this.nodeName || !(n.disabled || n.readOnly && !("keydown" === t.type && t.ctrlKey && 67 === t.keyCode || !1 === p.tabThrough && t.keyCode === u.keyCode.TAB))) { switch (t.type) { case "input": if (!0 === P) return P = !1, t.preventDefault(); if (r) { var c = arguments; return setTimeout(function () { a.apply(n, c), te(n, n.inputmask.caretPos, i, !0) }, 0), !1 } break; case "keydown": x = !1, P = !1; break; case "keypress": if (!0 === x) return t.preventDefault(); x = !0; break; case "click": if (o || l) { c = arguments; return setTimeout(function () { a.apply(n, c) }, 0), !1 } }var f = a.apply(n, arguments); return !1 === f && (t.preventDefault(), t.stopPropagation()), f } t.preventDefault() } }; t.inputmask.events[n] = t.inputmask.events[n] || [], t.inputmask.events[n].push(s), -1 !== e.inArray(n, ["submit", "reset"]) ? null !== t.form && e(t.form).on(n, s) : e(t).on(n, s) }, off: function (t, n) { var i; t.inputmask && t.inputmask.events && (n ? (i = [])[n] = t.inputmask.events[n] : i = t.inputmask.events, e.each(i, function (n, i) { for (; i.length > 0;) { var a = i.pop(); -1 !== e.inArray(n, ["submit", "reset"]) ? null !== t.form && e(t.form).off(n, a) : e(t).off(n, a) } delete t.inputmask.events[n] })) } }, J = { keydownEvent: function (t) { var n = this, i = e(n), a = t.keyCode, r = te(n); if (a === u.keyCode.BACKSPACE || a === u.keyCode.DELETE || l && a === u.keyCode.BACKSPACE_SAFARI || t.ctrlKey && a === u.keyCode.X && !f("cut")) t.preventDefault(), re(n, a, r), Q(n, G(!0), E().p, t, n.inputmask._valueGet() !== G().join("")); else if (a === u.keyCode.END || a === u.keyCode.PAGE_DOWN) { t.preventDefault(); var o = z(M()); p.insertMode || o !== E().maskLength || t.shiftKey || o-- , te(n, t.shiftKey ? r.begin : o, o, !0) } else a === u.keyCode.HOME && !t.shiftKey || a === u.keyCode.PAGE_UP ? (t.preventDefault(), te(n, 0, t.shiftKey ? r.begin : 0, !0)) : (p.undoOnEscape && a === u.keyCode.ESCAPE || 90 === a && t.ctrlKey) && !0 !== t.altKey ? (Y(n, !0, !1, d.split("")), i.trigger("click")) : a !== u.keyCode.INSERT || t.shiftKey || t.ctrlKey ? !0 === p.tabThrough && a === u.keyCode.TAB ? (!0 === t.shiftKey ? (null === F(r.begin).match.fn && (r.begin = z(r.begin)), r.end = q(r.begin, !0), r.begin = q(r.end, !0)) : (r.begin = z(r.begin, !0), r.end = z(r.begin, !0), r.end < E().maskLength && r.end--), r.begin < E().maskLength && (t.preventDefault(), te(n, r.begin, r.end))) : t.shiftKey || !1 === p.insertMode && (a === u.keyCode.RIGHT ? setTimeout(function () { var e = te(n); te(n, e.begin) }, 0) : a === u.keyCode.LEFT && setTimeout(function () { var e = te(n); te(n, b ? e.begin + 1 : e.begin - 1) }, 0)) : (p.insertMode = !p.insertMode, te(n, p.insertMode || r.begin !== E().maskLength ? r.begin : r.begin - 1)); p.onKeyDown.call(this, t, G(), te(n).begin, p), S = -1 !== e.inArray(a, p.ignorables) }, keypressEvent: function (t, n, a, r, o) { var s = this, l = e(s), c = t.which || t.charCode || t.keyCode; if (!(!0 === n || t.ctrlKey && t.altKey) && (t.ctrlKey || t.metaKey || S)) return c === u.keyCode.ENTER && d !== G().join("") && (d = G().join(""), setTimeout(function () { l.trigger("change") }, 0)), !0; if (c) { 46 === c && !1 === t.shiftKey && "" !== p.radixPoint && (c = p.radixPoint.charCodeAt(0)); var f, m = n ? { begin: o, end: o } : te(s), h = String.fromCharCode(c), g = 0; if (p._radixDance && p.numericInput) { var v = G().indexOf(p.radixPoint.charAt(0)) + 1; m.begin <= v && (c === p.radixPoint.charCodeAt(0) && (g = 1), m.begin -= 1, m.end -= 1) } E().writeOutBuffer = !0; var k = H(m, h, r); if (!1 !== k && (w(!0), f = k.caret !== i ? k.caret : z(k.pos.begin ? k.pos.begin : k.pos), E().p = f), f = (p.numericInput && k.caret === i ? q(f) : f) + g, !1 !== a && (setTimeout(function () { p.onKeyValidation.call(s, c, k, p) }, 0), E().writeOutBuffer && !1 !== k)) { var y = G(); Q(s, y, f, t, !0 !== n) } if (t.preventDefault(), n) return !1 !== k && (k.forwardPosition = f), k } }, pasteEvent: function (n) { var i, a = this, r = n.originalEvent || n, o = (e(a), a.inputmask._valueGet(!0)), s = te(a); b && (i = s.end, s.end = s.begin, s.begin = i); var l = o.substr(0, s.begin), u = o.substr(s.end, o.length); if (l === (b ? N().reverse() : N()).slice(0, s.begin).join("") && (l = ""), u === (b ? N().reverse() : N()).slice(s.end).join("") && (u = ""), t.clipboardData && t.clipboardData.getData) o = l + t.clipboardData.getData("Text") + u; else { if (!r.clipboardData || !r.clipboardData.getData) return !0; o = l + r.clipboardData.getData("text/plain") + u } var c = o; if (e.isFunction(p.onBeforePaste)) { if (!1 === (c = p.onBeforePaste.call(k, o, p))) return n.preventDefault(); c || (c = o) } return Y(a, !1, !1, c.toString().split("")), Q(a, G(), z(M()), n, d !== G().join("")), n.preventDefault() }, inputFallBackEvent: function (t) { var n, i, a = this, r = a.inputmask._valueGet(); if (G().join("") !== r) { var s = te(a); if (i = s, "." === (n = r).charAt(i.begin - 1) && "" !== p.radixPoint && ((n = n.split(""))[i.begin - 1] = p.radixPoint.charAt(0), n = n.join("")), r = function (e, t, n) { if (o) { var i = t.replace(G().join(""), ""); if (1 === i.length) { var a = t.split(""); a.splice(n.begin, 0, i), t = a.join("") } } return t }(0, r = n, s), G().join("") !== r) { var c = G().join(""), f = !p.numericInput && r.length > c.length ? -1 : 0, m = r.substr(0, s.begin), d = r.substr(s.begin), h = c.substr(0, s.begin + f), g = c.substr(s.begin + f), v = s, k = "", y = !1; if (m !== h) { for (var b = (y = m.length >= h.length) ? m.length : h.length, x = 0; m.charAt(x) === h.charAt(x) && x < b; x++); y && (0 === f && (v.begin = x), k += m.slice(x, v.end)) } if (d !== g && (d.length > g.length ? k += d.slice(0, 1) : d.length < g.length && (v.end += g.length - d.length, y || "" === p.radixPoint || "" !== d || m.charAt(v.begin + f - 1) !== p.radixPoint || (v.begin-- , k = p.radixPoint))), Q(a, G(), { begin: v.begin + f, end: v.end + f }), k.length > 0) e.each(k.split(""), function (t, n) { var i = new e.Event("keypress"); i.which = n.charCodeAt(0), S = !1, J.keypressEvent.call(a, i) }); else { v.begin === v.end - 1 && (v.begin = q(v.begin + 1), v.begin === v.end - 1 ? te(a, v.begin) : te(a, v.begin, v.end)); var P = new e.Event("keydown"); P.keyCode = p.numericInput ? u.keyCode.BACKSPACE : u.keyCode.DELETE, J.keydownEvent.call(a, P), l || !1 !== p.insertMode || te(a, te(a).begin - 1) } t.preventDefault() } } }, beforeInputEvent: function (t) { if (t.cancelable) { var n = this; switch (t.inputType) { case "insertText": return e.each(t.data.split(""), function (t, i) { var a = new e.Event("keypress"); a.which = i.charCodeAt(0), S = !1, J.keypressEvent.call(n, a) }), t.preventDefault(); case "deleteContentBackward": return (i = new e.Event("keydown")).keyCode = u.keyCode.BACKSPACE, J.keydownEvent.call(n, i), t.preventDefault(); case "deleteContentForward": var i; return (i = new e.Event("keydown")).keyCode = u.keyCode.DELETE, J.keydownEvent.call(n, i), t.preventDefault() } } }, setValueEvent: function (t) { this.inputmask.refreshValue = !1; var n = (n = t && t.detail ? t.detail[0] : arguments[1]) || this.inputmask._valueGet(!0); e.isFunction(p.onBeforeMask) && (n = p.onBeforeMask.call(k, n, p) || n), Y(this, !0, !1, n = n.split("")), d = G().join(""), (p.clearMaskOnLostFocus || p.clearIncomplete) && this.inputmask._valueGet() === N().join("") && this.inputmask._valueSet("") }, focusEvent: function (e) { var t = this, n = t.inputmask._valueGet(); p.showMaskOnFocus && (!p.showMaskOnHover || p.showMaskOnHover && "" === n) && (t.inputmask._valueGet() !== G().join("") ? Q(t, G(), z(M())) : !1 === A && te(t, z(M()))), !0 === p.positionCaretOnTab && !1 === A && J.clickEvent.apply(t, [e, !0]), d = G().join("") }, mouseleaveEvent: function (e) { if (A = !1, p.clearMaskOnLostFocus && n.activeElement !== this) { var t = G().slice(), i = this.inputmask._valueGet(); i !== this.getAttribute("placeholder") && "" !== i && (-1 === M() && i === N().join("") ? t = [] : ie(t), Q(this, t)) } }, clickEvent: function (t, a) { var r = this; setTimeout(function () { if (n.activeElement === r) { var t = te(r); if (a && (b ? t.end = t.begin : t.begin = t.end), t.begin === t.end) switch (p.positionCaretOnClick) { case "none": break; case "select": te(r, 0, G().length); break; case "ignore": te(r, z(M())); break; case "radixFocus": if (function (t) { if ("" !== p.radixPoint) { var n = E().validPositions; if (n[t] === i || n[t].input === W(t)) { if (t < z(-1)) return !0; var a = e.inArray(p.radixPoint, G()); if (-1 !== a) { for (var r in n) if (a < r && n[r].input !== W(r)) return !1; return !0 } } } return !1 }(t.begin)) { var o = G().join("").indexOf(p.radixPoint); te(r, p.numericInput ? z(o) : o); break } default: var s = t.begin, l = M(s, !0), u = z(l); if (s < u) te(r, U(s, !0) || U(s - 1, !0) ? s : z(s)); else { var c = E().validPositions[l], f = j(u, c ? c.match.locator : i, c), m = W(u, f.match); if ("" !== m && G()[u] !== m && !0 !== f.match.optionalQuantifier && !0 !== f.match.newBlockMarker || !U(u, p.keepStatic) && f.match.def === m) { var d = z(u); (s >= d || s === u) && (u = d) } te(r, u) } } } }, 0) }, cutEvent: function (i) { e(this); var a = te(this), r = i.originalEvent || i, o = t.clipboardData || r.clipboardData, s = b ? G().slice(a.end, a.begin) : G().slice(a.begin, a.end); o.setData("text", b ? s.reverse().join("") : s.join("")), n.execCommand && n.execCommand("copy"), re(this, u.keyCode.DELETE, a), Q(this, G(), E().p, i, d !== G().join("")) }, blurEvent: function (t) { var n = e(this); if (this.inputmask) { var a = this.inputmask._valueGet(), r = G().slice(); "" === a && v === i || (p.clearMaskOnLostFocus && (-1 === M() && a === N().join("") ? r = [] : ie(r)), !1 === ae(r) && (setTimeout(function () { n.trigger("incomplete") }, 0), p.clearIncomplete && (w(), r = p.clearMaskOnLostFocus ? [] : N().slice())), Q(this, r, i, t)), d !== G().join("") && (d = r.join(""), n.trigger("change")) } }, mouseenterEvent: function (e) { A = !0, n.activeElement !== this && p.showMaskOnHover && this.inputmask._valueGet() !== G().join("") && Q(this, G()) }, submitEvent: function (e) { d !== G().join("") && h.trigger("change"), p.clearMaskOnLostFocus && -1 === M() && y.inputmask._valueGet && y.inputmask._valueGet() === N().join("") && y.inputmask._valueSet(""), p.clearIncomplete && !1 === ae(G()) && y.inputmask._valueSet(""), p.removeMaskOnSubmit && (y.inputmask._valueSet(y.inputmask.unmaskedvalue(), !0), setTimeout(function () { Q(y, G()) }, 0)) }, resetEvent: function (e) { y.inputmask.refreshValue = !0, setTimeout(function () { h.trigger("setvalue") }, 0) } }; function Y(t, n, a, r, o) { var s = this || t.inputmask, l = r.slice(), c = "", f = -1, m = i; if (w(), a || !0 === p.autoUnmask) f = z(f); else { var d = N().slice(0, z(-1)).join(""), h = l.join("").match(new RegExp("^" + u.escapeRegex(d), "g")); h && h.length > 0 && (l.splice(0, h.length * d.length), f = z(f)) } -1 === f ? (E().p = z(f), f = 0) : E().p = f, s.caretPos = { begin: f }, e.each(l, function (n, r) { if (r !== i) if (E().validPositions[n] === i && l[n] === W(n) && U(n, !0) && !1 === H(n, l[n], !0, i, i, !0)) E().p++; else { var o = new e.Event("_checkval"); o.which = r.charCodeAt(0), c += r; var u = M(i, !0); p = f, d = c, -1 === C(!0, 0, !1).slice(p, z(p)).join("").replace(/'/g, "").indexOf(d) || U(p) || !(F(p).match.nativeDef === d.charAt(0) || null === F(p).match.fn && F(p).match.nativeDef === "'" + d.charAt(0) || " " === F(p).match.nativeDef && (F(p + 1).match.nativeDef === d.charAt(0) || null === F(p + 1).match.fn && F(p + 1).match.nativeDef === "'" + d.charAt(0))) ? (m = J.keypressEvent.call(t, o, !0, !1, a, s.caretPos.begin)) && (f = s.caretPos.begin + 1, c = "") : m = J.keypressEvent.call(t, o, !0, !1, a, u + 1), m && (Q(i, G(), m.forwardPosition, o, !1), s.caretPos = { begin: m.forwardPosition, end: m.forwardPosition }) } var p, d }), n && Q(t, G(), m ? m.forwardPosition : i, o || new e.Event("checkval"), o && "input" === o.type) } function X(t) { if (t) { if (t.inputmask === i) return t.value; t.inputmask && t.inputmask.refreshValue && J.setValueEvent.call(t) } var n = [], a = E().validPositions; for (var r in a) a[r].match && null != a[r].match.fn && n.push(a[r].input); var o = 0 === n.length ? "" : (b ? n.reverse() : n).join(""); if (e.isFunction(p.onUnMask)) { var s = (b ? G().slice().reverse() : G()).join(""); o = p.onUnMask.call(k, s, o, p) } return o } function ee(e) { return !b || "number" != typeof e || p.greedy && "" === p.placeholder || !y || (e = y.inputmask._valueGet().length - e), e } function te(a, r, o, s) { var u; if (r === i) return a.setSelectionRange ? (r = a.selectionStart, o = a.selectionEnd) : t.getSelection ? (u = t.getSelection().getRangeAt(0)).commonAncestorContainer.parentNode !== a && u.commonAncestorContainer !== a || (r = u.startOffset, o = u.endOffset) : n.selection && n.selection.createRange && (o = (r = 0 - (u = n.selection.createRange()).duplicate().moveStart("character", -a.inputmask._valueGet().length)) + u.text.length), { begin: s ? r : ee(r), end: s ? o : ee(o) }; if (e.isArray(r) && (o = b ? r[0] : r[1], r = b ? r[1] : r[0]), r.begin !== i && (o = b ? r.begin : r.end, r = b ? r.end : r.begin), "number" == typeof r) { r = s ? r : ee(r), o = "number" == typeof (o = s ? o : ee(o)) ? o : r; var c = parseInt(((a.ownerDocument.defaultView || t).getComputedStyle ? (a.ownerDocument.defaultView || t).getComputedStyle(a, null) : a.currentStyle).fontSize) * o; if (a.scrollLeft = c > a.scrollWidth ? c : 0, l || !1 !== p.insertMode || r !== o || o++ , a.inputmask.caretPos = { begin: r, end: o }, a.setSelectionRange) a.selectionStart = r, a.selectionEnd = o; else if (t.getSelection) { if (u = n.createRange(), a.firstChild === i || null === a.firstChild) { var f = n.createTextNode(""); a.appendChild(f) } u.setStart(a.firstChild, r < a.inputmask._valueGet().length ? r : a.inputmask._valueGet().length), u.setEnd(a.firstChild, o < a.inputmask._valueGet().length ? o : a.inputmask._valueGet().length), u.collapse(!0); var m = t.getSelection(); m.removeAllRanges(), m.addRange(u) } else a.createTextRange && ((u = a.createTextRange()).collapse(!0), u.moveEnd("character", o), u.moveStart("character", r), u.select()); se(a, { begin: r, end: o }) } } function ne(t) { var n, a, r = C(!0, M(), !0, !0), o = r.length, s = M(), l = {}, u = E().validPositions[s], c = u !== i ? u.locator.slice() : i; for (n = s + 1; n < r.length; n++)c = (a = j(n, c, n - 1)).locator.slice(), l[n] = e.extend(!0, {}, a); var p = u && u.alternation !== i ? u.locator[u.alternation] : i; for (n = o - 1; n > s && (((a = l[n]).match.optionality || a.match.optionalQuantifier && a.match.newBlockMarker || p && (p !== l[n].locator[u.alternation] && null != a.match.fn || null === a.match.fn && a.locator[u.alternation] && B(a.locator[u.alternation].toString().split(","), p.toString().split(",")) && "" !== R(n)[0].def)) && r[n] === W(n, a.match)); n--)o--; return t ? { l: o, def: l[o] ? l[o].match : i } : o } function ie(e) { e.length = 0; for (var t, n = C(!0, 0, !0, i, !0); (t = n.shift()) !== i;)e.push(t); return e } function ae(t) { if (e.isFunction(p.isComplete)) return p.isComplete(t, p); if ("*" === p.repeat) return i; var n = !1, a = ne(!0), r = q(a.l); if (a.def === i || a.def.newBlockMarker || a.def.optionality || a.def.optionalQuantifier) { n = !0; for (var o = 0; o <= r; o++) { var s = j(o).match; if (null !== s.fn && E().validPositions[o] === i && !0 !== s.optionality && !0 !== s.optionalQuantifier || null === s.fn && t[o] !== W(o, s)) { n = !1; break } } } return n } function re(e, t, n, a, r) { if ((p.numericInput || b) && (t === u.keyCode.BACKSPACE ? t = u.keyCode.DELETE : t === u.keyCode.DELETE && (t = u.keyCode.BACKSPACE), b)) { var o = n.end; n.end = n.begin, n.begin = o } if (t === u.keyCode.BACKSPACE && (n.end - n.begin < 1 || !1 === p.insertMode) ? (n.begin = q(n.begin), E().validPositions[n.begin] !== i && E().validPositions[n.begin].input === p.groupSeparator && n.begin-- , !1 === p.insertMode && n.end !== E().maskLength && n.end--) : t === u.keyCode.DELETE && n.begin === n.end && (n.end = U(n.end, !0) && E().validPositions[n.end] && E().validPositions[n.end].input !== p.radixPoint ? n.end + 1 : z(n.end) + 1, E().validPositions[n.begin] !== i && E().validPositions[n.begin].input === p.groupSeparator && n.end++), K(n), !0 !== a && !1 !== p.keepStatic || null !== p.regex) { var s = L(!0); if (s) { var l = s.caret !== i ? s.caret : s.pos ? z(s.pos.begin ? s.pos.begin : s.pos) : M(-1, !0); (t !== u.keyCode.DELETE || n.begin > l) && n.begin } } var c = M(n.begin, !0); if (c < n.begin || -1 === n.begin) E().p = z(c); else if (!0 !== a && (E().p = n.begin, !0 !== r)) for (; E().p < c && E().validPositions[E().p] === i;)E().p++ } function oe(i) { var a = (i.ownerDocument.defaultView || t).getComputedStyle(i, null); var r = n.createElement("div"); r.style.width = a.width, r.style.textAlign = a.textAlign, v = n.createElement("div"), i.inputmask.colorMask = v, v.className = "im-colormask", i.parentNode.insertBefore(v, i), i.parentNode.removeChild(i), v.appendChild(i), v.appendChild(r), i.style.left = r.offsetLeft + "px", e(v).on("mouseleave", function (e) { return J.mouseleaveEvent.call(i, [e]) }), e(v).on("mouseenter", function (e) { return J.mouseenterEvent.call(i, [e]) }), e(v).on("click", function (e) { return te(i, function (e) { var t, r = n.createElement("span"); for (var o in a) isNaN(o) && -1 !== o.indexOf("font") && (r.style[o] = a[o]); r.style.textTransform = a.textTransform, r.style.letterSpacing = a.letterSpacing, r.style.position = "absolute", r.style.height = "auto", r.style.width = "auto", r.style.visibility = "hidden", r.style.whiteSpace = "nowrap", n.body.appendChild(r); var s, l = i.inputmask._valueGet(), u = 0; for (t = 0, s = l.length; t <= s; t++) { if (r.innerHTML += l.charAt(t) || "_", r.offsetWidth >= e) { var c = e - u, p = r.offsetWidth - e; r.innerHTML = l.charAt(t), t = (c -= r.offsetWidth / 3) < p ? t - 1 : t; break } u = r.offsetWidth } return n.body.removeChild(r), t }(e.clientX)), J.clickEvent.call(i, [e]) }), e(i).on("keydown", function (e) { e.shiftKey || !1 === p.insertMode || setTimeout(function () { se(i) }, 0) }) } function se(e, t, a) { var r, o, s, l = [], u = !1, c = 0; function f(e) { if (e === i && (e = ""), u || null !== r.fn && o.input !== i) if (u && (null !== r.fn && o.input !== i || "" === r.def)) { u = !1; var t = l.length; l[t - 1] = l[t - 1] + "</span>", l.push(e) } else l.push(e); else u = !0, l.push("<span class='im-static'>" + e) } if (v !== i) { var m = G(); if (t === i ? t = te(e) : t.begin === i && (t = { begin: t, end: t }), !0 !== a) { var d = M(); do { E().validPositions[c] ? (o = E().validPositions[c], r = o.match, s = o.locator.slice(), f(m[c])) : (o = j(c, s, c - 1), r = o.match, s = o.locator.slice(), !1 === p.jitMasking || c < d || "number" == typeof p.jitMasking && isFinite(p.jitMasking) && p.jitMasking > c ? f(W(c, r)) : u = !1), c++ } while ((g === i || c < g) && (null !== r.fn || "" !== r.def) || d > c || u); u && f(), n.activeElement === e && (l.splice(t.begin, 0, t.begin === t.end || t.end > E().maskLength ? '<mark class="im-caret" style="border-right-width: 1px;border-right-style: solid;">' : '<mark class="im-caret-select">'), l.splice(t.end + 1, 0, "</mark>")) } var h = v.getElementsByTagName("div")[0]; h.innerHTML = l.join(""), e.inputmask.positionColorMask(e, h) } } if (u.prototype.positionColorMask = function (e, t) { e.style.left = t.offsetLeft + "px" }, a !== i) switch (a.action) { case "isComplete": return y = a.el, ae(G()); case "unmaskedvalue": return y !== i && a.value === i || ($ = a.value, $ = (e.isFunction(p.onBeforeMask) && p.onBeforeMask.call(k, $, p) || $).split(""), Y.call(this, i, !1, !1, $), e.isFunction(p.onBeforeWrite) && p.onBeforeWrite.call(k, i, G(), 0, p)), X(y); case "mask": !function (t) { Z.off(t); var a = function (t, a) { var r = t.getAttribute("type"), o = "INPUT" === t.tagName && -1 !== e.inArray(r, a.supportsInputType) || t.isContentEditable || "TEXTAREA" === t.tagName; if (!o) if ("INPUT" === t.tagName) { var l = n.createElement("input"); l.setAttribute("type", r), o = "text" === l.type, l = null } else o = "partial"; return !1 !== o ? function (t) { var r, o, l; function u() { return this.inputmask ? this.inputmask.opts.autoUnmask ? this.inputmask.unmaskedvalue() : -1 !== M() || !0 !== a.nullable ? n.activeElement === this && a.clearMaskOnLostFocus ? (b ? ie(G().slice()).reverse() : ie(G().slice())).join("") : r.call(this) : "" : r.call(this) } function c(t) { o.call(this, t), this.inputmask && e(this).trigger("setvalue", [t]) } if (!t.inputmask.__valueGet) { if (!0 !== a.noValuePatching) { if (Object.getOwnPropertyDescriptor) { "function" != typeof Object.getPrototypeOf && (Object.getPrototypeOf = "object" === s("test".__proto__) ? function (e) { return e.__proto__ } : function (e) { return e.constructor.prototype }); var p = Object.getPrototypeOf ? Object.getOwnPropertyDescriptor(Object.getPrototypeOf(t), "value") : i; p && p.get && p.set ? (r = p.get, o = p.set, Object.defineProperty(t, "value", { get: u, set: c, configurable: !0 })) : "INPUT" !== t.tagName && (r = function () { return this.textContent }, o = function (e) { this.textContent = e }, Object.defineProperty(t, "value", { get: u, set: c, configurable: !0 })) } else n.__lookupGetter__ && t.__lookupGetter__("value") && (r = t.__lookupGetter__("value"), o = t.__lookupSetter__("value"), t.__defineGetter__("value", u), t.__defineSetter__("value", c)); t.inputmask.__valueGet = r, t.inputmask.__valueSet = o } t.inputmask._valueGet = function (e) { return b && !0 !== e ? r.call(this.el).split("").reverse().join("") : r.call(this.el) }, t.inputmask._valueSet = function (e, t) { o.call(this.el, null === e || e === i ? "" : !0 !== t && b ? e.split("").reverse().join("") : e) }, r === i && (r = function () { return this.value }, o = function (e) { this.value = e }, function (t) { if (e.valHooks && (e.valHooks[t] === i || !0 !== e.valHooks[t].inputmaskpatch)) { var n = e.valHooks[t] && e.valHooks[t].get ? e.valHooks[t].get : function (e) { return e.value }, r = e.valHooks[t] && e.valHooks[t].set ? e.valHooks[t].set : function (e, t) { return e.value = t, e }; e.valHooks[t] = { get: function (e) { if (e.inputmask) { if (e.inputmask.opts.autoUnmask) return e.inputmask.unmaskedvalue(); var t = n(e); return -1 !== M(i, i, e.inputmask.maskset.validPositions) || !0 !== a.nullable ? t : "" } return n(e) }, set: function (t, n) { var i, a = e(t); return i = r(t, n), t.inputmask && a.trigger("setvalue", [n]), i }, inputmaskpatch: !0 } } }(t.type), l = t, Z.on(l, "mouseenter", function (t) { var n = e(this); this.inputmask._valueGet() !== G().join("") && n.trigger("setvalue") })) } }(t) : t.inputmask = i, o }(t, p); if (!1 !== a && (h = e(y = t), -1 === (g = y !== i ? y.maxLength : i) && (g = i), !0 === p.colorMask && oe(y), r && ("inputmode" in y && (y.inputmode = p.inputmode, y.setAttribute("inputmode", p.inputmode)), !0 === p.disablePredictiveText && ("autocorrect" in y ? y.autocorrect = !1 : (!0 !== p.colorMask && oe(y), y.type = "password"))), !0 === a && (Z.on(y, "submit", J.submitEvent), Z.on(y, "reset", J.resetEvent), Z.on(y, "blur", J.blurEvent), Z.on(y, "focus", J.focusEvent), !0 !== p.colorMask && (Z.on(y, "click", J.clickEvent), Z.on(y, "mouseleave", J.mouseleaveEvent), Z.on(y, "mouseenter", J.mouseenterEvent)), Z.on(y, "paste", J.pasteEvent), Z.on(y, "cut", J.cutEvent), Z.on(y, "complete", p.oncomplete), Z.on(y, "incomplete", p.onincomplete), Z.on(y, "cleared", p.oncleared), r || !0 === p.inputEventOnly ? y.removeAttribute("maxLength") : (Z.on(y, "keydown", J.keydownEvent), Z.on(y, "keypress", J.keypressEvent)), Z.on(y, "input", J.inputFallBackEvent), Z.on(y, "beforeinput", J.beforeInputEvent)), Z.on(y, "setvalue", J.setValueEvent), d = N().join(""), "" !== y.inputmask._valueGet(!0) || !1 === p.clearMaskOnLostFocus || n.activeElement === y)) { var o = e.isFunction(p.onBeforeMask) && p.onBeforeMask.call(k, y.inputmask._valueGet(!0), p) || y.inputmask._valueGet(!0); "" !== o && Y(y, !0, !1, o.split("")); var l = G().slice(); d = l.join(""), !1 === ae(l) && p.clearIncomplete && w(), p.clearMaskOnLostFocus && n.activeElement !== y && (-1 === M() ? l = [] : ie(l)), (!1 === p.clearMaskOnLostFocus || p.showMaskOnFocus && n.activeElement === y || "" !== y.inputmask._valueGet(!0)) && Q(y, l), n.activeElement === y && te(y, z(M())) } }(y); break; case "format": return $ = (e.isFunction(p.onBeforeMask) && p.onBeforeMask.call(k, a.value, p) || a.value).split(""), Y.call(this, i, !0, !1, $), a.metadata ? { value: b ? G().slice().reverse().join("") : G().join(""), metadata: m.call(this, { action: "getmetadata" }, c, p) } : b ? G().slice().reverse().join("") : G().join(""); case "isValid": a.value ? ($ = a.value.split(""), Y.call(this, i, !0, !0, $)) : a.value = G().join(""); for (var le = G(), ue = ne(), ce = le.length - 1; ce > ue && !U(ce); ce--); return le.splice(ue, ce + 1 - ue), ae(le) && a.value === G().join(""); case "getemptymask": return N().join(""); case "remove": if (y && y.inputmask) e.data(y, "_inputmask_opts", null), h = e(y), y.inputmask._valueSet(p.autoUnmask ? X(y) : y.inputmask._valueGet(!0)), Z.off(y), y.inputmask.colorMask && ((v = y.inputmask.colorMask).removeChild(y), v.parentNode.insertBefore(y, v), v.parentNode.removeChild(v)), Object.getOwnPropertyDescriptor && Object.getPrototypeOf ? Object.getOwnPropertyDescriptor(Object.getPrototypeOf(y), "value") && y.inputmask.__valueGet && Object.defineProperty(y, "value", { get: y.inputmask.__valueGet, set: y.inputmask.__valueSet, configurable: !0 }) : n.__lookupGetter__ && y.__lookupGetter__("value") && y.inputmask.__valueGet && (y.__defineGetter__("value", y.inputmask.__valueGet), y.__defineSetter__("value", y.inputmask.__valueSet)), y.inputmask = i; return y; case "getmetadata": if (e.isArray(c.metadata)) { var pe = C(!0, 0, !1).join(""); return e.each(c.metadata, function (e, t) { if (t.mask === pe) return pe = t, !1 }), pe } return c.metadata } } return u.prototype = { dataAttribute: "data-inputmask", defaults: { placeholder: "_", optionalmarker: ["[", "]"], quantifiermarker: ["{", "}"], groupmarker: ["(", ")"], alternatormarker: "|", escapeChar: "\\", mask: null, regex: null, oncomplete: e.noop, onincomplete: e.noop, oncleared: e.noop, repeat: 0, greedy: !1, autoUnmask: !1, removeMaskOnSubmit: !1, clearMaskOnLostFocus: !0, insertMode: !0, clearIncomplete: !1, alias: null, onKeyDown: e.noop, onBeforeMask: null, onBeforePaste: function (t, n) { return e.isFunction(n.onBeforeMask) ? n.onBeforeMask.call(this, t, n) : t }, onBeforeWrite: null, onUnMask: null, showMaskOnFocus: !0, showMaskOnHover: !0, onKeyValidation: e.noop, skipOptionalPartCharacter: " ", numericInput: !1, rightAlign: !1, undoOnEscape: !0, radixPoint: "", _radixDance: !1, groupSeparator: "", keepStatic: null, positionCaretOnTab: !0, tabThrough: !1, supportsInputType: ["text", "tel", "password", "search"], ignorables: [8, 9, 13, 19, 27, 33, 34, 35, 36, 37, 38, 39, 40, 45, 46, 93, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 0, 229], isComplete: null, preValidation: null, postValidation: null, staticDefinitionSymbol: i, jitMasking: !1, nullable: !0, inputEventOnly: !1, noValuePatching: !1, positionCaretOnClick: "lvp", casing: null, inputmode: "verbatim", colorMask: !1, disablePredictiveText: !1, importDataAttributes: !0 }, definitions: { 9: { validator: "[0-9１-９]", definitionSymbol: "*" }, a: { validator: "[A-Za-zА-яЁёÀ-ÿµ]", definitionSymbol: "*" }, "*": { validator: "[0-9１-９A-Za-zА-яЁёÀ-ÿµ]" } }, aliases: {}, masksCache: {}, mask: function (a) { var r = this; return "string" == typeof a && (a = n.getElementById(a) || n.querySelectorAll(a)), a = a.nodeName ? [a] : a, e.each(a, function (n, a) { var o = e.extend(!0, {}, r.opts); if (function (n, a, r, o) { if (!0 === a.importDataAttributes) { var s, l, u, p, f = function (e, a) { null !== (a = a !== i ? a : n.getAttribute(o + "-" + e)) && ("string" == typeof a && (0 === e.indexOf("on") ? a = t[a] : "false" === a ? a = !1 : "true" === a && (a = !0)), r[e] = a) }, m = n.getAttribute(o); if (m && "" !== m && (m = m.replace(/'/g, '"'), l = JSON.parse("{" + m + "}")), l) for (p in u = i, l) if ("alias" === p.toLowerCase()) { u = l[p]; break } for (s in f("alias", u), r.alias && c(r.alias, r, a), a) { if (l) for (p in u = i, l) if (p.toLowerCase() === s.toLowerCase()) { u = l[p]; break } f(s, u) } } return e.extend(!0, a, r), ("rtl" === n.dir || a.rightAlign) && (n.style.textAlign = "right"), ("rtl" === n.dir || a.numericInput) && (n.dir = "ltr", n.removeAttribute("dir"), a.isRTL = !0), Object.keys(r).length }(a, o, e.extend(!0, {}, r.userOptions), r.dataAttribute)) { var s = p(o, r.noMasksCache); s !== i && (a.inputmask !== i && (a.inputmask.opts.autoUnmask = !0, a.inputmask.remove()), a.inputmask = new u(i, i, !0), a.inputmask.opts = o, a.inputmask.noMasksCache = r.noMasksCache, a.inputmask.userOptions = e.extend(!0, {}, r.userOptions), a.inputmask.isRTL = o.isRTL || o.numericInput, a.inputmask.el = a, a.inputmask.maskset = s, e.data(a, "_inputmask_opts", o), m.call(a.inputmask, { action: "mask" })) } }), a && a[0] && a[0].inputmask || this }, option: function (t, n) { return "string" == typeof t ? this.opts[t] : "object" === (void 0 === t ? "undefined" : s(t)) ? (e.extend(this.userOptions, t), this.el && !0 !== n && this.mask(this.el), this) : void 0 }, unmaskedvalue: function (e) { return this.maskset = this.maskset || p(this.opts, this.noMasksCache), m.call(this, { action: "unmaskedvalue", value: e }) }, remove: function () { return m.call(this, { action: "remove" }) }, getemptymask: function () { return this.maskset = this.maskset || p(this.opts, this.noMasksCache), m.call(this, { action: "getemptymask" }) }, hasMaskedValue: function () { return !this.opts.autoUnmask }, isComplete: function () { return this.maskset = this.maskset || p(this.opts, this.noMasksCache), m.call(this, { action: "isComplete" }) }, getmetadata: function () { return this.maskset = this.maskset || p(this.opts, this.noMasksCache), m.call(this, { action: "getmetadata" }) }, isValid: function (e) { return this.maskset = this.maskset || p(this.opts, this.noMasksCache), m.call(this, { action: "isValid", value: e }) }, format: function (e, t) { return this.maskset = this.maskset || p(this.opts, this.noMasksCache), m.call(this, { action: "format", value: e, metadata: t }) }, setValue: function (t) { this.el && e(this.el).trigger("setvalue", [t]) }, analyseMask: function (t, n, a) { var r, o, s, l, c, p, f = /(?:[?*+]|\{[0-9\+\*]+(?:,[0-9\+\*]*)?(?:\|[0-9\+\*]*)?\})|[^.?*+^${[]()|\\]+|./g, m = /\[\^?]?(?:[^\\\]]+|\\[\S\s]?)*]?|\\(?:0(?:[0-3][0-7]{0,2}|[4-7][0-7]?)?|[1-9][0-9]*|x[0-9A-Fa-f]{2}|u[0-9A-Fa-f]{4}|c[A-Za-z]|[\S\s]?)|\((?:\?[:=!]?)?|(?:[?*+]|\{[0-9]+(?:,[0-9]*)?\})\??|[^.?*+^${[()|\\]+|./g, d = !1, h = new k, g = [], v = []; function k(e, t, n, i) { this.matches = [], this.openGroup = e || !1, this.alternatorGroup = !1, this.isGroup = e || !1, this.isOptional = t || !1, this.isQuantifier = n || !1, this.isAlternator = i || !1, this.quantifier = { min: 1, max: 1 } } function y(t, r, o) { o = o !== i ? o : t.matches.length; var s = t.matches[o - 1]; if (n) 0 === r.indexOf("[") || d && /\\d|\\s|\\w]/i.test(r) || "." === r ? t.matches.splice(o++, 0, { fn: new RegExp(r, a.casing ? "i" : ""), optionality: !1, newBlockMarker: s === i ? "master" : s.def !== r, casing: null, def: r, placeholder: i, nativeDef: r }) : (d && (r = r[r.length - 1]), e.each(r.split(""), function (e, n) { s = t.matches[o - 1], t.matches.splice(o++, 0, { fn: null, optionality: !1, newBlockMarker: s === i ? "master" : s.def !== n && null !== s.fn, casing: null, def: a.staticDefinitionSymbol || n, placeholder: a.staticDefinitionSymbol !== i ? n : i, nativeDef: (d ? "'" : "") + n }) })), d = !1; else { var l = (a.definitions ? a.definitions[r] : i) || u.prototype.definitions[r]; l && !d ? t.matches.splice(o++, 0, { fn: l.validator ? "string" == typeof l.validator ? new RegExp(l.validator, a.casing ? "i" : "") : new function () { this.test = l.validator } : new RegExp("."), optionality: !1, newBlockMarker: s === i ? "master" : s.def !== (l.definitionSymbol || r), casing: l.casing, def: l.definitionSymbol || r, placeholder: l.placeholder, nativeDef: r }) : (t.matches.splice(o++, 0, { fn: null, optionality: !1, newBlockMarker: s === i ? "master" : s.def !== r && null !== s.fn, casing: null, def: a.staticDefinitionSymbol || r, placeholder: a.staticDefinitionSymbol !== i ? r : i, nativeDef: (d ? "'" : "") + r }), d = !1) } } function b() { if (g.length > 0) { if (y(l = g[g.length - 1], o), l.isAlternator) { c = g.pop(); for (var e = 0; e < c.matches.length; e++)c.matches[e].isGroup && (c.matches[e].isGroup = !1); g.length > 0 ? (l = g[g.length - 1]).matches.push(c) : h.matches.push(c) } } else y(h, o) } function x(e) { var t = new k(!0); return t.openGroup = !1, t.matches = e, t } for (n && (a.optionalmarker[0] = i, a.optionalmarker[1] = i); r = n ? m.exec(t) : f.exec(t);) { if (o = r[0], n) switch (o.charAt(0)) { case "?": o = "{0,1}"; break; case "+": case "*": o = "{" + o + "}" }if (d) b(); else switch (o.charAt(0)) { case "(?=": case "(?!": case "(?<=": case "(?<!": break; case a.escapeChar: d = !0, n && b(); break; case a.optionalmarker[1]: case a.groupmarker[1]: if ((s = g.pop()).openGroup = !1, s !== i) if (g.length > 0) { if ((l = g[g.length - 1]).matches.push(s), l.isAlternator) { c = g.pop(); for (var P = 0; P < c.matches.length; P++)c.matches[P].isGroup = !1, c.matches[P].alternatorGroup = !1; g.length > 0 ? (l = g[g.length - 1]).matches.push(c) : h.matches.push(c) } } else h.matches.push(s); else b(); break; case a.optionalmarker[0]: g.push(new k(!1, !0)); break; case a.groupmarker[0]: g.push(new k(!0)); break; case a.quantifiermarker[0]: var S = new k(!1, !1, !0), A = (o = o.replace(/[{}]/g, "")).split("|"), C = A[0].split(","), E = isNaN(C[0]) ? C[0] : parseInt(C[0]), w = 1 === C.length ? E : isNaN(C[1]) ? C[1] : parseInt(C[1]); "*" !== E && "+" !== E || (E = "*" === w ? 0 : 1), S.quantifier = { min: E, max: w, jit: A[1] }; var M = g.length > 0 ? g[g.length - 1].matches : h.matches; if ((r = M.pop()).isAlternator) { M.push(r), M = r.matches; var D = new k(!0), O = M.pop(); M.push(D), M = D.matches, r = O } r.isGroup || (r = x([r])), M.push(r), M.push(S); break; case a.alternatormarker: var _ = function (e) { var t = e.pop(); return t.isQuantifier && (t = x([e.pop(), t])), t }; if (g.length > 0) { var j = (l = g[g.length - 1]).matches[l.matches.length - 1]; p = l.openGroup && (j.matches === i || !1 === j.isGroup && !1 === j.isAlternator) ? g.pop() : _(l.matches) } else p = _(h.matches); if (p.isAlternator) g.push(p); else if (p.alternatorGroup ? (c = g.pop(), p.alternatorGroup = !1) : c = new k(!1, !1, !1, !0), c.matches.push(p), g.push(c), p.openGroup) { p.openGroup = !1; var F = new k(!0); F.alternatorGroup = !0, g.push(F) } break; default: b() } } for (; g.length > 0;)s = g.pop(), h.matches.push(s); return h.matches.length > 0 && (!function t(r) { r && r.matches && e.each(r.matches, function (e, o) { var s = r.matches[e + 1]; (s === i || s.matches === i || !1 === s.isQuantifier) && o && o.isGroup && (o.isGroup = !1, n || (y(o, a.groupmarker[0], 0), !0 !== o.openGroup && y(o, a.groupmarker[1]))), t(o) }) }(h), v.push(h)), (a.numericInput || a.isRTL) && function e(t) { for (var n in t.matches = t.matches.reverse(), t.matches) if (t.matches.hasOwnProperty(n)) { var r = parseInt(n); if (t.matches[n].isQuantifier && t.matches[r + 1] && t.matches[r + 1].isGroup) { var o = t.matches[n]; t.matches.splice(n, 1), t.matches.splice(r + 1, 0, o) } t.matches[n].matches !== i ? t.matches[n] = e(t.matches[n]) : t.matches[n] = ((s = t.matches[n]) === a.optionalmarker[0] ? s = a.optionalmarker[1] : s === a.optionalmarker[1] ? s = a.optionalmarker[0] : s === a.groupmarker[0] ? s = a.groupmarker[1] : s === a.groupmarker[1] && (s = a.groupmarker[0]), s) } var s; return t }(v[0]), v } }, u.extendDefaults = function (t) { e.extend(!0, u.prototype.defaults, t) }, u.extendDefinitions = function (t) { e.extend(!0, u.prototype.definitions, t) }, u.extendAliases = function (t) { e.extend(!0, u.prototype.aliases, t) }, u.format = function (e, t, n) { return u(t).format(e, n) }, u.unmask = function (e, t) { return u(t).unmaskedvalue(e) }, u.isValid = function (e, t) { return u(t).isValid(e) }, u.remove = function (t) { "string" == typeof t && (t = n.getElementById(t) || n.querySelectorAll(t)), t = t.nodeName ? [t] : t, e.each(t, function (e, t) { t.inputmask && t.inputmask.remove() }) }, u.setValue = function (t, i) { "string" == typeof t && (t = n.getElementById(t) || n.querySelectorAll(t)), t = t.nodeName ? [t] : t, e.each(t, function (t, n) { n.inputmask ? n.inputmask.setValue(i) : e(n).trigger("setvalue", [i]) }) }, u.escapeRegex = function (e) { return e.replace(new RegExp("(\\" + ["/", ".", "*", "+", "?", "|", "(", ")", "[", "]", "{", "}", "\\", "$", "^"].join("|\\") + ")", "gim"), "\\$1") }, u.keyCode = { BACKSPACE: 8, BACKSPACE_SAFARI: 127, DELETE: 46, DOWN: 40, END: 35, ENTER: 13, ESCAPE: 27, HOME: 36, INSERT: 45, LEFT: 37, PAGE_DOWN: 34, PAGE_UP: 33, RIGHT: 39, SPACE: 32, TAB: 9, UP: 38, X: 88, CONTROL: 17 }, u }, a = [n(0), n(5), n(6)], void 0 === (r = "function" == typeof (i = o) ? i.apply(t, a) : i) || (e.exports = r) }, function (e, t) { e.exports = jQuery }, function (e, t, n) { "use strict"; n(4), n(7), n(8), n(9); var i = o(n(1)), a = o(n(0)), r = o(n(2)); function o(e) { return e && e.__esModule ? e : { default: e } } a.default === r.default && n(10), window.Inputmask = i.default }, function (e, t, n) { "use strict"; var i, a, r, o; "function" == typeof Symbol && Symbol.iterator; o = function (e, t) { var n = { d: ["[1-9]|[12][0-9]|3[01]", Date.prototype.setDate, "day", Date.prototype.getDate], dd: ["0[1-9]|[12][0-9]|3[01]", Date.prototype.setDate, "day", function () { return o(Date.prototype.getDate.call(this), 2) }], ddd: [""], dddd: [""], m: ["[1-9]|1[012]", Date.prototype.setMonth, "month", function () { return Date.prototype.getMonth.call(this) + 1 }], mm: ["0[1-9]|1[012]", Date.prototype.setMonth, "month", function () { return o(Date.prototype.getMonth.call(this) + 1, 2) }], mmm: [""], mmmm: [""], yy: ["[0-9]{2}", Date.prototype.setFullYear, "year", function () { return o(Date.prototype.getFullYear.call(this), 2) }], yyyy: ["[0-9]{4}", Date.prototype.setFullYear, "year", function () { return o(Date.prototype.getFullYear.call(this), 4) }], h: ["[1-9]|1[0-2]", Date.prototype.setHours, "hours", Date.prototype.getHours], hh: ["0[1-9]|1[0-2]", Date.prototype.setHours, "hours", function () { return o(Date.prototype.getHours.call(this), 2) }], hhh: ["[0-9]+", Date.prototype.setHours, "hours", Date.prototype.getHours], H: ["1?[0-9]|2[0-3]", Date.prototype.setHours, "hours", Date.prototype.getHours], HH: ["[01][0-9]|2[0-3]", Date.prototype.setHours, "hours", function () { return o(Date.prototype.getHours.call(this), 2) }], HHH: ["[0-9]+", Date.prototype.setHours, "hours", Date.prototype.getHours], M: ["[1-5]?[0-9]", Date.prototype.setMinutes, "minutes", Date.prototype.getMinutes], MM: ["[0-5][0-9]", Date.prototype.setMinutes, "minutes", function () { return o(Date.prototype.getMinutes.call(this), 2) }], s: ["[1-5]?[0-9]", Date.prototype.setSeconds, "seconds", Date.prototype.getSeconds], ss: ["[0-5][0-9]", Date.prototype.setSeconds, "seconds", function () { return o(Date.prototype.getSeconds.call(this), 2) }], l: ["[0-9]{3}", Date.prototype.setMilliseconds, "milliseconds", function () { return o(Date.prototype.getMilliseconds.call(this), 3) }], L: ["[0-9]{2}", Date.prototype.setMilliseconds, "milliseconds", function () { return o(Date.prototype.getMilliseconds.call(this), 2) }], t: ["[ap]"], tt: ["[ap]m"], T: ["[AP]"], TT: ["[AP]M"], Z: [""], o: [""], S: [""] }, i = { isoDate: "yyyy-mm-dd", isoTime: "HH:MM:ss", isoDateTime: "yyyy-mm-dd'T'HH:MM:ss", isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'" }; function a(e) { if (!e.tokenizer) { var t = []; for (var i in n) -1 === t.indexOf(i[0]) && t.push(i[0]); e.tokenizer = "(" + t.join("+|") + ")+?|.", e.tokenizer = new RegExp(e.tokenizer, "g") } return e.tokenizer } function r(e, i, r) { for (var o, s = ""; o = a(r).exec(e);) { if (void 0 === i) if (n[o[0]]) s += "(" + n[o[0]][0] + ")"; else switch (o[0]) { case "[": s += "("; break; case "]": s += ")?"; break; default: s += t.escapeRegex(o[0]) } else if (n[o[0]]) s += n[o[0]][3].call(i.date); else s += o[0] } return s } function o(e, t) { for (e = String(e), t = t || 2; e.length < t;)e = "0" + e; return e } function s(e, t, i) { var r, o, s, l, u = { date: new Date(1, 0, 1) }, c = e; function p(e, t, n) { e[r] = function (e) { var t; if (i.min && i.min[r] || i.max && i.max[r]) { var n = i.min && i.min[r] || i.max[r], a = i.max && i.max[r] || i.min[r]; for (t = e.replace(/[^0-9]/g, ""), t += (n.indexOf(t) < a.indexOf(t) ? a : n).toString().substr(t.length); !new RegExp(l).test(t);)t-- } else t = e.replace(/[^0-9]/g, "0"); return t }(t), e["raw" + r] = t, void 0 !== s && s.call(e.date, "month" == r ? parseInt(e[r]) - 1 : e[r]) } if ("string" == typeof c) { for (; o = a(i).exec(t);) { var f = c.slice(0, o[0].length); n.hasOwnProperty(o[0]) && (l = n[o[0]][0], r = n[o[0]][2], s = n[o[0]][1], p(u, f)), c = c.slice(f.length) } return u } } return t.extendAliases({ datetime: { mask: function (e) { return n.S = e.i18n.ordinalSuffix.join("|"), e.inputFormat = i[e.inputFormat] || e.inputFormat, e.displayFormat = i[e.displayFormat] || e.displayFormat || e.inputFormat, e.outputFormat = i[e.outputFormat] || e.outputFormat || e.inputFormat, e.placeholder = "" !== e.placeholder ? e.placeholder : e.inputFormat.replace(/[\[\]]/, ""), e.min = s(e.min, e.inputFormat, e), e.max = s(e.max, e.inputFormat, e), e.regex = r(e.inputFormat, void 0, e), null }, placeholder: "", inputFormat: "isoDateTime", displayFormat: void 0, outputFormat: void 0, min: null, max: null, i18n: { dayNames: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"], monthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"], ordinalSuffix: ["st", "nd", "rd", "th"] }, postValidation: function (e, t, n) { var i, a, r = t, o = s(e.join(""), n.inputFormat, n); return r && o.date.getTime() == o.date.getTime() && (i = o, a = r, r = (r = (!isFinite(i.rawday) || "29" == i.day && !isFinite(i.rawyear) || new Date(i.date.getFullYear(), isFinite(i.rawmonth) ? i.month : i.date.getMonth() + 1, 0).getDate() >= i.day) && a) && function (e, t) { var n = !0; if (t.min) { if (e.rawyear) { var i = e.rawyear.replace(/[^0-9]/g, ""); n = t.min.year.substr(0, i.length) <= i } e.year === e.rawyear && t.min.date.getTime() == t.min.date.getTime() && (n = t.min.date.getTime() <= e.date.getTime()) } return n && t.max && t.max.date.getTime() == t.max.date.getTime() && (n = t.max.date.getTime() >= e.date.getTime()), n }(o, n)), r }, onKeyDown: function (n, i, r, s) { if (n.ctrlKey && n.keyCode === t.keyCode.RIGHT) { for (var l, u = new Date, c = ""; l = a(s).exec(s.inputFormat);)"d" === l[0].charAt(0) ? c += o(u.getDate(), l[0].length) : "m" === l[0].charAt(0) ? c += o(u.getMonth() + 1, l[0].length) : "yyyy" === l[0] ? c += u.getFullYear().toString() : "y" === l[0].charAt(0) && (c += o(u.getYear(), l[0].length)); this.inputmask._valueSet(c), e(this).trigger("setvalue") } }, onUnMask: function (e, t, n) { return r(n.outputFormat, s(e, n.inputFormat, n), n) }, casing: function (e, t, n, i) { return 0 == t.nativeDef.indexOf("[ap]") ? e.toLowerCase() : 0 == t.nativeDef.indexOf("[AP]") ? e.toUpperCase() : e }, insertMode: !1 } }), t }, a = [n(0), n(1)], void 0 === (r = "function" == typeof (i = o) ? i.apply(t, a) : i) || (e.exports = r) }, function (e, t, n) { "use strict"; var i; "function" == typeof Symbol && Symbol.iterator; void 0 === (i = function () { return window }.call(t, n, t, e)) || (e.exports = i) }, function (e, t, n) { "use strict"; var i; "function" == typeof Symbol && Symbol.iterator; void 0 === (i = function () { return document }.call(t, n, t, e)) || (e.exports = i) }, function (e, t, n) { "use strict"; var i, a, r, o; "function" == typeof Symbol && Symbol.iterator; o = function (e, t) { return t.extendDefinitions({ A: { validator: "[A-Za-zА-яЁёÀ-ÿµ]", casing: "upper" }, "&": { validator: "[0-9A-Za-zА-яЁёÀ-ÿµ]", casing: "upper" }, "#": { validator: "[0-9A-Fa-f]", casing: "upper" } }), t.extendAliases({ cssunit: { regex: "[+-]?[0-9]+\\.?([0-9]+)?(px|em|rem|ex|%|in|cm|mm|pt|pc)" }, url: { regex: "(https?|ftp)//.*", autoUnmask: !1 }, ip: { mask: "i[i[i]].i[i[i]].i[i[i]].i[i[i]]", definitions: { i: { validator: function (e, t, n, i, a) { return n - 1 > -1 && "." !== t.buffer[n - 1] ? (e = t.buffer[n - 1] + e, e = n - 2 > -1 && "." !== t.buffer[n - 2] ? t.buffer[n - 2] + e : "0" + e) : e = "00" + e, new RegExp("25[0-5]|2[0-4][0-9]|[01][0-9][0-9]").test(e) } } }, onUnMask: function (e, t, n) { return e }, inputmode: "numeric" }, email: { mask: "*{1,64}[.*{1,64}][.*{1,64}][.*{1,63}]@-{1,63}.-{1,63}[.-{1,63}][.-{1,63}]", greedy: !1, casing: "lower", onBeforePaste: function (e, t) { return (e = e.toLowerCase()).replace("mailto:", "") }, definitions: { "*": { validator: "[0-9１-９A-Za-zА-яЁёÀ-ÿµ!#$%&'*+/=?^_`{|}~-]" }, "-": { validator: "[0-9A-Za-z-]" } }, onUnMask: function (e, t, n) { return e }, inputmode: "email" }, mac: { mask: "##:##:##:##:##:##" }, vin: { mask: "V{13}9{4}", definitions: { V: { validator: "[A-HJ-NPR-Za-hj-npr-z\\d]", casing: "upper" } }, clearIncomplete: !0, autoUnmask: !0 } }), t }, a = [n(0), n(1)], void 0 === (r = "function" == typeof (i = o) ? i.apply(t, a) : i) || (e.exports = r) }, function (e, t, n) { "use strict"; var i, a, r, o; "function" == typeof Symbol && Symbol.iterator; o = function (e, t, n) { function i(e, n) { for (var i = "", a = 0; a < e.length; a++)t.prototype.definitions[e.charAt(a)] || n.definitions[e.charAt(a)] || n.optionalmarker.start === e.charAt(a) || n.optionalmarker.end === e.charAt(a) || n.quantifiermarker.start === e.charAt(a) || n.quantifiermarker.end === e.charAt(a) || n.groupmarker.start === e.charAt(a) || n.groupmarker.end === e.charAt(a) || n.alternatormarker === e.charAt(a) ? i += "\\" + e.charAt(a) : i += e.charAt(a); return i } return t.extendAliases({ numeric: { mask: function (e) { if (0 !== e.repeat && isNaN(e.integerDigits) && (e.integerDigits = e.repeat), e.repeat = 0, e.groupSeparator === e.radixPoint && e.digits && "0" !== e.digits && ("." === e.radixPoint ? e.groupSeparator = "," : "," === e.radixPoint ? e.groupSeparator = "." : e.groupSeparator = ""), " " === e.groupSeparator && (e.skipOptionalPartCharacter = n), e.autoGroup = e.autoGroup && "" !== e.groupSeparator, e.autoGroup && ("string" == typeof e.groupSize && isFinite(e.groupSize) && (e.groupSize = parseInt(e.groupSize)), isFinite(e.integerDigits))) { var t = Math.floor(e.integerDigits / e.groupSize), a = e.integerDigits % e.groupSize; e.integerDigits = parseInt(e.integerDigits) + (0 === a ? t - 1 : t), e.integerDigits < 1 && (e.integerDigits = "*") } e.placeholder.length > 1 && (e.placeholder = e.placeholder.charAt(0)), "radixFocus" === e.positionCaretOnClick && "" === e.placeholder && !1 === e.integerOptional && (e.positionCaretOnClick = "lvp"), e.definitions[";"] = e.definitions["~"], e.definitions[";"].definitionSymbol = "~", !0 === e.numericInput && (e.positionCaretOnClick = "radixFocus" === e.positionCaretOnClick ? "lvp" : e.positionCaretOnClick, e.digitsOptional = !1, isNaN(e.digits) && (e.digits = 2), e.decimalProtect = !1); var r = "[+]"; if (r += i(e.prefix, e), !0 === e.integerOptional ? r += "~{1," + e.integerDigits + "}" : r += "~{" + e.integerDigits + "}", e.digits !== n) { var o = e.decimalProtect ? ":" : e.radixPoint, s = e.digits.toString().split(","); isFinite(s[0]) && s[1] && isFinite(s[1]) ? r += o + ";{" + e.digits + "}" : (isNaN(e.digits) || parseInt(e.digits) > 0) && (e.digitsOptional ? r += "[" + o + ";{1," + e.digits + "}]" : r += o + ";{" + e.digits + "}") } return r += i(e.suffix, e), r += "[-]", e.greedy = !1, r }, placeholder: "", greedy: !1, digits: "*", digitsOptional: !0, enforceDigitsOnBlur: !1, radixPoint: ".", positionCaretOnClick: "radixFocus", groupSize: 3, groupSeparator: "", autoGroup: !1, allowMinus: !0, negationSymbol: { front: "-", back: "" }, integerDigits: "+", integerOptional: !0, prefix: "", suffix: "", rightAlign: !0, decimalProtect: !0, min: null, max: null, step: 1, insertMode: !0, autoUnmask: !1, unmaskAsNumber: !1, inputmode: "numeric", preValidation: function (t, i, a, r, o, s) { if ("-" === a || a === o.negationSymbol.front) return !0 === o.allowMinus && (o.isNegative = o.isNegative === n || !o.isNegative, "" === t.join("") || { caret: i, dopost: !0 }); if (!1 === r && a === o.radixPoint && o.digits !== n && (isNaN(o.digits) || parseInt(o.digits) > 0)) { var l = e.inArray(o.radixPoint, t); if (-1 !== l && s.validPositions[l] !== n) return !0 === o.numericInput ? i === l : { caret: l + 1 } } return !0 }, postValidation: function (i, a, r) { var o = r.suffix.split(""), s = r.prefix.split(""); if (a.pos === n && a.caret !== n && !0 !== a.dopost) return a; var l = a.caret !== n ? a.caret : a.pos, u = i.slice(); r.numericInput && (l = u.length - l - 1, u = u.reverse()); var c = u[l]; if (c === r.groupSeparator && (c = u[l += 1]), l === u.length - r.suffix.length - 1 && c === r.radixPoint) return a; c !== n && c !== r.radixPoint && c !== r.negationSymbol.front && c !== r.negationSymbol.back && (u[l] = "?", r.prefix.length > 0 && l >= (!1 === r.isNegative ? 1 : 0) && l < r.prefix.length - 1 + (!1 === r.isNegative ? 1 : 0) ? s[l - (!1 === r.isNegative ? 1 : 0)] = "?" : r.suffix.length > 0 && l >= u.length - r.suffix.length - (!1 === r.isNegative ? 1 : 0) && (o[l - (u.length - r.suffix.length - (!1 === r.isNegative ? 1 : 0))] = "?")), s = s.join(""), o = o.join(""); var p = u.join("").replace(s, ""); if (p = (p = (p = (p = p.replace(o, "")).replace(new RegExp(t.escapeRegex(r.groupSeparator), "g"), "")).replace(new RegExp("[-" + t.escapeRegex(r.negationSymbol.front) + "]", "g"), "")).replace(new RegExp(t.escapeRegex(r.negationSymbol.back) + "$"), ""), isNaN(r.placeholder) && (p = p.replace(new RegExp(t.escapeRegex(r.placeholder), "g"), "")), p.length > 1 && 1 !== p.indexOf(r.radixPoint) && ("0" === c && (p = p.replace(/^\?/g, "")), p = p.replace(/^0/g, "")), p.charAt(0) === r.radixPoint && "" !== r.radixPoint && !0 !== r.numericInput && (p = "0" + p), "" !== p) { if (p = p.split(""), (!r.digitsOptional || r.enforceDigitsOnBlur && "blur" === a.event) && isFinite(r.digits)) { var f = e.inArray(r.radixPoint, p), m = e.inArray(r.radixPoint, u); -1 === f && (p.push(r.radixPoint), f = p.length - 1); for (var d = 1; d <= r.digits; d++)r.digitsOptional && (!r.enforceDigitsOnBlur || "blur" !== a.event) || p[f + d] !== n && p[f + d] !== r.placeholder.charAt(0) ? -1 !== m && u[m + d] !== n && (p[f + d] = p[f + d] || u[m + d]) : p[f + d] = a.placeholder || r.placeholder.charAt(0) } if (!0 !== r.autoGroup || "" === r.groupSeparator || c === r.radixPoint && a.pos === n && !a.dopost) p = p.join(""); else { var h = p[p.length - 1] === r.radixPoint && a.c === r.radixPoint; p = t(function (e, t) { var n = ""; if (n += "(" + t.groupSeparator + "*{" + t.groupSize + "}){*}", "" !== t.radixPoint) { var i = e.join("").split(t.radixPoint); i[1] && (n += t.radixPoint + "*{" + i[1].match(/^\d*\??\d*/)[0].length + "}") } return n }(p, r), { numericInput: !0, jitMasking: !0, definitions: { "*": { validator: "[0-9?]", cardinality: 1 } } }).format(p.join("")), h && (p += r.radixPoint), p.charAt(0) === r.groupSeparator && p.substr(1) } } if (r.isNegative && "blur" === a.event && (r.isNegative = "0" !== p), p = s + p, p += o, r.isNegative && (p = r.negationSymbol.front + p, p += r.negationSymbol.back), p = p.split(""), c !== n) if (c !== r.radixPoint && c !== r.negationSymbol.front && c !== r.negationSymbol.back) (l = e.inArray("?", p)) > -1 ? p[l] = c : l = a.caret || 0; else if (c === r.radixPoint || c === r.negationSymbol.front || c === r.negationSymbol.back) { var g = e.inArray(c, p); -1 !== g && (l = g) } r.numericInput && (l = p.length - l - 1, p = p.reverse()); var v = { caret: c === n || a.pos !== n ? l + (r.numericInput ? -1 : 1) : l, buffer: p, refreshFromBuffer: a.dopost || i.join("") !== p.join("") }; return v.refreshFromBuffer ? v : a }, onBeforeWrite: function (i, a, r, o) { if (i) switch (i.type) { case "keydown": return o.postValidation(a, { caret: r, dopost: !0 }, o); case "blur": case "checkval": var s; if ((l = o).parseMinMaxOptions === n && (null !== l.min && (l.min = l.min.toString().replace(new RegExp(t.escapeRegex(l.groupSeparator), "g"), ""), "," === l.radixPoint && (l.min = l.min.replace(l.radixPoint, ".")), l.min = isFinite(l.min) ? parseFloat(l.min) : NaN, isNaN(l.min) && (l.min = Number.MIN_VALUE)), null !== l.max && (l.max = l.max.toString().replace(new RegExp(t.escapeRegex(l.groupSeparator), "g"), ""), "," === l.radixPoint && (l.max = l.max.replace(l.radixPoint, ".")), l.max = isFinite(l.max) ? parseFloat(l.max) : NaN, isNaN(l.max) && (l.max = Number.MAX_VALUE)), l.parseMinMaxOptions = "done"), null !== o.min || null !== o.max) { if (s = o.onUnMask(a.join(""), n, e.extend({}, o, { unmaskAsNumber: !0 })), null !== o.min && s < o.min) return o.isNegative = o.min < 0, o.postValidation(o.min.toString().replace(".", o.radixPoint).split(""), { caret: r, dopost: !0, placeholder: "0" }, o); if (null !== o.max && s > o.max) return o.isNegative = o.max < 0, o.postValidation(o.max.toString().replace(".", o.radixPoint).split(""), { caret: r, dopost: !0, placeholder: "0" }, o) } return o.postValidation(a, { caret: r, placeholder: "0", event: "blur" }, o); case "_checkval": return { caret: r } }var l }, regex: { integerPart: function (e, n) { return n ? new RegExp("[" + t.escapeRegex(e.negationSymbol.front) + "+]?") : new RegExp("[" + t.escapeRegex(e.negationSymbol.front) + "+]?\\d+") }, integerNPart: function (e) { return new RegExp("[\\d" + t.escapeRegex(e.groupSeparator) + t.escapeRegex(e.placeholder.charAt(0)) + "]+") } }, definitions: { "~": { validator: function (e, i, a, r, o, s) { var l; if ("k" === e || "m" === e) { l = { insert: [], c: 0 }; for (var u = 0, c = "k" === e ? 2 : 5; u < c; u++)l.insert.push({ pos: a + u, c: 0 }); return l.pos = a + c, l } if (!0 === (l = r ? new RegExp("[0-9" + t.escapeRegex(o.groupSeparator) + "]").test(e) : new RegExp("[0-9]").test(e))) { if (!0 !== o.numericInput && i.validPositions[a] !== n && "~" === i.validPositions[a].match.def && !s) { var p = i.buffer.join(""), f = (p = (p = p.replace(new RegExp("[-" + t.escapeRegex(o.negationSymbol.front) + "]", "g"), "")).replace(new RegExp(t.escapeRegex(o.negationSymbol.back) + "$"), "")).split(o.radixPoint); f.length > 1 && (f[1] = f[1].replace(/0/g, o.placeholder.charAt(0))), "0" === f[0] && (f[0] = f[0].replace(/0/g, o.placeholder.charAt(0))), p = f[0] + o.radixPoint + f[1] || ""; var m = i._buffer.join(""); for (p === o.radixPoint && (p = m); null === p.match(t.escapeRegex(m) + "$");)m = m.slice(1); l = (p = (p = p.replace(m, "")).split(""))[a] === n ? { pos: a, remove: a } : { pos: a } } } else r || e !== o.radixPoint || i.validPositions[a - 1] !== n || (l = { insert: { pos: a, c: 0 }, pos: a + 1 }); return l }, cardinality: 1 }, "+": { validator: function (e, t, n, i, a) { return a.allowMinus && ("-" === e || e === a.negationSymbol.front) }, cardinality: 1, placeholder: "" }, "-": { validator: function (e, t, n, i, a) { return a.allowMinus && e === a.negationSymbol.back }, cardinality: 1, placeholder: "" }, ":": { validator: function (e, n, i, a, r) { var o = "[" + t.escapeRegex(r.radixPoint) + "]", s = new RegExp(o).test(e); return s && n.validPositions[i] && n.validPositions[i].match.placeholder === r.radixPoint && (s = { caret: i + 1 }), s }, cardinality: 1, placeholder: function (e) { return e.radixPoint } } }, onUnMask: function (e, n, i) { if ("" === n && !0 === i.nullable) return n; var a = e.replace(i.prefix, ""); return a = (a = a.replace(i.suffix, "")).replace(new RegExp(t.escapeRegex(i.groupSeparator), "g"), ""), "" !== i.placeholder.charAt(0) && (a = a.replace(new RegExp(i.placeholder.charAt(0), "g"), "0")), i.unmaskAsNumber ? ("" !== i.radixPoint && -1 !== a.indexOf(i.radixPoint) && (a = a.replace(t.escapeRegex.call(this, i.radixPoint), ".")), a = (a = a.replace(new RegExp("^" + t.escapeRegex(i.negationSymbol.front)), "-")).replace(new RegExp(t.escapeRegex(i.negationSymbol.back) + "$"), ""), Number(a)) : a }, isComplete: function (e, n) { var i = (n.numericInput ? e.slice().reverse() : e).join(""); return i = (i = (i = (i = (i = i.replace(new RegExp("^" + t.escapeRegex(n.negationSymbol.front)), "-")).replace(new RegExp(t.escapeRegex(n.negationSymbol.back) + "$"), "")).replace(n.prefix, "")).replace(n.suffix, "")).replace(new RegExp(t.escapeRegex(n.groupSeparator) + "([0-9]{3})", "g"), "$1"), "," === n.radixPoint && (i = i.replace(t.escapeRegex(n.radixPoint), ".")), isFinite(i) }, onBeforeMask: function (i, a) { if (a.isNegative = n, "number" == typeof i && "" !== a.radixPoint && (i = i.toString().replace(".", a.radixPoint)), i = i.toString().charAt(i.length - 1) === a.radixPoint ? i.toString().substr(0, i.length - 1) : i.toString(), "" !== a.radixPoint && isFinite(i)) { var r = i.split("."), o = "" !== a.groupSeparator ? parseInt(a.groupSize) : 0; 2 === r.length && (r[0].length > o || r[1].length > o || r[0].length <= o && r[1].length < o) && (i = i.replace(".", a.radixPoint)) } var s = i.match(/,/g), l = i.match(/\./g); if (i = l && s ? l.length > s.length ? (i = i.replace(/\./g, "")).replace(",", a.radixPoint) : s.length > l.length ? (i = i.replace(/,/g, "")).replace(".", a.radixPoint) : i.indexOf(".") < i.indexOf(",") ? i.replace(/\./g, "") : i.replace(/,/g, "") : i.replace(new RegExp(t.escapeRegex(a.groupSeparator), "g"), ""), 0 === a.digits && (-1 !== i.indexOf(".") ? i = i.substring(0, i.indexOf(".")) : -1 !== i.indexOf(",") && (i = i.substring(0, i.indexOf(",")))), "" !== a.radixPoint && isFinite(a.digits) && -1 !== i.indexOf(a.radixPoint)) { var u = i.split(a.radixPoint)[1].match(new RegExp("\\d*"))[0]; if (parseInt(a.digits) < u.toString().length) { var c = Math.pow(10, parseInt(a.digits)); i = i.replace(t.escapeRegex(a.radixPoint), "."), i = (i = Math.round(parseFloat(i) * c) / c).toString().replace(".", a.radixPoint) } } return function (t, n) { if (n.numericInput) { var i = e.inArray(n.radixPoint, t); -1 === i && (t.push(n.radixPoint), i = t.length - 1); for (var a = 1; a <= n.digits; a++)t[i + a] = t[i + a] || "0" } return t }(i.toString().split(""), a).join("") }, onKeyDown: function (n, i, a, r) { var o = e(this); if (n.ctrlKey) switch (n.keyCode) { case t.keyCode.UP: o.val(parseFloat(this.inputmask.unmaskedvalue()) + parseInt(r.step)), o.trigger("setvalue"); break; case t.keyCode.DOWN: o.val(parseFloat(this.inputmask.unmaskedvalue()) - parseInt(r.step)), o.trigger("setvalue") } } }, currency: { prefix: "$ ", groupSeparator: ",", alias: "numeric", placeholder: "0", autoGroup: !0, digits: 2, digitsOptional: !1, clearMaskOnLostFocus: !1 }, decimal: { alias: "numeric" }, integer: { alias: "numeric", digits: 0, radixPoint: "" }, percentage: { alias: "numeric", digits: 2, digitsOptional: !0, radixPoint: ".", placeholder: "0", autoGroup: !1, min: 0, max: 100, suffix: " %", allowMinus: !1 } }), t }, a = [n(0), n(1)], void 0 === (r = "function" == typeof (i = o) ? i.apply(t, a) : i) || (e.exports = r) }, function (e, t, n) { "use strict"; var i, a, r, o; "function" == typeof Symbol && Symbol.iterator; o = function (e, t) { function n(e, t) { var n = (e.mask || e).replace(/#/g, "0").replace(/\)/, "0").replace(/[+()#-]/g, ""), i = (t.mask || t).replace(/#/g, "0").replace(/\)/, "0").replace(/[+()#-]/g, ""); return n.localeCompare(i) } var i = t.prototype.analyseMask; return t.prototype.analyseMask = function (t, n, a) { var r = {}; return a.phoneCodes && (a.phoneCodes && a.phoneCodes.length > 1e3 && (function e(n, i, a) { i = i || "", a = a || r, "" !== i && (a[i] = {}); for (var o = "", s = a[i] || a, l = n.length - 1; l >= 0; l--)s[o = (t = n[l].mask || n[l]).substr(0, 1)] = s[o] || [], s[o].unshift(t.substr(1)), n.splice(l, 1); for (var u in s) s[u].length > 500 && e(s[u].slice(), u, s) }((t = t.substr(1, t.length - 2)).split(a.groupmarker[1] + a.alternatormarker + a.groupmarker[0])), t = function t(n) { var i = "", r = []; for (var o in n) e.isArray(n[o]) ? 1 === n[o].length ? r.push(o + n[o]) : r.push(o + a.groupmarker[0] + n[o].join(a.groupmarker[1] + a.alternatormarker + a.groupmarker[0]) + a.groupmarker[1]) : r.push(o + t(n[o])); return 1 === r.length ? i += r[0] : i += a.groupmarker[0] + r.join(a.groupmarker[1] + a.alternatormarker + a.groupmarker[0]) + a.groupmarker[1], i }(r)), t = t.replace(/9/g, "\\9")), i.call(this, t, n, a) }, t.extendAliases({ abstractphone: { groupmarker: ["<", ">"], countrycode: "", phoneCodes: [], keepStatic: "auto", mask: function (e) { return e.definitions = { "#": t.prototype.definitions[9] }, e.phoneCodes.sort(n) }, onBeforeMask: function (e, t) { var n = e.replace(/^0{1,2}/, "").replace(/[\s]/g, ""); return (n.indexOf(t.countrycode) > 1 || -1 === n.indexOf(t.countrycode)) && (n = "+" + t.countrycode + n), n }, onUnMask: function (e, t, n) { return e.replace(/[()#-]/g, "") }, inputmode: "tel" } }), t }, a = [n(0), n(1)], void 0 === (r = "function" == typeof (i = o) ? i.apply(t, a) : i) || (e.exports = r) }, function (e, t, n) { "use strict"; var i, a, r, o, s = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) { return typeof e } : function (e) { return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e }; o = function (e, t) { return void 0 === e.fn.inputmask && (e.fn.inputmask = function (n, i) { var a, r = this[0]; if (void 0 === i && (i = {}), "string" == typeof n) switch (n) { case "unmaskedvalue": return r && r.inputmask ? r.inputmask.unmaskedvalue() : e(r).val(); case "remove": return this.each(function () { this.inputmask && this.inputmask.remove() }); case "getemptymask": return r && r.inputmask ? r.inputmask.getemptymask() : ""; case "hasMaskedValue": return !(!r || !r.inputmask) && r.inputmask.hasMaskedValue(); case "isComplete": return !r || !r.inputmask || r.inputmask.isComplete(); case "getmetadata": return r && r.inputmask ? r.inputmask.getmetadata() : void 0; case "setvalue": t.setValue(r, i); break; case "option": if ("string" != typeof i) return this.each(function () { if (void 0 !== this.inputmask) return this.inputmask.option(i) }); if (r && void 0 !== r.inputmask) return r.inputmask.option(i); break; default: return i.alias = n, a = new t(i), this.each(function () { a.mask(this) }) } else { if ("object" == (void 0 === n ? "undefined" : s(n))) return a = new t(n), void 0 === n.mask && void 0 === n.alias ? this.each(function () { if (void 0 !== this.inputmask) return this.inputmask.option(n); a.mask(this) }) : this.each(function () { a.mask(this) }); if (void 0 === n) return this.each(function () { (a = new t(i)).mask(this) }) } }), e.fn.inputmask }, a = [n(2), n(1)], void 0 === (r = "function" == typeof (i = o) ? i.apply(t, a) : i) || (e.exports = r) }]);;
/**
 * @file
 * JavaScript behaviors for jquery.inputmask integration.
 */

(function ($, Drupal) {

  'use strict';

  /**
   * Initialize input masks.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.webformInputMask = {
    attach: function (context) {
      if (!$.fn.inputmask) {
        return;
      }

      $(context).find('input.js-webform-input-mask').once('webform-input-mask').inputmask();
    }
  };

})(jQuery, Drupal);
;
/**
 * @file
 * JavaScript behaviors for other elements.
 */

(function ($, Drupal) {

  'use strict';

  /**
   * Toggle other input (text) field.
   *
   * @param {boolean} show
   *   TRUE will display the text field. FALSE with hide and clear the text field.
   * @param {object} $element
   *   The input (text) field to be toggled.
   * @param {string} effect
   *   Effect.
   */
  function toggleOther(show, $element, effect) {
    var $input = $element.find('input');
    var hideEffect = (effect === false) ? 'hide' : 'slideUp';
    var showEffect = (effect === false) ? 'show' : 'slideDown';

    if (show) {
      // Limit the other inputs width to the parent's container.
      // If the parent container is not visible it's width will be 0
      // and ignored.
      var width = $element.parent().width();
      if (width) {
        $element.width(width);
      }

      // Display the element.
      $element[showEffect]();
      // If not initializing, then focus the other element.
      if (effect !== false) {
        $input.focus();
      }
      // Require the input.
      $input.prop('required', true).attr('aria-required', 'true');
      // Restore the input's value.
      var value = $input.data('webform-value');
      if (value !== undefined) {
        $input.val(value);
        var input = $input.get(0);
        // Move cursor to the beginning of the other text input.
        // @see https://stackoverflow.com/questions/21177489/selectionstart-selectionend-on-input-type-number-no-longer-allowed-in-chrome
        if ($.inArray(input.type, ['text', 'search', 'url', 'tel', 'password']) !== -1) {
          input.setSelectionRange(0, 0);
        }
      }
      // Refresh CodeMirror used as other element.
      $element.parent().find('.CodeMirror').each(function (index, $element) {
        $element.CodeMirror.refresh();
      });
    }
    else {
      // Hide the element.
      $element[hideEffect]();
      // Save the input's value.
      $input.data('webform-value', $input.val());
      // Empty and un-required the input.
      $input.val('').prop('required', false).removeAttr('aria-required');
    }
  }

  /**
   * Attach handlers to select other elements.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.webformSelectOther = {
    attach: function (context) {
      $(context).find('.js-webform-select-other').once('webform-select-other').each(function () {
        var $element = $(this);

        var $select = $element.find('select');
        var $otherOption = $element.find('option[value="_other_"]');
        var $input = $element.find('.js-webform-select-other-input');

        $select.on('change', function () {
          toggleOther($otherOption.is(':selected'), $input);
        });

        toggleOther($otherOption.is(':selected'), $input, false);
      });
    }
  };

  /**
   * Attach handlers to checkboxes other elements.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.webformCheckboxesOther = {
    attach: function (context) {
      $(context).find('.js-webform-checkboxes-other').once('webform-checkboxes-other').each(function () {
        var $element = $(this);
        var $checkbox = $element.find('input[value="_other_"]');
        var $input = $element.find('.js-webform-checkboxes-other-input');

        $checkbox.on('change', function () {
          toggleOther(this.checked, $input);
        });

        toggleOther($checkbox.is(':checked'), $input, false);
      });
    }
  };

  /**
   * Attach handlers to radios other elements.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.webformRadiosOther = {
    attach: function (context) {
      $(context).find('.js-webform-radios-other').once('webform-radios-other').each(function () {
        var $element = $(this);

        var $radios = $element.find('input[type="radio"]');
        var $input = $element.find('.js-webform-radios-other-input');

        $radios.on('change', function () {
          toggleOther(($radios.filter(':checked').val() === '_other_'), $input);
        });

        toggleOther(($radios.filter(':checked').val() === '_other_'), $input, false);
      });
    }
  };

  /**
   * Attach handlers to buttons other elements.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.webformButtonsOther = {
    attach: function (context) {
      $(context).find('.js-webform-buttons-other').once('webform-buttons-other').each(function () {
        var $element = $(this);

        var $buttons = $element.find('input[type="radio"]');
        var $input = $element.find('.js-webform-buttons-other-input');
        var $container = $(this).find('.js-webform-webform-buttons');

        // Create set onchange handler.
        $container.change(function () {
          toggleOther(($(this).find(':radio:checked').val() === '_other_'), $input);
        });

        toggleOther(($buttons.filter(':checked').val() === '_other_'), $input, false);
      });
    }
  };

})(jQuery, Drupal);
;
